@extends('layouts.auth.app')

@section('content')
<div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-50 p-b-20">
                <form class="login100-form validate-form" method="POST" action="{{ route('postLogin') }}">
                    @csrf
                    <span class="login100-form-avatar">
                        <img src="{{ asset ('resources/images/kemkominfo.jpg')}}" alt="AVATAR">
                    </span>
                    <span class="login100-form-title p-t-20">
                        Portal Layanan Data <br> ISR QR Code
                    </span>
                    <div class="wrap-input100 validate-input m-t-50 m-b-35" data-validate = "Email">
                        <input class="input100" type="email" name="email" required autocomplete="email" autofocus>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-50" data-validate="Password">
                        <input class="input100" type="password" name="password" required autocomplete="current-password">
                        <span class="focus-input100" data-placeholder="Password"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
