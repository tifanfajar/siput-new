@extends('layouts.core.app')
@section('title') 
Beranda
@endsection
@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fa fa-qrcode icon-gradient bg-primary">
                </i>
            </div>
            <div>Welcome, {{Auth::user()->name}}!
                <div class="page-title-subheading">Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code. <br>
                    {{date('l')}}, {{date('d-M-Y')}} ( {{date('h:i A')}} )
                </div>
            </div>
        </div>
    </div>
</div>  
<div class="row">
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-primary text-white">
            <div class="widget-content-outer">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="widget-heading">ISR QR Code</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-qrcode"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-warning text-white">
            <div class="widget-content-outer">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="widget-heading">Registrasi Stasiun Bumi Satelit</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-globe-asia"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card mb-3 widget-content bg-success text-white">
            <div class="widget-content-outer">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="widget-heading">Registrasi BTS IPFR</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-broadcast-tower"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection