@extends('layouts.core.app')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fa fa-qrcode icon-gradient bg-primary">
                </i>
            </div>
            <div>ISR QR Code
                <div class="page-title-subheading">Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code. <br>
                    {{date('l')}}, {{date('d-M-Y')}} ( {{date('h:i A')}} )
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection