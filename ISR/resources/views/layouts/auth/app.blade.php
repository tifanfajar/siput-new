<!DOCTYPE html>
<html lang="en">
<head>
	<title>ISR QR CODE</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="icon" type="image/png" href="{{ asset ('favicon.ico')}}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset ('jquery/jquery-confirm.css') }}">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset ('css/auth/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset ('css/auth/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset ('css/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset ('css/fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link href="{{ asset ('plugins/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" />
</head>
<body>
	@yield('content')

	<div id="dropDownSelect1"></div>
	
	<footer>
		<p class="copyright">© DPS SDPPI 2020</p>
	</footer>

	<script src="{{ asset ('jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{ asset ('js/auth/main.js')}}"></script>
	<script src="{{asset('jquery/jquery-confirm.js') }}"></script>

	<script>
		@if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
		$.confirm({
			title: '{{Session::get('info')}}',
			content: '{{Session::get('alert')}}',
			type: '{{Session::get('colors')}}',
			icon: '{{Session::get('icons')}}',
			typeAnimated: true,
			buttons: {
				close: function () {
				}
			}
		});
		@elseif(count($errors) == 0)
		return false;
		@else
		$.confirm({
			title: '{{Session::has('info')}}',
			content: '{{Session::get('alert')}}',
			type: 'red',
			typeAnimated: true,
			icon: 'fas fa-exclamation-triangle',
			buttons: {
				close: function () {
				}
			}
		});
		@endif
	</script>

</body>
</html>