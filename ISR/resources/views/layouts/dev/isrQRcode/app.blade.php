@extends('layouts.core.app')
@section('title') 
ISR QR Code
@endsection
@section('pageIcon') fa fa-qrcode @endsection
@section('pageTitle') ISR QR Code @endsection
@section('subPageTitle') Data Service portal for radio station identification with QR Code. @endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title text-center">QR Code ID ISR ( Peta Lokasi )</h5>
                <hr>
                <canvas id="data-isr"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title text-center">Data Inspeksi UPT</h5>
                <hr>
                <canvas id="data-inspeksi"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('layouts.dev.isrQRcode.script')
@endsection
