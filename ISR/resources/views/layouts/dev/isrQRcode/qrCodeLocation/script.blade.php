<script>
	mapboxgl.accessToken = 'pk.eyJ1IjoiYXJtMDIiLCJhIjoiY2s4OTIxb2FpMDJoNjNlbnQzcmF1and1cyJ9.lVxm5Y1NFV96PL3u7bGu1A';
	var coordinates = document.getElementById('coordinates');
	var map = new mapboxgl.Map({
		container: 'qrCodeMap',
		style: 'mapbox://styles/mapbox/streets-v11',
		center: [113.347842, -0.050812],
		zoom: 4,
	});

	var dragMarker = new mapboxgl.Marker({
		draggable: true,
		color: 'red'
	})
	.setLngLat([117.35918201131932, -4.227448935594907])
	.addTo(map);

	function onDragEnd() {
		var lngLat = dragMarker.getLngLat();
		$.confirm({
			title: 'Location',
			content: '<b>Longitude : </b> ' + lngLat.lng + '<br /><b>Latitude : </b>' + lngLat.lat,
			type: 'blue',
			icon: 'fa fa-compass',
			typeAnimated: true,
			buttons: {
				close: function () {
				}
			}
		});
	}

	dragMarker.on('dragend', onDragEnd);

	var geocoder = new MapboxGeocoder({
		accessToken: mapboxgl.accessToken,
		mapboxgl: mapboxgl,
		marker: {
			color: 'red'
		},
		zoom: 16,
		placeholder: "Enter an address or place name",

	});

	map.addControl(geocoder, 'top-left');

	var userLocation = new mapboxgl.GeolocateControl({
		positionOptions: {
			enableHighAccuracy: true,
		},
		fitBoundsOptions: {
			zoom: 18,
		},
		showAccuracyCircle: false,
		trackUserLocation: false,
		showUserLocation: true	
	});

	map.addControl(userLocation);

	function setCircleRadius(point)
	{	
		var mapLayer = map.getLayer('circle');

		if(typeof mapLayer !== 'undefined') {
			map.removeLayer('circle').removeSource('circle');
		}

		map.addSource("circle", {
			"type": "geojson",
			"data": {
				"type": "FeatureCollection",
				"features": [{
					"type": "Feature",
					"geometry": {
						"type": "Point",
						"coordinates": [point[0], point[1]]
					}
				}]
			}
		});

		const metersToPixelsAtMaxZoom = (meters, latitude) =>
		meters / 0.075 / Math.cos(latitude * Math.PI / 180)
		map.addLayer({
			"id": "circle",
			"type": "circle",
			"source": "circle",
			"paint": {
				"circle-radius": {
					stops: [
					[0, 0], [20, metersToPixelsAtMaxZoom(50, point[1])] ],
					base: 2
				},
				"circle-color": "lightblue",
				"circle-opacity": 0.6
			}
		});
	}

	function getDataSite(){
		var longtitude = $("#longtitudeInput").val();
		var latitude = $("#latitudeInput").val();
		if (longtitude && latitude) {
			var point = [longtitude, latitude];
			setCircleRadius(point);
			var query = '{{URL::current()}}/radius/' + longtitude + '/' + latitude;

			$.ajax({
				method: 'GET',
				url: query,
			}).done(function(data) {
				var is_null = 0;
				data.forEach(function(result) {
					if (result.site_id != null) {
						is_null++;
						var site_id = result.site_id;
					}else{
						var site_id = 'BELUM TERDAFTAR';
					}
					var markerSite = new mapboxgl.Marker({
						draggable: false,
						color: 'orange'
					})
					.setLngLat([result.sid_long, result.sid_lat])
					.setPopup(new mapboxgl.Popup({ offset: 25 }) 
						.setHTML('<img style="width: 70px;height: 60px;" src="http://localhost/ISR/public/sdppi.png" alt=""><br><div style="text-align: left;"><table class="table"><tbody><tr><td style="font-weight: bold">SITE ID</td><td>:</td><td>'+ site_id +'</td></tr><tr><td style="font-weight: bold">CLIENT ID</td><td>:</td><td>'+ result.clnt_id +'</td></tr><tr><td style="font-weight: bold">CLIENT NAME</td><td>:</td><td>'+ result.clnt_name +'</td></tr><tr><td style="font-weight: bold">CITY</td><td>:</td><td>'+ result.city +'</td></tr></tbody></table></div>'))
					.addTo(map);
					
				});

			});
		}else{
			$.confirm({
				title: 'Location',
				content: 'Empty Longitude & Latitude Input',
				type: 'red',
				icon: 'fa fa-compass',
				typeAnimated: true,
				buttons: {
					close: function () {
					}
				}
			});
		}
	}

	map.on('load', function() {

		userLocation.on('geolocate', function(geolocate) {
			$("#longtitudeInput").val(geolocate.coords.longitude);
			$("#latitudeInput").val(geolocate.coords.latitude);
			var point = [geolocate.coords.longitude, geolocate.coords.latitude];
			setCircleRadius(point);
			var markerLocation = new mapboxgl.Marker({
				draggable: true,
				color: 'green'
			})
			.setLngLat([$("#longtitudeInput").val(), $("#latitudeInput").val()])
			.addTo(map);

			function onDragEndLocation() {
				var location = markerLocation.getLngLat();
				$("#longtitudeInput").val(location.lng);
				$("#latitudeInput").val(location.lat);
			}
			markerLocation.on('drag', onDragEndLocation);
		});

		geocoder.on('result', function(data) {

			$("#addSiteBtn").hide();
			$("#editSiteBtn").hide();

			$("div.mapboxgl-marker.mapboxgl-marker-anchor-center").remove();

			var point = data.result.center;
			var query = '{{URL::current()}}/radius/' + point[0] + '/' + point[1];

			$.ajax({
				method: 'GET',
				url: query,
			}).done(function(data) {
				var is_null = 0;
				data.forEach(function(result) {
					if (result.site_id != null) {
						is_null++;
						var site_id = result.site_id;
					}else{
						var site_id = 'BELUM TERDAFTAR';
					}
					var marker = new mapboxgl.Marker({
						draggable: false,
						color: 'orange'
					})
					.setLngLat([result.sid_long, result.sid_lat])
					.setPopup(new mapboxgl.Popup({ offset: 25 }) 
						.setHTML('<img style="width: 70px;height: 60px;" src="http://localhost/ISR/public/sdppi.png" alt=""><br><div style="text-align: left;"><table class="table"><tbody><tr><td style="font-weight: bold">SITE ID</td><td>:</td><td>'+ site_id +'</td></tr><tr><td style="font-weight: bold">CLIENT ID</td><td>:</td><td>'+ result.clnt_id +'</td></tr><tr><td style="font-weight: bold">CLIENT NAME</td><td>:</td><td>'+ result.clnt_name +'</td></tr><tr><td style="font-weight: bold">CITY</td><td>:</td><td>'+ result.city +'</td></tr></tbody></table></div>'))
					.addTo(map);
				});
				if (data.length >= 1) {
					if (is_null >= 1) {
						$("#editSiteBtn").show();
						$("#editPayload").val(JSON.stringify(data));
					}
					else {
						$("#addSiteBtn").show();
						$("#createPayload").val(JSON.stringify(data));
					}
				}

				geocoder.clear();
				var styleSpec = data.result;

				dragMarker.setLngLat(point).addTo(map);
			})
		});
	});

	$("select").select2({
		theme: 'bootstrap4',
	});
</script>
