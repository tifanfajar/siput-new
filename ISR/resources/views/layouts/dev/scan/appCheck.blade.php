<!DOCTYPE html>
<html>
<head>
	<title>Scan Result</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0,viewport-fit=cover">
	<link rel="icon" type="image/png" href="{{ asset ('favicon.ico')}}"/>
	<link href="{{ asset ('css/core/main.css')}}" rel="stylesheet">
	<link href="{{ asset ('css/core/app.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset ('jquery/jquery-confirm.css') }}">
</head>
<body>
	<div style="margin: 50px;">
		<div class="left-bar">
			<img src="{{ asset('kominfo.png') }}" alt="">
		</div>
		<div class="right-bar">
			<img src="{{ asset('sdppi-t.png') }}" alt="">
		</div>
		<center>
			<h2 style="margin: 50px;font-weight: bold;">SCAN RESULT</h2>
		</center>
		<br>
		<div class="row">
			@foreach($result as $key => $value)
			<div class="col-md-3">
				<div class="mb-3 card card-body"><h5 class="card-title"><i class="fa fa-broadcast-tower"></i>&nbsp;&nbsp;{{$value->name}}</h5>
					<table class="table">
						<tbody>
							<tr>
								<td>CLIENT ID : {{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('clnt_id')}}</td>
							</tr>
							<tr>
								<td>{{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('clnt_name')}}</td>
							</tr>
							<tr>
								<td><b>{{$value->code}}</b></td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-info btn-sm text-white buttonDetail" data-toggle="modal" data-target=".modalDetail" data-id="{{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('id')}}"><i class="fa fa-desktop"></i>&nbsp;&nbsp;DETAIL</a>
				</div>
			</div>
			@endforeach
		</div>
		<a class="btn btn-success btn-lg text-white" href="{{ url ('login') }}"><i class="fa fa-sign-in-alt"></i>&nbsp;&nbsp;LOGIN TO MANAGE</a>
	</div>
</body>
<script src="{{ asset ('jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('jquery/jquery-confirm.js') }}"></script>
<script type="text/javascript" src="{{ asset ('js/core/main.js')}}"></script></body>
@include('layouts.dev.scan.detailCheck')
@include('layouts.dev.scan.scriptCheck')
</html>