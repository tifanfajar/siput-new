<script type="text/javascript">
    $('.buttonDetail').click(function(){
    var id = $(this).attr('data-id');
    $.ajax
    ({ 
        url: '{{URL::current()}}/' + id,
        type: 'get',
        success: function(result){
            $('#detailClientId').text(result.clnt_id);
            $('#detailClientName').text(result.clnt_name);
            $('#detailSidLong').text(result.sid_long);
            $('#detailSidLat').text(result.sid_lat);
            $('#detailCity').text(result.city);
            $('#detailSiteId').text(result.site_id);

        },
        error: function () {
            $('#detailClose').click();
            $.confirm({
                title: 'Something Wrong',
                content: 'Wait few Minutes',
                type: 'red',
                typeAnimated: true,
                icon: 'fas fa-exclamation-triangle',
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });
});
</script>