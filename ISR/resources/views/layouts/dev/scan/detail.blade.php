@extends('helpers.modals.detailModal')
@section('detailModalForm')
<div class="modal-body">
    <div class="cnt-crd" style="margin: 20px;">
        <div class="left-bar">
            <img src="{{ asset('kominfo.png') }}" alt="">
        </div>
        <div class="right-bar">
            <img src="{{ asset('sdppi-t.png') }}" alt="">
        </div>
        <span class="jdl1">Tanda Pengenal ISR</span>
        <table class="table">
            <tbody>
                <tr>
                    <td style="font-weight: bold;">CLIENT ID</td>
                    <td>:</td>
                    <td id="detailClientId"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">CLIENT NAME</td>
                    <td>:</td>
                    <td id="detailClientName"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">SID LONG</td>
                    <td>:</td>
                    <td id="detailSidLong"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">SID LAT</td>
                    <td>:</td>
                    <td id="detailSidLat"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">CITY</td>
                    <td>:</td>
                    <td id="detailCity">></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">SITE ID</td>
                    <td>:</td>
                    <td id="detailSiteId"></td>
                </tr>
                <!-- <tr>
                    <td style="font-weight: bold;">Inspeksi</td>
                    <td>:</td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Inspeksi Day</td>
                                    <td>:</td>
                                    <td>Sesuai</td>
                                </tr>
                                <tr>
                                    <td>Inspeksi Date</td>
                                    <td>:</td>
                                    <td>23 Sep 2020</td>
                                </tr>
                                <tr>
                                    <td>Update Date</td>
                                    <td>:</td>
                                    <td>24 Sep 2020</td>
                                </tr>
                                <tr>
                                    <td>UPT</td>
                                    <td>:</td>
                                    <td>Balmon Kelas 1 Jkt</td>
                                </tr>
                                <tr>
                                    <td>Long</td>
                                    <td>:</td>
                                    <td>103.81029380</td>
                                </tr>
                                <tr>
                                    <td>Lat</td>
                                    <td>:</td>
                                    <td>6.9809</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr> -->
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    @include('helpers.button.buttonClose')
</div>
@endsection 