@extends('layouts.core.app')

@section('title', 'Scan Result')
@section('pageIcon', 'fa fa-qrcode')
@section('pageTitle', 'Welcome, '.Auth::user()->name.'! ')
@section('subPageTitle', 'Data Service portal for radio station identification with QR Code.')

@section('content')
<div class="row">
	@foreach($result as $key => $value)
	<div class="col-md-3">
		<div class="mb-3 card card-body"><h5 class="card-title"><i class="fa fa-broadcast-tower"></i>&nbsp;&nbsp;{{$value->name}}</h5>
			<table class="table">
				<tbody>
					<tr>
						<td>CLIENT ID : {{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('clnt_id')}}</td>
					</tr>
					<tr>
						<td>{{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('clnt_name')}}</td>
					</tr>
					<tr>
						<td><b>{{$value->code}}</b></td>
					</tr>
				</tbody>
			</table>
			<a class="btn btn-info btn-sm text-white buttonDetail" data-toggle="modal" data-target=".modalDetail" data-id="{{\App\Model\ISR\ISRMaster::where('site_id',$value->id)->value('id')}}"><i class="fa fa-desktop"></i>&nbsp;&nbsp;DETAIL</a>
		</div>
	</div>
	@endforeach
</div>
@endsection
@section('modals')
@include('layouts.dev.scan.detail')
@endsection
@section('script')
@include('layouts.dev.scan.script')
@endsection
