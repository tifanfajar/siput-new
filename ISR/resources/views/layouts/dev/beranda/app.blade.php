@extends('layouts.core.app')

@section('title', 'Beranda')
@section('pageIcon', 'fa fa-qrcode')
@section('pageTitle', 'Welcome, '.Auth::user()->name.'! ')
@section('subPageTitle', 'Data Service portal for radio station identification with QR Code.')

@section('content')
<div class="row">
    <div class="col-md-6 col-xl-4">
        <a href="{{ route('isrQRcode') }}" style="text-decoration: none;">
            <div class="card mb-3 widget-content bg-primary text-white">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">ISR QR Code</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-qrcode"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-4">
        <a href="{{ route('regSTbumi') }}" style="text-decoration: none;">
            <div class="card mb-3 widget-content bg-warning text-white">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Registrasi Stasiun Bumi Satelit</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-globe-asia"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-4">
        <a href="{{ route('regBTSipfr') }}" style="text-decoration: none;">
            <div class="card mb-3 widget-content bg-success text-white">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Registrasi BTS IPFR</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><i class="nav-link-icon fa fa-broadcast-tower"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Grafik Data ISR</h5>
                <canvas id="data-isr"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Data Inspeksi UPT</h5>
                <canvas id="data-inspeksi"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('layouts.dev.beranda.script')
@endsection
