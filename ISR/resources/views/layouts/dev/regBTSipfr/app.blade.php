@extends('layouts.core.app')
@section('title') 
Registrasi BTS IPFR
@endsection
@section('pageIcon') fa fa-broadcast-tower @endsection
@section('pageTitle') Registrasi BTS IPFR @endsection
@section('subPageTitle') Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code. @endsection

@section('content')

<div id='beranda' style='width: 100%; height: 100%;'></div>
<br><br>

@endsection

@section('script')
@include('layouts.dev.regBTSipfr.script')
@endsection
