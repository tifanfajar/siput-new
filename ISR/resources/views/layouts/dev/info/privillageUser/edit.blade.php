@extends('layouts.core.app')
@section('title') 
Privillage
@endsection
@section('pageIcon') fa fa-users-cog @endsection
@section('pageTitle') Edit Privillage User @endsection
@section('subPageTitle') Edit Setting Access Rights for the ISR QR Code Service Portal Application. @endsection

@section('content')

<form method="POST" action="{{ route('updatePrivillage',$role->id) }}" enctype="multipart/form-data">
	@csrf
	<div class="row">
		<div class="position-relative form-group col-md-12">
			<a href="{{URL::previous()}}" class="btn btn-danger float-right"><i class="fa fa-save"></i>&nbsp;&nbsp;BACK</a>
			<button type="submit" class="btn btn-primary float-right mr-2"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>
		</div>
		<div class="position-relative form-group col-md-6">
			<label for="roleName" class="importantField">Role Name</label>
			<input name="role_name" id="createRoleName" placeholder="Enter a role name" type="text" class="form-control" value="{{$role->role_name}}" required>
		</div>
		<div class="position-relative form-group col-md-6">
			<label for="descriptionRole" class="importantField">Description Role</label>
			<input name="description" id="descriptionRole" placeholder="Enter a description role" type="text" class="form-control" value="{{$role->description}}" required>
		</div>
		<div class="position-relative form-group col-md-12">
			<table class="table table-striped table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col" style="width: 350px;">Beranda - Module</th>
						<th scope="col">Create</th>
						<th scope="col">Read</th>
						<th scope="col">Update</th>
						<th scope="col">Delete</th>
						<th scope="col">Check All</th>
					</tr>
				</thead>
				<tbody>
					<?php $pengaturan100019 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100019')->first(); ?>
					<tr class="minimal-checkbox-100019" id='minimal-checkbox-100019'>
						<td>Beranda - Dasboard</td>
						<td><input name="100019_create" class="minimal-checkbox-100019" type="checkbox" id="create-minimal-checkbox-100019" value="100019" {{$pengaturan100019 != null && $pengaturan100019->create_acl == '100019' ? 'checked' : ''}}></td>
						<td><input name="100019_read" class="minimal-checkbox-100019" type="checkbox" id="read-minimal-checkbox-100019" value="100019" {{$pengaturan100019 != null && $pengaturan100019->read_acl == '100019' ? 'checked' : ''}}></td>
						<td><input name="100019_update" class="minimal-checkbox-100019" type="checkbox" id="update-minimal-checkbox-100019" value="100019" {{$pengaturan100019 != null && $pengaturan100019->update_acl == '100019' ? 'checked' : ''}}></td>
						<td><input name="100019_delete" class="minimal-checkbox-100019" type="checkbox" id="delete-checkbox-100019" value="100019" {{$pengaturan100019 != null && $pengaturan100019->delete_acl == '100019' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100019" value="all" name="all" onChange="check(100019)" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-striped table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col" style="width: 350px;">ISR QR Code - Module</th>
						<th scope="col">Create</th>
						<th scope="col">Read</th>
						<th scope="col">Update</th>
						<th scope="col">Delete</th>
						<th scope="col">Check All</th>
					</tr>
				</thead>
				<tbody>
					<?php $pengaturan10001 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10001')->first(); ?>
					<tr id='minimal-checkbox-10001'>
						<td>ISR QR Code - Dashboard</td>
						<td><input name="10001_create" type="checkbox" id="create-minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->create_acl == '10001' ? 'checked' : ''}}></td>
						<td><input name="10001_read" type="checkbox" id="read-minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->read_acl == '10001' ? 'checked' : ''}}></td>
						<td><input name="10001_update" type="checkbox" id="update-minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->update_acl == '10001' ? 'checked' : ''}}></td>
						<td><input name="10001_delete" type="checkbox" id="delete-minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->delete_acl == '10001' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10001" value="all" name="all" onChange="check(10001)" /></td>
					</tr>
					<?php $pengaturan10002 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10002')->first(); ?>
					<tr id='minimal-checkbox-10002'>
						<td>ISR QR Code - QR Code Map</td>
						<td><input name="10002_create" type="checkbox" id="create-minimal-checkbox-10002" value="10002" {{$pengaturan10002 != null && $pengaturan10002->create_acl == '10002' ? 'checked' : ''}}></td>
						<td><input name="10002_read" type="checkbox" id="read-minimal-checkbox-10002" value="10002" {{$pengaturan10002 != null && $pengaturan10002->read_acl == '10002' ? 'checked' : ''}}></td>
						<td><input name="10002_update" type="checkbox" id="update-minimal-checkbox-10002" value="10002" {{$pengaturan10002 != null && $pengaturan10002->update_acl == '10002' ? 'checked' : ''}}></td>
						<td><input name="10002_delete" type="checkbox" id="delete-minimal-checkbox-10002" value="10002" {{$pengaturan10002 != null && $pengaturan10002->delete_acl == '10002' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10002" value="all" name="all" onChange="check(10002)" /></td>
					</tr>
					<?php $pengaturan10003 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10003')->first(); ?>
					<tr id='minimal-checkbox-10003'>
						<td>ISR QR Code - QR Code Location</td>
						<td><input name="10003_create" type="checkbox" id="create-minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->create_acl == '10003' ? 'checked' : ''}}></td>
						<td><input name="10003_read" type="checkbox" id="read-minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->read_acl == '10003' ? 'checked' : ''}}></td>
						<td><input name="10003_update" type="checkbox" id="update-minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->update_acl == '10003' ? 'checked' : ''}}></td>
						<td><input name="10003_delete" type="checkbox" id="delete-minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->delete_acl == '10003' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10003" value="all" name="all" onChange="check(10003)" /></td>
					</tr>
					<?php $pengaturan10004 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10004')->first(); ?>
					<tr id='minimal-checkbox-10004'>
						<td>ISR QR Code - Print QR Code</td>
						<td><input name="10004_create" type="checkbox" id="create-minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->create_acl == '10004' ? 'checked' : ''}}></td>
						<td><input name="10004_read" type="checkbox" id="read-minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->read_acl == '10004' ? 'checked' : ''}}></td>
						<td><input name="10004_update" type="checkbox" id="update-minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->update_acl == '10004' ? 'checked' : ''}}></td>
						<td><input name="10004_delete" type="checkbox" id="delete-minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->delete_acl == '10004' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10004" value="all" name="all" onChange="check(10004)" /></td>
					</tr>
					<?php $pengaturan10005 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10005')->first(); ?>
					<tr id='minimal-checkbox-10005'>
						<td>ISR QR Code - Form Inspeksi</td>
						<td><input name="10005_create" type="checkbox" id="create-minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->create_acl == '10005' ? 'checked' : ''}}></td>
						<td><input name="10005_read" type="checkbox" id="read-minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->read_acl == '10005' ? 'checked' : ''}}></td>
						<td><input name="10005_update" type="checkbox" id="update-minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->update_acl == '10005' ? 'checked' : ''}}></td>
						<td><input name="10005_delete" type="checkbox" id="delete-minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->delete_acl == '10005' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10005" value="all" name="all" onChange="check(10005)" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-striped table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col" style="width: 350px;">Registrasi Stasiun Bumi - Module</th>
						<th scope="col">Create</th>
						<th scope="col">Read</th>
						<th scope="col">Update</th>
						<th scope="col">Delete</th>
						<th scope="col">Check All</th>
					</tr>
				</thead>
				<tbody>
					<?php $pengaturan10006 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10006')->first(); ?>
					<tr id='minimal-checkbox-10006'>
						<td>Registrasi Stasiun Bumi - Dashboard</td>
						<td><input name="10006_create" type="checkbox" id="create-minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->create_acl == '10006' ? 'checked' : ''}}></td>
						<td><input name="10006_read" type="checkbox" id="read-minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->read_acl == '10006' ? 'checked' : ''}}></td>
						<td><input name="10006_update" type="checkbox" id="update-minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->update_acl == '10006' ? 'checked' : ''}}></td>
						<td><input name="10006_delete" type="checkbox" id="delete-minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->delete_acl == '10006' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10006" value="all" name="all" onChange="check(10006)" /></td>
					</tr>
					<?php $pengaturan10007 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10007')->first(); ?>
					<tr id='minimal-checkbox-10007'>
						<td>Registrasi Stasiun Bumi - QR Code Map</td>
						<td><input name="10007_create" type="checkbox" id="create-minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->create_acl == '10007' ? 'checked' : ''}}></td>
						<td><input name="10007_read" type="checkbox" id="read-minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->read_acl == '10007' ? 'checked' : ''}}></td>
						<td><input name="10007_update" type="checkbox" id="update-minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->update_acl == '10007' ? 'checked' : ''}}></td>
						<td><input name="10007_delete" type="checkbox" id="delete-minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->delete_acl == '10007' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10007" value="all" name="all" onChange="check(10007)" /></td>
					</tr>
					<?php $pengaturan10008 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10008')->first(); ?>
					<tr id='minimal-checkbox-10008'>
						<td>Registrasi Stasiun Bumi - QR Code Location</td>
						<td><input name="10008_create" type="checkbox" id="create-minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->create_acl == '10008' ? 'checked' : ''}}></td>
						<td><input name="10008_read" type="checkbox" id="read-minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->read_acl == '10008' ? 'checked' : ''}}></td>
						<td><input name="10008_update" type="checkbox" id="update-minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->update_acl == '10008' ? 'checked' : ''}}></td>
						<td><input name="10008_delete" type="checkbox" id="delete-minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->delete_acl == '10008' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10008" value="all" name="all" onChange="check(10008)" /></td>
					</tr>
					<?php $pengaturan10009 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10009')->first(); ?>
					<tr id='minimal-checkbox-10009'>
						<td>Registrasi Stasiun Bumi - Print QR Code</td>
						<td><input name="10009_create" type="checkbox" id="create-minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->create_acl == '10009' ? 'checked' : ''}}></td>
						<td><input name="10009_read" type="checkbox" id="read-minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->read_acl == '10009' ? 'checked' : ''}}></td>
						<td><input name="10009_update" type="checkbox" id="update-minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->update_acl == '10009' ? 'checked' : ''}}></td>
						<td><input name="10009_delete" type="checkbox" id="delete-minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->delete_acl == '10009' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-10009" value="all" name="all" onChange="check(10009)" /></td>
					</tr>
					<?php $pengaturan100010 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100010')->first(); ?>
					<tr id='minimal-checkbox-100010'>
						<td>Registrasi Stasiun Bumi - Form Inspeksi</td>
						<td><input name="100010_create" type="checkbox" id="create-minimal-checkbox-100010" value="100010" {{$pengaturan100010 != null && $pengaturan100010->create_acl == '100010' ? 'checked' : ''}}></td>
						<td><input name="100010_read" type="checkbox" id="read-minimal-checkbox-100010" value="100010" {{$pengaturan100010 != null && $pengaturan100010->read_acl == '100010' ? 'checked' : ''}}></td>
						<td><input name="100010_update" type="checkbox" id="update-minimal-checkbox-100010" value="100010" {{$pengaturan100010 != null && $pengaturan100010->update_acl == '100010' ? 'checked' : ''}}></td>
						<td><input name="100010_delete" type="checkbox" id="delete-minimal-checkbox-100010" value="100010" {{$pengaturan100010 != null && $pengaturan100010->delete_acl == '100010' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100010" value="all" name="all" onChange="check(100010)" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-striped table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col" style="width: 350px;">Registrasi BTS IPFR - Module</th>
						<th scope="col">Create</th>
						<th scope="col">Read</th>
						<th scope="col">Update</th>
						<th scope="col">Delete</th>
						<th scope="col">Check All</th>
					</tr>
				</thead>
				<tbody>
					<?php $pengaturan100011 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100011')->first(); ?>
					<tr id='minimal-checkbox-100011'>
						<td>Registrasi BTS IPFR - Dashboard</td>
						<td><input name="100011_create" type="checkbox" id="create-minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->create_acl == '100011' ? 'checked' : ''}}></td>
						<td><input name="100011_read" type="checkbox" id="read-minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->read_acl == '100011' ? 'checked' : ''}}></td>
						<td><input name="100011_update" type="checkbox" id="update-minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->update_acl == '100011' ? 'checked' : ''}}></td>
						<td><input name="100011_delete" type="checkbox" id="delete-minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->delete_acl == '100011' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100011" value="all" name="all" onChange="check(100011)" /></td>
					</tr>
					<?php $pengaturan100012 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100012')->first(); ?>
					<tr id='minimal-checkbox-100012'>
						<td>Registrasi BTS IPFR - QR Code Map</td>
						<td><input name="100012_create" type="checkbox" id="create-minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->create_acl == '100012' ? 'checked' : ''}}></td>
						<td><input name="100012_read" type="checkbox" id="read-minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->read_acl == '100012' ? 'checked' : ''}}></td>
						<td><input name="100012_update" type="checkbox" id="update-minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->update_acl == '100012' ? 'checked' : ''}}></td>
						<td><input name="100012_delete" type="checkbox" id="delete-minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->delete_acl == '100012' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100012" value="all" name="all" onChange="check(100012)" /></td>
					</tr>
					<?php $pengaturan100013 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100013')->first(); ?>
					<tr id='minimal-checkbox-100013'>
						<td>Registrasi BTS IPFR - QR Code Location</td>
						<td><input name="100013_create" type="checkbox" id="create-minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->create_acl == '100013' ? 'checked' : ''}}></td>
						<td><input name="100013_read" type="checkbox" id="read-minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->read_acl == '100013' ? 'checked' : ''}}></td>
						<td><input name="100013_update" type="checkbox" id="update-minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->update_acl == '100013' ? 'checked' : ''}}></td>
						<td><input name="100013_delete" type="checkbox" id="delete-minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->delete_acl == '100013' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100013" value="all" name="all" onChange="check(100013)" /></td>
					</tr>
					<?php $pengaturan100014 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100014')->first(); ?>
					<tr id='minimal-checkbox-100014'>
						<td>Registrasi BTS IPFR - Print QR Code</td>
						<td><input name="100014_create" type="checkbox" id="create-minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->create_acl == '100014' ? 'checked' : ''}}></td>
						<td><input name="100014_read" type="checkbox" id="read-minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->read_acl == '100014' ? 'checked' : ''}}></td>
						<td><input name="100014_update" type="checkbox" id="update-minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->update_acl == '100014' ? 'checked' : ''}}></td>
						<td><input name="100014_delete" type="checkbox" id="delete-minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->delete_acl == '100014' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100014" value="all" name="all" onChange="check(100014)" /></td>
					</tr>
					<?php $pengaturan100015 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100015')->first(); ?>
					<tr id='minimal-checkbox-100015'>
						<td>Registrasi BTS IPFR - Form Inspeksi</td>
						<td><input name="100015_create" type="checkbox" id="create-minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->create_acl == '100015' ? 'checked' : ''}}></td>
						<td><input name="100015_read" type="checkbox" id="read-minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->read_acl == '100015' ? 'checked' : ''}}></td>
						<td><input name="100015_update" type="checkbox" id="update-minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->update_acl == '100015' ? 'checked' : ''}}></td>
						<td><input name="100015_delete" type="checkbox" id="delete-minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->delete_acl == '100015' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100015" value="all" name="all" onChange="check(100015)" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-striped table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col">Info - Module</th>
						<th scope="col">Create</th>
						<th scope="col">Read</th>
						<th scope="col">Update</th>
						<th scope="col">Delete</th>
						<th scope="col">Check All</th>
					</tr>
				</thead>
				<tbody>
					<?php $pengaturan100016 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100016')->first(); ?>
					<tr id='minimal-checkbox-100016'>
						<td>Info - User Management</td>
						<td><input name="100016_create" type="checkbox" id="create-minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->create_acl == '100016' ? 'checked' : ''}}></td>
						<td><input name="100016_read" type="checkbox" id="read-minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->read_acl == '100016' ? 'checked' : ''}}></td>
						<td><input name="100016_update" type="checkbox" id="update-minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->update_acl == '100016' ? 'checked' : ''}}></td>
						<td><input name="100016_delete" type="checkbox" id="delete-minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->delete_acl == '100016' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100016" value="all" name="all" onChange="check(100016)" /></td>
					</tr>
					<?php $pengaturan100017 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100017')->first(); ?>
					<tr id='minimal-checkbox-100017'>
						<td>Info - Privillage</td>
						<td><input name="100017_create" type="checkbox" id="create-minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->create_acl == '100017' ? 'checked' : ''}}></td>
						<td><input name="100017_read" type="checkbox" id="read-minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->read_acl == '100017' ? 'checked' : ''}}></td>
						<td><input name="100017_update" type="checkbox" id="update-minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->update_acl == '100017' ? 'checked' : ''}}></td>
						<td><input name="100017_delete" type="checkbox" id="delete-minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->delete_acl == '100017' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100017" value="all" name="all" onChange="check(100017)" /></td>
					</tr>
					<?php $pengaturan100018 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100018')->first(); ?>
					<tr id='minimal-checkbox-100018'>
						<td>Info - Backup Database</td>
						<td><input name="100018_create" type="checkbox" id="create-minimal-checkbox-100018" value="100018" {{$pengaturan100018 != null && $pengaturan100018->create_acl == '100018' ? 'checked' : ''}}></td>
						<td><input name="100018_read" type="checkbox" id="read-minimal-checkbox-100018" value="100018" {{$pengaturan100018 != null && $pengaturan100018->read_acl == '100018' ? 'checked' : ''}}></td>
						<td><input name="100018_update" type="checkbox" id="update-minimal-checkbox-100018" value="100018" {{$pengaturan100018 != null && $pengaturan100018->update_acl == '100018' ? 'checked' : ''}}></td>
						<td><input name="100018_delete" type="checkbox" id="delete-minimal-checkbox-100018" value="100018" {{$pengaturan100018 != null && $pengaturan100018->delete_acl == '100018' ? 'checked' : ''}}></td>
						<td><input type="checkbox" id="all-100018" value="all" name="all" onChange="check(100018)" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</form>
@endsection

@section('script')
@include('layouts.dev.info.privillageUser.request')
@endsection