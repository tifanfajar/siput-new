@extends('layouts.core.app')
@section('title') 
Privillage
@endsection
@section('pageIcon') fa fa-users-cog @endsection
@section('pageTitle') Privillage User @endsection
@section('subPageTitle') Setting Access Rights for the ISR QR Code Service Portal Application. @endsection

@section('content')

<div class="col-lg-12">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Master Data
                @if($getCreate == $getKdModule)
                @include('helpers.button.buttonCreate')
                @endif
            </h5>
            <br>
            <div class="table-responsive">
                <table id="dataTable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $key => $value)
                        <tr>
                            <td>{{$value->role_name}}</td>
                            <td>{{$value->description}}</td>
                            <td>
                                <?php $id = Crypt::encryptString($value->id) ?>
                               <a class="btn btn-info btn-sm text-white" data-toggle="modal" href="{{ route('detailByIdPrivillage',$value->id) }}"><i class="fa fa-desktop"></i>&nbsp;&nbsp;DETAIL</a>
                                @if($getEdit == $getKdModule)
                                <a class="btn btn-success btn-sm text-white" data-toggle="modal" href="{{ route('viewByIdPrivillage',$value->id) }}"><i class="fa fa-edit"></i>&nbsp;&nbsp;EDIT</a>
                                @endif
                                @if($getDelete == $getKdModule)
                                <a  class="btn btn-danger btn-sm deleteConfirmation" href="{{ route ('deletePrivillage',$id)}}"><i class="fa fa-trash"></i>&nbsp;&nbsp;DELETE</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modals')
@include('layouts.dev.info.privillageUser.create')
@endsection

@section('script')
@include('layouts.dev.info.privillageUser.request')
@endsection