<script>
    $('input[type="checkbox"]').css('cursor','pointer');

    function check(no) {
     var data =  document.getElementById('minimal-checkbox-'+no);
     data.addEventListener('change', function(e) {
        var el = e.target;
        var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

        if (el.id === 'all-'+no) {
            for (var i = 0, input; input = inputs[i++]; ) {
                input.checked = el.checked;
            }
        } else {
            var numChecked = 0;

            for (var i = 1, input; input = inputs[i++]; ) {
                if (input.checked) {
                    numChecked++;
                }
            }
            inputs[0].checked = numChecked === inputs.length - 1;
        }
    }, false);
 }
</script>