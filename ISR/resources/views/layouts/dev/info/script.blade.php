<script type="text/javascript">
   var randomColorGenerator = function () { 
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
};

var isr = document.getElementById('data-user').getContext('2d');
var data_isr = new Chart(isr, {

    type: 'pie',


    data: {
        labels: ['User', 'Upt', 'Waba'],
        datasets: [{
            label: 'User',
            backgroundColor: [
            randomColorGenerator(),
            randomColorGenerator(),
            randomColorGenerator()
            ],
            data: [{{$grafikUser}}, {{$grafikUserUpt}}, {{$grafikUserWaba}}]
        }]
    },


    options: {}
});

var inspeksi = document.getElementById('data-site').getContext('2d');
var data_inspeksi = new Chart(inspeksi, {

    type: 'doughnut',


    data: {
        labels: [
        @foreach($siteConf as $key => $value)
        '{{$value->role_name}}',
        @endforeach
        ],
        datasets: [{
            label: 'Site Configuration',
            backgroundColor: [
            @foreach($siteConf as $key => $value)
            randomColorGenerator(),
            @endforeach
            ],
            data: [
            @foreach($siteConf as $key => $value)
            {{\App\User::where('role_id',$value->id)->count()}},
            @endforeach
            ]
        }]
    },


    options: {}
});
</script>