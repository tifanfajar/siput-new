@extends('layouts.core.app')
@section('title') 
User Management
@endsection
@section('pageIcon') fa fa-users @endsection
@section('pageTitle') User Management @endsection
@section('subPageTitle') User Management for the ISR QR Code Service Portal Application. @endsection
@section('content')
<div class="col-lg-12">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Master Data
                @if($getCreate == $getKdModule)
                @include('helpers.button.buttonCreate')
                @endif
            </h5>
            <br>
            <div class="table-responsive">
                <table id="dataTable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key => $value)
                        <tr>
                            <?php $id = Crypt::encryptString($value->id) ?>
                            <td>{{$value->name}}</td>
                            <td><a href="mailto:{{$value->email}}?Subject=Portal Layanan ISR QR Code" target="_top">{{$value->email}}</a></td>
                            <td>{{\App\Model\Privillage\Role::where('id',$value->role_id)->value('role_name')}}</td>
                            <td>
                                <?php $id = Crypt::encryptString($value->id) ?>
                                @include('helpers.button.buttonDetail')
                                @if($getEdit == $getKdModule)
                                @include('helpers.button.buttonEdit')
                                @endif
                                @if($getDelete == $getKdModule && $value->id != Auth::user()->id)
                                <a  class="btn btn-danger btn-sm deleteConfirmation" href="{{ route ('deleteUser',$id)}}"><i class="fa fa-trash"></i>&nbsp;&nbsp;DELETE</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@include('layouts.dev.info.userManagement.create')
@include('layouts.dev.info.userManagement.edit')
@include('layouts.dev.info.userManagement.detail')
@endsection

@section('script')
@include('layouts.dev.info.userManagement.request')
@endsection
