@extends('helpers.modals.editModal')
@section('editModalForm')
<form method="POST" action="{{ route ('updateUser')}}" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="row">
            <input type="hidden" name="id" id="editId">
            <div class="position-relative form-group col-md-6">
                <label for="name" class="importantField">Name</label>
                <input name="name" id="editName" placeholder="Enter a name" type="text" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="email" class="importantField">Email</label>
                <input name="email" id="editEmail" placeholder="Enter a email" type="email" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="password" class="importantField">Password ( Masukan Password untuk Mengubah Data )</label>
                <input name="password" id="editPassword" placeholder="Enter a password" type="password" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="role_id" class="importantField">Role</label>
                <select name="role_id" id="editRoleId" class="form-control" required>
                    <option value="" selected disabled>Enter a Role</option>
                    @foreach(\App\Model\Privillage\Role::all() as $key => $value)
                    <option value="{{$value->id}}">{{$value->role_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="editUserType" class="importantField">User Type</label>
                <select id="editUserType" name="userType" class="form-control" required>
                    <option value="" selected disabled>Enter a type</option>
                    <option value="user">User</option>
                    <option value="upt">UPT</option>
                    <option value="waba">Waba</option>
                </select>
            </div>
            <div class="position-relative form-group col-md-6 div-type-code">
                <label for="editTypeCode" id="labeleditTypeCode" class="importantField">Select User Type First</label>
                <select name="typeCode" id="editTypeCode" class="form-control selectData">
                    <option value="" selected disabled>Enter a sub type</option>
                </select>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="profile_photo" class="optionalField">Profile Photo</label>
                <input name="profile_photo" id="editProfilePhoto" type="file" class="form-control-file" accept="image/*" onchange="document.getElementById('editOldPhotoProfilePreview').src = window.URL.createObjectURL(this.files[0])">
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="profile_photo" class="optionalField">Preview Profile Photo</label>
                <div id="editOldPhotoProfile"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('helpers.button.buttonClose')
        @include('helpers.button.buttonSave')
    </div>
</form>
@endsection