<script>
    $('.buttonEdit').click(function(){
        var id = $(this).attr('data-id');
        $('.selectData').select2({ width: '100%', height: 'auto', theme: 'bootstrap4'});
        $.ajax
        ({ 
            url: '{{URL::current()}}/viewById/' + id,
            type: 'get',
            success: function(result){
                $.ajax
                ({ 
                    url: '{{URL::current()}}/getRoleName/' + result.role_id,
                    type: 'get',
                    success: function(roleName){
                        $('#editRoleId').append('<option value="'+ result.role_id +'" selected>'+ roleName +'</option>');
                    },
                    error: function () {
                        $('#editClose').click();
                        $.confirm({
                            title: 'Something Wrong',
                            content: 'Wait few Minutes',
                            type: 'red',
                            typeAnimated: true,
                            icon: 'fas fa-exclamation-triangle',
                            buttons: {
                                close: function () {
                                }
                            }
                        });
                    }
                });

                if (result.user_type == 'user') {
                    $('#editUserType').append('<option value="user" selected>User</option>');
                    $('.selectData').val('');
                    $('.div-type-code').css('visibility', 'hidden');
                }
                if (result.user_type == 'upt') {
                    $.ajax
                    ({ 
                        url: '{{URL::current()}}/getTypeCodeUpt/' + result.type_code,
                        type: 'get',
                        success: function(typeCode){
                            $('#editUserType').append('<option value="upt" selected>UPT</option>');
                            $.ajax({
                                url: "{{ route('getUpt') }}",
                                method: 'GET',
                                success: function(data) {
                                    $('.div-type-code').css('visibility', 'visible');                
                                    jQuery("label[for='editTypeCode']").html("UPT Name");
                                    $('#editTypeCode').html('<option value="'+ result.type_code +'" selected>'+ typeCode +'</option>' + data.html);
                                }
                            });
                        },
                        error: function () {
                            $('#editClose').click();
                            $.confirm({
                                title: 'Something Wrong',
                                content: 'Wait few Minutes',
                                type: 'red',
                                typeAnimated: true,
                                icon: 'fas fa-exclamation-triangle',
                                buttons: {
                                    close: function () {
                                    }
                                }
                            });
                        }
                    });
                }
                if (result.user_type == 'waba') {
                    $.ajax
                    ({ 
                        url: '{{URL::current()}}/getTypeCodeWaba/' + result.type_code,
                        type: 'get',
                        success: function(typeCode){
                            $('#editUserType').append('<option value="waba" selected>Waba</option>');
                            $.ajax({
                                url: "{{ route('getWaba') }}",
                                method: 'GET',
                                success: function(data) {
                                    $('.div-type-code').css('visibility', 'visible');                
                                    jQuery("label[for='editTypeCode']").html("Waba Name");
                                    $('#editTypeCode').html('<option value="'+ result.type_code +'" selected>'+ result.type_code +' - '+ typeCode +'</option>' + data.html);
                                }
                            });
                        },
                        error: function () {
                            $('#editClose').click();
                            $.confirm({
                                title: 'Something Wrong',
                                content: 'Wait few Minutes',
                                type: 'red',
                                typeAnimated: true,
                                icon: 'fas fa-exclamation-triangle',
                                buttons: {
                                    close: function () {
                                    }
                                }
                            });
                        }
                    });
                }

                $('#editId').val(result.id);
                $('#editName').val(result.name);
                $('#editEmail').val(result.email);
                $('#editOldPhotoProfile').html('<img width="150" style="border-radius: 50%;" id="editOldPhotoProfilePreview" height="150" src="'+ '../images/user/' + result.profile_photo +'">');

            },
            error: function () {
                $('#editClose').click();
                $.confirm({
                    title: 'Something Wrong',
                    content: 'Wait few Minutes',
                    type: 'red',
                    typeAnimated: true,
                    icon: 'fas fa-exclamation-triangle',
                    buttons: {
                        close: function () {
                        }
                    }
                });
            }
        });
});

$('.buttonDetail').click(function(){
    var id = $(this).attr('data-id');
    $.ajax
    ({ 
        url: '{{URL::current()}}/viewById/' + id,
        type: 'get',
        success: function(result){
            $.ajax
            ({ 
                url: '{{URL::current()}}/getRoleName/' + result.role_id,
                type: 'get',
                success: function(roleName){
                    $('#detailRoleId').val(roleName);
                },
                error: function () {
                    $('#detailClose').click();
                    $.confirm({
                        title: 'Something Wrong',
                        content: 'Wait few Minutes',
                        type: 'red',
                        typeAnimated: true,
                        icon: 'fas fa-exclamation-triangle',
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
            });
            if (result.user_type == 'user') {
                $('#detailTypeCode').val('There is no type code');
                $('#detailUserType').val('User');
            }
            if (result.user_type == 'upt') {
                $.ajax
                ({ 
                    url: '{{URL::current()}}/getTypeCodeUpt/' + result.type_code,
                    type: 'get',
                    success: function(typeCode){
                        $('#detailUserType').val('UPT');
                        $('#detailTypeCode').val(typeCode);
                    },
                    error: function () {
                        $('#detailClose').click();
                        $.confirm({
                            title: 'Something Wrong',
                            content: 'Wait few Minutes',
                            type: 'red',
                            typeAnimated: true,
                            icon: 'fas fa-exclamation-triangle',
                            buttons: {
                                close: function () {
                                }
                            }
                        });
                    }
                });
            }
            if (result.user_type == 'waba') {
                $.ajax
                ({ 
                    url: '{{URL::current()}}/getTypeCodeWaba/' + result.type_code,
                    type: 'get',
                    success: function(typeCode){
                        $('#detailUserType').val('Waba');
                        $('#detailTypeCode').val(typeCode);
                    },
                    error: function () {
                        $('#detailClose').click();
                        $.confirm({
                            title: 'Something Wrong',
                            content: 'Wait few Minutes',
                            type: 'red',
                            typeAnimated: true,
                            icon: 'fas fa-exclamation-triangle',
                            buttons: {
                                close: function () {
                                }
                            }
                        });
                    }
                });
            }

            $('#detailName').val(result.name);
            $('#detailEmail').val(result.email);
            $('#detailPhotoProfile').html('<img width="150" height="150" style="border-radius: 50%;" src="'+ '../images/user/' + result.profile_photo +'">');
        },
        error: function () {
            $('#detailClose').click();
            $.confirm({
                title: 'Something Wrong',
                content: 'Wait few Minutes',
                type: 'red',
                typeAnimated: true,
                icon: 'fas fa-exclamation-triangle',
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });
});

$("#userType").change(function () {
    var selected_option = $('#userType').val();
    if (selected_option === 'user') {
     $('.selectData').val('');
     $('.div-type-code').css('visibility', 'hidden');
 }
 if (selected_option === 'upt') {
    $('.selectData').select2({ width: '100%', height: 'auto',theme: 'bootstrap4'});
    $.ajax({
        url: "{{ route('getUpt') }}",
        method: 'GET',
        success: function(data) {
            $('.div-type-code').css('visibility', 'visible');                
            jQuery("label[for='typeCode']").html("Select UPT");
            $('#typeCode').html(data.html);
        }
    });
}
if (selected_option === 'waba') {
    $('.selectData').select2({ width: '100%', height: 'auto',theme: 'bootstrap4'});
    $.ajax({
        url: "{{ route('getWaba') }}",
        method: 'GET',
        success: function(data) {
            $('.div-type-code').css('visibility', 'visible');
            jQuery("label[for='typeCode']").html("Select Waba");
            $('#typeCode').html(data.html);
        }
    });
}
});

$("#editUserType").change(function () {
    var selected_option = $('#editUserType').val();
    if (selected_option === 'user') {
     $('.selectData').val('');
     $('.div-type-code').css('visibility', 'hidden');
 }
 if (selected_option === 'upt') {
    $('.selectData').select2({ width: '100%', height: 'auto', theme: 'bootstrap4' });
    $.ajax({
        url: "{{ route('getUpt') }}",
        method: 'GET',
        success: function(data) {
            $('.div-type-code').css('visibility', 'visible');                
            jQuery("label[for='editTypeCode']").html("Select UPT");
            $('#editTypeCode').html(data.html);
        }
    });
}
if (selected_option === 'waba') {
    $('.selectData').select2({ width: '100%', height: 'auto', theme: 'bootstrap4' });
    $.ajax({
        url: "{{ route('getWaba') }}",
        method: 'GET',
        success: function(data) {
            $('.div-type-code').css('visibility', 'visible');
            jQuery("label[for='editTypeCode']").html("Select Waba");
            $('#editTypeCode').html(data.html);
        }
    });
}
});

</script>