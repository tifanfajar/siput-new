@extends('layouts.core.app')
@section('title') 
Info
@endsection
@section('pageIcon') fa fa-question-circle @endsection
@section('pageTitle') Info @endsection
@section('subPageTitle') Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code. @endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title text-center">Grafik Data User</h5>
                <hr>
                <canvas id="data-user"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title text-center">Grafik Data Site Configuration</h5>
                <hr>
                <canvas id="data-site"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('layouts.dev.info.script')
@endsection