<div class="app-header-right">
	<div class="header-btn-lg pr-0">
		<div class="widget-content p-0">
			<div class="widget-content-wrapper">
				<div class="widget-content-left">
					<div class="btn-group">
						<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
							<img width="42" class="rounded-circle" src="{{ route ('userLoginImages',Auth::user()->profile_photo)}}" alt="">
							<i class="fa fa-angle-down ml-2 opacity-8"></i>
						</a>
						<div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
							<a type="button" class="dropdown-item" data-toggle="modal" data-target=".modalUserInfo"><i class="fas fa-user"></i>&nbsp;&nbsp;User Account</a>
							<a type="button" href="{{ route('info') }}" tabindex="0" class="dropdown-item"><i class="fas fa-cog"></i>&nbsp;&nbsp;Settings</a>
							<a type="button" tabindex="0" class="dropdown-item logoutConfirmation" href="{{ route('logout') }}">
								<i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;Logout</a>
							</div>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</div>
					<div class="widget-content-left  ml-3 header-user-info">
						<div class="widget-heading">
							{{Auth::user()->name}}
						</div>
						<div class="widget-subheading">
							{{App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('role_name')}}
						</div>
					</div>
				</div>
			</div>
		</div>        
	</div>

	<div class="modal fade modalUserInfo"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white" id="exampleModalLongTitle"><i class="fa fa-user"></i>&nbsp;&nbsp;User Account</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="POST" action="{{ route('updateUser') }}" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="row">
							<input type="hidden" name="id" value="{{Auth::user()->id}}">
							<div class="position-relative form-group col-md-6">
								<label for="name" class="importantField">Name</label>
								<input name="name" placeholder="Enter a name" type="text" class="form-control" value="{{Auth::user()->name}}" required>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="email" class="importantField">Email</label>
								<input name="email"  placeholder="Enter a email" type="email" class="form-control" value="{{Auth::user()->email}}" required>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="password" class="importantField">Password ( New Password For Update )</label>
								<input name="password" placeholder="Enter a password" type="password" class="form-control" required>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="role" class="importantField">Role</label>
								<input  placeholder="Enter a role" type="text" class="form-control" value="{{\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('role_name')}}" readonly>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="userType" class="importantField">User Type</label>
								<input  placeholder="Enter a user type" type="text" class="form-control" value="{{ucfirst(Auth::user()->user_type)}}" readonly>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="typeCode" class="importantField">@if(Auth::user()->user_type == 'user') User @elseif(Auth::user()->user_type == 'upt') UPT Name @elseif(Auth::user()->user_type == 'waba') Waba Name @endif</label>
								<input  placeholder="Enter a typeCode" type="text" class="form-control" value="@if(Auth::user()->user_type == 'user') User @elseif(Auth::user()->user_type == 'upt') {{\App\Model\Dev\Upt::where('office_id',Auth::user()->type_code)->value('office_name')}} @elseif(Auth::user()->user_type == 'waba') {{\App\Model\ISR\ISRMaster::where('clnt_id',Auth::user()->type_code)->value('clnt_name')}} @endif" readonly>
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="profile_photo" class="optionalField">Profile Photo</label>
								<input name="profile_photo" type="file" class="form-control-file" accept="image/*" onchange="document.getElementById('PhotoProfilePreview').src = window.URL.createObjectURL(this.files[0])">
							</div>
							<div class="position-relative form-group col-md-6">
								<label for="profile_photo" class="optionalField">Preview Photo</label>
								<br>
								<img style="border-radius: 50%;" id="PhotoProfilePreview" src="{{ route ('userLoginImages',Auth::user()->profile_photo)}}" width="150" height="150">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						@include('helpers.button.buttonClose')
						@include('helpers.button.buttonSave')
					</div>
				</form>
			</div>
		</div>
	</div>