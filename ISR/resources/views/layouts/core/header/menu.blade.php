<?php 
$module_parent = DB::table('role_acl')
->select('module_parent','module_name','pathParent','menu_icon')
->join('modules','role_acl.module_parent','=','modules.id')
->where('role_id', Auth::user()->role_id)
->where(function ($query){
	$query->where('create_acl','<>',0)
	->orWhere('read_acl','<>',0)
	->orWhere('update_acl','<>',0)
	->orWhere('delete_acl','<>',0);
})
->groupBy('module_parent')
->groupBy('module_name')
->groupBy('pathParent')
->groupBy('menu_icon')
->orderBy('module_parent','asc')
->get();
?>
<ul class="header-menu nav">
	@foreach($module_parent as $parent)
	<li class="btn-group nav-item{{ Request::is($parent->pathParent) ? '-active' : '' }}">
		<a href="{{url($parent->pathParent)}}" class="nav-link">
			<i class="nav-link-icon {{$parent->menu_icon}}"></i>
			{{$parent->module_name}}
		</a>
	</li>
	@endforeach
</ul> 