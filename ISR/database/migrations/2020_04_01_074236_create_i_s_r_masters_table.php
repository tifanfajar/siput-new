<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateISRMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_s_r_masters', function (Blueprint $table) {
            $table->id();
            $table->string('clnt_id')->nullable();
            $table->string('clnt_name')->nullable();
            $table->float('sid_long')->nullable();
            $table->float('sid_lat')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_s_r_masters');
    }
}
