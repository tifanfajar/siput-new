<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\User::create([
            'user_type' => 'user',
    		'name'  => 'Administrator',
    		'email' => 'isrAdmin@isr.com',
    		'password'  => bcrypt('isrAdmin'),
    		'role_id' => 1
    	]);

    	\App\Model\Privillage\Role::create([
    		'role_name' => 'Administrator',
    		'description' => 'Administrator',
    		'akses' => 'Administrator'
    	]);
    }
}
