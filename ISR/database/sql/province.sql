INSERT INTO "provinces" ("id", "name", "created_at", "updated_at") VALUES
(1, 'ACEH', NULL, NULL),
(2, 'SUMATERA UTARA', NULL, NULL),
(3, 'SUMATERA BARAT', NULL, NULL),
(4, 'RIAU', NULL, NULL),
(5, 'JAMBI', NULL, NULL),
(6, 'SUMATERA SELATAN', NULL, NULL),
(8, 'BENGKULU', NULL, NULL),
(7, 'LAMPUNG', NULL, NULL),
(30, 'BANGKA BELITUNG', NULL, NULL),
(33, 'KEPULAUAN RIAU', NULL, NULL),
(9, 'DKI JAKARTA', NULL, NULL),
(10, 'JAWA BARAT', NULL, NULL),
(11, 'JAWA TENGAH', NULL, NULL),
(12, 'DI YOGYAKARTA', NULL, NULL),
(13, 'JAWA TIMUR', NULL, NULL),
(32, 'BANTEN', NULL, NULL),
(22, 'BALI', NULL, NULL),
(23, 'NUSA TENGGARA BARAT', NULL, NULL),
(24, 'NUSA TENGGARA TIMUR', NULL, NULL),
(14, 'KALIMANTAN BARAT', NULL, NULL),
(15, 'KALIMANTAN TENGAH', NULL, NULL),
(16, 'KALIMANTAN SELATAN', NULL, NULL),
(17, 'KALIMANTAN TIMUR', NULL, NULL),
(100, 'KALIMANTAN UTARA', NULL, NULL),
(18, 'SULAWESI UTARA', NULL, NULL),
(19, 'SULAWESI TENGAH', NULL, NULL),
(20, 'SULAWESI SELATAN', NULL, NULL),
(21, 'SULAWESI TENGGARA', NULL, NULL),
(31, 'GORONTALO', NULL, NULL),
(27, 'SULAWESI BARAT', NULL, NULL),
(25, 'MALUKU', NULL, NULL),
(34, 'MALUKU UTARA', NULL, NULL),
(35, 'PAPUA BARAT', NULL, NULL),
(26, 'PAPUA', NULL, NULL);