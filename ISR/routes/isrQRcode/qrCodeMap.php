<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('isrQRcode')->group(function(){

			Route::prefix('map')->group( function () {
				Route::group(['middleware' => 'roler:isrQRcode/map'], function () {
					Route::get('/','ISRQRCODE\MapController@app')->name('ISRQrCodeMap');
					Route::get('radius/{long}/{lat}','ISRQRCODE\MapController@getRadius')->name('ISRQrCodeGetRadius');
					Route::get('longlat/{id}','ISRQRCODE\MapController@getLongLat')->name('ISRQrCodeGetLongLat');
				});
				Route::group(['middleware' => 'rolec:isrQRcode/map'], function () {
					Route::post('site','ISRQRCODE\MapController@site')->name('ISRQrCodeSiteMap');
				});
				Route::group(['middleware' => 'roleu:isrQRcode/map'], function () {

				});
				Route::group(['middleware' => 'roled:isrQRcode/map'], function () {

				});	
			});

		});

	});
});

