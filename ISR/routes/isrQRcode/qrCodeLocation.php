<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('isrQRcode')->group(function(){

			Route::prefix('location')->group( function () {
				Route::group(['middleware' => 'roler:isrQRcode/location'], function () {
					Route::get('/','ISRQRCODE\LocationController@app')->name('ISRQrCodeLocation');
					Route::get('radius/{long}/{lat}','ISRQRCODE\LocationController@getRadius')->name('ISRQrCodeGetRadius');
				});
				Route::group(['middleware' => 'rolec:isrQRcode/location'], function () {
					Route::post('site','ISRQRCODE\LocationController@site')->name('ISRQrCodeSiteLocation');
				});
				Route::group(['middleware' => 'roleu:isrQRcode/location'], function () {

				});
				Route::group(['middleware' => 'roled:isrQRcode/location'], function () {

				});	
			});

		});

	});
});

