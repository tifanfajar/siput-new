<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('isrQRcode')->group(function(){

			Route::prefix('printQRcode')->group( function () {
				Route::group(['middleware' => 'roler:isrQRcode/printQRcode'], function () {
					Route::get('/','ISRQRCODE\PrintQRCodeController@app')->name('ISRPrintQRcode');
					Route::get('radius/{long}/{lat}','ISRQRCODE\PrintQRCodeController@getRadius')->name('ISRPrintQrCodeGetRadius');
					Route::get('longlat/{id}','ISRQRCODE\PrintQRCodeController@getLongLat')->name('ISRPrintQrCodeGetLongLat');
					Route::post('print','ISRQRCODE\PrintQRCodeController@print')->name('ISRPrintQrCodePrint');
				});
				Route::group(['middleware' => 'rolec:isrQRcode/printQRcode'], function () {

				});
				Route::group(['middleware' => 'roleu:isrQRcode/printQRcode'], function () {

				});
				Route::group(['middleware' => 'roled:isrQRcode/printQRcode'], function () {

				});	
			});

		});

	});
});

