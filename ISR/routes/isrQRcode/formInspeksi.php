<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('isrQRcode')->group(function(){

			Route::prefix('inspeksi')->group( function () {
				Route::group(['middleware' => 'roler:isrQRcode/inspeksi'], function () {
					Route::get('/','ISRQRCODE\InspeksiController@app')->name('ISRInspeksi');
				});
				Route::group(['middleware' => 'rolec:isrQRcode/inspeksi'], function () {

				});
				Route::group(['middleware' => 'roleu:isrQRcode/inspeksi'], function () {

				});
				Route::group(['middleware' => 'roled:isrQRcode/inspeksi'], function () {

				});	
			});

		});

	});
});

