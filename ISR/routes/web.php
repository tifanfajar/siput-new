<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::get('site/{id}','SiteController@getISRIdentification')->name('getISRIdentification');
	Route::get('site/{site}/{id}','SiteController@getDetailISRIdentification')->name('getDetailISRIdentification');
	Route::group(['middleware' => ['auth']], function () {
		Route::get('/','AppController@index');
		Route::get('getUpt', 'AppController@getUpt')->name('getUpt');
		Route::get('getWaba', 'AppController@getWaba')->name('getWaba');
		Route::get('getRoleName/{id}','AppController@getRoleName')->name('getRoleName');
		Route::get('getTypeCodeUpt/{id}','AppController@getTypeCodeUpt')->name('getTypeCodeUpt');
		Route::get('getTypeCodeWaba/{id}','AppController@getTypeCodeWaba')->name('getTypeCodeWaba');
	});
});
Route::get('ass',function(){
	phpinfo();
});
