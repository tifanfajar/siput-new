<?php

use Illuminate\Support\Facades\Route;

Auth::routes([
  'register' => false, 
  'reset' => false, 
  'verify' => false,
]);
Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('postLogin', 'Auth\LoginController@postLogin')->name('postLogin');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');


