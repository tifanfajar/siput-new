<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('info')->group(function(){

			Route::prefix('user')->group( function () {
				Route::group(['middleware' => 'roler:info/user'], function () {
					Route::get('getRoleName/{id}','AppController@getRoleName')->name('getRoleNameUser');
					Route::get('getTypeCodeUpt/{id}','AppController@getTypeCodeUpt')->name('getTypeCodeUptUser');
					Route::get('getTypeCodeWaba/{id}','AppController@getTypeCodeWaba')->name('getTypeCodeWabaUser');
					Route::get('/','INFO\UserManagementController@app')->name('user');
					Route::get('viewById/{id}','INFO\UserManagementController@viewById')->name('viewByIdUser');
				});
				Route::group(['middleware' => 'rolec:info/user'], function () {
					Route::post('create','INFO\UserManagementController@create')->name('createUser');
				});
				Route::group(['middleware' => 'roleu:info/user'], function () {
					Route::post('update','INFO\UserManagementController@update')->name('updateUser');
				});
				Route::group(['middleware' => 'roled:info/user'], function () {
					Route::get('delete/{id}','INFO\UserManagementController@delete')->name('deleteUser');
				});	
			});

		});
	});
});

