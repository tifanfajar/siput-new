<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('info')->group(function(){

			Route::prefix('privillage')->group( function () {
				Route::group(['middleware' => 'roler:info/privillage'], function () {
					Route::get('/','INFO\PrivillageController@app')->name('privillage');
					Route::get('viewById/{id}','INFO\PrivillageController@viewById')->name('viewByIdPrivillage');
					Route::get('detailById/{id}','INFO\PrivillageController@detailById')->name('detailByIdPrivillage');
				});
				Route::group(['middleware' => 'rolec:info/privillage'], function () {
					Route::post('create','INFO\PrivillageController@create')->name('createPrivillage');
				});
				Route::group(['middleware' => 'roleu:info/privillage'], function () {
					Route::post('update/{id}','INFO\PrivillageController@update')->name('updatePrivillage');
				});
				Route::group(['middleware' => 'roled:info/privillage'], function () {
					Route::get('delete/{id}','INFO\PrivillageController@delete')->name('deletePrivillage');
				});	
			});

		});

	});
});

