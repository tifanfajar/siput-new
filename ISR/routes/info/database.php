<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::prefix('info')->group(function(){

			Route::prefix('database')->group( function () {
				Route::group(['middleware' => 'roler:info/database'], function () {
					Route::get('/','INFO\BackupDatabaseController@app')->name('database');
				});
				Route::group(['middleware' => 'rolec:info/database'], function () {

				});
				Route::group(['middleware' => 'roleu:info/database'], function () {

				});
				Route::group(['middleware' => 'roled:info/database'], function () {

				});	
			});

		});

	});
});

