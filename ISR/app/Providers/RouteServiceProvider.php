<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/web.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/auth.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/images.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/isrQRcode.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/regSTbumi.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/regBTSipfr.php'));
        
        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/info.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/info/userManagement.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/info/privillage.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/info/database.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/isrQRcode/qrCodeMap.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/isrQRcode/qrCodeLocation.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/isrQRcode/printQRcode.php'));

        Route::middleware('web')
        ->namespace($this->namespace)
        ->group(base_path('routes/isrQRcode/formInspeksi.php'));

    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('v1')
        ->middleware('api')
        ->namespace($this->namespace)
        ->group(base_path('routes/api.php'));
    }
}
