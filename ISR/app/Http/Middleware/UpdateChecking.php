<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Session;
class UpdateChecking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
      if (Auth::check()) {
          $mod = \App\Model\Modules\Module::where('menu_path', $role)->first();
          if ($mod != null) {
              $role = \App\Model\Privillage\Roleacl::where('module_id', $mod->kdModule)->where('role_id', Auth::user()->role_id)->first();
              if ($role != null) {
                  if ($role->update_acl == $mod->kdModule) {
                      return $next($request);
                  } else {
                    Session::flash('info', 'Failed');
                    Session::flash('colors', 'red');
                    Session::flash('icons', 'fas fa-times');
                    Session::flash('alert', 'Access Denied!');
                    return back();
                }
            } else {
                Session::flash('info', 'Failed');
                Session::flash('colors', 'red');
                Session::flash('icons', 'fas fa-times');
                Session::flash('alert', 'Access Denied!');
                return back();
            }
        } else {
            Session::flash('info', 'Failed');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Access Denied!');
            return back();
        }
    } else {
        Session::flash('info', 'Failed');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-times');
        Session::flash('alert', 'Username / Password Salah!');
        return redirect(url('/login'));
    }
}
}
