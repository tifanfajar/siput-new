<?php

namespace App\Http\Controllers\INFO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\Model\Privillage\RoleAcl;
use App\Model\Modules\Module;
use Session;
use DB;
use Auth;

class PrivillageController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(){
		$roles = Role::all();
		$getKdModule = DB::table('modules')->where('menu_path',request()->path())->value('kdModule');
		$getEdit = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('update_acl');
		$getDelete = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('delete_acl');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		return view('layouts.dev.info.privillageUser.app',compact('roles','getKdModule','getEdit','getDelete','getCreate'));
	}

	public function create(Request $request){
		$role = new Role;
		$role->role_name = $request->role_name;
		$role->description = $request->description;
		$role->akses = $request->role_name;
		$role->save();

		$roles = Module::where('menu_parent', '!=', 0)->get();
		foreach ($roles as $module) {
			Roleacl::create([
				'module_id' => $module->kdModule,
				'role_id' => $role->id,
				'create_acl' => $request->input($module->kdModule.'_create'),
				'read_acl' => $request->input($module->kdModule.'_read'),
				'update_acl' => $request->input($module->kdModule.'_update'),
				'delete_acl' => $request->input($module->kdModule.'_delete'),
				'module_parent' =>  $module->menu_parent,
			]);
		}
		$this->flashAlert('Success','green','clipboard-check','Success Input Data!');
		return redirect()->back();
	}

	public function viewById($id){
		$role = Role::find($id);
		return view('layouts.dev.info.privillageUser.edit')->with('role',$role);
	}

	public function detailById($id){
		$role = Role::find($id);
		return view('layouts.dev.info.privillageUser.detail')->with('role',$role);
	}

	public function update($id, Request $request)
	{
		$role = Role::find($id)->update([
			'role_name' => $request->role_name,
			'description' => $request->description,
			'akses' => $request->role_name
		]);

		$roles = Module::where('menu_parent', '!=', 0)->get();
		foreach ($roles as $modules) {
			$data = [
				'module_id' => $modules->kdModule,
				'role_id' => $id,
				'create_acl' => $request->input($modules->kdModule.'_create'),
				'read_acl' => $request->input($modules->kdModule.'_read'),
				'update_acl' => $request->input($modules->kdModule.'_update'),
				'delete_acl' => $request->input($modules->kdModule.'_delete'),
				'module_parent' => $modules->menu_parent,
			];
			$cek = RoleAcl::where('module_id', $modules->kdModule)->where('role_id', $id)->first();
			if ($cek == null) {
				$rolenya = RoleAcl::create($data);
			} else {
				$rolenya = RoleAcl::find($cek->id);
				$rolenya->update($data);
			}
		}

		$this->flashAlert('Success','green','clipboard-check','Success Edit Data!');
		return redirect(route('privillage'));

	}

	public function delete($id)
	{
		$getAkses = Role::where('id',$this->decryptId($id))->value('akses');
		Role::where('id', $this->decryptId($id))->delete();
		Roleacl::where('role_id', $this->decryptId($id))->delete();
		$this->flashAlert('Success','red','clipboard-check','Success Delete Data!');
		return redirect()->back();
		
	}
}
