<?php

namespace App\Http\Controllers\INFO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\User;
use Session;
use DB;
use Auth;

class UserManagementController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(){
		$users = User::all();
		$getKdModule = DB::table('modules')->where('menu_path',request()->path())->value('kdModule');
		$getEdit = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('update_acl');
		$getDelete = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('delete_acl');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		return view('layouts.dev.info.userManagement.app',compact('users','getKdModule','getEdit','getDelete','getCreate'));
	}

	public function create(Request $request){
		$user = new User;
		$user->role_id = $request->role_id;
		$user->user_type = $request->userType;
		$user->type_code = $request->typeCode;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		if ($request->hasFile('profile_photo')) {
			$files = $request->file('profile_photo');
			$cover = $request->email."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('assets/images/avatars'), $cover);
			$user->profile_photo = $cover;	
		}
		$user->save();
		$this->flashAlert('Success','green','clipboard-check','Success Input Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$user = User::find($request->id);
		$user->role_id = $request->role_id;
		$user->user_type = $request->userType;
		$user->type_code = $request->typeCode;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		if ($request->hasFile('profile_photo')) {
			$files = $request->file('profile_photo');
			$cover = $request->email."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('assets/images/avatars'), $cover);
			$user->profile_photo = $cover;	
		}
		$user->save();
		$this->flashAlert('Success','green','clipboard-check','Success Edit Data!');
		return redirect()->back();
	}
	public function viewById($id){
		$result = User::where('id',$id)->first();
		return Response($result);
	}
	
	public function delete($id){
		User::where('id',$this->decryptId($id))->delete(); 
		$this->flashAlert('Success','red','clipboard-check','Success Delete Data!');
		return redirect()->back();
	}
}
