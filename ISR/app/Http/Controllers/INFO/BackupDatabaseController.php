<?php

namespace App\Http\Controllers\INFO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BackupDatabaseController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(){
		return view('layouts.dev.info.database.app');
	}
}
