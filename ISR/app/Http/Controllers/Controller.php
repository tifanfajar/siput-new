<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Auth;
use Session;
use DB;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function flashAlert($info = 'Title', $colors = 'green', $icons = 'clipboard-check', $alert = 'Message.')
	{
		Session::flash('info', $info);
		Session::flash('colors', $colors);
		Session::flash('icons', 'fas fa-'.$icons);
		Session::flash('alert', $alert);	
	}
	public function modules(){
		$module_parent = DB::table('role_acl')
		->select('module_parent','module_name','pathParent','menu_icon')
		->join('modules','role_acl.module_parent','=','modules.id')
		->where('role_id', Auth::user()->role_id)
		->where(function ($query){
			$query->where('create_acl','<>',0)
			->orWhere('read_acl','<>',0)
			->orWhere('update_acl','<>',0)
			->orWhere('delete_acl','<>',0);
		})
		->groupBy('module_parent')
		->groupBy('module_name')
		->groupBy('pathParent')
		->groupBy('menu_icon')
		->orderBy('module_parent','asc')
		->get();
		return $module_parent;
	}

	public function decryptId($id){
		return Crypt::decryptString($id);
	}
}
