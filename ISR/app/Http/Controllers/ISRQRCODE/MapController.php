<?php

namespace App\Http\Controllers\ISRQRCODE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use QrCode;
use PDF;
Use DB;
use Illuminate\Support\Str;
use App\Model\ISR\ISRMaster;
use App\Model\ISR\Site;

class MapController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(Request $request)
	{
		$results = new ISRMaster;
		if ($request->filled('client_id') && $request->client_id == true) {
			{
				$results = $results->select('clnt_id as id', 'clnt_id as text');
				if ($request->filled('search')) {
					$results = $results->where('clnt_id', 'like', $request->search.'%');
				}
				$data['results'] = $results->groupBy('clnt_id')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		if ($request->filled('client_name') && $request->client_name == true) {
			{
				$results = $results->select('id', 'clnt_name as text');
				if ($request->filled('search')) {
					$results = $results->where(DB::raw('LOWER(clnt_name)'), 'like', '%'.strtolower($request->search).'%');
				}
				$data['results'] = $results->distinct('clnt_name')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		if ($request->filled('site_id') && $request->site_id == true) {
			{
				$results = new Site;
				$results = $results->select('id', 'code as text');
				if ($request->filled('search')) {
					$results = $results->where(DB::raw('LOWER(code)'), 'like', '%'.strtolower($request->search).'%');
				}
				$data['results'] = $results->distinct('code')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		return view('layouts.dev.isrQRcode.qrCodeMap.app');
	}

	public function getLongLat($id)
	{
		return response()->json(Site::select('longitude','latitude')->find($id), 200);
	}

	public function getRadius(Request $request, $long, $lat){
		$circle_radius = 6371;
		$max_distance = 50 / 1000;
		$lng = $long;
		$lat = $lat;

		$additional_query = null;
		if ($request->filled('app_no')) {
			$additional_query .= " AND clnt_id = '".$request->app_no."'";
		}
		if ($request->filled('sims_no')) {
			$sims_no = ISRMaster::find($request->sims_no)->clnt_name;
			$additional_query .= " AND clnt_name = '".$sims_no."'";
		}
		if ($request->filled('qrcode_id')) {
			$additional_query .= " AND site_id = '".$request->qrcode_id."'";
		}

		$new_response = new ISRMaster;

		$response = DB::select('SELECT * FROM 
			(SELECT id, clnt_id, clnt_name, sid_long, sid_lat, city, site_id, 
			(' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(sid_lat)) * cos(radians(sid_long) - radians(' . $lng . ')) + sin(radians(' . $lat . ')) * sin(radians(sid_lat)))
			) AS distance 
			FROM i_s_r_masters) AS distances 
			WHERE distance < ' . $max_distance . ' 
			'. $additional_query .'
			ORDER BY distance 
			OFFSET 0 
			LIMIT 20;');

		return $response;

	}

	public function generateSite()
	{
		$generated = strtoupper(Str::random(2).'-'.Str::random(3).'-'.Str::random(3));
		if (Site::where('code', $generated)->exists()) {
			$generated = $this->generateSite();
		}
		return $generated;
	}

	public function site(Request $request)
	{
		if ($request->filled('site')) {
			$data['id'] = $this->generateSite();
			$payload = json_decode($request->payload);
			$longlat = json_decode($request->longlat);
			$site = new Site;
			$site->code = $data['id'];
			$site->name = $request->site;
			$site->latitude = $longlat->lat;
			$site->longitude = $longlat->lng;
			$site->save();
			foreach ($payload as $key => $value) {
				$isr_master = ISRMaster::find($value->id);
				$isr_master->site_id = $site->id;
				$isr_master->update();
			}
			$this->flashAlert('Success','green','clipboard-check','Success Input Site!');
			return redirect(url('isrQRcode/map'));
		}
		return back()->withInput();
	}
	
}
