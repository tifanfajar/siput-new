<?php

namespace App\Http\Controllers\ISRQRCODE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ISR\ISRMaster;
use App\Model\ISR\Site;
use QrCode;
use PDF;
use DB;

class PrintQRCodeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(Request $request)
	{
		$results = new ISRMaster;
		if ($request->filled('client_id') && $request->client_id == true) {
			{
				$results = $results->select('clnt_id as id', 'clnt_id as text');
				if ($request->filled('search')) {
					$results = $results->where('clnt_id', 'like', $request->search.'%');
				}
				$data['results'] = $results->groupBy('clnt_id')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		if ($request->filled('client_name') && $request->client_name == true) {
			{
				$results = $results->select('id', 'clnt_name as text');
				if ($request->filled('search')) {
					$results = $results->where(DB::raw('LOWER(clnt_name)'), 'like', '%'.strtolower($request->search).'%');
				}
				$data['results'] = $results->distinct('clnt_name')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		if ($request->filled('site_id') && $request->site_id == true) {
			{
				$results = new Site;
				$results = $results->select('id', 'code as text');
				if ($request->filled('search')) {
					$results = $results->where(DB::raw('LOWER(code)'), 'like', '%'.strtolower($request->search).'%');
				}
				$data['results'] = $results->distinct('code')->limit(5)->get();
				return response()->json($data, 200);
			}
		}
		if ($request->filled('url')) {
			$data['url'] = $request->url;
			$data['image'] = asset('sdppi-t.png');
			$data['id'] = 'xx-yyy-zzz';
			$view = 'layouts.dev.isrQRcode.qrCodeMap.pdf';
			// return view($view)->with($data);
			$pdf = PDF::loadView($view, $data);
			return $pdf->stream();
		}
		return view('layouts.dev.isrQRcode.printQRcode.app');
	}

	public function getLongLat($id)
	{
		return response()->json(Site::select('longitude','latitude')->find($id), 200);
	}

	public function getRadius(Request $request, $long, $lat){
		$circle_radius = 6371;
		$max_distance = 50 / 1000;
		$lng = $long;
		$lat = $lat;

		$additional_query = null;
		if ($request->filled('app_no')) {
			$additional_query .= " AND clnt_id = '".$request->app_no."'";
		}
		if ($request->filled('sims_no')) {
			$sims_no = ISRMaster::find($request->sims_no)->clnt_name;
			$additional_query .= " AND clnt_name = '".$sims_no."'";
		}
		if ($request->filled('qrcode_id')) {
			$qrcode_id = ISRMaster::find($request->qrcode_id)->site_id;
			$additional_query .= " AND site_id = '".$qrcode_id."'";
		}

		$new_response = new ISRMaster;

		$response = DB::select('SELECT * FROM 
			(SELECT id, clnt_id, clnt_name, sid_long, sid_lat, city, site_id, 
			(' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(sid_lat)) * cos(radians(sid_long) - radians(' . $lng . ')) + sin(radians(' . $lat . ')) * sin(radians(sid_lat)))
			) AS distance 
			FROM i_s_r_masters) AS distances 
			WHERE distance < ' . $max_distance . ' 
			'. $additional_query .'
			ORDER BY distance 
			OFFSET 0 
			LIMIT 20;');

		return $response;

	}

	public function print(Request $request)
	{
		if ($request->filled('site_code')) {
			$data['id'] = $request->site_code;
			$data['image'] = public_path('sdppi-t.png');
			$data['url'] = url('site/'. $request->site_code);
			$view = 'layouts.dev.isrQRcode.qrCodeMap.pdf';
			$pdf = PDF::loadView($view, $data);
			return $pdf->stream(Site::where('code', $request->site_code)->first()->name.'.pdf');
		}
		return back()->withInput();
	}
}
