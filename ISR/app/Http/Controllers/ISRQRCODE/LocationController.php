<?php

namespace App\Http\Controllers\ISRQRCODE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use QrCode;
use PDF;
Use DB;
use Illuminate\Support\Str;
use App\Model\ISR\ISRMaster;

class LocationController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(){
		$data['client_ids'] = ISRMaster::select('clnt_id')->groupBy('clnt_id')->pluck('clnt_id');
		$data['client_names'] = ISRMaster::select('clnt_name')->groupBy('clnt_name')->pluck('clnt_name');
		return view('layouts.dev.isrQRcode.qrCodeLocation.app')->with($data);
	}

	public function getRadius(Request $request, $long, $lat){
		$circle_radius = 6371;
		$max_distance = 50 / 1000;
		$lng = $long;
		$lat = $lat;

		$additional_query = null;
		if ($request->filled('app_no')) {
			$additional_query .= " AND clnt_id = '".$request->app_no."'";
		}
		if ($request->filled('sims_no')) {
			$sims_no = ISRMaster::find($request->sims_no)->clnt_name;
			$additional_query .= " AND clnt_name = '".$sims_no."'";
		}
		if ($request->filled('qrcode_id')) {
			$additional_query .= " AND site_id = '".$request->qrcode_id."'";
		}

		$new_response = new ISRMaster;

		$response = DB::select('SELECT * FROM 
			(SELECT id, clnt_id, clnt_name, sid_long, sid_lat, city, site_id, 
			(' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(sid_lat)) * cos(radians(sid_long) - radians(' . $lng . ')) + sin(radians(' . $lat . ')) * sin(radians(sid_lat)))
			) AS distance 
			FROM i_s_r_masters) AS distances 
			WHERE distance < ' . $max_distance . ' 
			'. $additional_query .'
			ORDER BY distance 
			OFFSET 0 
			LIMIT 20;');

		return $response;

	}

	public function generateSite()
	{
		$generated = strtoupper(Str::random(2).'-'.Str::random(3).'-'.Str::random(3));
		if (ISRMaster::where('site_id', $generated)->exists()) {
			$generated = $this->generateSite();
		}
		return $generated;
	}

	public function site(Request $request)
	{
		if ($request->filled('site')) {
			$data['id'] = $this->generateSite();
			$payload = json_decode($request->payload);
			foreach ($payload as $key => $value) {
				$isr_master = ISRMaster::find($value->id);
				$isr_master->site_id = $data['id'];
				$isr_master->update();
			}
			$data['image'] = asset('sdppi-t.png');
			$data['url'] = url('site/'. $data['id']);
			$view = 'layouts.dev.isrQRcode.qrCodeLocation.pdf';
			// return view($view)->with($data);
			$pdf = PDF::loadView($view, $data);
			return $pdf->stream();
		}
		return back()->withInput();
	}
}
