<?php

namespace App\Http\Controllers\ISRQRCODE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InspeksiController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function app(Request $request)
	{
		return view('layouts.dev.isrQRcode.formInspeksi.app');
	}
}
