<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Dev\Upt;
use \App\Model\ISR\Site;
use \App\Model\ISR\ISRMaster;
use App\Model\Privillage\Role;
use Auth;

class SiteController extends Controller
{
    public function getISRIdentification($id){
		$result = Site::where('code',$id)->get();
		if (Auth::check()) {
			return view('layouts.dev.scan.app',compact('result'));
		}else{
			return view('layouts.dev.scan.appCheck',compact('result'));
		}
	}
	public function getDetailISRIdentification($site,$id){
		$site_id = Site::where('code',$site)->value('id');
		$result = ISRMaster::where('site_id',$site_id)->where('id',$id)->first();
		return Response($result);
	}
}
