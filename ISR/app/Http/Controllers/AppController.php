<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Dev\Upt;
use \App\Model\ISR\ISRMaster;
use App\Model\Privillage\Role;
use Auth;

class AppController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('layouts.dev.beranda.app');
	}

	public function info(){
		$grafikUser = \App\User::where('user_type','user')->count();
		$grafikUserUpt = \App\User::where('user_type','upt')->count();
		$grafikUserWaba = \App\User::where('user_type','waba')->count();
		$siteConf = \App\Model\Privillage\Role::all(); 
		return view('layouts.dev.info.app',compact('grafikUser','grafikUserWaba','grafikUserUpt','siteConf'));
	}

	public function getRoleName($id){
		$result = Role::where('id',$id)->value('role_name');
		return Response($result);
	}

	public function getTypeCodeUpt($id){
		$result = Upt::where('office_id',$id)->value('office_name');
		return Response($result);
	}

	public function getTypeCodeWaba($id){
		$result = ISRMaster::where('clnt_id',$id)->value('clnt_name');
		return Response($result);
	}

	public function getUpt()
	{
		$upts = Upt::select('office_id','office_name')->distinct()->get();
		if (!$upts) {
			$html = '<option value="">Not Available</option>';
		} else {
			$html = '';
			foreach ($upts as $upt) {
				if ($upt != '') {
					$html .= '<option value="'.$upt->office_id.'">'.$upt->office_name.'</option>';
				}
				else{
					$html = '<option value="">Not Available</option>';
				}
			}
		}

		return response()->json(['html' => $html]);
	}

	public function getWaba()
	{
		$wabas = ISRMaster::select('clnt_id','clnt_name')->distinct()->get();
		if (!$wabas) {
			$html = '<option value="">Not Available</option>';
		} else {
			$html = '';
			foreach ($wabas as $waba) {
				if ($waba != '') {
					$html .= '<option value="'.$waba->clnt_id.'">'.$waba->clnt_id.'&nbsp;-&nbsp;'.$waba->clnt_name.'</option>';
				}
				else{
					$html = '<option value="">Not Avaiable</option>';
				}
			}
		}

		return response()->json(['html' => $html]);
	}

}
