<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function isr(){
        return view('layouts.dev.isrQRcode.app');
    }

    public function bumi(){
        return view('layouts.dev.regSTbumi.app');
    }

    public function ipfr(){
        return view('layouts.dev.regBTSipfr.app');
    }

    public function info(){
        $grafikUser = \App\User::where('user_type','user')->count();
        dd($grafikUser);
        return view('layouts.dev.info.app',compact('grafikUser'));
    }
}
