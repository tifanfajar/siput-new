<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBahanSosialisasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_sosialisasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
            $table->string('id_prov')->nullable();
            $table->string('kode_upt')->nullable();
            $table->string('id_materi')->nullable();
            $table->string('judul')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('author')->nullable();
            $table->string('lampiran')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('ket_reject')->nullable();
            $table->string('created_by')->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_sosialisasis');
    }
}
