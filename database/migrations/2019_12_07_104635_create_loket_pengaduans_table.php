<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoketPengaduansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loket_pengaduans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_prov')->nullable();
            $table->string('kode_upt')->nullable();
            $table->string('status')->nullable();
            $table->date('tanggal_pelayanan')->nullable();
            $table->string('nama_pengunjung')->nullable();
            $table->string('jabatan_pengunjung')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('email')->nullable();
            $table->text('keperluan')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('lampiran')->nullable();
            $table->string('active')->default('-');
            $table->string('tujuan')->nullable();
            $table->string('created_by')->nullable();
            $table->text('keterangan_reject')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loket_pengaduans');
    }
}
