<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRencanaSosialisasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rencana_sosialisasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->default(0);
            $table->string('id_prov')->nullable();
            $table->string('kode_upt')->nullable();
            $table->string('tempat')->nullable();
            $table->string('jenis_kegiatan')->nullable();
            $table->date('tanggal_pelaksanaan')->nullable();
            $table->string('tema')->nullable();
            $table->string('lampiran')->default(1);
            $table->string('narasumber')->nullable();
            $table->string('target_peserta')->nullable();
            $table->string('jumlah_peserta')->nullable();
            $table->string('anggaran')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('keterangan_reject')->nullable();
            $table->string('created_by')->nullable();
            $table->string('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rencana_sosialisasis');
    }
}
