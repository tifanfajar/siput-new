<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTbItemProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('item_program');
        Schema::create('item_program', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_template');
            $table->string('kolom');
            $table->string('tipe_kolom');
            $table->string('label');
            $table->string('target');
            $table->string('order_by');
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by')->nullable();
            $table->string('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('item_program');
    }
}
