<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUPTSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('u_p_t_s', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->bigInteger('office_id')->nullable();
           $table->string('office_name')->nullable();
           $table->string('district_id')->nullable();
           $table->string('district_name')->nullable();
           $table->string('province_code')->nullable();
           $table->string('province_name')->nullable();
           $table->bigInteger('zone')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('u_p_t_s');
    }
}
