<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update4TbTahunProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tb_tahun_program_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tahun_program');
            $table->integer('id_template_program_detail');
            $table->foreign('id_tahun_program')->references('id')->on('tb_tahun_program');
            $table->timestamps();
            $table->foreign('id_template_program_detail')->references('id')->on('template_program_detail');
            $table->string('satuan');
            $table->string('unit');
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tb_tahun_program_detail');
    }
}
