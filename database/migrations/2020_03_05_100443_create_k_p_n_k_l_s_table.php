<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKPNKLSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_p_n_k_l_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('province_code');
            $table->string('kpknl_name')->nullable();
            $table->string('kanwil')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_p_n_k_l_s');
    }
}
