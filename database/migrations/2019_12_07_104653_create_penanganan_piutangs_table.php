<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenangananPiutangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penanganan_piutangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_prov')->nullable();
            $table->string('kode_upt')->nullable(); 
            $table->string('status')->nullable(); 
            $table->string('no_client')->nullable(); //number
            $table->string('nilai_penyerahan')->nullable(); //number
            $table->string('tahun_pelimpahan')->nullable(); //date
            $table->string('nama_kpknl')->nullable(); //string
            $table->string('tahapan_pengurusan')->nullable(); //string
            $table->string('lunas')->nullable(); //number
            $table->string('angsuran')->nullable(); //number
            $table->date('tanggal')->nullable(); //date
            $table->string('psbdt')->nullable(); //string
            $table->string('tanggal_psbdt')->nullable(); //date
            $table->string('pembatalan')->nullable(); //number
            $table->string('tanggal_pembatalan')->nullable(); //data
            $table->string('sisa_piutang')->nullable(); //number
            $table->text('keterangan')->nullable(); //text
            $table->string('keterangan_reject')->nullable(); //null
            $table->string('created_by')->nullable(); //Auth::user()->id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penanganan_piutangs');
    }
}
