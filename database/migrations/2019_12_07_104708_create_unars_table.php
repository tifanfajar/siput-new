<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_unar')->nullable();
            $table->date('tanggal_ujian')->nullable();
            $table->string('lokasi_ujian')->nullable();
            $table->string('id_prov')->nullable();
            $table->string('kode_upt')->nullable();
            $table->string('tipe')->nullable();
            $table->integer('jumlah_siaga')->nullable();
            $table->integer('jumlah_penggalang')->nullable();
            $table->integer('jumlah_penegak')->nullable();
            $table->integer('lulus_siaga')->nullable();
            $table->integer('lulus_penggalang')->nullable();
            $table->integer('lulus_penegak')->nullable();
            $table->integer('tdk_lulus_siaga')->nullable();
            $table->integer('tdk_lulus_penggalang')->nullable();
            $table->integer('tdk_lulus_penegak')->nullable();
            $table->text('lampiran')->nullable();
            $table->string('full_cat')->nullable();
            $table->text('ket_reject')->nullable();
            $table->string('created_by')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unars');
    }
}
