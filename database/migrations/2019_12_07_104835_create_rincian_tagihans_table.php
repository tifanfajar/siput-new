<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRincianTagihansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_tagihans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('no_spp')->unique();
            $table->bigInteger('no_klien')->nullable();
            $table->bigInteger('no_aplikasi')->nullable();
            $table->string('no_rr')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->text('status_simf')->nullable();
            $table->text('alamat')->nullable();
            $table->string('service_name')->nullable();
            $table->string('subservice_name')->nullable();
            $table->string('payment_description')->nullable();
            $table->date('bi_money_received')->nullable();
            $table->date('bi_begin')->nullable();
            $table->date('bi_end')->nullable();
            $table->date('bi_create_date')->nullable();
            $table->date('bi_cancel')->nullable();
            $table->bigInteger('bi_type')->nullable();
            $table->string('katagori_spp')->nullable();
            $table->string('status_izin')->nullable();
            $table->string('status_remainder')->nullable();
            $table->double('tagihan')->nullable();
            $table->string('trans_h2h')->nullable();
            $table->double('terbayar')->nullable();
            $table->string('bi_manual')->nullable();
            $table->text('bi_comment')->nullable();
            $table->date('freeze_date')->nullable();
            $table->string('upt')->nullable();
            $table->string('bi_id_reminder_orig')->nullable();
            $table->string('sa_sat_name')->nullable();
            $table->string('city')->nullable();
            $table->string('district')->nullable();
            $table->string('province')->nullable();
            $table->string('status')->nullable();
            $table->string('archived_date')->nullable();
            $table->date('license_date')->nullable();
            $table->string('no_npwp')->nullable();
            $table->date('bi_pay_until')->nullable();
            $table->date('tanggal_jatuh_tempo')->nullable();

            
            $table->string('upaya_metode')->nullable();
            $table->date('tanggal_upaya')->nullable();
            $table->string('bukti_dukung')->nullable();
            $table->text('ket_operator')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('ket_reject')->nullable();
            $table->text('ket_bila_tdk_terkirim')->nullable();
            $table->string('created_by')->nullable();
            $table->integer('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_tagihans');
    }
}
