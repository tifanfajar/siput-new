<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMetadataProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metadata_program', function (Blueprint $table) {
            $table->dropColumn('id_template_program');
            $table->unsignedBigInteger('id_tb_tahun_program_detail')->after('value')->index('id_tb_tahun_program_detail')->nullable();
            $table->unsignedBigInteger('id_upt')->after('id_tb_tahun_program_detail')->nullable();

            $table->foreign('id_tb_tahun_program_detail')->references('id')->on('tb_tahun_program_detail')->onUpdate('CASCADE')->onDelete('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadata_program', function (Blueprint $table) {
            $table->dropColumn('id_upt');
            $table->dropColumn('id_tb_tahun_program_detail');
            $table->unsignedBigInteger('id_template_program')->after('id')->nullable();
        });
    }
}
