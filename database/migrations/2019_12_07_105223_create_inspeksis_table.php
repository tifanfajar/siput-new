<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInspeksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspeksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->nullable();
            $table->string('active')->nullable();
            $table->bigInteger('id_prov')->nullable();
            $table->bigInteger('kode_upt')->nullable();
            $table->date('tanggal_lapor')->nullable();
            $table->bigInteger('data_sampling')->nullable();
            $table->bigInteger('hi_sesuai_isr')->nullable();
            $table->bigInteger('hi_tidak_sesuai_isr')->nullable();
            $table->bigInteger('hi_tidak_aktif')->nullable();
            $table->bigInteger('hi_proses_isr')->nullable();
            $table->bigInteger('tl_sesuai_isr')->nullable();
            $table->bigInteger('tl_belum_isr')->nullable();
            $table->double('capaian_valid')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('lampiran')->nullable();
            $table->string('type')->nullable();
            $table->text('keterangan_reject')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspeksis');
    }
}
