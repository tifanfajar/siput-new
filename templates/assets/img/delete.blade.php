@include('templates.helpers.data')
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deid">
					<img src="http://localhost:8000/templates/assets/img/del.jpg" alt="" style="width: 100%; margin-top: -40px;">
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
				</form>
			</div>
		</div>
	</div>
</div>