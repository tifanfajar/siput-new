/*
 * action types
 */

export const SESSION_TOKEN = 'SESSION_TOKEN'
export const SESSION_ROLE = 'SESSION_ROLE'
/*
 * action creators
 */

export function sessionToken(token) {
  return { type: SESSION_TOKEN, token }
}

export function sessionRole(role){
  return { type: SESSION_ROLE, role}
}