import {
    SESSION_TOKEN,
    SESSION_ROLE
  } from '../actions/session'
  
  export function sessionToken(state=null, action){
    switch(action.type){
      case SESSION_TOKEN:
        return action.token;
      default: 
        return state
    }
  }
  
  export function sessionRole(state=[], action){
    switch(action.type){
      case SESSION_ROLE:
        return action.role;
      default: 
        return state
    }
  }
  
  export default sessionToken