import React, { Component } from 'react'
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class UserDetail extends Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }

    delete = () => {
        notify({ message: 'Berhasil Menghapus Data', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <a href="/user-detail/add" className="btn btn-primary text-white">Tambah</a>
                    </div>
                    <div className="col-md-12 my-3">
                        <table className="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Username</th>
                                <th scope="col">Born Place</th>
                                <th scope="col">Born Date</th>
                                <th scope="col">Address</th>
                                <th scope="col">Description</th>
                                <th scope="col">Profile Picture</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>kevinstategami</td>
                                    <td>karawang</td>
                                    <td>03 February 2001</td>
                                    <td>Jl Rusun Blok 2 Lt 4</td>
                                    <td>Ini Deskripsi</td>
                                    <td>Ini photo profile</td>
                                    <td>Kelvin Febrian Go</td>
                                    <td>
                                        <a href="user-detail/1" className="btn btn-success mr-3">Edit</a>
                                        <a href="#" onClick={this.delete.bind(this)} className="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserDetail