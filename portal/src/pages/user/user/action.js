import React, { Component } from 'react'
import { Form } from "devextreme-react";
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class UserAction extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: {}
        }

        this.items = [
            {
                dataField: 'fullName',
                label: {
                    text: 'Full Name',
                    alignment: "left",
                },
            },
            {
                dataField: 'email',
                label: {
                    text: 'Email',
                    alignment: "left",
                },
            },
            {
                dataField: 'noTelp',
                label: {
                    text: 'No Telp',
                    alignment: "left",
                },
            },
            {
                dataField: 'status',
                label: {
                    text: 'Status',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Status 1'
                        },
                        {
                            id: 2,
                            value: 'Status 2'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'level',
                label: {
                    text: 'Level',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Super Admin'
                        },
                        {
                            id: 2,
                            value: 'Admin'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'active',
                label: {
                    text: 'Active',
                    alignment: "left",
                },
                editorType:'dxCheckBox',
            },
            {
                dataField: 'KdModules',
                label: {
                    text: 'KD Modules',
                    alignment: "left",
                },
            },
            {
                dataField: 'Simpan',
                label:{
                    visible:false,
                },
                editorType: 'dxButtonGroup',
                editorOptions: {
                    items: [
                        {
                            text: 'Simpan',
                            onClick: (e) => {
                                notify({ message: 'Berhasil', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
                                window.location.href="/user"
                            }
                        },
                        {
                            text: 'Kembali',
                            onClick: (e) => {
                                window.location.href="/user"
                            }
                        }

                    ]
                }
            },
        ]
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header font-weight-bold">
                                User
                            </div>
                            <div className="card-body">
                                <Form
                                    colCount={1}
                                    id={'formFilter'}
                                    formData={this.state.data}
                                    items={this.items}
                                    labelLocation="left"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserAction