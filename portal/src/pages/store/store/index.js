import React, { Component } from 'react'
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class Store extends Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }

    delete = () => {
        notify({ message: 'Berhasil Menghapus Data', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <a href="/store/add" className="btn btn-primary text-white">Tambah</a>
                    </div>
                    <div className="col-md-12 my-3">
                        <table className="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Store Name</th>
                                <th scope="col">Store Desc</th>
                                <th scope="col">Store Industry</th>
                                <th scope="col">Store Telp</th>
                                <th scope="col">Store Category</th>
                                <th scope="col">Store Picture Header</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Kelvin Febrian Go</td>
                                    <td>Dalgona</td>
                                    <td>Banyak Varian Rasa</td>
                                    <td>IMS</td>
                                    <td>021432342234</td>
                                    <td>Minuman</td>
                                    <td>Ini Photo</td>
                                    <td>
                                        <a href="store/1" className="btn btn-success mr-3">Edit</a>
                                        <a href="#" onClick={this.delete.bind(this)} className="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Store