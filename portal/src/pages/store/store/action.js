import React, { Component } from 'react'
import { Form } from "devextreme-react";
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class StoreAction extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: {}
        }

        this.items = [
            {
                dataField: 'userId',
                label: {
                    text: 'User Id',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Kelvin Febrian Go'
                        },
                        {
                            id: 2,
                            value: 'Adrian Milano'
                        },
                        {
                            id: 3,
                            value: 'Dwiky Aliansyah'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'storeName',
                label: {
                    text: 'Store Name',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeDesc',
                label: {
                    text: 'Store Desc',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeIndustry',
                label: {
                    text: 'Store Industry',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Industry 1'
                        },
                        {
                            id: 2,
                            value: 'Industry 2'
                        },
                        {
                            id: 3,
                            value: 'Industry 3'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'storeTelp',
                label: {
                    text: 'Store Telp',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeCategory',
                label: {
                    text: 'Store Category',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Makanan'
                        },
                        {
                            id: 2,
                            value: 'Minuman'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'storePictureHeader',
                label: {
                    text: 'Store Picture Header',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Picture 1'
                        },
                        {
                            id: 2,
                            value: 'Picture 2'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'Simpan',
                label:{
                    visible:false,
                },
                editorType: 'dxButtonGroup',
                editorOptions: {
                    items: [
                        {
                            text: 'Simpan',
                            onClick: (e) => {
                                notify({ message: 'Berhasil', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
                                window.location.href="/store"
                            }
                        },
                        {
                            text: 'Kembali',
                            onClick: (e) => {
                                window.location.href="/store"
                            }
                        }

                    ]
                }
            },
        ]
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header font-weight-bold">
                                Store
                            </div>
                            <div className="card-body">
                                <Form
                                    colCount={1}
                                    id={'formFilter'}
                                    formData={this.state.data}
                                    items={this.items}
                                    labelLocation="left"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default StoreAction