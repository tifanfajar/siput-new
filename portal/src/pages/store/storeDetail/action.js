import React, { Component } from 'react'
import { Form } from "devextreme-react";
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class StoreDetailAction extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: {}
        }

        this.items = [
            {
                dataField: 'storeId',
                label: {
                    text: 'Store',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Dalgona'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'userId',
                label: {
                    text: 'User Id',
                    alignment: "left",
                },
                editorType: 'dxSelectBox',
                editorOptions:{
                    dataSource: [
                        {
                            id: 1,
                            value: 'Kelvin Febrian Go'
                        },
                        {
                            id: 2,
                            value: 'Adrian Milano'
                        },
                        {
                            id: 3,
                            value: 'Dwiky Aliansyah'
                        },
                    ],
                    valueExpr: 'id',
                    displayExpr: 'value'
                }
            },
            {
                dataField: 'storeAddress',
                label: {
                    text: 'Store Address',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeLong',
                label: {
                    text: 'Store Long',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeLat',
                label: {
                    text: 'Store Lat',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeWebsite',
                label: {
                    text: 'Store Website',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeInstagram',
                label: {
                    text: 'Store Instagram',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeFacebook',
                label: {
                    text: 'Store Facebook',
                    alignment: "left",
                },
            },
            {
                dataField: 'storeTag',
                label: {
                    text: 'Store Tag',
                    alignment: "left",
                },
            },
            {
                dataField: 'Simpan',
                label:{
                    visible:false,
                },
                editorType: 'dxButtonGroup',
                editorOptions: {
                    items: [
                        {
                            text: 'Simpan',
                            onClick: (e) => {
                                notify({ message: 'Berhasil', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
                                window.location.href="/store-detail"
                            }
                        },
                        {
                            text: 'Kembali',
                            onClick: (e) => {
                                window.location.href="/store-detail"
                            }
                        }

                    ]
                }
            },
        ]
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header font-weight-bold">
                                Store Detail
                            </div>
                            <div className="card-body">
                                <Form
                                    colCount={1}
                                    id={'formFilter'}
                                    formData={this.state.data}
                                    items={this.items}
                                    labelLocation="left"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default StoreDetailAction