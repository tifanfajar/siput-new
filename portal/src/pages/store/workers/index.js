import React, { Component } from 'react'
import notify from 'devextreme/ui/notify';
import { addURL } from '../../../redux/actions/urls';

class StoreWorkers extends Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        let store = this.props.store;
        store.dispatch(addURL(this.props.location.pathname))
    }

    delete = () => {
        notify({ message: 'Berhasil Menghapus Data', width: 'AUTO', shading: true, position:{at: 'center', my: 'center', of: window} }, 'success', 600);  
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <a href="/workers/add" className="btn btn-primary text-white">Tambah</a>
                    </div>
                    <div className="col-md-12 my-3">
                        <table className="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Store</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Fullname</th>
                                <th scope="col">Email</th>
                                <th scope="col">No Telp</th>
                                <th scope="col">Address</th>
                                <th scope="col">Profile Picture</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Kelvin Febrian Go</td>
                                    <td>Dalgona</td>
                                    <td>Adrian Milano</td>
                                    <td>adrianmilano@gmail.com</td>
                                    <td>08953800212926</td>
                                    <td>Jl.Cipayung SMA 64</td>
                                    <td>Ini Photo Profile</td>
                                    <td>3 February 2020</td>
                                    <td>
                                        <a href="/workers/1" className="btn btn-success mr-3">Edit</a>
                                        <a href="#" onClick={this.delete.bind(this)} className="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default StoreWorkers