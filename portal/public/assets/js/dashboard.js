$( document ).ready(function() {
    $('a').on('click',function(e){
        var parent = $(this).parent()
        if(parent.hasClass('dropdown')){
            if(parent.hasClass('open')){
                parent.removeClass('open')
                parent.find('.sidebar-nav').removeClass('collapse show')
            }else{
                parent.addClass('open')
                parent.find('.sidebar-nav').addClass('collapse show')
            }
        }        
    })
})