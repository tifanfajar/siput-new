<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;

use Maatwebsite\Excel\Concerns\Exportable;

class RTUptTahunExportNotPaid implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['BULAN', 'TOTAL RT TERBIT', 'RT TERBAYAR', '% RT TERBAYAR', 'JUMLAH RT YANG AKAN DI TL' ,'SUDAH DI-TL UPT', 'BELUM DI-TL UPT', '% RT YANG DI TL', 'JUMLAH RT','TANGGAL UPLOAD'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
