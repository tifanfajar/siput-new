<?php

namespace App\Exports;

use App\Model\Inspeksi\Inspeksi;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Auth;

class InspeksiDataExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Tahun','Bulan','Provinsi', 'Nama UPT', 'Tanggal Lapor','Data Sampling', 'HI Sesuai ISR', 'HI Tidak Sesuai ISR', 'HI Tidak Aktif', 'HI Proses ISR', 'TL Sesuai ISR',
         'TL Belum ISR', 'Capaian Valid', 'Keterangan', 'Nama Lampiran', 'Tipe', 'Pembuat'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
