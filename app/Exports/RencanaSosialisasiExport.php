<?php

namespace App\Exports;

use App\Model\SosialisasiBimtek\RencanaSosialisasi;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Auth;

class RencanaSosialisasiExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
     /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Tanggal Dibuat', 'Tahun', 'Bulan','Tanggal Pelaksanaan', 'Nama UPT', 'Jenis Kegiatan','Tempat','Tema', 'Jumlah Peserta','Kategori Peserta', 'Narasumber', 'Anggaran', 'Keterangan', 'Lampiran'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
