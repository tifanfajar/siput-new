<?php

namespace App\Exports;

use App\Model\Service\LoketPengaduan;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Auth;

class LoketPengaduanExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
     /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Provinsi', 'Nama UPT', 'Tanggal Pelayanan','Tahun','Bulan','Nama Pengunjung', 'Jabatan Pengunjung', 'Nama Perusahaan', 'No. Telepon', 'No. HP', 'Email',
         'Keperluan', 'Keterangan', 'Tujuan', 'Status'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
