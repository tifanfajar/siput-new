<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;

class UnarExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['NO','ID_UNAR', 'Tanggal Dibuat', 'Provinsi', 'Nama UPT', 'Lokasi', 'Hari/Tanggal',   'Jumlah Siaga', 'Jumlah Penggalang', 'Jumlah Penegak', 'Lulus Siaga', 'Lulus Penggalang', 'Lulus Penegak', 'Tidak Lulus Siaga', 'Tidak Lulus Penggalang', 'Tidak Lulus Penegak', 'Full Cat'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
