<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

use Auth;

class SampelMonevSosialisasiExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['Tanggal Buat', 'Rencana Sosialisasi', 'Provinsi', 'UPT', 'Jenis Kegiatan','Tempat','Tema','Target Peserta', 'Anggaran', 'Narasumber', 'Tanggal Pelaksanaan', 'Tempat', 'Tema', 'Jumlah Peserta', 'Realisasi Anggaran', 'Narasumber', 'Keterangan'];

    }

    public function startCell(): string
    {
        return 'A1';
    }
}
