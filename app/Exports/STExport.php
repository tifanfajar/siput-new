<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;

use Maatwebsite\Excel\Concerns\Exportable;
use Auth;
use Carbon\Carbon;

class STExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['Provinsi', 'Nama UPT', 'No. Tagihan', 'No. Klien','Nama Klien','Jenis ST', 'BHP (Rp)', 'Tanggal (BI Create Date)', 'Status Pembayaran','Service','Tanggal Jatuh Tempo','Upaya / Methode','Tanggal Upaya', 'Keterangan'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
