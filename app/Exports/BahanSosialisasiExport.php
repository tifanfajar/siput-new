<?php

namespace App\Exports;

use App\Model\SosialisasiBimtek\BahanSosialisasi;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Auth;

class BahanSosialisasiExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Tanggal','Tahun', 'Bulan', 'Provinsi', 'Nama UPT', 'Kategori', 'Judul', 'Deksripsi', 'Keterangan' ,'Kontributor'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
