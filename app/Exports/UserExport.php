<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;


class UserExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Nama', 'Email', 'Provinsi','Nama UPT', 'No Telp', 'No HP', 'No Fax', 'Alamat', 'Hak Akses'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
