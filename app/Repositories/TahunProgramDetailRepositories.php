<?php

namespace App\Repositories;

use App\Model\TahunProgramDetail as model;
class TahunProgramDetailRepositories
{
    private $model; 

    public function __construct(model $model)
    {
        $this->model = $model;
    }

    public function get($id) {
       $getData = $this->model->where('id_tahun_program', $id);

       return $getData->get();
    }

    public function save($data) {
        $save = new $this->model;

        $save->id_tahun_program = $data['id_tahun_program'];
        $save->id_template_program_detail = $data['id_template_program_detail'];
        $save->unit = $data['unit'];
        $save->satuan = $data['satuan'];
        $save->status = $data['status'] ? $data['status'] : 1;
        // print_r($save->save());
        $save->save();

        return $save->fresh();
    }

    public function delete($id){
       $db = new $this->model;
       $getData = $db->where('id', $id);
       // $getData->update(['deleted_at' => date("Y-m-d h:i:s")]);
       $getData->delete();
       // return $getData->first();
    }
}