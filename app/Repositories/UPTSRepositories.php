<?php
namespace App\Repositories;

use App\Model\UPTSModel;

class UPTSRepositories
{
    public function getData(){
        return UPTSModel::get();
    }
}
