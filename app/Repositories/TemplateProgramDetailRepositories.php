<?php

namespace App\Repositories;

use App\Model\TemplateProgram as model;

class TemplateProgramDetailRepositories
{
    private $model; 

    public function __construct(model $model)
    {
        $this->model = $model;
    }

    public function get(){
        return $this->model->where('deleted_at', null)->get();
    }

    public function getById($id){
        $program = $this->model->find($id);
        $item = $program->itemProgram;
        $program['item_program'] = $item;
        return $program;
    }

    public function save($data){
        $save = new $this->model;

        $save->nama = $data['nama'];
        $save->status = $data['status'] ? $data['status'] : 1;
        $save->created_by = $data['created_by'];
        $save->logo = $data['logo'];
        $save->save();

        return $save->fresh();
    }

    public function update($id, $data){
        $db = new $this->model;
        $getData = $db->where('id', $id);
        $getData->update($data);

        return $getData->first();
    }

    public function delete($id){
       $db = new $this->model;
       $getData = $db->where('id', $id);
       $getData->update(['deleted_at' => date("Y-m-d h:i:s")]);
       // $getData->delete();
       return $getData->first();
    }
}