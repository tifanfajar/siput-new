<?php

namespace App\Repositories;

use App\Model\TahunProgram as model;

class TahunProgramRepositories
{
    private $model; 

    public function __construct(model $model)
    {
        $this->model = $model;
    }

    public function get($offset, $limit){
        // print_r($limit);
        // if ($offset == 1) {
        //   $offset = 0;
        // } else {
        // $offset = ($offset - 1) * $limit;
        // }
        return $this->model->where('status', 1)->offset($offset)->take($limit)->get();
        // return $this->model->get();
    }
    public function getById($id){
        $program = $this->model->find($id);
        $item = $program->tahunProgram;
        // $program['list_program'] = $item;
        return $program;
    }
    public function getCount() {
      return $this->model->where('status', 1)->count();
    }

    public function save($data){
       $save = new $this->model;

       $save->label_tahun_program = $data['label'];
       $save->value = $data['value'];
       $save->status = $data['status'] ? $data['status'] : 1;
       $save->created_by = $data['created_by'];
       $save->save();

       return $save->fresh();
   }

   public function delete($id){
     $db = new $this->model;
     $getData = $db->where('id', $id);
     $getData->update(['status' => 2]);
     // $getData->delete();
     return $getData->first();
  }
}
