<?php

namespace App\Repositories;

use App\Model\ItemProgramModel as model;

class ItemProgramRepositories
{
    protected $model;

    public function __construct(model $model)
    {
        $this->model = $model;
    }

    public function save($data){
        $save = new $this->model;

        $save->id_template = $data['id_template'];
        $save->kolom = $data['kolom'];
        $save->tipe_kolom = $data['tipe_kolom'];
        $save->label = $data['label'];
        $save->target = $data['target'];
        $save->order_by = $data['order_by'];
        $save->created_by = $data['created_by'];
        $save->save();

        return $save->fresh();
    }

    public function update($id,$data){
        $db = new $this->model;
        $getData = $db->where('id', $id);
        $getData->update($data);

        return $getData->first();
    }

    public function get(){        
        return $this->model->get();
    }
}