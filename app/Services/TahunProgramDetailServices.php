<?php

namespace App\Services;

use App\Repositories\TahunProgramDetailRepositories as repo;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class TahunProgramDetailServices {
   private $repo;

   public function __construct(repo $repo)
   {
       $this->repo = $repo;
   }

   public function repoGetData($id) {
      return $this->repo->get($id);
   }

   public function repoSave($data) {
      return $this->repo->save($data);
   }

   public function repoDeleteById($id) {
      return $this->repo->delete($id);
   }

}