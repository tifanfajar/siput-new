<?php

namespace App\Services;

use App\Repositories\TemplateProgramDetailRepositories as repo;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Illuminate\Validation\ValidationException;

class TemplateProgramDetailServices
{
    private $repo;
    private $path = 'assets\images\logo';
    private $allowedFileExtension = [
        'jpg',
        'png',
        'jpeg'
    ];

    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }

    public function repoGetData(){
        return $this->repo->get();
    }

    public function repoSave($data){
        if($data['file']){
            $path = $this->uploadImage($data['file']);
            $data['logo'] = $path;
        }
        return $this->repo->save($data);
    }

    public function repoUpdate($id,$data){
        return $this->repo->update($id, $data);
    }

    public function repoGetDataByID($id){
        return $this->repo->getById($id);
    }

    public function repoDeleteById($id) {
       return $this->repo->delete($id);
    }

    public function uploadImage($image){
        $check = in_array($image->extension(), $this->allowedFileExtension);

        if($check){
            $filename = $image->getClientOriginalName();
            $path = $image->move(public_path($this->path), $filename);
            return $path->getPathName();
        }else{
            throw ValidationException::withMessages(['file' => 'Wrong file extension']);
        }
        
    }
}