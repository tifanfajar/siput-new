<?php

namespace App\Services;

use App\Repositories\ItemProgramRepositories as repo;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class ItemProgramServices
{
    protected $repo;

    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }

    public function saveItemProgram($data){
        return $this->repo->save($data);
    }

    public function updateItemProgram($id,$data){
        return $this->repo->update($id, $data);
    }

    public function repoGetData(){
        return $this->repo->get();
    }
}