<?php

namespace App\Services;

use App\Repositories\TahunProgramRepositories as repo;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class TahunProgramServices {
   private $repo;

   public function __construct(repo $repo)
   {
       $this->repo = $repo;
   }

   public function repoGetData($offset, $limit) {
      return $this->repo->get($offset, $limit);
   }

   public function repoGetCount() {
     return $this->repo->getCount();
   }

   public function repoGetDataByID($id){
     return $this->repo->getById($id);
   }
   public function repoSave($data){
       return $this->repo->save($data);
   }

   public function repoDeleteById($id) {
      return $this->repo->delete($id);
   }
}