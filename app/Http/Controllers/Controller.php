<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Model\SPP\StatusTagihan;
use App\Model\Privillage\Role;
use Auth;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function trimmedParameters($request)
	{
		$requestParameters = $request->all();
		if (!empty($requestParameters['_token'])) {
			unset($requestParameters['_token']);
		}

		return $requestParameters;
	}

	public function withParameters($route, $request, $is_route = false)
	{
		switch ($is_route) {
			case true:
			return redirect(route($route, $this->trimmedParameters($request)));
			break;
			case false:
			return redirect(url($route.'?'.http_build_query($this->trimmedParameters($request))));
			break;
			default:
			abort(400);
			break;
		}
	}

	private function emptyTable()
	{
		return datatables()->of((array)null)->make(true);
	}

	public function flashAlert($info = 'Title', $colors = 'green', $icons = 'clipboard-check', $alert = 'Message.')
	{
		Session::flash('info', $info);
		Session::flash('colors', $colors);
		Session::flash('icons', 'fas fa-'.$icons);
		Session::flash('alert', $alert);	
	}

	public function paidDate($month = null, $year = null)
	{
		if (is_null($month)) {
			$month = date('n');
		}
		if (is_null($year)) {
			$year = date('Y');
		}
		$data['month'] = $month - 1; 
		$data['year'] = $year;
		if ($month == 1) {
			$data['month'] = 12;
			$data['year']--;
		}
		return $data;
	}

	public function rtsPaid($array = [])
	{
		$rts_paid = new StatusTagihan;
		$rts_paid = $rts_paid->where('status', 'paid');
		$rts_paid = $rts_paid->where('status_izin', 'Perpanjangan');
		if (!empty($array['dataTerbayar'])) {
			$rts_paid = $rts_paid->whereMonth('bi_create_date', $array['dataTerbayar']);
		}
		if (!empty($array['fixYear'])) {
			$rts_paid = $rts_paid->whereYear('bi_create_date', $array['fixYear']);
		}
		if (!empty($array['jenis_st'])) {
			$rts_paid = $rts_paid->where('bi_type', $array['jenis_st']);
		}
		if (!empty($array['jenis_st_paid'])) {
			$rts_paid = $rts_paid->where('bi_type', $array['jenis_st_paid']);
		}
		if (!empty($array['getUptName'])) {
			$rts_paid = $rts_paid->where('upt', $array['getUptName']);
		}
		$rts_paid = $rts_paid->get();
	}

	public function isAdmin()
	{
		return Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator';
	}
}
