<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TahunProgramDetailServices as service;


class TahunProgramDetailController extends Controller
{
     protected $service;
 
     public function __construct(service $service)
     {
         $this->service = $service;
     }

     public function get(Request $request) {
      $idTahun = $request->query('idTahun');
     
      $result = ['status' => 200];
      try{
          $result['data'] = $this->service->repoGetData($idTahun);
      }catch(\Exception $e){
          $result = [
              'status' => 500,
              'error' => $e->getMessage()
          ];
      }

      return response()->json($result, $result['status']);
     }

     public function addNew(Request $request) {
     
      $result = ['status' => 200];
      try{
          $result['data'] = $this->service->repoSave($request);
      }catch(\Exception $e){
          $result = [
              'status' => 500,
              'error' => $e->getMessage()
          ];
      }

      return response()->json($result, $result['status']);
     }

     public function delete(Request $request) {
        $result = ['status' => 200];
        try{
         $result['data'] = $this->service->repoDeleteById($request->route('id'));
        } catch (\Execption $e) {
          $result = [
              'status' => 500,
              'error' => $e->getMessage()
          ];
        }
        return response()->json($result, $result['status']);
     }
}