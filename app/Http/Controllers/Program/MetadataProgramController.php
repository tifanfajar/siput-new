<?php

namespace App\Http\Controllers\Program;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\Model\Inspeksi\Inspeksi;
use App\Model\Service\Unar;
use App\Model\MetadataProgram;
use App\Model\ItemProgramModel;
use App\Model\TemplateProgram;
use App\Model\TahunProgramDetail;
use Auth;
use Session;

class MetadataProgramController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        $this->table = new MetadataProgram;
    }

	public function index(Request $request){
        $tahunProgramDetail = TahunProgramDetail::find($request->id);
        $template = TemplateProgram::with('itemProgram')->find($tahunProgramDetail->id_template_program_detail);

        $metadata = new MetadataProgram;
        $metadata = $metadata->where('id_tb_tahun_program_detail',$tahunProgramDetail->id)->get();

        $getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
		if($getStatus!='administrator'){
			$id_upt = Auth::user()->upt;
			$metadata = $metadata->where('id_upt',$id_upt);
		}
        
        if ($request->filled('type') && $request->type = 'WEB') {
            $metadata = datatables()->of($metadata);

            $metadata = $metadata->editColumn('id', function($data) {
				return $data['id'];
			});

            foreach($template->itemProgram as $key => $row){
                $metadata = $metadata->editColumn($row->kolom, function($data) use($key,$row) {
                    if(!empty($data[$row->kolom])){
                        return $data[$row->kolom];
                    }else{
                        return '-';
                    }
                });
            }
			

			$metadata = $metadata->make(true);
			return $metadata;
		}

		return view('program.metadata_program.core.index',compact('template','metadata','tahunProgramDetail'));
	}

    public function create(Request $request){
        $template = TemplateProgram::with('itemProgram')->find($request->id_template_program);

        $attribute = [
            'id_tb_tahun_program_detail' => $request->id_tb_tahun_program_detail,
            'id_upt' => !empty(Auth::user())?Auth::user()->upt:null,
        ];

        foreach($template->itemProgram as $row){
            $attribute[$row->kolom] = $request->get('value_'.$row->kolom);
        }

        $query = MetadataProgram::create($attribute);
        if($query){
            Session::flash('info', $template->nama);
            Session::flash('colors', 'green');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Berhasil Menyimpan Data!');       
            return redirect()->back();
        }else{
            Session::flash('info', $template->nama);
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal Menyimpan Data!');       
            return redirect()->back();
        }
    }

    public function update($id, Request $request){
        $template = TemplateProgram::with('itemProgram')->find($request->id_template_program);

        $attribute = [
            'id_tb_tahun_program_detail' => $request->id_tb_tahun_program_detail,
        ];

        foreach($template->itemProgram as $row){
            $attribute[$row->kolom] = $request->get('value_'.$row->kolom);
        }

        $query = MetadataProgram::find($id)->update($attribute);
        if($query){
            Session::flash('info', $template->nama);
            Session::flash('colors', 'green');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Berhasil Menyimpan Data!');       
            return redirect()->back();
        }else{
            Session::flash('info', $template->nama);
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal Menyimpan Data!');       
            return redirect()->back();
        }
    }

    public function delete($id){
        $query = MetadataProgram::find($id);
        if(!empty($query) && $query->delete()){
            Session::flash('info', 'Sukses');
            Session::flash('colors', 'green');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Berhasil Menghapus Data!');       
            return redirect()->back();
        }else{
            Session::flash('info', 'Gagal');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Gagal Menghapus Data!');       
            return redirect()->back();
        }
    }

}
