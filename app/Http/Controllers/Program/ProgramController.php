<?php

namespace App\Http\Controllers\Program;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\Model\Inspeksi\Inspeksi;
use Auth;
use App\Model\TemplateProgram;
use App\Model\TahunProgramDetail;
use Session;

class ProgramController extends Controller
{
	public function __construct()
    {
		$this->table = New TahunProgramDetail;
        $this->middleware('auth');
    }

	public function index(){
		$data = $this->table->with('templateProgram','tahunProgram');

		$year = date('Y');
		$data = $data->whereHas('tahunProgram',function($query) use ($year){
			$query->where('value',$year);
		});

		$getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
		if($getStatus!='administrator'){
			$id_upt = Auth::user()->upt;
			$data = $data->where('unit',$id_upt);
		}
		
		$data = $data->orderBy('id','asc')->get();
		return view('program.program',compact('data'));
	}

}
