<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Crypt;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use Session;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $loginPath = 'login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
      $this->myTime = Carbon::now();
      $this->day = $this->myTime->format('d');
      $this->month = $this->myTime->format('m');
      $this->years = '20'.$this->myTime->format('y');
    }

    public function getLogin(){
      return view('auth.login');
    }

    public function logout(){
      Auth::logout();
      return redirect(url('login'));
    }

    public function postLogin(Request $r){
      $username = $r->input('email');
      $password = $r->input('password');
      if (Auth::attempt(['email' => $username, 'password' => $password])) {
        $getAkses = Role::where('id',Auth::user()->role_id)->value('akses');
        $capitalizeAkses = ucfirst($getAkses);
        $getUptName = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->distinct()->value('office_name');
        if ($getAkses == 'administrator') {
          Session::flash('colors', 'green');
          Session::flash('icons', 'fas fa-check-circle');
          Session::flash('info', 'Selamat Datang!');
          Session::flash('alert', 'Hallo '.$capitalizeAkses);
        }
        else{
          if ($getAkses == 'kepala-upt') {
            Session::flash('colors', 'green');
            Session::flash('icons', 'fas fa-check-circle');
            Session::flash('info', 'Pejabat UPT');
            Session::flash('alert', $getUptName); 
          }
          else{
            Session::flash('colors', 'green');
            Session::flash('icons', 'fas fa-check-circle');
            Session::flash('info', $capitalizeAkses);
            Session::flash('alert', $getUptName); 
          }
        }
        return redirect(url('/'));
      }
      Session::flash('info', 'Terjadi Kesalahan');
      Session::flash('colors', 'red');
      Session::flash('icons', 'fas fa-times');
      Session::flash('alert', 'Username/Password salah');
      return redirect()->back();
    }


  }


