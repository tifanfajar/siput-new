<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\UPT;

class GetUptController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function getUpt($id,Request $request)
	{
		if (!$id) {
			$html = '<option value="">Tidak Tersedia</option>';
		} else {
			$html = '';
			$upts = UPT::where('province_code',$id)->select('office_id','office_name')->distinct()->get();
			foreach ($upts as $upt) {
				if ($upt != '') {
					$html .= '<option value="'.$upt->office_id.'">'.$upt->office_name.'</option>';
				}
				else{
					$html = '<option value="">Tidak Tersedia</option>';
				}
			}
		}

		return response()->json(['html' => $html]);
	}
}
