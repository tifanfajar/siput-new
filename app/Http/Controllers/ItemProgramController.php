<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ItemProgramServices as service;
use App\Http\Requests\ItemProgramRequest;

class ItemProgramController extends Controller
{
    protected $service;

    public function __construct(service $service)
    {
        $this->service = $service;
    }

    public function store(ItemProgramRequest $request){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->saveItemProgram($request);
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

    public function update(Request $request){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->updateItemProgram($request->route('id'), $request->toArray());
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

    public function getData(){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->repoGetData();
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }
}
