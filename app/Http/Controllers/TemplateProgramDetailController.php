<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TemplateProgramDetailServices as service;

class TemplateProgramDetailController extends Controller
{
    protected $service;

    public function __construct(service $service)
    {
        $this->service = $service;
    }

    public function getData(){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->repoGetData();
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

    public function store(Request $request){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->repoSave($request);
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

    public function getById(Request $request){
        $result = ['status' => 200];
        try{
            $result['data'] = $this->service->repoGetDataByID($request->route('id'));
        }catch(\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }
    
    public function delete(Request $request) {
       $result = ['status' => 200];
       try{
        $result['data'] = $this->service->repoDeleteById($request->route('id'));
       } catch (\Exception $e) {
         $result = [
             'status' => 500,
             'error' => $e->getMessage()
         ];
       }
       return response()->json($result, $result['status']);
    }
}
