<?php

namespace App\Http\Controllers\Core\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Model\SPP\FreezeDate;
use Auth;

class FreezeDateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$freezeDate = FreezeDate::where('id',1)->first();
    	return view('templates.SPP.freezeDate.index',compact('freezeDate'));
    }

    public function update(Request $request){
    	FreezeDate::where('id',1)->update([
    		'date_start' => $request->date_start,
    		'date_end' => $request->date_end,
    		'updated_by' => Auth::user()->id
    	]);
    	return redirect()->back();
    }
}
