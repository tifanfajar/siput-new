<?php

namespace App\Http\Controllers\Core\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Exports\STExport;
use App\Exports\SampelSTExport;
use App\Exports\RTUptBulanTahunExport;
use App\Exports\RTUptTahunExport;
use App\Exports\RTUptTahunExportNotPaid;
use App\Exports\STSeluruhUptBulanTahunJenis;
use App\Imports\RTImport;
use App\Model\Refrension\Metode;
use Carbon\Carbon;
use Auth;
use App\Model\Privillage\Role;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Barryvdh\Snappy;
use App\Model\SPP\StatusTagihan;
use App\Model\Notification\Notification;
use App\Model\Date\ListMonth;
use App\Model\Setting\UPT;
use DB;
use Validator;
use App\Model\Region\Provinsi;

class StatusTagihanController extends Controller
{
	public function __construct()
	{
		$this->myTime = Carbon::now();
		$this->day = $this->myTime->format('d');
		$this->month = $this->myTime->format('m');
		$this->years = '20'.$this->myTime->format('y');
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		$month = $this->month;
		$year = $this->years;
		if ($month == 1) {
			$dataTerbayar = 12;
			$dataBelumTerbayar = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataTerbayar = $month - 1;
			$dataBelumTerbayar = $month - 1; 
			$fixYear = $year;
		}
		$freezeTime = $this->myTime->format('d');
		$freezeMonth = $this->myTime->format('m');
		$jenis_st = 'SURAT TAGIHAN';
		$statusReminder = '';
		$checkStatusReminder = '';
		$checkStatusApproved = '';
		$rts_paid = new StatusTagihan;
		$rts_paid = $rts_paid->whereMonth('bi_create_date', $dataTerbayar);
		$rts_paid = $rts_paid->whereYear('bi_create_date', $fixYear);
		$rts_paid = $rts_paid->where('status','paid');
		$rts_paid = $rts_paid->where('status_izin','Perpanjangan');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$getUptName = 'SELURUH UPT';
			$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
			$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status','paid')->count();
			$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status','not paid')->count();
			$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status','not paid')->where('active',0)->get();
			$total = $paid_rts + $nopaid_rts;
			$sendReport = 'INDEX';
			$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('status', 'not paid')->count();
			$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('status', 'not paid')->count();

		}
		else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
			$rts_paid = $rts_paid->where('upt',$getUptName);
			$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
			$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
			$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
			$total = $paid_rts + $nopaid_rts;
			$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
			$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
			$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
			$checkStatusApproved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('status','not paid')->where('upt',$getUptName)->count();
			$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->where('status', 'not paid')->count();
			$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->where('status', 'not paid')->count();

		}
		// dd($sudah_tl_upt, $belum_tl_upt);
		$rts_paid = $rts_paid->get();		
		return view('templates.SPP.ST.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
	}

	protected function table($request, $id = null)
	{	
		$data = [];
		$paid_date = $this->paidDate($request->month, $request->year);
		$freezeTime = $this->myTime->format('d');
		$freezeMonth = $this->myTime->format('m');
		$jenis_st = 'SURAT TAGIHAN';
		$statusReminder = '';
		$checkStatusReminder = '';
		$checkStatusApproved = '';
		$rts_paid = new StatusTagihan;
		$rts_paid = $rts_paid->whereMonth('bi_create_date', $paid_date['month']);
		$rts_paid = $rts_paid->whereYear('bi_create_date', $paid_date['year']);
		$rts_paid = $rts_paid->where('status', 'paid');
		$rts_paid = $rts_paid->where('status_izin', 'Perpanjangan');
		if ($request->filled('jenis_st')) {
			switch ($request->jenis_st) {
				case 1:
				$jenis_st_paid = 9;
				break;
				case 2:
				$jenis_st_paid = 10;
				break;
				case 3:
				$jenis_st_paid = 11;
				break;
				case 16:
				$jenis_st_paid = 49;
				break;
				default:
				$jenis_st_paid = 9;
				break;
			}
			$rts_paid = $rts_paid->where('bi_type', $jenis_st_paid);	
		}
		if (!is_null($id)) {
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
				$changeUPT = $id;
				$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
				$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
				if ($changeUPT) {
					$rts_paid = $rts_paid->where('upt', $getUptName);
				}
				else {
					return $this->emptyTable();
				}
			}
			else {
				return $this->emptyTable();
			}
		}
		else {
			if (Role::where('id', Auth::user()->role_id)->value('akses') != 'administrator') {
				$getUptName = UPT::where('office_id', Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
				$rts_paid = $rts_paid->where('upt', $getUptName);
			}
			if ($request->filled('id_map')) {
				$changeID = Provinsi::where('id_map', $request->id_map)->value('id_row');
				$changeUPT = UPT::where('province_code', $changeID)->select('office_id','office_name')->distinct()->value('office_id');
				$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
				$rts_paid = $rts_paid->where('upt', $getUptName);
			}
		}
		$data = $rts_paid->get();
		return datatables()->of($data)
		->editColumn('bhp', function($data) {
			return 'Rp. '.number_format($data['tagihan'], 2);
		})
		->editColumn('bi_created_at', function($data) {
			return \Carbon\Carbon::parse($data['bi_create_date'])
			->format('d, M Y');
		})
		->editColumn('due_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_pay_until'])->format('d, M Y');
		})
		->editColumn('pay_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_money_received'])->format('d, M Y');
		})
		->editColumn('method', function($data) {
			return Metode::where('id', StatusTagihan::where('no_spp', $data['no_spp'])->value('upaya_metode'))->value('metode');
		})
		->make(true);
	}

	protected function query_table($request)
	{	
		$data = [];
		$paid_date = $this->paidDate($request->month, $request->year);
		$rts_paid = new StatusTagihan;
		$rts_paid = $rts_paid->whereMonth('bi_create_date', $paid_date['month']);
		$rts_paid = $rts_paid->whereYear('bi_create_date', $paid_date['year']);
		$rts_paid = $rts_paid->where('status', 'paid');
		$rts_paid = $rts_paid->where('status_izin', 'Perpanjangan');
		$getUptName = null;
		if ($request->filled('upt')) {
			$getUptName = $request->upt;
		}
		if (Role::where('id', Auth::user()->role_id)->value('akses') != 'administrator' && (!$request->filled('upt') || is_null($request->upt) || empty($request->upt))) {
			$getUptName = UPT::where('office_id', Auth::user()->upt)->value('office_name');
		}
		if (!is_null($getUptName)) {
			$rts_paid = $rts_paid->where('upt', $getUptName);
		}
		if ($request->filled('jenis_st')) {
			switch ($request->jenis_st) {
				case 1:
				$jenis_st_paid = 9;
				break;
				case 2:
				$jenis_st_paid = 10;
				break;
				case 3:
				$jenis_st_paid = 11;
				break;
				case 16:
				$jenis_st_paid = 49;
				break;
				default:
				$jenis_st_paid = 9;
				break;
			}
			$rts_paid = $rts_paid->where('bi_type', $jenis_st_paid);	
		}
		$data = $rts_paid->get();
		return datatables()->of($data)
		->editColumn('bhp', function($data) {
			return 'Rp. '.number_format($data['tagihan'], 2);
		})
		->editColumn('bi_created_at', function($data) {
			return \Carbon\Carbon::parse($data['bi_create_date'])
			->format('d, M Y');
		})
		->editColumn('due_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_pay_until'])->format('d, M Y');
		})
		->editColumn('pay_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_money_received'])->format('d, M Y');
		})
		->editColumn('method', function($data) {
			return Metode::where('id', StatusTagihan::where('no_spp', $data['no_spp'])->value('upaya_metode'))->value('metode');
		})
		->make(true);
	}

	public function search(Request $request){
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->get();
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.spp.rt.search.multiple',compact('getUPT'));
				}
				else{
					$month = $this->month;
					$year = $this->years;
					if ($month == 1) {
						$dataTerbayar = 12;
						$dataBelumTerbayar = 12;
						$fixYear = $year - 1;
					}
					else{
						$dataTerbayar = $month - 1;
						$dataBelumTerbayar = $month - 1; 
						$fixYear = $year;
					}
					$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->get();
					$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->get();
					$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
					$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
					$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('active',0)->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('status','not paid')->get();
					$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
					$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->select('ket_reject')->distinct()->value('ket_reject');
					$sendReport = StatusTagihan::where('upt',$getUptId)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->select('updated_at')->distinct()->value('updated_at');
					$total = $paid_rts + $nopaid_rts;
					$statusReminder = '';
					$checkStatusReminder = '';
					$checkStatusApproved = '';
					$jenis_st = 'SURAT TAGIHAN';
					$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
					$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->count();
					$freezeTime = $this->myTime->format('d');
					$freezeMonth = $this->myTime->format('m');
				}
				return view('templates.SPP.ST.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function searchMultiple($id,Request $request){
		if ($request->type == 'WEB') {
			return $this->table($request, $id);
		}
		$jenis_st = $request->jenis_st;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($changeUPT) {
				$month = $this->month;
				$year = $this->years;
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status',0)->where('status','not paid')->where('upt',$changeUPT)->get();
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->select('ket_reject')->distinct()->value('ket_reject');
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->select('updated_at')->distinct()->value('updated_at');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();
				$total = $paid_rts + $nopaid_rts;
				$statusReminder = '';
				$checkStatusReminder = '';
				$checkStatusApproved = '';
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.ST.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function search_query(Request $request){
		if ($request->filled('type') && $request->type == 'WEB') {
			return $this->query_table($request);
		}
		$month = $request->month;
		$year = $request->year;
		$jenis_st = $request->jenis_st;
		$getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			if ($request->upt) {
				$getUptName = $request->upt;
			}else{
				$getUptName = 'SELURUH UPT';
			}
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}

				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}

				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st == null) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if ($getUptName != 'SELURUH UPT' && $jenis_st && $month && $year == null) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}

				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if ($month && $jenis_st == null && $year == null && $getUptName == null) {
				$getUptName = 'SELURUH UPT';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if ($getUptName && $year == null && $month == null && $jenis_st == null) {
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if ($getUptName != 'SELURUH UPT' && $year && $jenis_st && $month == null) {
				if ($this->month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $this->month - 1;
					$year = $year;
				}
				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}
				return view('templates.SPP.ST.search.upt-bulan-tahun',compact('getUptName','year','month','getUptId','jenis_st','jenis_st_paid'));
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st && $year && $month == null){
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}
				return view('templates.SPP.ST.search.seluruh-upt-tahun',compact('getUptName','year','month','getUptId','jenis_st_paid','jenis_st'));
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st){
				return view('templates.SPP.ST.search.seluruh-upt-bulan-tahun',compact('getUptName','year','month','getUptId','jenis_st'));
			}
			if ($jenis_st && $year == null && $month == null && $getUptName == null) {
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}

				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st && $year){
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}

			if($getUptName == 'SELURUH UPT' && $year){
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt'));
			}
			if($getUptName == 'SELURUH UPT' && $month && $year){
				return view('templates.SPP.ST.table.upt-all',compact('getUptName','year','month','getUptId','jenis_st'));
			}
		}else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if($getUptName != 'SELURUH UPT' && !$month && !$year && !$jenis_st){
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			if($getUptName == 'SELURUH UPT' && $month && !$year && !$jenis_st){
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			if($getUptName != 'SELURUH UPT' && $month && !$year && !$jenis_st){
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			if ($jenis_st && $year && $month == null) {
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}

				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}
				
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}
			if($jenis_st && $year && $month){
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}

				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}

				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));

			}
			if($jenis_st && $month){
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}

				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}

				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));

			}	
			if ($month && $year) {
				$jenis_st = 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->where('status', 'not paid')->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}

			if ($year) {
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}
			if($month){
				$jenis_st = 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}
			if ($jenis_st) {
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				if ($jenis_st == 1) {
					$jenis_st_paid = 9;
				}elseif($jenis_st == 2){
					$jenis_st_paid = 10;
				}elseif ($jenis_st == 3) {
					$jenis_st_paid = 11;
				}elseif ($jenis_st == 16) {
					$jenis_st_paid = 49;
				}
				$rts_paid = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->get();
				$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', '!=' , 'paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->get();
				$statusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->count();
				$checkStatusReminder = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
				$checkStatusApproved = StatusTagihan::where('status',0)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$getUptName)->count();
				$sendReport = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				$catatanPetugas = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = StatusTagihan::where('upt',$getUptName)->whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('ket_reject')->distinct()->value('ket_reject');
				$sudah_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->where('upt',$getUptName)->count();
				$belum_tl_upt = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',null)->where('upt',$getUptName)->count();

				return view('templates.SPP.ST.table.upt-all',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport','jenis_st','sudah_tl_upt','belum_tl_upt', 'jenis_st'));
			}
			return view('templates.SPP.ST.table.upt-all',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','jenis_st'));
		}
	}

	public function postORupdate(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$data = $request->all();
		$no_spp = $data['no_spp'];
		$upayametode = $data['upayametode'];
		$tanggal_upaya = $data['tanggal_upaya'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			$st = StatusTagihan::where('no_spp',$value)->first();
			$st->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
			$st->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
			$st->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
			$st->ket_operator = $request->ket_operator;
			$st->created_by = Auth::user()->id;
			$uniq = uniqid();	
			$filename[$key]->move(public_path('lampiran/spp/st'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
			$st->bukti_dukung = 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension();
			$st->active = 0;
			$st->save();
		}

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 8;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengupload Data Status Tagihan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Status Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect()->back();
	}

	public function preview(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		$status_pembayaran = $request->status_pembayaran == 'Telah Dibayar' ? 'paid' : 'not paid';
		if ($month == 1) {
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataBulan = $month - 1;
			$fixYear = $year;
		}
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$nama_export = 'ST-'.date('d F Y').'.pdf';
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
		}else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$nama_export = 'ST-'.$getUptName.'-'.date('d F Y').'.pdf';
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->where('upt', $getUptName)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status',$status_pembayaran)->get();
		}
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
		return $pdf->stream($nama_export);
	}

	public function preview_search(Request $request)
	{	
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$myTime = Carbon::now();
		if($request->status_pembayaran === 'Telah Dibayar'){
			$status_pembayaran = 'paid';
		}else{
			$status_pembayaran = 'not paid';
		}
		if($request->jenis_st){
			$jenis_st = $request->jenis_st;
		}else{
			$jenis_st = 'SELURUH ST';
		}
		$bi_type = $request->bi_type;
		$month = $request->month;
		$year = $request->year;

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			if ($request->upt) {
				$getUptName = $request->upt;
			}else{
				$getUptName = 'SELURUH UPT';
			}
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $jenis_st && $month && $year == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($month && $jenis_st == 'SELURUH ST' && $year == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-'.date('d F Y');
				$getUptName = 'SELURUH UPT';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $year == null && $month == -1 && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-'.date('d F Y');
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			// if ($getUptName != 'SELURUH UPT' && $year && $jenis_st == 'SELURUH ST' && $month != -1) {
			// 	$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
			// 	if ($this->month == 1) {
			// 		$dataBulan = 12;
			// 		$fixYear = $this->years - 1;
			// 	}
			// 	else{
			// 		$dataBulan = $this->month - 1;
			// 		$fixYear = $this->years;
			// 	}
			// 	$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			// }

			if ($getUptName != 'SELURUH UPT' && $year && $jenis_st != 'SELURUH ST' && $month == -1) {
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-upt-tahun-paid', compact('getUptName','year','month','jenis_st', 'bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','jenis_st', 'bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->stream($nama_export.'.pdf');
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year && $month == -1 ){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-tahun-paid', compact('getUptName','year','month','jenis_st','bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-tahun-not-paid', compact('getUptName','year','month','jenis_st','bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->stream($nama_export.'.pdf');
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y');
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-bulan-tahun-paid', compact('getUptName','year','month','jenis_st' , 'bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-bulan-tahun-not-paid', compact('getUptName','year','month','jenis_st' , 'bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->stream($nama_export.'.pdf');
			}
			if ($jenis_st && $year == null && $month == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-'.date('d F Y');
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1; 
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year){
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-TAHUN-'.date('d F Y');
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}

			if($getUptName == 'SELURUH UPT' && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-'.date('d F Y');
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y');
// $jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();

// return $rts_data;
			}

			$rt['rt'] = $rts_data;
			$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
			return $pdf->stream($nama_export.".pdf");

		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($jenis_st != 'SELURUH ST') {
				$nama_export = "ST-UPT-JENIS-ST-". $getUptName .'-'.date('d F Y');
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($jenis_st != 'SELURUH ST' && $year) {
				$nama_export = "ST-UPT-JENIS-ST-TAHUN-". $getUptName;
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($jenis_st != 'SELURUH ST' && $year && $month){
				$nama_export = "ST-UPT-JENIS-ST-BULAN-TAHUN-". $getUptName .'-'.date('d F Y');
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();

			}	
			if ($month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-BULAN-TAHUN-". $getUptName .'-'.date('d F Y');
				// $jenis_st == 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}		
			if ($year && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-TAHUN-". $getUptName .'-'.date('d F Y');
				// $jenis_st == 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($month && $jenis_st == 'SELURUH ST'){
				$nama_export = "ST-UPT-BULAN-". $getUptName .'-'.date('d F Y');
				// $jenis_st == 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			$rt['rt'] = $rts_data;
			$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
			return $pdf->stream($nama_export.".pdf");
		}
	}

	public function print(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		$status_pembayaran = $request->status_pembayaran == 'Telah Dibayar' ? 'paid' : 'not paid';
		if ($month == 1) {
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataBulan = $month - 1;
			$fixYear = $year;
		}
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$nama_export = 'ST-'.date('d F Y').'.pdf';
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
		}else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$nama_export = 'ST-'.$getUptName.'-'.date('d F Y').'.pdf';
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->where('upt', $getUptName)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status',$status_pembayaran)->get();
		}	
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
		return $pdf->download($nama_export);
	}

	public function print_search(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$myTime = Carbon::now();
		$status_pembayaran = $request->status_pembayaran == 'Telah Dibayar' ? 'paid' : 'not paid';
		if($request->jenis_st){
			$jenis_st = $request->jenis_st;
		}else{
			$jenis_st = 'SELURUH ST';
		}	
		$bi_type = $request->bi_type;		
		$month = $request->month;
		$year = $request->year;

		if ($request->upt) {
			$getUptName = $request->upt;
		}else{
			$getUptName = 'SELURUH UPT';
		}

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			if ($request->upt) {
				$getUptName = $request->upt;
			}else{
				$getUptName = 'SELURUH UPT';
			}
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $jenis_st && $month && $year == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($month && $jenis_st == 'SELURUH ST' && $year == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-'.date('d F Y');
				$getUptName = 'SELURUH UPT';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $year == null && $month == -1 && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-'.date('d F Y');
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			// if ($getUptName != 'SELURUH UPT' && $year && $jenis_st == 'SELURUH ST' && $month != -1) {
			// 	$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
			// 	if ($this->month == 1) {
			// 		$dataBulan = 12;
			// 		$fixYear = $this->years - 1;
			// 	}
			// 	else{
			// 		$dataBulan = $this->month - 1;
			// 		$fixYear = $this->years;
			// 	}
			// 	$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			// }

			if ($getUptName != 'SELURUH UPT' && $year && $jenis_st != 'SELURUH ST' && $month == -1) {
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-upt-tahun-paid', compact('getUptName','year','month','jenis_st', 'bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','jenis_st', 'bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->download($nama_export.'.pdf');
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year && $month == -1 ){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y');
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-tahun-paid', compact('getUptName','year','month','jenis_st','bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-tahun-not-paid', compact('getUptName','year','month','jenis_st','bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->download($nama_export.'.pdf');
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y');
				if($status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-bulan-tahun-paid', compact('getUptName','year','month','jenis_st' , 'bi_type'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.ST.dev.print-seluruh-upt-bulan-tahun-not-paid', compact('getUptName','year','month','jenis_st' , 'bi_type'))->setPaper('a3','landscape');					
				}
				return $pdf->download($nama_export.'.pdf');
			}
			if ($jenis_st && $year == null && $month == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-'.date('d F Y');
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1; 
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year){
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-TAHUN-'.date('d F Y');
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}

			if($getUptName == 'SELURUH UPT' && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-'.date('d F Y');
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y');
// $jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();

// return $rts_data;
			}

			$rt['rt'] = $rts_data;
			$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
			return $pdf->download($nama_export.".pdf");


			$getUptName = $request->upt;
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y').'.pdf';
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->whereNull('bi_type', '!=', 'Pokok')->get();

				$rt['rt'] = $rts_data; 
				$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
				return $pdf->download($getUptName.'_ST.pdf');				

			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				$nama_export = 'ST-'.$getUptName.'-TAHUN-'.date('d F Y').'.pdf';
				$pdf = PDF::loadView('templates.SPP.ST.table.upt-tahun', compact('getUptName','year','month','getUptId','jenis_st'))->setPaper('a3','landscape');
//$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
				return $pdf->download('ST.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				$nama_export = 'ST-SELURUH-UPT-BULAN-TAHUN-'.date('d F Y').'.pdf';
				$rts_data = StatusTagihan::whereYear('bi_create_date',$year)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->orWhereNull('bi_type', '!=', 'Pokok')->get();

				$rt['rt'] = $rts_data; 
				$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
				return $pdf->download($nama_export);
			}	

			$rt['rt'] = $rts_data; 
			$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
			return $pdf->download($nama_export);

		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($jenis_st != 'SELURUH ST') {
				$nama_export = "ST-UPT-JENIS-ST-". $getUptName .'-'.date('d F Y');
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($jenis_st != 'SELURUH ST' && $year) {
				$nama_export = "ST-UPT-JENIS-ST-TAHUN-". $getUptName;
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($jenis_st != 'SELURUH ST' && $year && $month){
				$nama_export = "ST-UPT-JENIS-ST-BULAN-TAHUN-". $getUptName .'-'.date('d F Y');
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();

			}	
			if ($month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-BLAN-TAHUN-". $getUptName .'-'.date('d F Y');
// $jenis_st == 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}		
			if ($year && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-TAHUN-". $getUptName .'-'.date('d F Y');
// $jenis_st == 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($month && $jenis_st == 'SELURUH ST'){
				$nama_export = "ST-UPT-BULAN-". $getUptName .'-'.date('d F Y');
// $jenis_st == 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			$rt['rt'] = $rts_data; 
			$pdf = PDF::loadView('templates.SPP.ST.dev.print', $rt)->setPaper('a3','landscape');		
			return $pdf->download($nama_export.".pdf");
		}
	}
	public function ORUpdate(Request $request){
		$id = Crypt::decryptString($request->id);
		$rt = StatusTagihan::find($id);		
		$rt->keterangan = $request->keterangan;
		if ($request->hasFile('bukti_dukung')) {
			$files = $request->file('bukti_dukung');
			$cover = 'Bukti Dukung-'.uniqid().$id."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/spp/st'), $cover);
			$rt->bukti_dukung = $cover;	
		}
		$rt->upaya_metode = $request->upaya_metode;
		$rt->tanggal_upaya = $request->tanggal_upaya;
		$rt->created_by = Auth::user()->id;
		$rt->active = 0;
		$rt->save();
		Session::flash('info', 'Status Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}


	public function export(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];				
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		$status_pembayaran = $request->status_pembayaran == 'Telah Dibayar' ? 'paid' : 'not paid';
		if ($month == 1) {			
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBulan = $month - 1; 
			$fixYear = $year;
		}

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$nama_export = 'ST-'. date('d F Y') .'.xlsx';
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('status',$status_pembayaran)->get();

// return $rts_data;
		}else{
			$nama_export = 'ST-'. date('d F Y') .'.xlsx';
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('status',$status_pembayaran)->get();
// return $rts_data;
		}

// $tes = json_decode($rts_data);		
		$export = new STExport([          
			$push,
		]);
		foreach ($rts_data as $key => $value) {
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
				$provinsi = $value->province;
				$nama_upt = $value->upt;

// $provinsi = Provinsi::where('id', $id_prov)->value('nama');
// $nama_upt = UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}

			$tl = StatusTagihan::where('no_spp', $value->no_spp)->first();

			if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
				$metode = Metode::where('id', $value->upaya_metode)->value('metode');
				$tanggal_upaya = $value->tanggal_upaya;
				$keterangan = $value->keterangan;
			}else{
				$metode = '';
				$tanggal_upaya = '';
				$keterangan = '';
			}

			// if($request->status_pembayaran == 'paid'){
			// 	$status_pembayaran = 'Telah Bayar';
			// }else{
			// 	$status_pembayaran = 'Belum Bayar';
			// }

			if($value->bi_type == 1){
				$jenisST = "ST 1";
			}elseif($value->bi_type == 9){
				$jenisST = "ST 1 Paid";
			}elseif($value->bi_type == 2){
				$jenisST = "ST 2";
			}elseif($value->bi_type == 10){
				$jenisST = "ST 2 Paid";
			}elseif($value->bi_type == 3){
				$jenisST = "ST 3";
			}elseif($value->bi_type == 11){
				$jenisST = "ST 3 Paid";
			}elseif($value->bi_type == 16){
				$jenisST = "STT";
			}elseif($value->bi_type == 49){
				$jenisST = "STT Paid";
			}
			$tanggal_jatuh_tempo = date("d/m/Y", strtotime($value->bi_pay_until));
			array_push($push, array(
				$provinsi,
				$nama_upt,
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$jenisST,
				$value->tagihan,
				$value->bi_create_date,
				$status_pembayaran,
				$value->service_name,
				$tanggal_jatuh_tempo,
				$metode,
				$tanggal_upaya,
				$keterangan,
			));

			$export = new STExport([          
				$push,
			]);
		}

		return Excel::download($export, $nama_export);		
	}

	public function export_search(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];		
		$status_pembayaran = $request->status_pembayaran == 'Telah Dibayar' ? 'paid' : 'not paid';		
		if($request->jenis_st){
			$jenis_st = $request->jenis_st;
		}else{
			$jenis_st = 'SELURUH ST';
		}
		$bi_type = $request->bi_type;		
		$month = $request->month;
		$year = $request->year;
		if ($request->upt) {
			$getUptName = $request->upt;
		}else{
			$getUptName = 'SELURUH UPT';
		}
		$myTime = Carbon::now();	
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
			if ($request->upt) {
				$getUptName = $request->upt;
			}else{
				$getUptName = 'SELURUH UPT';
			}
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y').'.xlsx';
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($getUptName != 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y').'.xlsx';
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $month && $year == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-JENIS-ST-'.date('d F Y').'.xlsx';
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($month && $jenis_st == 'SELURUH ST' && $year == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-BULAN-'.date('d F Y').'.xlsx';
				$getUptName = 'SELURUH UPT';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName && $year == null && $month == -1 && $jenis_st == 'SELURUH ST') {
				$nama_export = 'ST-'.$getUptName.'-'.date('d F Y').'.xlsx';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if ($getUptName != 'SELURUH UPT' && $year && $jenis_st != 'SELURUH ST' && $month == -1) {
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y').'.xlsx';
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				return $this->exportPerUptYear($year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export);
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year && $month == -1 ){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-JENIS-ST-'.date('d F Y').'.xlsx';
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				return $this->exportPerYear($year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export);
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st != 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-JENIS-ST-'.date('d F Y').'.xlsx';
				return $this->exportPerMonthYear($month,$year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export);
			}
			if ($jenis_st != 'SELURUH ST' && $year == null && $month == null && $getUptName == null) {
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-'.date('d F Y').'.xlsx';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $this->years - 1; 
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($getUptName == 'SELURUH UPT' && $jenis_st != 'SELURUH ST' && $year){
				$nama_export = 'ST-'.$getUptName.'-JENIS-ST-TAHUN-'.date('d F Y').'.xlsx';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}

			if($getUptName == 'SELURUH UPT' && $year && $jenis_st != 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-TAHUN-'.date('d F Y').'.xlsx';
				// $jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($getUptName == 'SELURUH UPT' && $month && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y').'.xlsx';
				$jenis_st = 'SELURUH ST';
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBulan)->whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($getUptName == 'SELURUH UPT' && $year && $jenis_st == 'SELURUH ST'){
				$nama_export = 'ST-'.$getUptName.'-BULAN-TAHUN-'.date('d F Y').'.xlsx';
				$jenis_st = 'SELURUH ST';
				$fixYear = $year;
				$rts_data = StatusTagihan::whereYear('bi_create_date',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($jenis_st != 'SELURUH ST') {
				$nama_export = "ST-UPT-JENIS-ST-". $getUptName . ".xlsx";
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if ($jenis_st != 'SELURUH ST' && $year) {
				$nama_export = "ST-UPT-JENIS-ST-TAHUN-". $getUptName . ".xlsx";
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
			if($jenis_st != 'SELURUH ST' && $year && $month){
				$nama_export = "ST-UPT-JENIS-ST-BULAN-TAHUN-". $getUptName . ".xlsx";
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();

			}	
			if ($month && $year && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-BLAN-TAHUN-". $getUptName . ".xlsx";
				$jenis_st == 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}		
			if ($year  && $jenis_st == 'SELURUH ST') {
				$nama_export = "ST-UPT-TAHUN-". $getUptName . ".xlsx";
				$jenis_st == 'SELURUH ST';
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
			if($month && $jenis_st == 'SELURUH ST'){
				$nama_export = "ST-UPT-BULAN-". $getUptName . ".xlsx";
				$jenis_st == 'SELURUH ST';
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_data = StatusTagihan::whereMonth('bi_create_date',$dataTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->get();
			}
		}

// if(!empty($rts_data) || isset($rts_data[0]) || $rts_data == '' || $rts_data == null){
		$export = new STExport([          
			$push,
		]);
		foreach ($rts_data as $key => $value) {
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
				$getUptId = UPT::where('office_name',$getUptName)->select('province_code')->distinct()->value('province_code');				
				if ($getUptId == '' || $getUptId == null){
					$nama_upt = $value->upt;		
					$provinsi = $value->province;		
				}else{
					$nama_upt = $request->upt;		
					$provinsi = Provinsi::where('id', $getUptId)->value('nama');				
				}
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');				
			}

			$tl = StatusTagihan::where('no_spp', $value->no_spp)->first();

			if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
				$metode = Metode::where('id', $value->upaya_metode)->value('metode');
				$tanggal_upaya = $value->tanggal_upaya;
				$keterangan = $value->keterangan;
			}else{
				$metode = '';
				$tanggal_upaya = '';
				$keterangan = '';
			}

			if($request->status_pembayaran == 'paid'){
				$status_pembayaran = 'Telah Bayar';
			}else{
				$status_pembayaran = 'Belum Bayar';
			}

			if($value->bi_type == 1){
				$jenisST = "ST 1";
			}elseif($value->bi_type == 9){
				$jenisST = "ST 1 Paid";
			}elseif($value->bi_type == 2){
				$jenisST = "ST 2";
			}elseif($value->bi_type == 10){
				$jenisST = "ST 2 Paid";
			}elseif($value->bi_type == 3){
				$jenisST = "ST 3";
			}elseif($value->bi_type == 11){
				$jenisST = "ST 3 Paid";
			}elseif($value->bi_type == 16){
				$jenisST = "STT";
			}elseif($value->bi_type == 49){
				$jenisST = "STT Paid";
			} 

			$tanggal_jatuh_tempo = date("d/m/Y", strtotime($value->bi_pay_until));

			array_push($push, array(
				$provinsi,
				$nama_upt,
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$jenisST,
				$value->tagihan,
				$value->bi_create_date,
				$status_pembayaran,
				$value->service_name,
				$tanggal_jatuh_tempo,
				$metode,
				$tanggal_upaya,
				$keterangan,
			));

			$export = new STExport([          
				$push,
			]);

		}

		return Excel::download($export, $nama_export);

	}



	public function exportPerUptYear($year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export)
	{
		if($getUptName == 'SELURUH UPT'){
			$upt = UPT::select('office_name', 'office_id')->distinct()->get();
		}else{
			$upt = UPT::where('office_name', $getUptName)->select('office_name', 'office_id')->distinct()->get();
		}
		$push = [];
		$fixYear = $year - 1; 
		$list_month = ListMonth::all();
// return $status_pembayaran;


		$export = new STExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			foreach ($upt as $keyUpt => $getUpt) {
				$list_month = ListMonth::all();

				array_push($push,array( 
					$getUpt->office_name,
				));

				foreach ($list_month as $key => $value) {
					if($value->id_bulan == 1){
						$fixMonth = 12;
						$fixYear = $year - 1;
					}else{
						$fixMonth = $value->id_bulan - 1;
						$fixYear = $year;
					}
					$count = '';
					$checkingPengiriman = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$getUpt->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();

					if($checkingPengiriman == 0){
						$count = 'BELUM';
					}else{
						$count = 'YA';
					}
					array_push($push[$keyUpt],
						$count
					);
				}


			}
			$export = new RTUptTahunExport([          
				$push,
			]);
		}else{
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
//Januari
				$paid_rts_januari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_januari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
				$sudah_tl_januari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
				if ($total_rts_januari != 0) {
					$percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
					$percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
				}else{
					$percent_belum_tl_januari = 0;
					$percent_sudah_tl_januari = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_januari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_januari = 0;
				}
				if ($jumlah_rt_januari != 0) {
					$percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
				}else{
					$percent_jumlah_rt_januari = 0;
				}
				$tanggal_upload_januari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');

//Februari
				$paid_rts_februari = StatusTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_februari = StatusTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
				$sudah_tl_februari = StatusTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
				if ($total_rts_februari != 0) {
					$percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
					$percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
				}else{
					$percent_belum_tl_februari = 0;
					$percent_sudah_tl_februari = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_februari = StatusTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_februari = 0;
				}
				if ($jumlah_rt_februari != 0) {
					$percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
				}else{
					$percent_jumlah_rt_februari = 0;
				}
				$tanggal_upload_februari = StatusTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

//Maret
				$paid_rts_maret = StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_maret = StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
				$sudah_tl_maret = StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
				if ($total_rts_maret != 0) {
					$percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
					$percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
				}else{
					$percent_belum_tl_maret = 0;
					$percent_sudah_tl_maret = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_maret = StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_maret = 0;
				}
				if ($jumlah_rt_maret != 0) {
					$percent_jumlah_rt_maret = $jumlah_rt_maret / $total_rts_maret * 100;
				}else{
					$percent_jumlah_rt_maret = 0;
				}
				$tanggal_upload_maret = StatusTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//April
				$paid_rts_april = StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_april = StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_april = $paid_rts_april + $nopaid_rts_april;
				$sudah_tl_april = StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_april = $total_rts_april - $sudah_tl_april;
				if ($total_rts_april != 0) {
					$percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
					$percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
				}else{
					$percent_belum_tl_april = 0;
					$percent_sudah_tl_april = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_april = StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_april = 0;
				}
				if ($jumlah_rt_april != 0) {
					$percent_jumlah_rt_april = $jumlah_rt_april / $total_rts_april * 100;
				}else{
					$percent_jumlah_rt_april = 0;
				}
				$tanggal_upload_april = StatusTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Mei

				$paid_rts_mei = StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_mei = StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
				$sudah_tl_mei = StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
				if ($total_rts_mei != 0) {
					$percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
					$percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
				}else{
					$percent_belum_tl_mei = 0;
					$percent_sudah_tl_mei = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_mei = StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_mei = 0;
				}
				if ($jumlah_rt_mei != 0) {
					$percent_jumlah_rt_mei = $jumlah_rt_mei / $total_rts_mei * 100;
				}else{
					$percent_jumlah_rt_mei = 0;
				}
				$tanggal_upload_mei = StatusTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Juni
				$paid_rts_juni = StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juni = StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
				$sudah_tl_juni = StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
				if ($total_rts_juni != 0) {
					$percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
					$percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
				}else{
					$percent_belum_tl_juni = 0;
					$percent_sudah_tl_juni = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_juni = StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_juni = 0;
				}
				if ($jumlah_rt_juni != 0) {
					$percent_jumlah_rt_juni = $jumlah_rt_juni / $total_rts_juni * 100;
				}else{
					$percent_jumlah_rt_juni = 0;
				}
				$tanggal_upload_juni = StatusTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Juli
				$paid_rts_juli = StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juli = StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
				$sudah_tl_juli = StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status',1)->count();
				$belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
				if ($total_rts_juli != 0) {
					$percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
					$percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
				}else{
					$percent_belum_tl_juli = 0;
					$percent_sudah_tl_juli = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_juli = StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_juli = 0;
				}
				if ($jumlah_rt_juli != 0) {
					$percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
				}else{
					$percent_jumlah_rt_juli = 0;
				}
				$tanggal_upload_juli = StatusTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Agustus
				$paid_rts_agustus = StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_agustus = StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
				$sudah_tl_agustus = StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
				if ($total_rts_agustus != 0) {
					$percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
					$percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
				}else{
					$percent_belum_tl_agustus = 0;
					$percent_sudah_tl_agustus = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_agustus = StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_agustus = 0;
				}
				if ($jumlah_rt_agustus != 0) {
					$percent_jumlah_rt_agustus = $jumlah_rt_agustus / $total_rts_agustus * 100;
				}else{
					$percent_jumlah_rt_agustus = 0;
				}
				$tanggal_upload_agustus = StatusTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//September
				$paid_rts_september = StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_september = StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_september = $paid_rts_september + $nopaid_rts_september;
				$sudah_tl_september = StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_september = $total_rts_september - $sudah_tl_september;
				if ($total_rts_september != 0) {
					$percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
					$percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
				}else{
					$percent_belum_tl_september = 0;
					$percent_sudah_tl_september = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_september = StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_september = 0;
				}
				if ($jumlah_rt_september != 0) {
					$percent_jumlah_rt_september = $jumlah_rt_september / $total_rts_september * 100;
				}else{
					$percent_jumlah_rt_september = 0;
				}
				$tanggal_upload_september = StatusTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Oktober
				$paid_rts_oktober = StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_oktober = StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
				$sudah_tl_oktober = StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
				if ($total_rts_oktober != 0) {
					$percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
					$percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
				}else{
					$percent_belum_tl_oktober = 0;
					$percent_sudah_tl_oktober = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_oktober = StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_oktober = 0;
				}
				if ($jumlah_rt_oktober != 0) {
					$percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
				}else{
					$percent_jumlah_rt_oktober = 0;
				}
				$tanggal_upload_oktober = StatusTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//November
				$paid_rts_november = StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_november = StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_november = $paid_rts_november + $nopaid_rts_november;
				$sudah_tl_november = StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_november = $total_rts_november - $sudah_tl_november;
				if ($total_rts_november != 0) {
					$percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
					$percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
				}else{
					$percent_belum_tl_november = 0;
					$percent_sudah_tl_november = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_november = StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_november = 0;
				}
				if ($jumlah_rt_november != 0) {
					$percent_jumlah_rt_november = $jumlah_rt_november / $total_rts_november * 100;
				}else{
					$percent_jumlah_rt_november = 0;
				}
				$tanggal_upload_november = StatusTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Desember
				$paid_rts_desember = StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_desember = StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
				$sudah_tl_desember = StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
				if ($total_rts_desember != 0) {
					$percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
					$percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
				}else{
					$percent_belum_tl_desember = 0;
					$percent_sudah_tl_desember = 0;
				}
				if (StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count()) {
					$jumlah_rt_desember = StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				}else{
					$jumlah_rt_desember = 0;
				}
				if ($jumlah_rt_desember != 0) {
					$percent_jumlah_rt_desember = $jumlah_rt_desember / $total_rts_desember * 100;
				}else{
					$percent_jumlah_rt_desember = 0;
				}
				$tanggal_upload_desember = StatusTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');



			}else{
//Januari
				$paid_rts_januari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_januari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
				$sudah_tl_januari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
				$percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
				$percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
				$jumlah_rt_januari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_januari != 0) {
					$percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
				}else{
					$percent_jumlah_rt_januari = 0;
				}
				$tanggal_upload_januari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');

//Februari
				$paid_rts_februari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_februari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
				$sudah_tl_februari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
				$percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
				$percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
				$jumlah_rt_februari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_februari != 0) {
					$percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
				}else{
					$percent_jumlah_rt_februari = 0;
				}
				$tanggal_upload_februari = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

//Maret
				$paid_rts_maret = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_maret = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
				$sudah_tl_maret = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
				$percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
				$percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
				$jumlah_rt_maret = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_maret != 0) {
					$percent_jumlah_rt_maret =  $jumlah_rt_maret / $total_rts_maret * 100;
				}else{
					$percent_jumlah_rt_maret = 0;
				}
				$tanggal_upload_maret = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//April
				$paid_rts_april = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_april = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_april = $paid_rts_april + $nopaid_rts_april;
				$sudah_tl_april = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_april = $total_rts_april - $sudah_tl_april;
				$percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
				$percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
				$jumlah_rt_april = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_april != 0) {
					$percent_jumlah_rt_april = $jumlah_rt_april/ $total_rts_april * 100;
				}else{
					$percent_jumlah_rt_april = 0;
				}
				$tanggal_upload_april = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Mei

				$paid_rts_mei = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_mei = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
				$sudah_tl_mei = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
				$percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
				$percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
				$jumlah_rt_mei = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_mei != 0) {
					$percent_jumlah_rt_mei =  $jumlah_rt_mei / $total_rts_mei * 100;
				}else{
					$percent_jumlah_rt_mei = 0;
				}
				$tanggal_upload_mei = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Juni
				$paid_rts_juni = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juni = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
				$sudah_tl_juni = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
				$percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
				$percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
				$jumlah_rt_juni = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juni != 0) {
					$percent_jumlah_rt_juni =  $jumlah_rt_juni / $total_rts_juni * 100;
				}else{
					$percent_jumlah_rt_juni = 0;
				}
				$tanggal_upload_juni = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Juli
				$paid_rts_juli = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juli = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
				$sudah_tl_juli = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status',1)->count();
				$belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
				$percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
				$percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
				$jumlah_rt_juli = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juli != 0) {
					$percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
				}else{
					$percent_jumlah_rt_juli = 0;
				}
				$tanggal_upload_juli = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Agustus
				$paid_rts_agustus = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_agustus = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
				$sudah_tl_agustus = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
				$percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
				$percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
				$jumlah_rt_agustus = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_agustus != 0) {
					$percent_jumlah_rt_agustus =  $jumlah_rt_agustus / $total_rts_agustus * 100;
				}else{
					$percent_jumlah_rt_agustus = 0;
				}
				$tanggal_upload_agustus = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//September
				$paid_rts_september = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_september = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_september = $paid_rts_september + $nopaid_rts_september;
				$sudah_tl_september = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_september = $total_rts_september - $sudah_tl_september;
				$percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
				$percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
				$jumlah_rt_september = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_september != 0) {
					$percent_jumlah_rt_september =  $jumlah_rt_september / $total_rts_september * 100;
				}else{
					$percent_jumlah_rt_september = 0;
				}
				$tanggal_upload_september = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Oktober
				$paid_rts_oktober = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_oktober = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
				$sudah_tl_oktober = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
				$percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
				$percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
				$jumlah_rt_oktober = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_oktober != 0) {
					$percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
				}else{
					$percent_jumlah_rt_oktober = 0;
				}
				$tanggal_upload_oktober = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//November
				$paid_rts_november = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_november = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_november = $paid_rts_november + $nopaid_rts_november;
				$sudah_tl_november = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_november = $total_rts_november - $sudah_tl_november;
				$percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
				$percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
				$jumlah_rt_november = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_november != 0) {
					$percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
				}else{
					$percent_jumlah_rt_november = 0;
				}
				$tanggal_upload_november = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Desember
				$paid_rts_desember = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_desember = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
				$sudah_tl_desember = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
				$percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
				$percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
				$jumlah_rt_desember = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_desember != 0) {
					$percent_jumlah_rt_desember =  $jumlah_rt_desember / $total_rts_desember * 100;
				}else{
					$percent_jumlah_rt_desember = 0;
				}
				$tanggal_upload_desember = StatusTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');
			}
			$export = new RTUptTahunExport([          
				$push,
			]);
			foreach($list_month as $bulan){

				if($bulan->id_bulan == 1){
					$total = $total_rts_januari; 
					$paid = $paid_rts_januari; 
					$sudah_tl = $sudah_tl_januari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_januari), 0, 5).'%'; 
					$belum_tl = $belum_tl_januari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_januari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_januari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_januari), 0, 5).'%';
					if($tanggal_upload_januari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_januari)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}

				if($bulan->id_bulan == 2){
					$total = $total_rts_februari; 
					$paid = $paid_rts_februari; 
					$sudah_tl = $sudah_tl_februari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_februari), 0, 5).'%'; 
					$belum_tl = $belum_tl_februari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_februari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_februari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_februari), 0, 5).'%';
					if($tanggal_upload_februari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_februari)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 3){
					$total = $total_rts_maret; 
					$paid = $paid_rts_maret; 
					$sudah_tl = $sudah_tl_maret; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_maret), 0, 5).'%'; 
					$belum_tl = $belum_tl_maret; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_maret), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_maret;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_maret), 0, 5).'%';
					if($tanggal_upload_maret ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_maret)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 4){
					$total = $total_rts_april; 
					$paid = $paid_rts_april; 
					$sudah_tl = $sudah_tl_april; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_april), 0, 5).'%'; 
					$belum_tl = $belum_tl_april; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_april), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_april;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_april), 0, 5).'%';
					if($tanggal_upload_april ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_april)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 5){
					$total = $total_rts_mei; 
					$paid = $paid_rts_mei; 
					$sudah_tl = $sudah_tl_mei; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_mei), 0, 5).'%'; 
					$belum_tl = $belum_tl_mei; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_mei), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_mei;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_mei), 0, 5).'%';
					if($tanggal_upload_mei ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_mei)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 6){
					$total = $total_rts_juni; 
					$paid = $paid_rts_juni; 
					$sudah_tl = $sudah_tl_juni; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juni), 0, 5).'%'; 
					$belum_tl = $belum_tl_juni; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juni), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juni;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juni), 0, 5).'%';
					if($tanggal_upload_juni ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juni)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 7){
					$total = $total_rts_juli; 
					$paid = $paid_rts_juli; 
					$sudah_tl = $sudah_tl_juli; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juli), 0, 5).'%'; 
					$belum_tl = $belum_tl_juli; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juli), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juli;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juli), 0, 5).'%';
					if($tanggal_upload_juli ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juli)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 8){
					$total = $total_rts_agustus; 
					$paid = $paid_rts_agustus; 
					$sudah_tl = $sudah_tl_agustus; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_agustus), 0, 5).'%'; 
					$belum_tl = $belum_tl_agustus; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_agustus), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_agustus;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_agustus), 0, 5).'%';
					if($tanggal_upload_agustus ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_agustus)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 9){
					$total = $total_rts_september; 
					$paid = $paid_rts_september; 
					$sudah_tl = $sudah_tl_september; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_september), 0, 5).'%'; 
					$belum_tl = $belum_tl_september; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_september), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_september;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_september), 0, 5).'%';
					if($tanggal_upload_september ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_september)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 10){
					$total = $total_rts_oktober; 
					$paid = $paid_rts_oktober; 
					$sudah_tl = $sudah_tl_oktober; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_oktober), 0, 5).'%'; 
					$belum_tl = $belum_tl_oktober; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_oktober), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_oktober;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_oktober), 0, 5).'%';
					if($tanggal_upload_oktober ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_oktober)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 11){
					$total = $total_rts_november; 
					$paid = $paid_rts_november; 
					$sudah_tl = $sudah_tl_november; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_november), 0, 5).'%'; 
					$belum_tl = $belum_tl_november; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_november), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_november;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_november), 0, 5).'%';
					if($tanggal_upload_november ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_november)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				if($bulan->id_bulan == 12){
					$total = $total_rts_desember; 
					$paid = $paid_rts_desember; 
					$sudah_tl = $sudah_tl_desember; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_desember), 0, 5).'%'; 
					$belum_tl = $belum_tl_desember; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_desember), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_desember;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_desember), 0, 5).'%';
					if($tanggal_upload_desember ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_desember)->format('d/m/Y');
					}else{
						$tanggalUpload = 'NIHIL';
					}
				}
				array_push($push, array(
					$bulan->nama,
					''.$total.'',
					''.$paid.'',
					''.$sudah_tl.'',
					$percent_sudah_tl,
					''.$belum_tl.'',
					$percent_belum_tl,
					''.$jumlah_tl.'',
					$percent_jumlah,
					$tanggalUpload,
				));

				$export = new RTUptTahunExportNotPaid([          
					$push,
				]);
			}
		}

		return Excel::download($export, $nama_export);
	}

	public function exportPerYear($year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export)
	{
		if($getUptName == 'SELURUH UPT'){
			$upt = UPT::select('office_name', 'office_id')->distinct()->get();
		}else{
			$upt = UPT::where('office_name', $getUptName)->select('office_name', 'office_id')->distinct()->get();
		}
		$push = [];
		$fixYear = $year - 1; 
		$list_month = ListMonth::all();
// return $status_pembayaran;


		$export = new STExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			foreach ($upt as $keyUpt => $getUpt) {
				$list_month = ListMonth::all();

				array_push($push,array( 
					$getUpt->office_name,
				));

				foreach ($list_month as $key => $value) {
					if($value->id_bulan == 1){
						$fixMonth = 12;
						$fixYear = $year - 1;
					}else{
						$fixMonth = $value->id_bulan - 1;
						$fixYear = $year;
					}
					$count = '';
					$checkingPengiriman = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$getUpt->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();

					if($checkingPengiriman == 0){
						$count = 'BELUM';
					}else{
						$count = 'YA';
					}
					array_push($push[$keyUpt],
						$count
					);
				}


			}
			$export = new RTUptTahunExport([          
				$push,
			]);
		}else{
			foreach ($upt as $keyUpt => $getUpt) {
				$list_month = ListMonth::all();

				array_push($push,array( 
					$getUpt->office_name,
				));

				foreach ($list_month as $key => $value) {
					if($value->id_bulan == 1){
						$fixMonth = 12;
						$fixYear = $year - 1;
					}else{
						$fixMonth = $value->id_bulan - 1;
						$fixYear = $year;
					}
					$count = '';
					$paid_rts = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$getUpt->office_name)->count();
					array_push($push[$keyUpt],
						''.$paid_rts.''
					);
				}


			}	
			$export = new RTUptTahunExport([          
				$push,
			]);
		}

		return Excel::download($export, $nama_export);
	}

	public function exportPerMonthYear($month,$year,$status_pembayaran,$getUptName,$jenis_st,$bi_type,$nama_export)
	{	
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$upt = UPT::select('office_id','office_name','province_name')->distinct()->get();
		}else{
			$upt = UPT::where('office_id', Auth::user()->upt)->select('office_id','office_name','province_name')->distinct()->get();
		}
		$push = [];
		if($month == 1){
			$fixMonth = 12;
			$fixYear = $year - 1;
		} else{
			$fixMonth = $month - 1;
			$fixYear = $year;
		}
		$list_month = ListMonth::all();


		$export = new RTUptBulanTahunExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			foreach ($upt as $key => $value) {
				$checkingPengiriman = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();

				if($checkingPengiriman == 0){
					$countCheckingPengiriman = 'BELUM';
				}else{
					$countCheckingPengiriman = 'YA';
				}

				array_push($push, array(
					$value->office_name,
					$countCheckingPengiriman,
				));

				$export = new RTUptBulanTahunExport([          
					$push,
				]);

			}
			return Excel::download($export, $nama_export);
		}else{
			foreach($upt as $value){
				$paid_rts = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
				$nopaid_rts = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
				$total_rts = $paid_rts + $nopaid_rts;
				$sudah_tl = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl = $total_rts - $sudah_tl;
				$tanggal_upload = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');
				$jumlah_rt = StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
				if($tanggal_upload ){
					$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload)->format('d/m/Y');
				}else{
					$tanggalUpload = 'NIHIL';
				}
				array_push($push, array(
					$value->office_name,
					''.$total_rts == 0 ? 'NIHIL' : $total_rts .'',
					''.$paid_rts == 0 ? 'NIHIL' : $paid_rts .'',
					''.$sudah_tl == 0 ? 'NIHIL' : $sudah_tl .'',
					'0 %',
					''.$belum_tl == 0 ? 'NIHIL' : $belum_tl .'',
					'0 %',
					''.$jumlah_rt == 0 ? 'NIHIL' : $jumlah_rt .'',
					'0 %',
					$tanggalUpload,
				));

				$export = new STSeluruhUptBulanTahunJenis([          
					$push,
				]);
			}
		}

		return Excel::download($export, $nama_export);
	}

	public function approved(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
			if ($request->getStatus == 0) {
				$ids = $request->id;
				if ($request->id) {
					$action = $request->action;
					DB::table('status_tagihans')->whereIn('no_spp', $ids)
					->update(['active' => $action,'ket_reject' => $request->catatankasi]);
					Session::flash('info', 'Status Tagihan');
					Session::flash('colors', 'green');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Belum Mengapprove Data!');
					return redirect()->back();
				}
				else{
					Session::flash('info', 'Status Tagihan');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Belum Memilih Data!');
					return redirect()->back();
				}	
			}
			else{
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Status Tagihanatus Salah!');
				return redirect()->back();
			}

		}else{
			Session::flash('info', 'Status Tagihan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

		// Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new RtImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];
		$result = array_splice($body, 1);
		// if($header[0] == 'Provinsi' && $header[1] == 'Nama UPT' && $header[2] == 'No. Tagihan' && $header[3] == 'No. Klien' && $header[4] == 'Nama Klien' && $header[5] == 'BHP (Rp)' && $header[6] == 'Tanggal (BI Create Date)' && $header[7] == 'Status Pembayaran' && $header[8] == 'Service' && $header[9] == 'Tanggal Jatuh Tempo' && $header[10] == 'Upaya / Methode' && $header[11] == 'Tanggal Upaya' && $header[12] == 'Keterangan'){
		if($header[0] == 'No. Tagihan' && $header[1] == 'No. Klien' && $header[2] == 'Nama Klien' && $header[3] == 'BHP (Rp)' && $header[4] == 'Tanggal (BI Create Date)' && $header[5] == 'Status Pembayaran' && $header[6] == 'Service' && $header[7] == 'Tanggal Jatuh Tempo' && $header[8] == 'Upaya / Methode' && $header[9] == 'Tanggal Upaya' && $header[10] == 'Keterangan'){
			return view('templates.SPP.ST.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'ST');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');


		// $data = $request->all();
		// $no_spp = $data['no_spp'];
		// $upayametode = $data['upayametode'];
		// $tanggal_upaya = $data['tanggal_upaya'];
		// $keterangan = $data['keterangan'];
		// $filename = $request->file('bukti_dukung');
		// $created_at = date('Y-m-d H:m:s');
		// $updated_at = date('Y-m-d H:m:s');
		$data = $request->all();
		$no_spp = isset($data['no_spp']) ? $data['no_spp'] : '';
		$upayametode = isset($data['upayametode']) ? $data['upayametode'] : '';
		$tanggal_upaya = isset($data['tanggal_upaya']) ? $data['tanggal_upaya'] : '';
		$keterangan = isset($data['keterangan']) ? $data['keterangan'] : '';
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			try{
				$st = StatusTagihan::where('no_spp',$value)->first();
				if($st){
					$uniq = uniqid();
					if(isset($filename[$key])){
						$filename[$key]->move(public_path('lampiran/spp/st'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
					}
					// $st->no_spp = isset($no_spp[$key]) ? $no_spp[$key] : '';
					// $st->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
					// $st->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
					// $st->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
					// $st->ket_operator = $request->ket_operator;
					// $st->created_by = Auth::user()->id;
					// $st->bukti_dukung = isset($filename[$key]) ? 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension() : '';
					// $st->active = 0;
					$st = StatusTagihan::where('no_spp',$value)->first();
					$st->id = $st->id;
					$st->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
					$st->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
					$st->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
					$st->created_by = Auth::user()->id;
					$st->bukti_dukung = isset($filename[$key]) ? 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension() : '';
					$st->active = 0;
					$st->save();


				}else{
					Session::flash('info', 'Status Tagihan');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-times');
					Session::flash('alert', 'Data SPP ada yang tidak sesuai dengan data di data aplikasi!');		
					return redirect(url('spp/st'));
				}
			}catch (Exception $e){
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Data SPP ada yang tidak sesuai dengan data di data aplikasi!');		
				return redirect(url('spp/st'));
			}
		}
		Session::flash('info', 'Status Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect(url('spp/st'));

		// $data = $request->all();		
		// $no_spp = $data['no_tagihan'];
		// $upayametode = $data['upayametode'];
		// $tanggal_upaya = $data['tanggal_upaya'];
		// $keterangan = $data['keterangan'];
		// $bi_create_date = $data['bi_create_date'];
		// $filename = $request->file('bukti_dukung');
		// $created_at = date('Y-m-d H:m:s');
		// $updated_at = date('Y-m-d H:m:s');		
		// $rows = [];
		// $variables = array_filter($request->tanggal_upaya);
		// return $variables;
		// foreach($variables as $key => $input) {
		// 	$uniq = uniqid();
		// $filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
		// if($status_pembayaran[$key]  == 'Belum Bayar' || $status_pembayaran[$key]  == 'belum bayar'){
		// 	$pembayaran = 'not paid';
		// }else{
		// 	$pembayaran = 'paid';
		// }

		// dd((int)$no_spp[$key]);
		// 	array_push($rows, 
		// 		[
		// 			'active' => 0,
		// 			'upaya_metode' => isset($upayametode[$key]) ? $upayametode[$key] : '',
		// 			'tanggal_upaya' => isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '',
		// 			'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
		// 			'ket_operator' => $request->ket_operator,
		// 			'bukti_dukung' => 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension(),
		// 			'created_by' => Auth::user()->id,
		// 			'created_at' => $created_at,
		// 			'updated_at' => $updated_at,
		// 		]);
		// }
		// $hasil = StatusTagihan::insert($rows);
		// Session::flash('info', 'ST');
		// Session::flash('colors', 'green');
		// Session::flash('icons', 'fas fa-clipboard-check');
		// Session::flash('alert', 'Berhasil Mengupload Data!');
		// return redirect(url('spp/rt'));

	}

	public function downloadSampel(){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		if ($month == 1) {			
			$dataBelumTerbayar = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBelumTerbayar = $month - 1; 
			$fixYear = $year;
		}

		$tanggal_upaya = Date('Y-m-d');
		$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
		$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
		$rts_nopaid = StatusTagihan::whereMonth('bi_create_date',$dataBelumTerbayar)->whereYear('bi_create_date',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->get();
		$tes = json_decode($rts_nopaid);
		foreach ($tes as $key => $value) {
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				if($value->upaya_metode){
					$upaya_metode = Metode::where('id', $value->upaya_metode)->value('metode');
				}else{
					$upaya_metode = '';
				}

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;


				if($value->upaya_metode){
					$upaya_metode = Metode::where('id', $value->upaya_metode)->value('metode');
				}else{
					$upaya_metode = '';
				}

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}

			$tanggal_jatuh_tempo = date("Y-m-d", strtotime($value->bi_pay_until));


			// array_push($push, array(
			// 	$provinsi,
			// 	$nama_upt,
			// 	$value->no_spp,
			// 	$value->no_klien,
			// 	$value->nama_perusahaan,
			// 	$value->tagihan,
			// 	$value->bi_create_date,
			// 	'Belum Bayar',
			// 	$value->service_name,
			// 	$tanggal_jatuh_tempo,
			// 	'',
			// 	$tanggal_upaya,
			// 	'',

			// ));

			array_push($push, array(
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$value->tagihan,
				$value->bi_create_date,
				'Belum Bayar',
				$value->service_name,
				$tanggal_jatuh_tempo,
				$upaya_metode,
				$tanggal_upaya,
				'KETERANGAN',

			));

		}
		$export = new SampelSTExport([          
			$push,
		]);

		return Excel::download($export, 'ST IMPORT '. date('d F Y') .'.xlsx');

	}
}
