<?php

namespace App\Http\Controllers\Core\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Exports\sampelRTExport;
use App\Exports\RTExport;
use App\Exports\RTUptBulanTahunExport;
use App\Exports\RTUptTahunExport;
use App\Exports\RTUptTahunExportNotPaid;
use App\Imports\RtImport;
use App\Model\Refrension\Metode;
use Carbon\Carbon;
use Auth;
use App\Model\Privillage\Role;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Barryvdh\Snappy;
use App\Model\SPP\RincianTagihan;
use App\Model\Notification\Notification;
use App\Model\Date\ListMonth;
use App\Model\Setting\UPT;
use DB;
use Validator;
use App\Model\Region\Provinsi;
use \App\Model\SPP\StatusTagihan;

class RincianTagihanController extends Controller
{
	public function __construct()
	{
		$this->myTime = Carbon::now();
		$this->day = $this->myTime->format('d');
		$this->month = $this->myTime->format('m');
		$this->years = '20'.$this->myTime->format('y');
		$this->middleware('auth', ['except' => ['table']]);
	}

	public function index(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$getUptName = 'SELURUH UPT';
			$month = $this->month;
			$year = $this->years;
			if ($month == 1) {
				$dataTerbayar = 12;
				$dataBelumTerbayar = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataTerbayar = $month - 1;
				$dataBelumTerbayar = $month - 1; 
				$fixYear = $year;
			}
			$year = $this->years;
			$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
			$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
			$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('bi_type',8)->where('status_izin','Perpanjangan')->count();
			$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',0)->paginate(100);
			$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
			$total = $paid_rts + $nopaid_rts;
			$sendReport = 'INDEX';
			$statusReminder = '';
			$checkStatusReminder = '';
			$checkStatusApproved = '';
			$freezeTime = $this->myTime->format('j');
			$freezeMonth = $this->myTime->format('m');

		}
		else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
			$month = $this->month;
			$year = $this->years;
			if ($month == 1) {
				$dataTerbayar = 12;
				$dataBelumTerbayar = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataTerbayar = $month - 1;
				$dataBelumTerbayar = $month - 1; 
				$fixYear = $year;
			}
			$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
			$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
			$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
			$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
			$total = $paid_rts + $nopaid_rts;
			$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->paginate(100);
			$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
			$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
			$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
			$freezeTime = $this->myTime->format('j');
			$freezeMonth = $this->myTime->format('m');
		}		
		return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','sendReport'));
	}

	protected function table($request, $id = null)
	{
		$data = [];
		$rts_paid = new RincianTagihan;
		$getUptName = 'SELURUH UPT';
		$paid_date = $this->paidDate();
		$rts_paid = $rts_paid->whereMonth('bi_begin', $paid_date['month']);
		$rts_paid = $rts_paid->whereYear('bi_begin', $paid_date['year']);
		$rts_paid = $rts_paid->where('status','paid');
		$rts_paid = $rts_paid->where('status_izin','Perpanjangan');
		$rts_paid = $rts_paid->where('bi_type',8);
		if ($request->filled('id_map')) {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$rts_paid = $rts_paid->where('upt', $getUptName);
		}
		else if (Role::where('id', Auth::user()->role_id)->value('akses') != 'administrator') {
			$getUptName = UPT::where('office_id', Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$rts_paid = $rts_paid->where('upt', $getUptName);
		}
		$data = $rts_paid->get();
		return datatables()->of($data)
		->editColumn('bhp', function($data) {
			return 'Rp. '.number_format($data['tagihan'], 2);
		})
		->editColumn('bi_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_begin'])
			->format('d, M Y');
		})
		->editColumn('due_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_pay_until'])->format('d, M Y');
		})
		->editColumn('pay_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_money_received'])->format('d, M Y');
		})
		->editColumn('method', function($data) {
			return Metode::where('id', RincianTagihan::where('no_spp', $data['no_spp'])->value('upaya_metode'))->value('metode');
		})
		->make(true);
	}

	public function table_query($request)
	{
		$getUptName = 'SELURUH UPT';
		if (!is_null($request->upt)) {
			$getUptName = $request->upt;
		}
		$paid_date = $this->paidDate($request->month, $request->year);
		$rts_paid = new RincianTagihan;
		$rts_paid = $rts_paid->whereMonth('bi_begin', $paid_date['month']);
		$rts_paid = $rts_paid->whereYear('bi_begin', $paid_date['year']);
		$rts_paid = $rts_paid->where('upt', $getUptName);
		$rts_paid = $rts_paid->where('status','paid');
		$rts_paid = $rts_paid->where('status_izin','Perpanjangan');
		$rts_paid = $rts_paid->where('bi_type',8);
		$data = $rts_paid->get();
		return datatables()->of($data)
		->editColumn('bhp', function($data) {
			return 'Rp. '.number_format($data['tagihan'], 2);
		})
		->editColumn('bi_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_begin'])
			->format('d, M Y');
		})
		->editColumn('due_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_pay_until'])->format('d, M Y');
		})
		->editColumn('pay_date', function($data) {
			return \Carbon\Carbon::parse($data['bi_money_received'])->format('d, M Y');
		})
		->editColumn('method', function($data) {
			return Metode::where('id', RincianTagihan::where('no_spp', $data['no_spp'])->value('upaya_metode'))->value('metode');
		})
		->make(true);
	}

	public function search(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if ($request->filled('type') && $request->type == 'WEB') {
			return $this->table($request);
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->get();
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					$month = $this->month;
					$year = $this->years;
					return view('templates.SPP.RT.search.multiple',compact('getUPT'));
				}
				else{
					$month = $this->month;
					$year = $this->years;
					if ($month == 1) {
						$dataTerbayar = 12;
						$dataBelumTerbayar = 12;
						$fixYear = $year - 1;
					}
					else{
						$dataTerbayar = $month - 1;
						$dataBelumTerbayar = $month - 1; 
						$fixYear = $year;
					}
					$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
					$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
					$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
					$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
					$total = $paid_rts + $nopaid_rts;
					$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
					$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
					$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
					$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
					$sendReport = 'INDEX';
					$freezeTime = $this->myTime->format('j');
					$freezeMonth = $this->myTime->format('m');
				}
				return view('templates.SPP.RT.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','sendReport'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function searchMultiple($id,Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			if ($changeUPT) {
				$month = $this->month;
				$year = $this->years;
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
				$sendReport = 'INDEX';
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','catatanPetugas','catatanKasi','sendReport'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pelayanan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	
	public function search_query(Request $request)
	{
		if ($request->filled('type') && $request->type == 'WEB') {
			return $this->table_query($request);
		}
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$month = $request->month;
		$year = $request->year;
		$getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			if ($request->upt == null) {
				$getUptName = 'SELURUH UPT';
				$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			}else{
				$getUptName = $request->upt;
				$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			}
			if($getUptName != 'SELURUH UPT' && !$month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName == 'SELURUH UPT' && $month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName != 'SELURUH UPT' && $month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif ($getUptName != 'SELURUH UPT' && $month && $year) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->where('active',0)->get();
				$statusReminder = '';
				$checkStatusReminder = '';
				$checkStatusApproved = '';
				$catatanPetugas = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',0)->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',0)->select('ket_reject')->distinct()->value('ket_reject');
				$sendReport = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.search.index',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport'));
			}
			elseif($getUptName != 'SELURUH UPT' && $year){
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				return view('templates.SPP.RT.search.upt-bulan-tahun',compact('getUptName','year','month','getUptId'));
			}elseif($getUptName == 'SELURUH UPT' && $year && $month == null){
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				return view('templates.SPP.RT.search.seluruh-upt-tahun',compact('getUptName','year','month','getUptId'));
			}
			elseif($getUptName == 'SELURUH UPT' && $month && $year){
				return view('templates.SPP.RT.search.seluruh-upt-bulan-tahun',compact('getUptName','year','month','getUptId'));
			}
			elseif($month && $year){
				return view('templates.SPP.RT.search.seluruh-upt-bulan-tahun',compact('getUptName','year','month','getUptId'));
			}
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt'){
			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			if($getUptName != 'SELURUH UPT' && !$month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName == 'SELURUH UPT' && !$month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName != 'SELURUH UPT' && $month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			if ($getUptName != 'SELURUH UPT' && $year && $month == null) {
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}
			elseif($getUptName != 'SELURUH UPT' && $month && $year == null){
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}		
			elseif ($getUptName != 'SELURUH UPT' && $month && $year) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}	

		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator'){
			$getUptName = UPT::where('office_id',Auth::user()->upt)->value('office_name');
			if($getUptName != 'SELURUH UPT' && !$month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName == 'SELURUH UPT' && !$month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			elseif($getUptName != 'SELURUH UPT' && $month && !$year){
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'green');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Pencarian tidak tersedia!');
				return redirect()->back();
			}
			if ($getUptName != 'SELURUH UPT' && $year && $month == null) {
				if ($this->month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $this->month - 1;
					$dataBelumTerbayar = $this->month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}
			elseif($getUptName != 'SELURUH UPT' && $month && $year == null){
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}		
			elseif ($getUptName != 'SELURUH UPT' && $month && $year) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',8)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('status_izin','Perpanjangan')->where('bi_type',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('j');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
			}	

		}
	}

	public function postORupdate(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$data = $request->all();
		$no_spp = $data['no_spp'];
		$upayametode = $data['upayametode'];
		$tanggal_upaya = $data['tanggal_upaya'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			$rt = RincianTagihan::where('no_spp',$value)->first();
			$rt->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
			$rt->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
			$rt->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
			$rt->ket_operator = $request->ket_operator;
			$rt->created_by = Auth::user()->id;
			$uniq = uniqid();
			$filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
			$rt->bukti_dukung = 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension();
			$rt->active = 0;
			$rt->save();
		}

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 8;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengupload Data Rincian Tagihan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Rincian Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect()->back();
	}

	public function preview(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];				
		$month = $request->month;
		$year = $request->year;
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			if ($month == 1) {
				$dataBulan = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataBulan = $month - 1;
				$fixYear = $year;
			}
			$nama_export = 'RT-'. date('d F Y') .'.xlsx';
				// $rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->get();
		}else{
			$getUptName = UPT::where('office_id', Auth::user()->upt)->value('office_name');
			if($month && $year == date('Y')){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-'. date('d F Y') .'.xlsx';

			// $rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();

				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();

			}elseif($month && $year){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-UPT-TAHUN-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->distinct()->where('bi_type',$request->bi_type)->get();
			}elseif($year){
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();
				$nama_export = 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx';
			}
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct('office_name')->value('office_name');
		}
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');
		return $pdf->stream('RT-'. date('d F Y') .'.pdf');
	}

	public function preview_search(Request $request)
	{	
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$myTime = Carbon::now();
		$status_pembayaran = $request->status_pembayaran;		
		$month = $request->month;
		$year = $request->year;					
		$myTime = Carbon::now();

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			if($request->getUptName == ''){
				$getUptName = 'SELURUH UPT';
			}else{
				$getUptName = $request->getUptName;
			}
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year) {
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();

				$rt['rt'] = $rts_data; 
				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');		
				return $pdf->stream($getUptName.'_RT-'. date('d F Y') .'.pdf');				

			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
			// $pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');
				return $pdf->stream('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-seluruh-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
			// $pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');
				return $pdf->stream('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->stream('RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.pdf');
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			if ($month != '' && $year != '') {
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();

				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');
				return $pdf->stream($getUptName.'_RT-'. date('d F Y') .'.pdf');
			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->stream('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->stream('RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.pdf');
			}
		}
	}

	public function print(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$push = [];				
		$month = $request->month;
		$year = $request->year;
		if ($month == 1) {
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataBulan = $month - 1;
			$fixYear = $year;
		}

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$nama_export = 'RT-'. date('d F Y') .'.xlsx';
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->get();
			// return $rts;
		}else{
			$getUptName = UPT::where('office_id', Auth::user()->upt)->value('office_name');
			if($month && $year == date('Y')){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();
			}elseif($month && $year){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-UPT-TAHUN-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->distinct()->where('bi_type',$request->bi_type)->get();
			}elseif($year){
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();
				$nama_export = 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx';
			}
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
		}
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');		
		return $pdf->download('RT-'. date('d F Y') .'.pdf');
	}

	public function print_search(Request $request)
	{	
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$myTime = Carbon::now();
		$status_pembayaran = $request->status_pembayaran;		
		$month = $request->month;
		$year = $request->year;					
		$myTime = Carbon::now();

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			if($request->getUptName == '' || $request->getUptName == 'SELURUH UPT'){
				$getUptName = 'SELURUH UPT';
				$getUptId = UPT::select('office_id')->distinct()->get();
			}else{
				$getUptName = $request->getUptName;
				$getUptId = UPT::where('office_name',$getUptName)->orWhereNull('office_name')->distinct()->value('office_id');
			}
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year) {
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();

				$rt['rt'] = $rts_data; 
				$rt['getUptName'] = $getUptName; 
				$pdf = PDF::loadView('templates.SPP.RT.dev.print-search', $rt)->setPaper('a3','landscape');		
				return $pdf->download($getUptName.'_RT-'. date('d F Y') .'.pdf');				

			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}

				return $pdf->download('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					return $getUptName;
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-seluruh-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}

				return $pdf->download('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($month && $year){
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->download('RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.pdf');
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');

			if ($month != '' && $year != '') {
				$dataBulan = $month;
				$fixYear = $year;
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();

				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a3','landscape');
				return $pdf->download($getUptName.'_RT-'. date('d F Y') .'.pdf');
			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->download('RT-UPT-TAHUN-'. date('d F Y') .'.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a3','landscape');					
				}
				return $pdf->download('RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.pdf');
			}
		}
	}

	public function ORUpdate(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$id = Crypt::decryptString($request->id);
		$rt = RincianTagihan::find($id);		
		$rt->keterangan = $request->keterangan;
		if ($request->hasFile('bukti_dukung')) {
			$files = $request->file('bukti_dukung');
			$cover = 'Bukti Dukung-'.uniqid().$id."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/spp/rt'), $cover);
			$rt->bukti_dukung = $cover;	
		}
		$rt->upaya_metode = $request->upaya_metode;
		$rt->tanggal_upaya = $request->tanggal_upaya;
		$rt->active = 0;
		$rt->save();
		Session::flash('info', 'Rincian Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}


	public function export(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];				
		$month = $request->month;
		$year = $request->year;
		if ($month == 1) {
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataBulan = $month - 1;
			$fixYear = $year;
		}

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$nama_export = 'RT-'. date('d F Y') .'.xlsx';
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->get();
		}else{
			$getUptName = UPT::where('office_id', Auth::user()->upt)->value('office_name');
			if($month && $year == date('Y')){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();
			}elseif($month && $year){
				if ($month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1;
					$fixYear = $year;
				}
				$nama_export = 'RT-UPT-TAHUN-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->distinct()->where('bi_type',$request->bi_type)->get();
			}elseif($year){
				if ($this->month == 1) {
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $this->month - 1;
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$request->bi_type)->get();
				$nama_export = 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx';
			}
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
		}

		$export = new RTExport([          
			$push,
		]);
		foreach ($rts_data as $key => $value) {
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
				$provinsi = $value->province;
				$nama_upt = $value->upt;				
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}

			$tl = RincianTagihan::where('no_spp', $value->no_spp)->first();

			if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
				$metode = Metode::where('id', $value->upaya_metode)->value('metode');
				$tanggal_upaya = $value->tanggal_upaya;
				$keterangan = $value->keterangan;
			}else{
				$metode = '';
				$tanggal_upaya = '';
				$keterangan = '';
			}

			if($request->status_pembayaran == 'paid'){
				$status_pembayaran = 'Telah Bayar';
				$tanggal_jatuh_tempo = date("d/m/Y", strtotime($value->bi_pay_until));
			}else{
				$status_pembayaran = 'Belum Bayar';
				$tanggal_jatuh_tempo = \Carbon\Carbon::parse($value->bi_begin)->subDays(1)->format('d/m/Y');
			}
			$bi_begin = date("d/m/Y", strtotime($value->bi_begin));
			$bi_money_received = date("d/m/Y", strtotime($value->bi_money_received));


			array_push($push, array(
				$provinsi,
				$nama_upt,
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$value->tagihan,
				$bi_begin,
				$status_pembayaran,
				$value->service_name,
				$tanggal_jatuh_tempo,
				$bi_money_received,
				$metode,
				$tanggal_upaya,
				$keterangan,
			));

			$export = new RTExport([          
				$push,
			]);
		}

		return Excel::download($export, $nama_export);		
	}

	public function export_search(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];		
		$status_pembayaran = $request->status_pembayaran;		
		$bi_type = $request->bi_type;
		$month = $request->month;
		$year = $request->year;
		$myTime = Carbon::now();	
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			if($request->getUptName == ''){
				$getUptName = 'SELURUH UPT';
			}else{
				$getUptName = $request->getUptName;
			}
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year != '') {
				$nama_export = 'RT-UPT-BULAN-TAHUN-'.$getUptName.'.xlsx';
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();

				// $rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
				// return $rts_data;
			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year != ''){
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status', $status_pembayaran)->where('upt', $getUptName)->get();
				return $this->exportPerYear($year,$status_pembayaran,$getUptName,$request->bi_type);

			}elseif($getUptName == 'SELURUH UPT' && $month == -1 && $year != ''){
				$listUpt = UPT::select('office_id','office_name','province_name')->distinct('office_name')->get();
				$nama_export = 'RT-SELURUH-UPT-TAHUN.xlsx';
				if($status_pembayaran == 'paid'){
					$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status', $status_pembayaran)->get(); 
					// $rts_data = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$uptName)->get();
					return $this->exportPerYear($year,$status_pembayaran,$getUptName,$request->bi_type);
				}else{
					foreach($listUpt as $key => $value){
						$uptName = $value->office_name;
						foreach(ListMonth::all() as $key => $bulan){
							if($bulan->id_bulan == 1){
								$fixMonth = 12;
								$fixYear = $year - 1;
							}else{
								$fixMonth = $bulan->id_bulan - 1;
								$fixYear = $year;
							}
							// $paid_rts[$key] = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status',$status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();

							$paid_rts[$key] = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
						}
						array_push($push, array(
							$value->office_name,
							$paid_rts[0] == 0 ? '0' : $paid_rts[0] ,
							$paid_rts[1] == 0 ? '0' : $paid_rts[1] ,
							$paid_rts[2] == 0 ? '0' : $paid_rts[2] ,
							$paid_rts[3] == 0 ? '0' : $paid_rts[3] ,
							$paid_rts[4] == 0 ? '0' : $paid_rts[4] ,
							$paid_rts[5] == 0 ? '0' : $paid_rts[5] ,
							$paid_rts[6] == 0 ? '0' : $paid_rts[6] ,
							$paid_rts[7] == 0 ? '0' : $paid_rts[7] ,
							$paid_rts[8] == 0 ? '0' : $paid_rts[8] ,
							$paid_rts[9] == 0 ? '0' : $paid_rts[9] ,
							$paid_rts[10] == 0 ? '0' : $paid_rts[10] ,
							$paid_rts[11] == 0 ? '0' : $paid_rts[11] ,
						));
					}
					$export = new RTUptTahunExport([          
						$push,
					]);

					return Excel::download($export, $nama_export);
				}

			}elseif($getUptName == 'SELURUH UPT' && $month != -1 && $year == ''){
				$nama_export = 'RT-SELURUH-UPT-BULAN-TAHUN.xlsx';
				$rts = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}elseif($getUptName == 'SELURUH UPT' && $month != -1 && $year != ''){
				$nama_export = 'RT-SELURUH-UPT';
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
				return $this->exportPerMonthYear($rts_data,$month,$year,$status_pembayaran,$getUptName, $request->bi_type);
			}


		}else{
			if ($month == 1) {					
				$dataBulan = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataBulan = $month - 1; 
				$fixYear = $year;
			}	
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			if ($month != '' && $year != '') {
				$nama_export = 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx';
				$dataBulan = $month;
				$fixYear = $year;
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();				
			}elseif($month == '' && $year != ''){
				$nama_export = 'RT-UPT-TAHUN-'. date('d F Y') .'.xlsx';
				$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->get();
			}
		}

		if(!empty($rts_data) || isset($rts_data[0]) || $rts_data == '' || $rts_data == null){
			$export = new RTExport([          
				$push,
			]);
			foreach ($rts_data as $key => $value) {
				if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
					$getUptId = UPT::where('office_name',$getUptName)->select('province_code')->distinct()->value('province_code');				
					$provinsi = Provinsi::where('id', $getUptId)->value('nama');			
					if ($getUptId == '' || $getUptId == null){
						$nama_upt = $value->upt;		
					}else{
						$nama_upt = $request->getUptName;		
					}
				}else{
					$id_prov = Auth::user()->upt_provinsi;
					$kode_upt = Auth::user()->upt;

					$provinsi = Provinsi::where('id', $id_prov)->value('nama');
					$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');				
				}

				$tl = RincianTagihan::where('no_spp', $value->no_spp)->first();

				if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
					$metode = Metode::where('id', $value->upaya_metode)->value('metode');
					$tanggal_upaya = $value->tanggal_upaya;
					$keterangan = $value->keterangan;
				}else{
					$metode = '';
					$tanggal_upaya = '';
					$keterangan = '';
				}

				if($request->status_pembayaran == 'paid'){
					$status_pembayaran = 'Telah Bayar';
					$tanggal_jatuh_tempo = date("d/m/Y", strtotime($value->bi_pay_until));
				}else{
					$status_pembayaran = 'Belum Bayar';
					$tanggal_jatuh_tempo = \Carbon\Carbon::parse($value->bi_begin)->subDays(1)->format('d/m/Y');
				}

				$bi_begin = date("d/m/Y", strtotime($value->bi_begin));
				$bi_money_received = date("d/m/Y", strtotime($value->bi_money_received));

				array_push($push, array(
					$provinsi,
					$nama_upt,
					$value->no_spp,
					$value->no_klien,
					$value->nama_perusahaan,
					$value->tagihan,
					$bi_begin,
					$status_pembayaran,
					$value->service_name,
					$tanggal_jatuh_tempo,
					$bi_money_received,
					$metode,
					$tanggal_upaya,
					$keterangan,
				));

				$export = new RTExport([          
					$push,
				]);

			}

			return Excel::download($export, $nama_export);
		}else{			
			Session::flash('info', 'RT');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data Kosong!');
			return redirect()->back();
		}
	}

	public function exportPerYear($year,$status_pembayaran,$getUptName,$bi_type)
	{	
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$upt = UPT::select('office_name','office_id')->distinct()->get();
		}else{
			$upt = UPT::where('upt', Auth::user()->upt)->select('office_name','office_id')->distinct()->get();
		}
		$push = [];
		$fixYear = $year - 1;
		$list_month = ListMonth::all();


		$export = new RTExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			if($getUptName != 'SELURUH UPT'){
				$januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();

				if($januari == 0){
					$countjanuari = 'BELUM';
				}else{
					$countjanuari = 'YA';
				}

				if($februari == 0){
					$countfebruari = 'BELUM';
				}else{
					$countfebruari = 'YA';
				}

				if($maret == 0){
					$countmaret = 'BELUM';
				}else{
					$countmaret = 'YA';
				}

				if($april == 0){
					$countapril = 'BELUM';
				}else{
					$countapril = 'YA';
				}

				if($mei == 0){
					$countmei = 'BELUM';
				}else{
					$countmei = 'YA';
				}

				if($juni == 0){
					$countjuni = 'BELUM';
				}else{
					$countjuni = 'YA';
				}

				if($juli == 0){
					$countjuli = 'BELUM';
				}else{
					$countjuli = 'YA';
				}

				if($agustus == 0){
					$countagustus = 'BELUM';
				}else{
					$countagustus = 'YA';
				}

				if($september == 0){
					$countseptember = 'BELUM';
				}else{
					$countseptember = 'YA';
				}

				if($oktober == 0){
					$countoktober = 'BELUM';
				}else{
					$countoktober = 'YA';
				}

				if($november == 0){
					$countnovember = 'BELUM';
				}else{
					$countnovember = 'YA';
				}

				if($desember == 0){
					$countdesember = 'BELUM';
				}else{
					$countdesember = 'YA';
				}

				array_push($push, array(
					$getUptName,
					$countjanuari,
					$countfebruari,
					$countmaret,
					$countapril,
					$countmei,
					$countjuni,
					$countjuli,
					$countagustus,
					$countseptember,
					$countoktober,
					$countnovember,
					$countdesember,
				));

				$export = new RTUptTahunExport([          
					$push,
				]);
			}else{	
				foreach ($upt as $key => $value) {
					// return $upt;
					$januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
					$desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();

					if($januari == 0){
						$countjanuari = 'BELUM';
					}else{
						$countjanuari = 'YA';
					}

					if($februari == 0){
						$countfebruari = 'BELUM';
					}else{
						$countfebruari = 'YA';
					}

					if($maret == 0){
						$countmaret = 'BELUM';
					}else{
						$countmaret = 'YA';
					}

					if($april == 0){
						$countapril = 'BELUM';
					}else{
						$countapril = 'YA';
					}

					if($mei == 0){
						$countmei = 'BELUM';
					}else{
						$countmei = 'YA';
					}

					if($juni == 0){
						$countjuni = 'BELUM';
					}else{
						$countjuni = 'YA';
					}

					if($juli == 0){
						$countjuli = 'BELUM';
					}else{
						$countjuli = 'YA';
					}

					if($agustus == 0){
						$countagustus = 'BELUM';
					}else{
						$countagustus = 'YA';
					}

					if($september == 0){
						$countseptember = 'BELUM';
					}else{
						$countseptember = 'YA';
					}

					if($oktober == 0){
						$countoktober = 'BELUM';
					}else{
						$countoktober = 'YA';
					}

					if($november == 0){
						$countnovember = 'BELUM';
					}else{
						$countnovember = 'YA';
					}

					if($desember == 0){
						$countdesember = 'BELUM';
					}else{
						$countdesember = 'YA';
					}

					array_push($push, array(
						$value->office_name,
						$countjanuari,
						$countfebruari,
						$countmaret,
						$countapril,
						$countmei,
						$countjuni,
						$countjuli,
						$countagustus,
						$countseptember,
						$countoktober,
						$countnovember,
						$countdesember,
					));

					$export = new RTUptTahunExport([          
						$push,
					]);
				}

			}
		}else{
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
				//Januari
				$paid_rts_januari = RincianTagihan::whereMonth('bi_begin',12)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_januari = RincianTagihan::whereMonth('bi_begin',12)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
				$sudah_tl_januari = RincianTagihan::whereMonth('bi_begin',12)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
				$percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
				$percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
				$jumlah_rt_januari = RincianTagihan::whereMonth('bi_begin',12)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_januari != 0) {
					$percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
				}else{
					$percent_jumlah_rt_januari = 0;
				}
				$tanggal_upload_januari = RincianTagihan::whereMonth('bi_begin',12)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');

              //Februari
				$paid_rts_februari = RincianTagihan::whereMonth('bi_begin',1)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_februari = RincianTagihan::whereMonth('bi_begin',1)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
				$sudah_tl_februari = RincianTagihan::whereMonth('bi_begin',1)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
				$percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
				$percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
				$jumlah_rt_februari = RincianTagihan::whereMonth('bi_begin',1)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_februari != 0) {
					$percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
				}else{
					$percent_jumlah_rt_februari = 0;
				}
				$tanggal_upload_februari = RincianTagihan::whereMonth('bi_begin',1)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

              //Maret
				$paid_rts_maret = RincianTagihan::whereMonth('bi_begin',2)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_maret = RincianTagihan::whereMonth('bi_begin',2)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
				$sudah_tl_maret = RincianTagihan::whereMonth('bi_begin',2)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
				$percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
				$percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
				$jumlah_rt_maret = RincianTagihan::whereMonth('bi_begin',2)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_maret != 0) {
					$percent_jumlah_rt_maret =  $jumlah_rt_maret / $total_rts_maret * 100;
				}else{
					$percent_jumlah_rt_maret = 0;
				}
				$tanggal_upload_maret = RincianTagihan::whereMonth('bi_begin',2)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //April
				$paid_rts_april = RincianTagihan::whereMonth('bi_begin',3)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_april = RincianTagihan::whereMonth('bi_begin',3)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_april = $paid_rts_april + $nopaid_rts_april;
				$sudah_tl_april = RincianTagihan::whereMonth('bi_begin',3)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_april = $total_rts_april - $sudah_tl_april;
				$percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
				$percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
				$jumlah_rt_april = RincianTagihan::whereMonth('bi_begin',3)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_april != 0) {
					$percent_jumlah_rt_april = $jumlah_rt_april/ $total_rts_april * 100;
				}else{
					$percent_jumlah_rt_april = 0;
				}
				$tanggal_upload_april = RincianTagihan::whereMonth('bi_begin',3)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Februari
				$paid_rts_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
				$sudah_tl_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
				$percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
				$percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
				$jumlah_rt_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_februari != 0) {
					$percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
				}else{
					$percent_jumlah_rt_februari = 0;
				}
				$tanggal_upload_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

				$paid_rts_mei = RincianTagihan::whereMonth('bi_begin',4)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_mei = RincianTagihan::whereMonth('bi_begin',4)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
				$sudah_tl_mei = RincianTagihan::whereMonth('bi_begin',4)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
				$percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
				$percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
				$jumlah_rt_mei = RincianTagihan::whereMonth('bi_begin',4)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_mei != 0) {
					$percent_jumlah_rt_mei =  $jumlah_rt_mei / $total_rts_mei * 100;
				}else{
					$percent_jumlah_rt_mei = 0;
				}
				$tanggal_upload_mei = RincianTagihan::whereMonth('bi_begin',4)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //Juni
				$paid_rts_juni = RincianTagihan::whereMonth('bi_begin',5)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juni = RincianTagihan::whereMonth('bi_begin',5)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
				$sudah_tl_juni = RincianTagihan::whereMonth('bi_begin',5)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
				$percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
				$percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
				$jumlah_rt_juni = RincianTagihan::whereMonth('bi_begin',5)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juni != 0) {
					$percent_jumlah_rt_juni =  $jumlah_rt_juni / $total_rts_juni * 100;
				}else{
					$percent_jumlah_rt_juni = 0;
				}
				$tanggal_upload_juni = RincianTagihan::whereMonth('bi_begin',5)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //Juli
				$paid_rts_juli = RincianTagihan::whereMonth('bi_begin',6)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juli = RincianTagihan::whereMonth('bi_begin',6)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
				$sudah_tl_juli = RincianTagihan::whereMonth('bi_begin',6)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status',1)->count();
				$belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
				$percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
				$percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
				$jumlah_rt_juli = RincianTagihan::whereMonth('bi_begin',6)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juli != 0) {
					$percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
				}else{
					$percent_jumlah_rt_juli = 0;
				}
				$tanggal_upload_juli = RincianTagihan::whereMonth('bi_begin',6)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //Agustus
				$paid_rts_agustus = RincianTagihan::whereMonth('bi_begin',7)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_agustus = RincianTagihan::whereMonth('bi_begin',7)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
				$sudah_tl_agustus = RincianTagihan::whereMonth('bi_begin',7)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
				$percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
				$percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
				$jumlah_rt_agustus = RincianTagihan::whereMonth('bi_begin',7)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_agustus != 0) {
					$percent_jumlah_rt_agustus =  $jumlah_rt_agustus / $total_rts_agustus * 100;
				}else{
					$percent_jumlah_rt_agustus = 0;
				}
				$tanggal_upload_agustus = RincianTagihan::whereMonth('bi_begin',7)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //September
				$paid_rts_september = RincianTagihan::whereMonth('bi_begin',8)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_september = RincianTagihan::whereMonth('bi_begin',8)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_september = $paid_rts_september + $nopaid_rts_september;
				$sudah_tl_september = RincianTagihan::whereMonth('bi_begin',8)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_september = $total_rts_september - $sudah_tl_september;
				$percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
				$percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
				$jumlah_rt_september = RincianTagihan::whereMonth('bi_begin',8)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_september != 0) {
					$percent_jumlah_rt_september =  $jumlah_rt_september / $total_rts_september * 100;
				}else{
					$percent_jumlah_rt_september = 0;
				}
				$tanggal_upload_september = RincianTagihan::whereMonth('bi_begin',8)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //Oktober
				$paid_rts_oktober = RincianTagihan::whereMonth('bi_begin',9)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_oktober = RincianTagihan::whereMonth('bi_begin',9)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
				$sudah_tl_oktober = RincianTagihan::whereMonth('bi_begin',9)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
				$percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
				$percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
				$jumlah_rt_oktober = RincianTagihan::whereMonth('bi_begin',9)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_oktober != 0) {
					$percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
				}else{
					$percent_jumlah_rt_oktober = 0;
				}
				$tanggal_upload_oktober = RincianTagihan::whereMonth('bi_begin',9)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //November
				$paid_rts_november = RincianTagihan::whereMonth('bi_begin',10)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_november = RincianTagihan::whereMonth('bi_begin',10)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_november = $paid_rts_november + $nopaid_rts_november;
				$sudah_tl_november = RincianTagihan::whereMonth('bi_begin',10)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_november = $total_rts_november - $sudah_tl_november;
				$percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
				$percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
				$jumlah_rt_november = RincianTagihan::whereMonth('bi_begin',10)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_november != 0) {
					$percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
				}else{
					$percent_jumlah_rt_november = 0;
				}
				$tanggal_upload_november = RincianTagihan::whereMonth('bi_begin',10)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

              //Desember
				$paid_rts_desember = RincianTagihan::whereMonth('bi_begin',11)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_desember = RincianTagihan::whereMonth('bi_begin',11)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
				$sudah_tl_desember = RincianTagihan::whereMonth('bi_begin',11)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
				$percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
				$percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
				$jumlah_rt_desember = RincianTagihan::whereMonth('bi_begin',11)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_desember != 0) {
					$percent_jumlah_rt_desember =  $jumlah_rt_desember / $total_rts_desember * 100;
				}else{
					$percent_jumlah_rt_desember = 0;
				}
				$tanggal_upload_desember = RincianTagihan::whereMonth('bi_begin',11)->where('upt', $getUptName)->orWhereNull('upt')->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//Oktober
				$paid_rts_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
				$sudah_tl_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
				$percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
				$percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
				$jumlah_rt_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_oktober != 0) {
					$percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
				}else{
					$percent_jumlah_rt_oktober = 0;
				}
				$tanggal_upload_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

//November
				$paid_rts_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_november = $paid_rts_november + $nopaid_rts_november;
				$sudah_tl_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_november = $total_rts_november - $sudah_tl_november;
				$percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
				$percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
				$jumlah_rt_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_november != 0) {
					$percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
				}else{
					$percent_jumlah_rt_november = 0;
				}
				$tanggal_upload_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

			}else{
                    //Januari
				$paid_rts_januari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_januari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
				$sudah_tl_januari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
				$percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
				$percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
				$jumlah_rt_januari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_januari != 0) {
					$percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
				}else{
					$percent_jumlah_rt_januari = 0;
				}
				$tanggal_upload_januari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');

                    //Februari
				$paid_rts_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
				$sudah_tl_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
				$percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
				$percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
				$jumlah_rt_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_februari != 0) {
					$percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
				}else{
					$percent_jumlah_rt_februari = 0;
				}
				$tanggal_upload_februari = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

                    //Maret
				$paid_rts_maret = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_maret = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
				$sudah_tl_maret = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
				$percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
				$percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
				$jumlah_rt_maret = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_maret != 0) {
					$percent_jumlah_rt_maret =  $jumlah_rt_maret / $total_rts_maret * 100;
				}else{
					$percent_jumlah_rt_maret = 0;
				}
				$tanggal_upload_maret = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //April
				$paid_rts_april = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_april = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_april = $paid_rts_april + $nopaid_rts_april;
				$sudah_tl_april = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_april = $total_rts_april - $sudah_tl_april;
				$percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
				$percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
				$jumlah_rt_april = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_april != 0) {
					$percent_jumlah_rt_april = $jumlah_rt_april/ $total_rts_april * 100;
				}else{
					$percent_jumlah_rt_april = 0;
				}
				$tanggal_upload_april = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Mei

				$paid_rts_mei = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_mei = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
				$sudah_tl_mei = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
				$percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
				$percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
				$jumlah_rt_mei = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_mei != 0) {
					$percent_jumlah_rt_mei =  $jumlah_rt_mei / $total_rts_mei * 100;
				}else{
					$percent_jumlah_rt_mei = 0;
				}
				$tanggal_upload_mei = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Juni
				$paid_rts_juni = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juni = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
				$sudah_tl_juni = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
				$percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
				$percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
				$jumlah_rt_juni = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juni != 0) {
					$percent_jumlah_rt_juni =  $jumlah_rt_juni / $total_rts_juni * 100;
				}else{
					$percent_jumlah_rt_juni = 0;
				}
				$tanggal_upload_juni = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Juli
				$paid_rts_juli = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_juli = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
				$sudah_tl_juli = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('status',1)->count();
				$belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
				$percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
				$percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
				$jumlah_rt_juli = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_juli != 0) {
					$percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
				}else{
					$percent_jumlah_rt_juli = 0;
				}
				$tanggal_upload_juli = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Agustus
				$paid_rts_agustus = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_agustus = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
				$sudah_tl_agustus = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
				$percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
				$percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
				$jumlah_rt_agustus = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_agustus != 0) {
					$percent_jumlah_rt_agustus =  $jumlah_rt_agustus / $total_rts_agustus * 100;
				}else{
					$percent_jumlah_rt_agustus = 0;
				}
				$tanggal_upload_agustus = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //September
				$paid_rts_september = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_september = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_september = $paid_rts_september + $nopaid_rts_september;
				$sudah_tl_september = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_september = $total_rts_september - $sudah_tl_september;
				$percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
				$percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
				$jumlah_rt_september = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_september != 0) {
					$percent_jumlah_rt_september =  $jumlah_rt_september / $total_rts_september * 100;
				}else{
					$percent_jumlah_rt_september = 0;
				}
				$tanggal_upload_september = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Oktober
				$paid_rts_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
				$sudah_tl_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
				$percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
				$percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
				$jumlah_rt_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_oktober != 0) {
					$percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
				}else{
					$percent_jumlah_rt_oktober = 0;
				}
				$tanggal_upload_oktober = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //November
				$paid_rts_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_november = $paid_rts_november + $nopaid_rts_november;
				$sudah_tl_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_november = $total_rts_november - $sudah_tl_november;
				$percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
				$percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
				$jumlah_rt_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_november != 0) {
					$percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
				}else{
					$percent_jumlah_rt_november = 0;
				}
				$tanggal_upload_november = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');

                    //Desember
				$paid_rts_desember = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$nopaid_rts_desember = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				$total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
				$sudah_tl_desember = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
				$belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
				$percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
				$percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
				$jumlah_rt_desember = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
				if ($jumlah_rt_desember != 0) {
					$percent_jumlah_rt_desember =  $jumlah_rt_desember / $total_rts_desember * 100;
				}else{
					$percent_jumlah_rt_desember = 0;
				}
				$tanggal_upload_desember = RincianTagihan::where('upt', Auth::user()->upt)->whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->distinct()->value('updated_at');
			}
			foreach($list_month as $bulan){

				if($bulan->id_bulan == 1){
					$total = $total_rts_januari; 
					$paid = $paid_rts_januari; 
					$sudah_tl = $sudah_tl_januari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_januari), 0, 5).'%'; 
					$belum_tl = $belum_tl_januari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_januari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_januari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_januari), 0, 5).'%';
					if($tanggal_upload_januari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_januari)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}

				if($bulan->id_bulan == 2){
					$total = $total_rts_februari; 
					$paid = $paid_rts_februari; 
					$sudah_tl = $sudah_tl_februari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_februari), 0, 5).'%'; 
					$belum_tl = $belum_tl_februari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_februari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_februari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_februari), 0, 5).'%';
					if($tanggal_upload_februari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_februari)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 3){
					$total = $total_rts_maret; 
					$paid = $paid_rts_maret; 
					$sudah_tl = $sudah_tl_maret; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_maret), 0, 5).'%'; 
					$belum_tl = $belum_tl_maret; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_maret), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_maret;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_maret), 0, 5).'%';
					if($tanggal_upload_maret ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_maret)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 4){
					$total = $total_rts_april; 
					$paid = $paid_rts_april; 
					$sudah_tl = $sudah_tl_april; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_april), 0, 5).'%'; 
					$belum_tl = $belum_tl_april; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_april), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_april;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_april), 0, 5).'%';
					if($tanggal_upload_april ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_april)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 5){
					$total = $total_rts_mei; 
					$paid = $paid_rts_mei; 
					$sudah_tl = $sudah_tl_mei; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_mei), 0, 5).'%'; 
					$belum_tl = $belum_tl_mei; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_mei), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_mei;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_mei), 0, 5).'%';
					if($tanggal_upload_mei ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_mei)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 6){
					$total = $total_rts_juni; 
					$paid = $paid_rts_juni; 
					$sudah_tl = $sudah_tl_juni; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juni), 0, 5).'%'; 
					$belum_tl = $belum_tl_juni; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juni), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juni;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juni), 0, 5).'%';
					if($tanggal_upload_juni ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juni)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 7){
					$total = $total_rts_juli; 
					$paid = $paid_rts_juli; 
					$sudah_tl = $sudah_tl_juli; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juli), 0, 5).'%'; 
					$belum_tl = $belum_tl_juli; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juli), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juli;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juli), 0, 5).'%';
					if($tanggal_upload_juli ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juli)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 8){
					$total = $total_rts_agustus; 
					$paid = $paid_rts_agustus; 
					$sudah_tl = $sudah_tl_agustus; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_agustus), 0, 5).'%'; 
					$belum_tl = $belum_tl_agustus; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_agustus), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_agustus;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_agustus), 0, 5).'%';
					if($tanggal_upload_agustus ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_agustus)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 9){
					$total = $total_rts_september; 
					$paid = $paid_rts_september; 
					$sudah_tl = $sudah_tl_september; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_september), 0, 5).'%'; 
					$belum_tl = $belum_tl_september; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_september), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_september;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_september), 0, 5).'%';
					if($tanggal_upload_september ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_september)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 10){
					$total = $total_rts_oktober; 
					$paid = $paid_rts_oktober; 
					$sudah_tl = $sudah_tl_oktober; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_oktober), 0, 5).'%'; 
					$belum_tl = $belum_tl_oktober; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_oktober), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_oktober;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_oktober), 0, 5).'%';
					if($tanggal_upload_oktober ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_oktober)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 11){
					$total = $total_rts_november; 
					$paid = $paid_rts_november; 
					$sudah_tl = $sudah_tl_november; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_november), 0, 5).'%'; 
					$belum_tl = $belum_tl_november; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_november), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_november;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_november), 0, 5).'%';
					if($tanggal_upload_november ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_november)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				if($bulan->id_bulan == 12){
					$total = $total_rts_desember; 
					$paid = $paid_rts_desember; 
					$sudah_tl = $sudah_tl_desember; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_desember), 0, 5).'%'; 
					$belum_tl = $belum_tl_desember; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_desember), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_desember;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_desember), 0, 5).'%';
					if($tanggal_upload_desember ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_desember)->format('d/m/Y');
					}else{
						$tanggalUpload = 'BELUM MENGIRIM';
					}
				}
				array_push($push, array(
					$bulan->nama,
					$total,
					$paid,
					$sudah_tl,
					$percent_sudah_tl,
					$belum_tl,
					$percent_belum_tl,
					$jumlah_tl,
					$percent_jumlah,
					$tanggalUpload,
				));

				$export = new RTUptTahunExportNotPaid([          
					$push,
				]);
			}
		}

		return Excel::download($export, 'RT-UPT-TAHUN-'.$getUptName.'.xlsx');
	}

	public function exportPerMonthYear($rts_data,$month,$year,$status_pembayaran,$getUptName, $bi_type)
	{	
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$upt = UPT::select('office_id','office_name','province_name')->distinct('office_name')->get();
		$push = [];
		if($month == 1){
			$fixMonth = 12;
			$fixYear = $year - 1;
		} else{
			$fixMonth = $month - 1;
			$fixYear = $year;
		}
		$list_month = ListMonth::all();


		$export = new RTExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			foreach ($upt as $key => $value) {
				$checkingPengiriman = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();

				if($checkingPengiriman == 0){
					$countCheckingPengiriman = 'BELUM';
				}else{
					$countCheckingPengiriman = 'YA';
				}

				array_push($push, array(
					$value->office_name,
					$countCheckingPengiriman,
				));

				$export = new RTUptBulanTahunExport([          
					$push,
				]);

			}
			return Excel::download($export, 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx');
		}else{
			foreach($upt as $value){
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
				$total_rts = $paid_rts + $nopaid_rts;
				if ($paid_rts == 0) {
					$rt_terbayar = 0;
				}else{
					$rt_terbayar = $paid_rts / $total_rts * 100;
				}
				$sudah_tl = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status','not paid')->where('active',1)->count();
				$belum_tl = $nopaid_rts - $sudah_tl;
				if ($sudah_tl == 0) {
					$rt_yang_tl = 0;
				}else{  
					$rt_yang_tl = $sudah_tl / $nopaid_rts * 100;
				}
				$tanggal_upload = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct('upt')->value('updated_at');
				
				$jumlah_rt = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
				if($tanggal_upload ){
					$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload)->format('d/m/Y');
				}else if($sudah_tl == 0 && $belum_tl == 0 && $nopaid_rts == 0 && $rt_yang_tl == 0 ){
					$tanggalUpload = 'NIHIL';
				}else{
					$tanggalUpload = 'BELUM MENGIRIM';
				}

				array_push($push, array(
					$value->office_name,
					''.$total_rts.'',
					''.$paid_rts.'',
					''.number_format($rt_terbayar, 2).'%',
					''.$nopaid_rts.'',
					''.$sudah_tl.'',
					''.$belum_tl.'',
					''.number_format($rt_yang_tl, 2).'%',
					''.$jumlah_rt.'',
					$tanggalUpload,
				));

				$export = new RTUptTahunExportNotPaid([          
					$push,
				]);
			}
		}

		return Excel::download($export, 'RT-UPT-BULAN-TAHUN-'. date('d F Y') .'.xlsx');
	}

	public function approved(Request $request){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
			if ($request->getStatus == 0) {
				$ids = $request->id;
				if ($request->id) {
					$action = $request->action;

					DB::table('rincian_tagihans')->whereIn('no_spp', $ids)
					->update(['active' => $action,'ket_reject' => $request->catatankasi, 'updated_at' => date("Y-m-d H:i:s")]);
					Session::flash('info', 'Rincian Tagihan');
					Session::flash('colors', 'green');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Sudah Mengapprove Data!');
					return redirect()->back();
				}
				else{
					Session::flash('info', 'Rincian Tagihan');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Belum Memilih Data!');
					return redirect()->back();
				}	
			}
			else{
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Status Salah!');
				return redirect()->back();
			}

		}else{
			Session::flash('info', 'Rincian Tagihan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

	    // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new RtImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);
		// if($header[0] == 'Provinsi' && $header[1] == 'Nama UPT' && $header[2] == 'No. Tagihan' && $header[3] == 'No. Klien' && $header[4] == 'Nama Klien' && $header[5] == 'BHP (Rp)' && $header[6] == 'Tanggal (BI Begin)' && $header[7] == 'Status Pembayaran' && $header[8] == 'Service' && $header[9] == 'Tanggal Jatuh Tempo' && $header[10] == 'Tanggal Bayar' && $header[11] == 'Upaya / Methode' && $header[12] == 'Tanggal Upaya' && $header[13] == 'Keterangan'){
		if($header[0] == 'No. Tagihan' && $header[1] == 'No. Klien' && $header[2] == 'Nama Klien' && $header[3] == 'BHP (Rp)' && $header[4] == 'Tanggal (BI Begin)' && $header[5] == 'Status Pembayaran' && $header[6] == 'Service' && $header[7] == 'Tanggal Jatuh Tempo' && $header[8] == 'Upaya / Methode' && $header[9] == 'Tanggal Upaya' && $header[10] == 'Keterangan'){

			foreach($result as $key => $values){
				$tanggal_upaya = $values[7];
				if(is_int($values[7])){
					try{
						$format_tanggal_upaya = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($values[7]));
						$tanggal_upaya = $format_tanggal_upaya->format('Y-m-d');							
					}catch (Exception $e){
						Session::flash('info', 'RT');
						Session::flash('colors', 'red');
						Session::flash('icons', 'fas fa-clipboard-check');
						Session::flash('alert', 'Mohon isi data di cek kembali!');		
						return redirect()->back();
					}
				}
			}
			// return \Carbon\Carbon::parse($result[0][9])->format('d, M Y');
				// return $result;
			return view('templates.SPP.RT.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'RT');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');

		$data = $request->all();
		$no_spp = $data['no_spp'];
		$upayametode = $data['upayametode'];
		$tanggal_upaya = $data['tanggal_upaya'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			$rt = RincianTagihan::where('no_spp',$value)->first();
			if($rt){
				$uniq = uniqid();
				if(isset($filename[$key])){
					$filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
				}
				$rt->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
				$rt->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
				$rt->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
				$rt->ket_operator = $request->ket_operator;
				$rt->created_by = Auth::user()->id;
				$rt->bukti_dukung = isset($filename[$key]) ? 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension() : '';
				$rt->active = 0;
				$rt->save();
			}else{
				Session::flash('info', 'Status Tagihan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Data SPP ada yang tidak sesuai dengan data di data aplikasi!');		
				return redirect(url('spp/st'));
			}
		}
		Session::flash('info', 'Rincian Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect(url('spp/rt'));
	}

	public function downloadSampel(){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		if ($month == 1) {			
			$dataBelumTerbayar = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBelumTerbayar = $month - 1; 
			$fixYear = $year;
		}

		$tanggal_upaya = Date('Y-m-d');
		$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
		$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
		$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		$tes = json_decode($rts_nopaid);
		foreach ($tes as $key => $value) {
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				if($value->upaya_metode){
					$upaya_metode = Metode::where('id', $value->upaya_metode)->first();
				}else{
					$upaya_metode = '';
				}
				
				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->kode_upt;

				if($value->upaya_metode){
					$upaya_metode = Metode::where('id', $value->upaya_metode)->value('metode');
				}else{
					$upaya_metode = '';
				}

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}

			$tanggal_jatuh_tempo = \Carbon\Carbon::parse($value->bi_begin)->subDays(1)->format('Y-m-d');

			array_push($push, array(
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$value->tagihan,
				$value->bi_begin,
				'Belum Bayar',
				$value->service_name,
				$tanggal_jatuh_tempo,
				// $value->bi_money_received,
				$upaya_metode,
				$tanggal_upaya,
				$value->keterangan,

			));
			// array_push($push, array(
			// 	$provinsi,
			// 	$nama_upt,
			// 	$value->no_spp,
			// 	$value->no_klien,
			// 	$value->nama_perusahaan,
			// 	$value->tagihan,
			// 	$value->bi_begin,
			// 	'Belum Bayar',
			// 	$value->service_name,
			// 	$tanggal_jatuh_tempo,
			// 	$value->bi_money_received,
			// 	'',
			// 	$tanggal_upaya,
			// 	'',

			// ));

		}
		$export = new sampelRTExport([          
			$push,
		]);

		return Excel::download($export, 'RT IMPORT '. date('d F Y') .'.xlsx');

	}
}
