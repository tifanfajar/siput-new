<?php

namespace App\Http\Controllers\Core\Pelayanan\PenangananPiutang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service\PenangananPiutang;
use Illuminate\Support\Facades\Crypt;
use App\Imports\PenangananPiutangImport;
use App\Exports\PenangananPiutangExport;
use App\Exports\PenangananPiutangExportId;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Setting\Perusahaan;
use App\Model\Privillage\Role;
use App\Model\KPNKL;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class PenangananPiutangController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		$data['getStatus'] = Role::where('id', Auth::user()->role_id)->value('akses');
		return view('templates.pelayanan.penanganan-piutang.core.index')->with($data);
	}

	protected function filteringByLocation($petang)
	{
		if (Auth::user()->province_code != 0 && Auth::user()->upt != 0) {
			$petang = $petang->where('kode_upt', Auth::user()->upt);
			$petang = $petang->where('id_prov', Auth::user()->province_code);
		}
		return $petang;
	}

	protected function table($request, $id = null)
	{	
		$data = [];
		$petang = new PenangananPiutang;
		$petang = $this->filteringByLocation($petang);
		if (Role::where('id', Auth::user()->role_id)->value('akses') == 'administrator') {
			if (!is_null($id)) {
				$changeUPT = $id;
				$provinceCode = PenangananPiutang::where('kode_upt', $changeUPT)->value('id_prov');
				if ($changeUPT) {
					$petang = $petang->where('kode_upt', $changeUPT);
				}
			}
			if ($request->filled('id_map')) {
				$changeID = Provinsi::where('id_map', $request->id_map)->value('id_row');
				$changeUPT = UPT::where('province_code', $changeID)->select('office_id','office_name')->distinct()->value('office_id');
				$provinceCode = PenangananPiutang::where('kode_upt', $changeUPT)->value('id_prov');
				$getUPT = UPT::where('province_code', $changeID)->select('office_name','office_id')->distinct()->get();
				if ($changeUPT) {
					$count = $getUPT->count();
					if ($count <= 1) {
						$petang = $petang->where('kode_upt', $changeUPT);
					}
				}
			}
		}
		if ($request->filled('dari') && $request->filled('sampai')) {
			$petang = $petang->whereBetween('created_at', array($request->dari, $request->sampai));
		}

		$data = $petang->where('status', $request->status)->get();
		return datatables()->of($data)
		->editColumn('id', function($data) {
			return Crypt::encryptString($data['id']);
		})
		->editColumn('province', function($data) {
			return Provinsi::where('id', $data['id_prov'])->value('nama');
		})
		->editColumn('upt_name', function($data) {
			return UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
		})
		->editColumn('company_id', function($data) {
			return Perusahaan::where('id', $data['no_client'])->value('company_id');
		})
		->editColumn('company_name', function($data) {
			return Perusahaan::where('id', $data['no_client'])->value('name');
		})
		->editColumn('submission_value', function($data) {
			return 'Rp. '.number_format($data['nilai_penyerahan'], 2);
		})
		->editColumn('kpknl_name', function($data) {
			return KPNKL::where('id', $data['nama_kpknl'])->value('kpknl_name');
		})
		->editColumn('paid_off', function($data) {
			return 'Rp. '.number_format($data['lunas'], 2);
		})
		->editColumn('installments', function($data) {
			return 'Rp. '.number_format($data['angsuran'], 2);
		})
		->editColumn('psbdt', function($data) {
			return 'Rp. '.number_format($data['psbdt'], 2);
		})
		->editColumn('cancellation', function($data) {
			return 'Rp. '.number_format($data['pembatalan'], 2);
		})
		->editColumn('remaining_receivable', function($data) {
			return 'Rp. '.number_format($data['sisa_piutang'], 2);
		})
		->make(true);
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereBetween('tanggal', array($dari, $sampai))->get();
			}else{
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.pelayanan.penanganan-piutang.dev.print-search', $petang)->setPaper('a4','landscape');
			return $pdf->download('Penanganan Piutang '.$getUptName.'-'.date('d-F-Y').'.pdf');
		}
	}
	public function export_search(Request $request, $id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang = PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereBetween('tanggal', array($dari, $sampai))->get();
			}else{
				$petang = PenangananPiutang::where('kode_upt',$id)->where('status',1)->get();;
			}

			$export = new PenangananPiutangExport([          
				$push,
			]);

			foreach ($petang as $key => $value) {			
				$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				$kabupaten = KabupatenKota::where('id',$value->nama_kpknl)->value('nama');
				$kpknl = KPNKL::where('id', $value->nama_kpknl)->value('kpknl_name');
				$no_client = Perusahaan::where('id',$value->no_client)->value('company_id');
				$perusahaan = Perusahaan::where('id', $value->no_client)->value('name');


				array_push($push, array(
					$value->id,
					$provinsi,
					$nama_upt,
					$no_client,
					$perusahaan,
					$value->nilai_penyerahan,
					$value->tahun_pelimpahan,
					$kpknl,
					$value->tahapan_pengurusan,
					$value->lunas,
					$value->angsuran,
					$value->tanggal,
					$value->psbdt,
					$value->tanggal_psbdt,
					$value->pembatalan,
					$value->tanggal_pembatalan,
					$value->created_at	,
					$value->updated_at,
					$value->sisa_piutang,
					$value->keterangan,
				));

				$export = new PenangananPiutangExport([          
					$push,
				]);

			}
			return Excel::download($export, 'Penanganan Piutang '.$changeName.'-'.date('d-F-Y').'.xlsx');
		}
	}

	public function searchMultiple($id, Request $request){
		if ($request->type == 'WEB') {
			return $this->table($request, $id);
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$provinceCode = PenangananPiutang::where('kode_upt',$changeUPT)->value('id_prov');
			if ($changeUPT) {
				$petang = PenangananPiutang::where('kode_upt',$changeUPT)->get();
				return view('templates.pelayanan.penanganan-piutang.search.index',compact('petang','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function save(Request $request){
		$petang = new PenangananPiutang;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$petang->id_prov = $request->id_prov;
			$petang->kode_upt = $request->id_upt;
			$petang->nama_kpknl = $request->nama_kpknl;
		}else{
			$petang->id_prov = Auth::user()->province_code;
			$petang->kode_upt = Auth::user()->upt;
			$petang->nama_kpknl = $request->nama_kpknl;
		}
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->nama_kpknl = $request->nama_kpknl;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;

		$petang->tanggal_psbdt = $request->tanggal_psbdt;
		$petang->tanggal_pembatalan = $request->tanggal_pembatalan;

		$petang->keterangan = $request->keterangan;
		$petang->created_by = Auth::user()->id;
		$petang->status = 0;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$petang = PenangananPiutang::find($id);
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;

		$petang->tanggal_psbdt = $request->tanggal_psbdt;
		$petang->tanggal_pembatalan = $request->tanggal_pembatalan;

		$petang->keterangan = $request->keterangan;
		$petang->created_by = Auth::user()->id;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}
	public function updateUnencrypt(Request $request)
	{
		$id = $request->id;
		$petang = PenangananPiutang::find($id);
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->nama_kpknl = $request->nama_kpknl;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;

		$petang->tanggal_psbdt = $request->tanggal_psbdt;
		$petang->tanggal_pembatalan = $request->tanggal_pembatalan;

		$petang->keterangan = $request->keterangan;
		$petang->created_by = Auth::user()->id;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		PenangananPiutang::where('id',$id)->delete();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}

	public function updateRejected(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$petang = PenangananPiutang::find($id);
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;

		$petang->tanggal_psbdt = $request->tanggal_psbdt;
		$petang->tanggal_pembatalan = $request->tanggal_pembatalan;

		$petang->keterangan = $request->keterangan;;
		$petang->created_by = Auth::user()->id;
		$petang->status = 3;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function approved(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
			$ids = [];
			foreach ($request->id as $key => $value) {
				$ids[] = Crypt::decryptString($value);
			}
			if ($request->id) {
				$action = $request->action;
				DB::table('penanganan_piutangs')->whereIn('id', $ids)
				->update(['status' => $action]);
				if ($action == 1) {
					Session::flash('info', 'Penanganan Piutang');
					Session::flash('colors', 'green');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Berhasil Mengapprove Data!');
				}else{
					Session::flash('info', 'Penanganan Piutang');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Berhasil Mereject Data!');
				}
				return redirect()->back();
			}
			else{
				Session::flash('info', 'Penanganan Piutang');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Belum Memilih Data!');
				return redirect()->back();
			}

		}else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function reupload($id){
		$id = Crypt::decryptString($id);
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator') {
			$petang = PenangananPiutang::find($id);
			$petang->status = 0;
			$petang->save();
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'green');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Berhasil Mengirim Ulang');
			return redirect()->back();
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function search(Request $request){
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$provinceCode = PenangananPiutang::where('kode_upt',$changeUPT)->value('id_prov');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.pelayanan.penanganan-piutang.search.multiple',compact('getUPT'));
				}
				else{
					$petang = PenangananPiutang::where('kode_upt',$changeUPT)->get();
				}
				return view('templates.pelayanan.penanganan-piutang.search.index',compact('petang','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function print(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang['petang'] = PenangananPiutang::whereBetween('tanggal', array($dari, $sampai))->where('status', 1)->get();
			}else{
				$petang['petang'] = PenangananPiutang::where('status', 1)->get();
			}
		}else{
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang['petang'] = PenangananPiutang::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->whereBetween('tanggal', array($dari, $sampai))->where('status', 1)->get();
			}else{
				$petang['petang'] = PenangananPiutang::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
			}

		}
		$pdf = PDF::loadView('templates.pelayanan.penanganan-piutang.dev.print', $petang)->setPaper('a4','landscape');
		return $pdf->download('Data-Penanganan-Piutang-'.date('d-F-Y').'.pdf');
	}
	public function export(Request $request)
	{
		$push = [];
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang = PenangananPiutang::where('status',1)->whereBetween('created_at', array($dari, $sampai))->get();
			}else{
				$petang = PenangananPiutang::where('status',1)->get();
			}
		}
		else{
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$petang = PenangananPiutang::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->whereBetween('created_at', array($dari, $sampai))->get();
			}else{
				$petang = PenangananPiutang::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
			}
		}
		$export = new PenangananPiutangExport([          
			$push,
		]);

		foreach ($petang as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$kabupaten = KabupatenKota::where('id',$value->nama_kpknl)->value('nama');
			$kpknl = KPNKL::where('id', $value->nama_kpknl)->value('kpknl_name');
			$no_client = Perusahaan::where('id',$value->no_client)->value('company_id');
			$perusahaan = Perusahaan::where('id', $value->no_client)->value('name');


			array_push($push, array(
				$value->id,
				$provinsi,
				$nama_upt,
				$no_client,
				$perusahaan,
				$value->nilai_penyerahan,
				$value->tahun_pelimpahan,
				$kpknl,
				$value->tahapan_pengurusan,
				$value->lunas,
				$value->angsuran,
				$value->tanggal,
				$value->psbdt,
				$value->tanggal_psbdt,
				$value->pembatalan,
				$value->tanggal_pembatalan,
				$value->created_at	,
				$value->updated_at,
				$value->sisa_piutang,
				$value->keterangan,
			));

			$export = new PenangananPiutangExport([          
				$push,
			]);
		}
		return Excel::download($export, 'PenangananPiutang-'.date('d-F-Y').'.xlsx');
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

		// Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new PenangananPiutangImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);

		// return $header;

		if($header[0] == 'Nilai Penyerahan' && $header[1] == 'Tahun Pelimpahan' && $header[2] == 'Nama KPKNL' && $header[3] == 'Nama Perusahaan' && $header[4] == 'Tahapanan Pengurusan' && $header[5] == 'Lunas' && $header[6] == 'Angsuran' && $header[7] == 'Tanggal' && $header[8] == 'PSBDT' && $header[9] == 'Tanggal PSBDT' && $header[10] == 'Pembatalan'  && $header[11] == 'Tanggal Pembatalan' && $header[12] == 'Sisa Piutang' && $header[13] == 'Keterangan'){

			return view('templates.pelayanan.penanganan-piutang.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');        
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		$data = $request->all();
		$nilai_penyerahan = $data['nilai_penyerahan'];
		$nama_kpknl = $data['nama_kpknl'];
		$nama_perusahaan = $data['nama_perusahaan'];
		$tahun_pelimpahan = $data['tahun_pelimpahan'];            
		$tahapanan_pengurusan = $data['tahapanan_pengurusan'];
		$lunas = $data['lunas'];
		$angsuran = $data['angsuran'];
		$tanggal = $data['tanggal'];
		$psbdt = $data['psbdt'];
		$pembatalan = $data['pembatalan'];
		$sisa_piutang = $data['sisa_piutang'];

		$tanggal_psbdt = $data['tanggal_psbdt'];
		$tanggal_pembatalan = $data['tanggal_pembatalan'];

		$keterangan = $data['keterangan'];
		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$id_prov = $request->upt_provinsi;
			$kode_upt = $request->id_upt;                
		}
		else{
			$id_prov = Auth::user()->province_code;  
			$kode_upt = Auth::user()->upt;                
		}
		$rows = [];
		foreach($tanggal as $key => $input) {
			array_push($rows, [            
				'nilai_penyerahan' => isset($nilai_penyerahan[$key]) ? $nilai_penyerahan[$key] : '',
				'tahun_pelimpahan' => isset($tahun_pelimpahan[$key]) ? $tahun_pelimpahan[$key] : '',
				'nama_kpknl' => isset($nama_kpknl[$key]) ? $nama_kpknl[$key] : '',
				'tahapan_pengurusan' => isset($tahapanan_pengurusan[$key]) ? $tahapanan_pengurusan[$key] : '',
				'lunas' => isset($lunas[$key]) ? $lunas[$key] : '',
				'angsuran' => isset($angsuran[$key]) ? $angsuran[$key] : '',
				'tanggal' => isset($tanggal[$key]) ? $tanggal[$key] : '',
				'psbdt' => isset($psbdt[$key]) ? $psbdt[$key] : '',
				'pembatalan' => isset($pembatalan[$key]) ? $pembatalan[$key] : '',
				'sisa_piutang' => isset($sisa_piutang[$key]) ? $sisa_piutang[$key] : '',
				'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',

				'tanggal_psbdt' => isset($tanggal_psbdt[$key]) ? $tanggal_psbdt[$key] : '',
				'tanggal_pembatalan' => isset($tanggal_pembatalan[$key]) ? $tanggal_pembatalan[$key] : '',

				'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
				'created_by' => Auth::user()->id,            
				'no_client' => isset($nama_perusahaan[$key]) ? $nama_perusahaan[$key] : 0,
				'id_prov' => $id_prov,
				'status' => 0,
				'kode_upt' => $kode_upt,
				'created_at' => $created_at,
				'updated_at' => $updated_at,

			]);
		}
		$hasil = PenangananPiutang::insert($rows);
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');       
		return redirect(url('pelayanan/penanganan-piutang'));

	}

	public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelPenangananPiutang.xlsx"));
		ob_end_clean();
		return $responses;

	}

	public function getPerusahaan($id)
	{
		$petang = PenangananPiutang::where('no_client', $id)->where('status', '!=', '4')->first();

		if($petang){
			$perusahaan = $petang;
		}else{
			$perusahaan = false;
		}

		return $perusahaan;
	}

	public function getUpt($kode_upt)
	{
		if (!$kode_upt) {
			$html = '<option value="">Tidak Tersedia</option>';
		} else {
			$html = '';
			$upts = UPT::where('office_id',$kode_upt)->select('office_id','office_name')->distinct()->get();
			foreach ($upts as $upt) {
				if ($upt != '') {
					$html .= '<option value="'.$upt->office_id.'">'.$upt->office_name.'</option>';
				}
				else{
					$html = '<option value="">Tidak Tersedia</option>';
				}
			}
		}

		return response()->json(['html' => $html]);
	}

	public function getKabKot($nama_kpknl)
	{
		if (!$nama_kpknl) {
			$html = '<option value="">Tidak Tersedia</option>';
		} else {
			$html = '';
			$upts = KabupatenKota::where('id',$nama_kpknl)->select('id','nama')->distinct()->get();
			foreach ($upts as $upt) {
				if ($upt != '') {
					$html .= '<option value="'.$upt->id.'">'.$upt->nama.'</option>';
				}
				else{
					$html = '<option value="">Tidak Tersedia</option>';
				}
			}
		}if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$petang = PenangananPiutang::all();
		}
		else{
			$petang = PenangananPiutang::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->get();
		}

		return response()->json(['html' => $html]);
	}

	public function filterData(Request $request)
	{
		if ($request->type == 'WEB') {
			return $this->table($request);
		}
		$data['getStatus'] = Role::where('id', Auth::user()->role_id)->value('akses');
		Session::flash('info', 'Filter Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Data Berhasil Diambil!');
		return view('templates.pelayanan.penanganan-piutang.core.index')->with($data);
	}
}
