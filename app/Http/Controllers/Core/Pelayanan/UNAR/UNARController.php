<?php

namespace App\Http\Controllers\Core\Pelayanan\UNAR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service\Unar;
use Illuminate\Support\Facades\Crypt;
use App\Model\Notification\Notification;
use App\Imports\UnarImport;
use App\Exports\UnarExport;
use App\Exports\UnarExportId;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Privillage\Role;
use Carbon\Carbon;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class UNARController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request){
		$unar = new Unar;
		$tahun_sekarang = date('Y');
		$unar_data = \App\RencanaUnar::where('tahun', $tahun_sekarang)->first();
		$getUpt = new \App\Model\Setting\UPT;

		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$getUpt = $getUpt->distinct('office_id')->count();
		}
		else{
			$unar = $unar->where('kode_upt', Auth::user()->upt);
			$unar = $unar->where('id_prov', Auth::user()->province_code);
			$getUpt = $getUpt->where('office_id',Auth::user()->upt)->count();
		}
		$unar = $unar->get();

		if ($request->filled('type') && $request->type = 'WEB') {
			$unar = datatables()->of($unar)
			->editColumn('id', function($data) {
				return Crypt::encryptString($data['id']);
			})
			->editColumn('year', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('Y');
			})
			->editColumn('month', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('M');
			})
			->editColumn('date', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('d, M Y');
			})
			->editColumn('province', function($data) {
				return \App\Model\Region\Provinsi::where('id', $data['id_prov'])->value('nama');
			})
			->editColumn('upt_name', function($data) {
				return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
			})
			->editColumn('exam_date', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_ujian'])->format('d,M Y');
			})
			->editColumn('unar_type', function($data) use ($unar) {
				switch ($data['tipe']) {
					case '1':
					return 'REGULER';
					break;
					case '0':
					return 'NON-REGULER';
					break;
					default:
					return '-';
					break;
				}
			})
			->editColumn('all_total', function($data) {
				return $data['jumlah_siaga'] + $data['jumlah_penggalang'] + $data['jumlah_penegak'];
			})
			->editColumn('pass_total', function($data) {
				return $data['lulus_siaga'] + $data['lulus_penggalang'] + $data['lulus_penegak'];
			})
			->editColumn('not_pass_total', function($data) {
				return $data['tdk_lulus_siaga'] + $data['tdk_lulus_penggalang'] + $data['tdk_lulus_penegak'];
			})
			->editColumn('created_by', function($data) {
				return Auth::user()->where('id', $data['created_by'])->value('name');
			})
			->editColumn('city', function($data) {
				return \App\Model\Region\KabupatenKota::where('id', $data['id_kabkot'])->value('nama');
			})
			->make(true);
			return $unar;
		}

		return view('templates.pelayanan.unar.core.index',compact('unar','getUpt','unar_data'));
	}

	public function preview(Request $request){
		$date = $request->date;
		$month = $request->month;
		$year = $request->year;
		if ($date && $month && $year) {
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
		}elseif($date && $month){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
		}elseif($date){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->get();
		}elseif($month && $year){
			$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
		}elseif($date && $year){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
		}elseif($month){
			$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->get();
		}elseif($year){
			$unar['unar'] = Unar::whereYear('tanggal_ujian',$year)->get();
		}else{
			$unar['unar'] = Unar::all();
		}
		$pdf = PDF::loadView('templates.pelayanan.unar.dev.print', $unar)->setPaper('a3','landscape');
		return $pdf->stream('Data-Unar-'.date('d-F-Y').'.pdf');
	}

	public function print(Request $request){
// $date = $request->date;
// $month = $request->month;
// $year = $request->year;
		$dari = $request->dari;
		$sampai = $request->sampai;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
// if ($date && $month && $year) {
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $month){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($date){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->get();
// }elseif($month && $year){
// 	$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $year){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
// }elseif($month){
// 	$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->get();
// }elseif($year){
// 	$unar['unar'] = Unar::whereYear('tanggal_ujian',$year)->get();
// }else{
// 	$unar['unar'] = Unar::all();
// }
			if ($dari && $sampai) {
				$unar['unar'] = Unar::whereBetween('tanggal_ujian', array($dari, $sampai))->get();
			}else{
				$unar['unar'] = Unar::all();
			}
		}else{
// if ($date && $month && $year) {
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($date && $month){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($date){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($month && $year){
// 	$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($date && $year){
// 	$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($month){
// 	$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->where('kode_upt', Auth::user()->upt)->get();
// }elseif($year){
// 	$unar['unar'] = Unar::whereYear('tanggal_ujian',$year)->where('kode_upt', Auth::user()->upt)->get();
// }else{
// 	$unar['unar'] = Unar::all();
// }
			if ($dari && $sampai) {
				$unar['unar'] = Unar::whereBetween('tanggal_ujian', array($dari, $sampai))->where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
			}else{
				$unar['unar'] = Unar::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
			}
		}
		$pdf = PDF::loadView('templates.pelayanan.unar.dev.print', $unar)->setPaper('a3','landscape');
		return $pdf->download('Data-Unar-'.date('d-F-Y').'.pdf');
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
// $date = $request->date;
// $month = $request->month;
// $year = $request->year;
// if ($date && $month && $year) {
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $month){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($date){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->get();
// }elseif($month && $year){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $year){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
// }elseif($month){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($year){
// 	$unar['unar'] = Unar::where('kode_upt',$id)->whereYear('tanggal_ujian',$year)->get();
// }else{
// 	$unar['unar'] = Unar::where('kode_upt',$id)->get();
// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$unar['unar'] = Unar::where('kode_upt',$id)->whereBetween('tanggal_ujian', array($dari, $sampai))->get();
			}else{
				$unar['unar'] = Unar::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.pelayanan.unar.dev.print', $unar)->setPaper('a3','landscape');
			return $pdf->download('UNAR '.$getUptName.'-'.date('d-F-Y').'.pdf');
		}
	}
	public function export(Request $request)
	{
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
	// $date = request()->date;
	// $month = request()->month;
	// $year = request()->year;
	// if ($date && $month && $year) {
	// 	$unar = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
	// }elseif($date && $month){
	// 	$unar = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
	// }elseif($date){
	// 	$unar = Unar::whereDay('tanggal_ujian',$date)->get();
	// }elseif($month && $year){
	// 	$unar = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
	// }elseif($date && $year){
	// 	$unar = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
	// }elseif($month){
	// 	$unar = Unar::whereMonth('tanggal_ujian',$month)->get();
	// }elseif($year){
	// 	$unar = Unar::whereYear('tanggal_ujian',$year)->get();
	// }else{
	// 	$unar = Unar::get();;
	// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$unar = Unar::whereBetween('tanggal_ujian', array($dari, $sampai))->get();
			}else{
				$unar = Unar::all();
			}
		}
		else{
// $date = request()->date;
// $month = request()->month;
// $year = request()->year;

// if ($date && $month && $year) {
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $month){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($date){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->get();
// }elseif($month && $year){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $year){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
// }elseif($month){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereMonth('tanggal_ujian',$month)->get();
// }elseif($year){
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereYear('tanggal_ujian',$year)->get();
// }else{
// 	$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::get();;
// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$unar = Unar::whereBetween('tanggal_ujian', array($dari, $sampai))->where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
			}else{
				$unar = Unar::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
			}
		}
		$export = new UnarExport([          
			$push,
		]);

		foreach ($unar as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			if($value->full_cat == 1){
				$full_cat = "Iya";
			}elseif ($value->full_cat == 0) {
				$full_cat = "Tidak";
			}else{
				$full_cat = "-";
			}

// return $kabupaten;

			array_push($push, array(
				$key+1,
				$value->id_unar,
				$value->created_at,
				$provinsi,
				$nama_upt,
				$value->lokasi_ujian,
				$value->tanggal_ujian,
				$value->jumlah_siaga,
				$value->jumlah_penggalang,
				$value->jumlah_penegak,
				$value->lulus_siaga,
				$value->lulus_penggalang,
				$value->lulus_penegak,
				$value->tdk_lulus_siaga,
				$value->tdk_lulus_penggalang,
				$value->tdk_lulus_penegak,
				$full_cat,
			));

			$export = new UnarExport([          
				$push,
			]);
		}


		return Excel::download($export, 'Unar-'.date('d-F-Y').'.xlsx');
	}
	public function export_search(Request $request, $id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
// $date = request()->date;
// $month = request()->month;
// $year = request()->year;
// if ($date && $month && $year) {
// 	$unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $month){
// 	$unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($date){
// 	$unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->get();
// }elseif($month && $year){
// 	$unar = Unar::where('kode_upt', $id)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
// }elseif($date && $year){
// 	$unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
// }elseif($month){
// 	$unar = Unar::where('kode_upt', $id)->whereMonth('tanggal_ujian',$month)->get();
// }elseif($year){
// 	$unar = Unar::where('kode_upt', $id)->whereYear('tanggal_ujian',$year)->get();
// }else{
// 	$unar = Unar::where('kode_upt', $id)->get();;
// }
				$dari = $request->dari;
				$sampai = $request->sampai;
				if ($dari && $sampai) {
					$unar = Unar::where('kode_upt', $id)->whereBetween('tanggal_ujian', array($dari, $sampai))->get();
				}else{
					$unar = Unar::where('kode_upt', $id)->get();
				}
			}
			$export = new UnarExport([          
				$push,
			]);

			foreach ($unar as $key => $value) {			
				$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				if($value->full_cat == 1){
					$full_cat = "Iya";
				}elseif ($value->full_cat == 0) {
					$full_cat = "Tidak";
				}else{
					$full_cat = "-";
				}

// return $kabupaten;

				array_push($push, array(
					$key+1,
					$value->id_unar,
					$value->created_at,
					$provinsi,
					$nama_upt,
					$value->lokasi_ujian,
					$value->tanggal_ujian,
					$value->jumlah_siaga,
					$value->jumlah_penggalang,
					$value->jumlah_penegak,
					$value->lulus_siaga,
					$value->lulus_penggalang,
					$value->lulus_penegak,
					$value->tdk_lulus_siaga,
					$value->tdk_lulus_penggalang,
					$value->tdk_lulus_penegak,
					$full_cat,
				));

				$export = new UnarExport([          
					$push,
				]);
			}

			return Excel::download($export, 'UNAR '.$changeName.'-'.date('d-F-Y').'.xlsx');
		}
	}

	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			if ($changeUPT) {
				$unar = Unar::where('kode_upt', $changeUPT)->get();
				
				if ($request->filled('type') && $request->type = 'WEB') {
					$unar = datatables()->of($unar)
					->editColumn('id', function($data) {
						return Crypt::encryptString($data['id']);
					})
					->editColumn('year', function($data) {
						return \Carbon\Carbon::parse($data['created_at'])
						->format('Y');
					})
					->editColumn('month', function($data) {
						return \Carbon\Carbon::parse($data['created_at'])
						->format('M');
					})
					->editColumn('date', function($data) {
						return \Carbon\Carbon::parse($data['created_at'])
						->format('d, M Y');
					})
					->editColumn('province', function($data) {
						return \App\Model\Region\Provinsi::where('id', $data['id_prov'])->value('nama');
					})
					->editColumn('upt_name', function($data) {
						return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
					})
					->editColumn('exam_date', function($data) {
						return \Carbon\Carbon::parse($data['tanggal_ujian'])->format('d,M Y');
					})
					->editColumn('unar_type', function($data) use ($unar) {
						switch ($data['tipe']) {
							case '1':
							return 'REGULER';
							break;
							case '0':
							return 'NON-REGULER';
							break;
							default:
							return '-';
							break;
						}
					})
					->editColumn('all_total', function($data) {
						return $data['jumlah_siaga'] + $data['jumlah_penggalang'] + $data['jumlah_penegak'];
					})
					->editColumn('pass_total', function($data) {
						return $data['lulus_siaga'] + $data['lulus_penggalang'] + $data['lulus_penegak'];
					})
					->editColumn('not_pass_total', function($data) {
						return $data['tdk_lulus_siaga'] + $data['tdk_lulus_penggalang'] + $data['tdk_lulus_penegak'];
					})
					->editColumn('created_by', function($data) {
						return Auth::user()->where('id', $data['created_by'])->value('name');
					})
					->editColumn('city', function($data) {
						return \App\Model\Region\KabupatenKota::where('id', $data['id_kabkot'])->value('nama');
					})
					->make(true);
					return $unar;
				}

				$tahun_sekarang = date('Y');
				$getUpt = \App\Model\Setting\UPT::where('office_id',$changeUPT)->count();
				$unar_data = \App\RencanaUnar::where('tahun',$tahun_sekarang)->first();
				return view('templates.pelayanan.unar.search.index',compact('unar','changeUPT','getUpt','unar_data'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Unar');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function save(Request $request){
		date_default_timezone_set('Asia/Jakarta');
		$unar = new Unar;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$unar->id_prov = $request->id_prov;
			$unar->kode_upt = $request->id_upt;
		}else{
			$unar->id_prov = Auth::user()->province_code;
			$unar->kode_upt = Auth::user()->upt;
		}
		$unar->id_unar = $request->id_unar;
		$unar->tipe = $request->tipe;
		$unar->tanggal_ujian = $request->tanggal_ujian;
		$unar->lokasi_ujian = $request->lokasi_ujian;
		$unar->jumlah_siaga = $request->jumlah_siaga;
		$unar->jumlah_penggalang = $request->jumlah_penggalang;
		$unar->jumlah_penegak = $request->jumlah_penegak;
		$unar->lulus_siaga = $request->lulus_siaga;
		$unar->lulus_penggalang = $request->lulus_penggalang;
		$unar->lulus_penegak = $request->lulus_penegak;
		$unar->tdk_lulus_siaga = $request->tdk_lulus_siaga;
		$unar->tdk_lulus_penggalang = $request->tdk_lulus_penggalang;
		$unar->tdk_lulus_penegak = $request->tdk_lulus_penegak;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->id_unar."-".$request->tanggal_ujian."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/pelayanan/unar'), $cover);
			$unar->lampiran = $cover;	
		}
		$unar->full_cat = $request->full_cat;
		$unar->created_by = Auth::user()->id;
		$unar->save();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 10;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menambah Data UNAR';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'UNAR');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		date_default_timezone_set('Asia/Jakarta');
		$id = Crypt::decryptString($request->id);
		$unar = Unar::find($id);
		$unar->id_unar = $request->id_unar;
		$unar->tipe = $request->tipe;
		$unar->tanggal_ujian = $request->tanggal_ujian;
		$unar->lokasi_ujian = $request->lokasi_ujian;
		$unar->jumlah_siaga = $request->jumlah_siaga;
		$unar->jumlah_penggalang = $request->jumlah_penggalang;
		$unar->jumlah_penegak = $request->jumlah_penegak;
		$unar->lulus_siaga = $request->lulus_siaga;
		$unar->lulus_penggalang = $request->lulus_penggalang;
		$unar->lulus_penegak = $request->lulus_penegak;
		$unar->tdk_lulus_siaga = $request->tdk_lulus_siaga;
		$unar->tdk_lulus_penggalang = $request->tdk_lulus_penggalang;
		$unar->tdk_lulus_penegak = $request->tdk_lulus_penegak;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->id_unar."-".$request->tanggal_ujian."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/pelayanan/unar'), $cover);
			$unar->lampiran = $cover;	
		}
		$unar->full_cat = $request->full_cat;
		$unar->created_by = Auth::user()->id;
		$unar->save();
		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 10;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Loket UNAR';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Unar::where('id',$id)->delete();
		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 10;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Loket UNAR';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.pelayanan.unar.search.multiple',compact('getUPT'));
				}
				else{
					$unar = Unar::where('kode_upt',$changeUPT)->get();

					if ($request->filled('type') && $request->type = 'WEB') {
						$unar = datatables()->of($unar)
						->editColumn('id', function($data) {
							return Crypt::encryptString($data['id']);
						})
						->editColumn('year', function($data) {
							return \Carbon\Carbon::parse($data['created_at'])
							->format('Y');
						})
						->editColumn('month', function($data) {
							return \Carbon\Carbon::parse($data['created_at'])
							->format('M');
						})
						->editColumn('date', function($data) {
							return \Carbon\Carbon::parse($data['created_at'])
							->format('d, M Y');
						})
						->editColumn('province', function($data) {
							return \App\Model\Region\Provinsi::where('id', $data['id_prov'])->value('nama');
						})
						->editColumn('upt_name', function($data) {
							return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
						})
						->editColumn('exam_date', function($data) {
							return \Carbon\Carbon::parse($data['tanggal_ujian'])->format('d,M Y');
						})
						->editColumn('unar_type', function($data) use ($unar) {
							switch ($data['tipe']) {
								case '1':
								return 'REGULER';
								break;
								case '0':
								return 'NON-REGULER';
								break;
								default:
								return '-';
								break;
							}
						})
						->editColumn('all_total', function($data) {
							return $data['jumlah_siaga'] + $data['jumlah_penggalang'] + $data['jumlah_penegak'];
						})
						->editColumn('pass_total', function($data) {
							return $data['lulus_siaga'] + $data['lulus_penggalang'] + $data['lulus_penegak'];
						})
						->editColumn('not_pass_total', function($data) {
							return $data['tdk_lulus_siaga'] + $data['tdk_lulus_penggalang'] + $data['tdk_lulus_penegak'];
						})
						->editColumn('created_by', function($data) {
							return Auth::user()->where('id', $data['created_by'])->value('name');
						})
						->editColumn('city', function($data) {
							return \App\Model\Region\KabupatenKota::where('id', $data['id_kabkot'])->value('nama');
						})
						->make(true);
						return $unar;
					}

					$tahun_sekarang = date('Y');
					$getUpt = \App\Model\Setting\UPT::where('office_id',$changeUPT)->count();
					$unar_data = \App\RencanaUnar::where('tahun',$tahun_sekarang)->first();
				}
				return view('templates.pelayanan.unar.search.index',compact('unar','changeUPT','getUpt','unar_data'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Unar');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

	// Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new UnarImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);

	// return $header;

		if($header[0] == 'tanggal_ujian' && $header[1] == 'id_unar' && $header[2] == 'lokasi_ujian' && $header[3] == 'jumlah_siaga' && $header[4] == 'jumlah_penggalang' && $header[5] == 'jumlah_penegak' && $header[6] == 'lulus_siaga' && $header[7] == 'lulus_penggalang' && $header[8] == 'lulus_penegak' && $header[9] == 'tdk_lulus_siaga' && $header[10] == 'tdk_lulus_penggalang' && $header[11] == 'tdk_lulus_penegak'){
			return view('templates.pelayanan.unar.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'UNAR');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');        
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		date_default_timezone_set('Asia/Jakarta');
		$data = $request->all();
		$tanggal_ujian = $data['tanggal_ujian'];
		$id_unar = $data['id_unar'];
		$lokasi_ujian = $data['lokasi_ujian'];            
		$jumlah_siaga = $data['jumlah_siaga'];            
		$jumlah_penggalang = $data['jumlah_penggalang'];            
		$jumlah_penegak = $data['jumlah_penegak'];            
		$lulus_siaga = $data['lulus_siaga'];            
		$lulus_penggalang = $data['lulus_penggalang'];
		$lulus_penegak = $data['lulus_penegak'];
		$tdk_lulus_siaga = $data['tdk_lulus_siaga'];
		$tdk_lulus_penggalang = $data['tdk_lulus_penggalang'];
		$tdk_lulus_penegak = $data['tdk_lulus_penegak'];
		if($request->lampiran){
			$lampiran = $data['lampiran'];
		}else{
			$lampiran = '';
		}
		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');
		if (Auth::user()->province_code == 0 && Auth::user()->id_upt == 0) {
			$id_prov = $request->upt_provinsi;
			$kode_upt = $request->id_upt;
		}
		else{
			$id_prov = Auth::user()->province_code;  
			$kode_upt = Auth::user()->upt;                                
		}
		$rows = [];
		foreach($tanggal_ujian as $key => $input) {
			$uniq = uniqid();
			if(isset($lampiran[$key])){
				$namaLampiran = 'Unar-'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension();
				$lampiran[$key]->move(public_path('lampiran/pelayanan/unar'),'Unar-'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension());
			}else{
				$namaLampiran = '';
			}
			array_push($rows, [
				'tanggal_ujian' => isset($tanggal_ujian[$key]) ? $tanggal_ujian[$key] : '',
				'lokasi_ujian' => isset($lokasi_ujian[$key]) ? $lokasi_ujian[$key] : '',            
				'id_unar' => isset($id_unar[$key]) ? $id_unar[$key] : '',            
				'jumlah_siaga' => isset($jumlah_siaga[$key]) ? $jumlah_siaga[$key] : '',            
				'jumlah_penggalang' => isset($jumlah_penggalang[$key]) ? $jumlah_penggalang[$key] : '',            
				'jumlah_penegak' => isset($jumlah_penegak[$key]) ? $jumlah_penegak[$key] : '',            
				'lulus_siaga' => isset($lulus_siaga[$key]) ? $lulus_siaga[$key] : '',            
				'lulus_penggalang' => isset($lulus_penggalang[$key]) ? $lulus_penggalang[$key] : '',            
				'lulus_penegak' => isset($lulus_penegak[$key]) ? $lulus_penegak[$key] : '',            
				'tdk_lulus_siaga' => isset($tdk_lulus_siaga[$key]) ? $tdk_lulus_siaga[$key] : '',            
				'tdk_lulus_penggalang' => isset($tdk_lulus_penggalang[$key]) ? $tdk_lulus_penggalang[$key] : '',            
				'tdk_lulus_penegak' => isset($tdk_lulus_penegak[$key]) ? $tdk_lulus_penegak[$key] : '',            
				'lampiran' => $namaLampiran,
				'created_by' => Auth::user()->id,            
				'id_prov' => $id_prov,
				'kode_upt' => $kode_upt,
				'created_at' => $created_at,
				'updated_at' => $updated_at,

			]);
		}
		$hasil = Unar::insert($rows);
		Session::flash('info', 'UNAR');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');       
		return redirect(url('pelayanan/unar'));

	}
	public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelUnar.xlsx"));
		ob_end_clean();
		return $responses;

	}

	public function filterData(Request $request)
	{
		$unar = new Unar;
		$tahun_sekarang = date('Y');
		$unar_data = \App\RencanaUnar::where('tahun',$tahun_sekarang)->first();
		$unar = $unar->whereBetween('tanggal_ujian', array($request->dari, $request->sampai));
		if (Auth::user()->province_code != 0 && Auth::user()->upt != 0) {
			$unar = $unar->where('kode_upt', Auth::user()->upt);
			$unar = $unar->where('id_prov', Auth::user()->province_code);
		}
		$unar = $unar->get();

		Session::flash('info', 'Filter Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Data Berhasil Diambil!');

		if ($request->filled('type') && $request->type = 'WEB') {
			$unar = datatables()->of($unar)
			->editColumn('id', function($data) {
				return Crypt::encryptString($data['id']);
			})
			->editColumn('year', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('Y');
			})
			->editColumn('month', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('M');
			})
			->editColumn('date', function($data) {
				return \Carbon\Carbon::parse($data['created_at'])
				->format('d, M Y');
			})
			->editColumn('province', function($data) {
				return \App\Model\Region\Provinsi::where('id', $data['id_prov'])->value('nama');
			})
			->editColumn('upt_name', function($data) {
				return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
			})
			->editColumn('exam_date', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_ujian'])->format('d,M Y');
			})
			->editColumn('unar_type', function($data) use ($unar) {
				switch ($data['tipe']) {
					case '1':
					return 'REGULER';
					break;
					case '0':
					return 'NON-REGULER';
					break;
					default:
					return '-';
					break;
				}
			})
			->editColumn('all_total', function($data) {
				return $data['jumlah_siaga'] + $data['jumlah_penggalang'] + $data['jumlah_penegak'];
			})
			->editColumn('pass_total', function($data) {
				return $data['lulus_siaga'] + $data['lulus_penggalang'] + $data['lulus_penegak'];
			})
			->editColumn('not_pass_total', function($data) {
				return $data['tdk_lulus_siaga'] + $data['tdk_lulus_penggalang'] + $data['tdk_lulus_penegak'];
			})
			->editColumn('created_by', function($data) {
				return Auth::user()->where('id', $data['created_by'])->value('name');
			})
			->editColumn('city', function($data) {
				return \App\Model\Region\KabupatenKota::where('id', $data['id_kabkot'])->value('nama');
			})
			->make(true);
			return $unar;
		}

		return view('templates.pelayanan.unar.core.index',compact('unar', 'unar_data'));
	}

	public function updateRencanaUnar(Request $request){
		$rencanaUnar = \App\RencanaUnar::find($request->id_rencana_unar);
		$rencanaUnar->jumlah = $request->jumlah_rencana_unar;
		$rencanaUnar->save();
		Session::flash('info', 'Update Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Data Mengubah Data!');
		return redirect()->back();
	}
}
