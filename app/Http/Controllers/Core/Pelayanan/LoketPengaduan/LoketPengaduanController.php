<?php

namespace App\Http\Controllers\Core\Pelayanan\LoketPengaduan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service\LoketPengaduan;
use Illuminate\Support\Facades\Crypt;
use App\Imports\LoketPengaduanImport;
use App\Exports\LoketPengaduanExport;
use App\Exports\LoketPengaduanExportId;
use App\Model\Notification\Notification;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Setting\Perusahaan;
use App\Model\Privillage\Role;
use App\Model\Refrension\Tujuan;
use Carbon\Carbon;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class LoketPengaduanController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$loketpengaduan = new LoketPengaduan;
		if (Role::where('id',Auth::user()->role_id)->value('akses') != 'administrator') {
			$loketpengaduan = $loketpengaduan->where('kode_upt', Auth::user()->upt);
			$loketpengaduan = $loketpengaduan->where('id_prov', Auth::user()->province_code);
		}
			//consume from filter data
		if ($request->filled('dari') && $request->filled('sampai')) {
			$loketpengaduan = $loketpengaduan->whereBetween('tanggal_pelayanan', array($request->dari, $request->sampai));
			Session::flash('info', 'Filter Loket Pelayanan');
			Session::flash('colors', 'green');	
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data Berhasil Diambil!');
		}
			//end consume filter data
		$loketpengaduan = $loketpengaduan->get();
		if ($request->filled('type') && $request->type = 'WEB') {
			$loketpengaduan = datatables()->of($loketpengaduan)
			->editColumn('id', function($data) {
				return Crypt::encryptString($data['id']);
			})
			->editColumn('year', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('Y');
			})
			->editColumn('month', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('M');
			})
			->editColumn('date', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('d, M Y');
			})
			->editColumn('upt_name', function($data) {
				return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
			})
			->editColumn('purpose', function($data) {
				return \App\Model\Refrension\Tujuan::where('id', $data['tujuan'])->value('tujuan');
			})
			->editColumn('province', function($data) {
				return \App\Model\Region\Provinsi::where('id_row', $data['id_prov'])->value('nama');
			})
			->editColumn('created_by', function($data) {
				return Auth::user()->where('id', $data['created_by'])->value('name');
			})
			->editColumn('status', function($data) use ($loketpengaduan) {
				if ($data['tujuan'] == 1 && !is_null($data['active'])) {
					switch ($data['active']) {
						case '1':
						return 'OPEN';
						break;
						case '0':
						return 'CLOSE';
						break;
						case '3':
						return 'RE-OPEN';
						break;
						default:
						return '-';
						break;
					}
				}
				return '-';
			})
			->make(true);
			return $loketpengaduan;
		}
		return view('templates.pelayanan.loket-pengaduan.core.index',compact('loketpengaduan'));
	}


	public function save(Request $request){
		$loketpengaduan = new LoketPengaduan;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$loketpengaduan->id_prov = $request->id_prov;
			$loketpengaduan->kode_upt = $request->kode_upt;
		}else{
			$loketpengaduan->id_prov = Auth::user()->province_code;
			$loketpengaduan->kode_upt = Auth::user()->upt;
		}
		$loketpengaduan->tanggal_pelayanan = $request->tanggal_pelayanan;
		$loketpengaduan->nama_pengunjung = $request->nama_pengunjung;
		$loketpengaduan->jabatan_pengunjung = $request->jabatan_pengunjung;
		$loketpengaduan->nama_perusahaan = $request->nama_perusahaan;
		$loketpengaduan->no_telp = $request->no_telp;
		$loketpengaduan->no_hp = $request->no_hp;
		$loketpengaduan->email = $request->email;
		$loketpengaduan->keperluan = $request->keperluan;
		$loketpengaduan->keterangan = $request->keterangan;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->keperluan."-".$request->tanggal_pelayanan."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/pelayanan/loketpengaduan'), $cover);
			$loketpengaduan->lampiran = $cover;	
		}
		$loketpengaduan->active = $request->active;
		$loketpengaduan->tujuan = $request->tujuan;
		$loketpengaduan->status = 1;
		$loketpengaduan->created_by = Auth::user()->id;
		$loketpengaduan->save();
		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 5;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menambah Data Loket Pelayanan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'Loket Pelayanan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$loketpengaduan = LoketPengaduan::find($id);
		$loketpengaduan->tanggal_pelayanan = $request->tanggal_pelayanan;
		$loketpengaduan->nama_pengunjung = $request->nama_pengunjung;
		$loketpengaduan->jabatan_pengunjung = $request->jabatan_pengunjung;
		$loketpengaduan->nama_perusahaan = $request->nama_perusahaan;
		$loketpengaduan->no_telp = $request->no_telp;
		$loketpengaduan->no_hp = $request->no_hp;
		$loketpengaduan->email = $request->email;
		$loketpengaduan->keperluan = $request->keperluan;
		$loketpengaduan->keterangan = $request->keterangan;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->keperluan."-".$request->tanggal_pelayanan."-lampiran".".".$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/pelayanan/loketpengaduan'), $cover);
			$loketpengaduan->lampiran = $cover;	
		}
		$loketpengaduan->active = $request->active;
		$loketpengaduan->tujuan = $request->tujuan;
		$loketpengaduan->status = 1;
		$loketpengaduan->created_by = Auth::user()->id;
		$loketpengaduan->save();
		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 5;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Loket Pelayanan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'Loket Pelayanan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	// public function updateRejected(Request $request)
	// {
	// 	$id = Crypt::decryptString($request->id);
	// 	$loketpengaduan = LoketPengaduan::find($id);
	// 	$loketpengaduan->tanggal_pelayanan = $request->tanggal_pelayanan;
	// 	$loketpengaduan->nama_pengunjung = $request->nama_pengunjung;
	// 	$loketpengaduan->jabatan_pengunjung = $request->jabatan_pengunjung;
	// 	$loketpengaduan->nama_perusahaan = $request->nama_perusahaan;
	// 	$loketpengaduan->no_telp = $request->no_telp;
	// 	$loketpengaduan->no_hp = $request->no_hp;
	// 	$loketpengaduan->email = $request->email;
	// 	$loketpengaduan->keperluan = $request->keperluan;
	// 	$loketpengaduan->keterangan = $request->keterangan;
	// 	if ($request->hasFile('lampiran')) {
	// 		$files = $request->file('lampiran');
	// 		$cover = $request->keperluan."-".$request->tanggal_pelayanan."-lampiran".".".$files->getClientOriginalExtension();
	// 		$files->move(public_path('lampiran/loketpengaduan'), $cover);
	// 		$loketpengaduan->lampiran = $cover;	
	// 	}
	// 	$loketpengaduan->active = $request->active;
	// 	$loketpengaduan->tujuan = $request->tujuan;
	// 	$loketpengaduan->status = 3;
	// 	$loketpengaduan->created_by = Auth::user()->id;
	// 	$loketpengaduan->save();
	// 	Session::flash('info', 'Loket Pengaduan');
	// 	Session::flash('colors', 'green');
	// 	Session::flash('icons', 'fas fa-clipboard-check');
	// 	Session::flash('alert', 'Berhasil Mengubah Data!');
	// 	return redirect()->back();
	// }

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		LoketPengaduan::where('id',$id)->delete();
		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 5;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Loket Pelayanan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		Session::flash('info', 'Loket Pelayanan');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}

	// public function approved(Request $request){
	// 	if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
	// 		if ($request->getStatus == 0) {
	// 			$ids = $request->id;
	// 			$action = $request->action;
	// 			if ($request->id) {
	// 				$action = $request->action;
	// 				DB::table('loket_pengaduans')->whereIn('id', $ids)->update(['status' => $action]);
	// 				if ($action == 1) {
	// 					Session::flash('info', 'Loket Pengaduan');
	// 					Session::flash('colors', 'green');
	// 					Session::flash('icons', 'fas fa-clipboard-check');
	// 					Session::flash('alert', 'Berhasil Mengapprove Data!');
	// 				}else{
	// 					Session::flash('info', 'Loket Pengaduan');
	// 					Session::flash('colors', 'red');
	// 					Session::flash('icons', 'fas fa-clipboard-check');
	// 					Session::flash('alert', 'Berhasil Mereject Data!');
	// 				}
	// 				return redirect()->back();
	// 			}
	// 			else{
	// 				Session::flash('info', 'Loket Pengaduan');
	// 				Session::flash('colors', 'red');
	// 				Session::flash('icons', 'fas fa-clipboard-check');
	// 				Session::flash('alert', 'Belum Memilih Data!');
	// 				return redirect()->back();
	// 			}	
	// 		}
	// 		else{
	// 			Session::flash('info', 'Loket Pengaduan');
	// 			Session::flash('colors', 'red');
	// 			Session::flash('icons', 'fas fa-clipboard-check');
	// 			Session::flash('alert', 'Status Salah!');
	// 			return redirect()->back();
	// 		}

	// 	}else{
	// 		Session::flash('info', 'Loket Pengaduan');
	// 		Session::flash('colors', 'red');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Tidak Memiliki Akses!');
	// 		return redirect()->back();
	// 	}
	// }

	// public function reupload($id){
	// 	if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator') {
	// 		$loketpengaduan = LoketPengaduan::find($id);
	// 		$loketpengaduan->status = 0;
	// 		$loketpengaduan->save();
	// 		Session::flash('info', 'Loket Pengaduan');
	// 		Session::flash('colors', 'green');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Berhasil Mengirim Ulang');
	// 		return redirect()->back();
	// 	}
	// 	else{
	// 		Session::flash('info', 'Loket Pengaduan');
	// 		Session::flash('colors', 'red');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Tidak Memiliki Akses!');
	// 		return redirect()->back();
	// 	}
	// }

	public function search(Request $request){

		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$provinceCode = LoketPengaduan::where('kode_upt',$changeUPT)->value('id_prov');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.pelayanan.loket-pengaduan.search.multiple',compact('getUPT'));
				}
				else{
					$loketpengaduan = LoketPengaduan::where('kode_upt',$changeUPT)->get();
					if ($request->filled('type') && $request->type) {
						$loketpengaduan = datatables()->of($loketpengaduan)
						->editColumn('id', function($data) {
							return Crypt::encryptString($data['id']);
						})
						->editColumn('year', function($data) {
							return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
							->format('Y');
						})
						->editColumn('month', function($data) {
							return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
							->format('M');
						})
						->editColumn('date', function($data) {
							return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
							->format('d, M Y');
						})
						->editColumn('upt_name', function($data) {
							return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
						})
						->editColumn('purpose', function($data) {
							return \App\Model\Refrension\Tujuan::where('id', $data['tujuan'])->value('tujuan');
						})
						->editColumn('province', function($data) {
							return \App\Model\Region\Provinsi::where('id_row', $data['id_prov'])->value('nama');
						})
						->editColumn('created_by', function($data) {
							return Auth::user()->where('id', $data['created_by'])->value('name');
						})
						->editColumn('status', function($data) use ($loketpengaduan) {
							if ($data['tujuan'] == 1 && !is_null($data['active'])) {
								switch ($data['active']) {
									case '1':
									return 'OPEN';
									break;
									case '0':
									return 'CLOSE';
									break;
									case '3':
									return 'RE-OPEN';
									break;
									default:
									return '-';
									break;
								}
							}
							return '-';
						})
						->make(true);
						return $loketpengaduan;
					}
				}
				return view('templates.pelayanan.loket-pengaduan.search.index',compact('loketpengaduan','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pelayanan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$provinceCode = LoketPengaduan::where('kode_upt',$changeUPT)->value('id_prov');
			if ($changeUPT) {
				$loketpengaduan = LoketPengaduan::where('kode_upt',$changeUPT)->get();
				if ($request->filled('type') && $request->type = 'WEB') {
					$loketpengaduan = datatables()->of($loketpengaduan)
					->editColumn('id', function($data) {
						return Crypt::encryptString($data['id']);
					})
					->editColumn('year', function($data) {
						return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
						->format('Y');
					})
					->editColumn('month', function($data) {
						return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
						->format('M');
					})
					->editColumn('date', function($data) {
						return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
						->format('d, M Y');
					})
					->editColumn('upt_name', function($data) {
						return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
					})
					->editColumn('purpose', function($data) {
						return \App\Model\Refrension\Tujuan::where('id', $data['tujuan'])->value('tujuan');
					})
					->editColumn('province', function($data) {
						return \App\Model\Region\Provinsi::where('id_row',$data['id_prov'])->value('nama');
					})
					->editColumn('created_by', function($data) {
						return Auth::user()->where('id', $data['created_by'])->value('name');
					})
					->editColumn('status', function($data) use ($loketpengaduan) {
						if ($data['tujuan'] == 1 && !is_null($data['active'])) {
							switch ($data['active']) {
								case '1':
								return 'OPEN';
								break;
								case '0':
								return 'CLOSE';
								break;
								case '3':
								return 'RE-OPEN';
								break;
								default:
								return '-';
								break;
							}
						}
						return '-';
					})
					->make(true);
					return $loketpengaduan;
				}

				return view('templates.pelayanan.loket-pengaduan.search.index',compact('loketpengaduan','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pelayanan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function print(Request $request){
		$date = $request->date;
		$month = $request->month;
		$year = $request->year;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			// if ($date && $month && $year) {
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($date && $month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->get();
			// }elseif($date){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->get();
			// }elseif($month && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($date && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereMonth('tanggal_pelayanan',$month)->get();
			// }elseif($year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereYear('tanggal_pelayanan',$year)->get();
			// }else{
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::all();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('status', 1)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('status',1)->get();
			}
		}else{
			// if ($date && $month && $year) {
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($date && $month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($date){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($month && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($date && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereMonth('tanggal_pelayanan',$month)->where('kode_upt', Auth::user()->upt)->get();
			// }elseif($year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::whereYear('tanggal_pelayanan',$year)->where('kode_upt', Auth::user()->upt)->get();
			// }else{
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt', Auth::user()->upt)->get();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
			}
		}
		$pdf = PDF::loadView('templates.pelayanan.loket-pengaduan.dev.print', $loketpengaduan)->setPaper('a2','landscape');
		return $pdf->download('Data-Loket-Pelayanan-'.date('d-F-Y').'.pdf');
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($date && $month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->get();
			// }elseif($date){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereDay('tanggal_pelayanan',$date)->get();
			// }elseif($month && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($date && $year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->get();
			// }elseif($month){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereMonth('tanggal_pelayanan',$month)->get();
			// }elseif($year){
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereYear('tanggal_pelayanan',$year)->get();
			// }else{
			// 	$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->get();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan['loketpengaduan'] = LoketPengaduan::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.pelayanan.loket-pengaduan.dev.print-search', $loketpengaduan)->setPaper('a2','landscape');
			return $pdf->download('Loket Pelayanan '.$getUptName.'.pdf');
		}
	}

	public function export(Request $request)
	{
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->upt == 0) {
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($date && $month){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->get();
            // }elseif($date){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereDay('tanggal_pelayanan',$date)->get();
            // }elseif($month && $year){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($date && $year){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($month){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereMonth('tanggal_pelayanan',$month)->get();
            // }elseif($year){
            //     $loketpengaduan = LoketPengaduan::where('status',1)->whereYear('tanggal_pelayanan',$year)->get();
            // }else{
            //     $loketpengaduan = LoketPengaduan::where('status',1)->get();;
            // }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan = LoketPengaduan::where('status', 1)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan = LoketPengaduan::where('status',1)->get();
			}
		}else{
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($date && $month){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->get();
            // }elseif($date){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelayanan',$date)->get();
            // }elseif($month && $year){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($date && $year){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->get();
            // }elseif($month){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelayanan',$month)->get();
            // }elseif($year){
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereYear('tanggal_pelayanan',$year)->get();
            // }else{
            //     $loketpengaduan = LoketPengaduan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();;
            // }

			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan = LoketPengaduan::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan = LoketPengaduan::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
			}
		}
		$export = new LoketPengaduanExport([          
			$push,
		]);

		foreach ($loketpengaduan as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$perusahaan = $value->nama_perusahaan;
			$tujuan = Tujuan::where('id',$value->tujuan)->value('tujuan');
			$bulan = Carbon::parse($value->tanggal_pelayanan)->format('F');
			$tahun = Carbon::parse($value->tanggal_pelayanan)->format('Y');
			if($value->active == 1){
				$status = 'Open';
			}elseif($value->active == 3){
				$status = 'Re-Open';
			}
			else{
				$status = 'Close';
			}

			array_push($push, array(
				$value->id,
				$provinsi,
				$nama_upt,
				$value->tanggal_pelayanan,
				$tahun,
				$bulan,
				$value->nama_pengunjung,
				$value->jabatan_pengunjung,
				$perusahaan,
				$value->no_telp,
				$value->no_hp,
				$value->email,
				$value->keperluan,
				$value->keterangan,
				$tujuan,
				$status,
			));

			$export = new LoketPengaduanExport([          
				$push,
			]);
		}
		return Excel::download($export, 'LoketPengaduan'.date('d-F-Y').'.xlsx');
	}

	public function export_search(Request $request, $id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];

			// $date = request()->date;
	  //       $month = request()->month;
	  //       $year = request()->year;
	  //       if ($date && $month && $year) {
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
	  //       }elseif($date && $month){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereMonth('tanggal_pelayanan',$month)->get();
	  //       }elseif($date){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelayanan',$date)->get();
	  //       }elseif($month && $year){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelayanan',$month)->whereYear('tanggal_pelayanan',$year)->get();
	  //       }elseif($date && $year){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelayanan',$date)->whereYear('tanggal_pelayanan',$year)->get();
	  //       }elseif($month){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelayanan',$month)->get();
	  //       }elseif($year){
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereYear('tanggal_pelayanan',$year)->get();
	  //       }else{
	  //           $loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->get();;
	  //       }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->whereBetween('tanggal_pelayanan', array($dari, $sampai))->get();
			}else{
				$loketpengaduan = LoketPengaduan::where('kode_upt',$id)->where('status',1)->get();
			}

			$export = new LoketPengaduanExport([          
				$push,
			]);

			foreach ($loketpengaduan as $key => $value) {			
				$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				$perusahaan = $value->nama_perusahaan;
				$tujuan = Tujuan::where('id',$value->tujuan)->value('tujuan');
				$bulan = Carbon::parse($value->tanggal_pelayanan)->format('F');
				$tahun = Carbon::parse($value->tanggal_pelayanan)->format('Y');

				array_push($push, array(
					$value->id,
					$provinsi,
					$nama_upt,
					$value->tanggal_pelayanan,
					$tahun,
					$bulan,
					$value->nama_pengunjung,
					$value->jabatan_pengunjung,
					$perusahaan,
					$value->no_telp,
					$value->no_hp,
					$value->email,
					$value->keperluan,
					$value->keterangan,
					$tujuan,
				));

				$export = new LoketPengaduanExport([          
					$push,
				]);
			}
			return Excel::download($export, 'Loket Pengaduan '.$changeName.'.xlsx');
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

        // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new LoketPengaduanImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);

        // return $result;

		if($header[0] == 'tanggal_pelayanan' && $header[1] == 'nama_pengunjung' && $header[2] == 'jabatan_pengunjung' && $header[3] == 'no_telp' && $header[4] == 'no_hp' && $header[5] == 'email' && $header[6] == 'keperluan' && $header[7] == 'keterangan' && $header[8] == 'tujuan' && $header[9] == 'perusahaan' && $header[10] == 'status'){

			return view('templates.pelayanan.loket-pengaduan.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');        
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		Session::flash('info', 'Loket Pengaduan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');

		$data = $request->all();            
		$tanggal_pelayanan = $data['tanggal_pelayanan'];
		$nama_pengunjung = $data['nama_pengunjung'];
		$jabatan_pengunjung = $data['jabatan_pengunjung'];
		$no_telp = $data['no_telp'];
		$no_hp = $data['no_hp'];
		$email = $data['email'];
		$keperluan = $data['keperluan'];
		$keterangan = $data['keterangan'];
		$tujuan = $data['tujuan'];
		$nama_perusahaan = $data['nama_perusahaan'];
		if($request->lampiran){
			$lampiran = $data['lampiran'];
		}else{
			$lampiran = '';
		}

		$active = $data['active'];

		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$id_prov = $request->upt_provinsi;
			$kode_upt = $request->id_upt;
		}
		else{
			$id_prov = Auth::user()->province_code;  
			$kode_upt = Auth::user()->upt;           
		}  
		$rows = [];
		// return $lampiran;
		foreach($tanggal_pelayanan as $key => $input) {
			$uniq = uniqid();
			if(isset($lampiran[$key])){
				$namaLampiran = 'Loket-Pengaduan'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension();
				$lampiran[$key]->move(public_path('lampiran/pelayanan/loketpengaduan'),'Loket-Pengaduan'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension());
			}else{
				$namaLampiran = '';
			}
			array_push($rows, [
				'tanggal_pelayanan' => isset($tanggal_pelayanan[$key]) ? $tanggal_pelayanan[$key] : '',
				'nama_pengunjung' => isset($nama_pengunjung[$key]) ? $nama_pengunjung[$key] : '',
				'jabatan_pengunjung' => isset($jabatan_pengunjung[$key]) ? $jabatan_pengunjung[$key] : '',
				'no_telp' => isset($no_telp[$key]) ? $no_telp[$key] : '',
				'no_hp' => isset($no_hp[$key]) ? $no_hp[$key] : '',
				'email' => isset($email[$key]) ? $email[$key] : '',
				'keperluan' => isset($keperluan[$key]) ? $keperluan[$key] : '',
				'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
				'active' => isset($active[$key]) ? $active[$key] : '',
				'tujuan' => (isset($tujuan[$key]) ? strval($tujuan[$key]) : 0),
				'created_by' => Auth::user()->id,            
				'lampiran' => $namaLampiran,
				'nama_perusahaan' => (isset($nama_perusahaan[$key]) ? strval($nama_perusahaan[$key]) : 0),
				'id_prov' => $id_prov,
				'kode_upt' => $kode_upt,
				'status' => 1,
				'created_at' => $created_at,
				'updated_at' => $updated_at,

			]);

			// return $nama_perusahaan[$key];
		}
		$hasil = LoketPengaduan::insert($rows);
		Session::flash('info', 'Loket Pengaduan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');       
		return redirect(url('pelayanan/loket-pengaduan'));


	}

	public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelLoketPengaduan.xlsx"));
		ob_end_clean();
		return $responses;

	}

	public function filterData(Request $request)
	{
		$loketpengaduan = new LoketPengaduan;
		$loketpengaduan = $loketpengaduan->whereBetween('tanggal_pelayanan', array($request->dari, $request->sampai));
		if (Auth::user()->province_code != 0 && Auth::user()->upt != 0) {
			$loketpengaduan = $loketpengaduan->where('kode_upt' ,Auth::user()->upt);
			$loketpengaduan = $loketpengaduan->where('id_prov', Auth::user()->province_code);
		}
		$loketpengaduan = $loketpengaduan->get();

		if ($request->filled('type') && $request->type = 'WEB') {
			$loketpengaduan = datatables()->of($loketpengaduan)
			->editColumn('id', function($data) {
				return Crypt::encryptString($data['id']);
			})
			->editColumn('year', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('Y');
			})
			->editColumn('month', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('M');
			})
			->editColumn('date', function($data) {
				return \Carbon\Carbon::parse($data['tanggal_pelayanan'])
				->format('d, M Y');
			})
			->editColumn('upt_name', function($data) {
				return \App\Model\Setting\UPT::where('office_id', $data['kode_upt'])->select('office_name')->distinct()->value('office_name');
			})
			->editColumn('purpose', function($data) {
				return \App\Model\Refrension\Tujuan::where('id', $data['tujuan'])->value('tujuan');
			})
			->editColumn('province', function($data) {
				return \App\Model\Region\Provinsi::where('id_row',$data['id_prov'])->value('nama');
			})
			->editColumn('created_by', function($data) {
				return Auth::user()->where('id', $data['created_by'])->value('name');
			})
			->editColumn('status', function($data) use ($loketpengaduan) {
				if ($data['tujuan'] == 1 && !is_null($data['active'])) {
					switch ($data['active']) {
						case '1':
						return 'OPEN';
						break;
						case '0':
						return 'CLOSE';
						break;
						case '3':
						return 'RE-OPEN';
						break;
						default:
						return '-';
						break;
					}
				}
				return '-';
			})
			->make(true);
			return $loketpengaduan;
		}
		Session::flash('info', 'Filter Loket Pelayanan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Data Berhasil Diambil!');
		return view('templates.pelayanan.loket-pengaduan.core.index',compact('loketpengaduan'));
	}
}
