<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Model\Setting\Kuisioner\Answer;
use Auth;
use Session;
use App\Model\Setting\Kuisioner\Setting;
use DB;
use App\User;
use App\Model\Setting\Kuisioner\Kuisioner;
use App\Model\Setting\Kuisioner\Question;
use App\Model\Setting\UPT;
use App\Model\Setting\Kuisioner\Option;


class FormKuisionerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$kuesioner = Kuisioner::all();
    	return view('kuesioner',compact('kuesioner'));
    }

    public function form($id){
    	$id_kuesioner = Crypt::decryptString($id);
    	$question = Question::where('id_kuisioner',$id_kuesioner)->orderBy('id','asc')->get();
    	$kuisioner = Kuisioner::where('id',$id_kuesioner)->value('title');
    	return view('form',compact('question','kuisioner','id_kuesioner'));
    }

    public function formPost(Request $request){
        $data = $request->all();
        $id_kuesioner = $data['id_kuesioner'];
        $id_pertanyaan = $data['id_pertanyaan'];
        $option = $data['option'];
        $jenis_pertanyaan = $data['jenis_pertanyaan'];
        foreach ($id_kuesioner as $key => $value) {
            $answer = new Answer;
            $answer->id_kuisioner = isset($id_kuesioner[$key]) ? $id_kuesioner[$key] : '';
            $answer->id_pertanyaan = isset($id_pertanyaan[$key]) ? $id_pertanyaan[$key] : '';
            $answer->answer = isset($option[$key]) ? $option[$key] : '';
            $answer->jenis_pertanyaan = isset($jenis_pertanyaan[$key]) ? $jenis_pertanyaan[$key] : '';
            $answer->user = Auth::user()->id;
            $answer->save();
        }
        Session::flash('info', 'Kuesioner');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Terima Kasih!');
        return redirect(route('kuesioner'));
    }

    public function dashboard(Request $request)
    {
        $kuesioner = new Kuisioner;
        if (!$this->isAdmin()) {
            $kuesioner->whereIn('id', Setting::getKuesionerId());
        }
        $kuesioner = $kuesioner->orderBy('created_at', 'desc');
        $data['questionnaires'] = $kuesioner->get();
        if ($request->filled('kuesioner')) {
            $data['kuesioner'] = $kuesioner->find(Crypt::decryptString($request->kuesioner));
        }
        else {
            $data['kuesioner'] = $kuesioner->first();
        }
        $data['upts'] = UPT::distinct('office_id')->get();
        foreach ((array)$data['upts'] as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $value = 0;
                $max = 0;
                $total = 0;
                $ids = Answer::where('jenis_pertanyaan', 1)->whereIn('user', User::where('upt', $value2->office_id)->pluck('id'))->pluck('answer');
                $points = Option::where('id_kuisioner', $data['kuesioner']->id)->whereIn('id', $ids)->get();
                $data['percentage'][$value2->office_id] = 0;
                foreach ($points as $key3 => $value3) {
                    $value = $value + $value3->value;
                    $max = $max + Option::where('id_kuisioner', $data['kuesioner']->id)->where('id_pertanyaan', $value3->id_pertanyaan)->max('value');
                }
                $data['percentage'][$value2->office_id] = $value == 0 || $max == 0 ? 0 : round(($value / $max) * 100);
            }
        }
        $ids = Answer::where('jenis_pertanyaan', 1)->whereIn('user', User::where('upt', 0)->pluck('id'))->pluck('answer');
        $points = Option::where('id_kuisioner', $data['kuesioner']->id)->whereIn('id', $ids)->get();
        $data['percentage'][0] = 0;
        foreach ($points as $key3 => $value3) {
            $value = $value + $value3->value;
            $max = $max + Option::where('id_kuisioner', $data['kuesioner']->id)->where('id_pertanyaan', $value3->id_pertanyaan)->max('value');
        }
        $data['percentage'][0] = $value == 0 || $max == 0 ? 0 : round(($value / $max) * 100);
        $values = 0;
        $upt = 0;
        foreach ($data['percentage'] as $key => $value) {
            if ($value >= 1) {
                $values = $values + $value;
                $upt++;
            }
        }
        $data['all_percentage'] = $values == 0 || $upt == 0 ? 0 : $values / $upt;
        if ($request->filled('debug')) {
            dump($data);
        }
        return view('kuesioner-dashboard')->with($data);
    }

    public function detail($id)
    {
        $id = Crypt::decryptString($id);
        $data['kuesioner'] = Kuisioner::find($id);
        $data['questions'] = $this->pertanyaan($id);
        $data['essais'] = Question::where('id_kuisioner', $id)->where('jenis_pertanyaan', 0)->get();
        $data['essay_answer'] = $this->essay($id);
        $data['options'] = $this->option($id);
        return view('kuesioner-detail')->with($data);
    }

    public function pertanyaan($id)
    {
        $question = Question::where('id_kuisioner', $id)->where('jenis_pertanyaan', 1)->get()->toArray();
        foreach ($question as $key => $value) {
            $answer = new Answer;
            $answer = $answer->where('id_pertanyaan', $value['id']);
            $count_answer = $answer->count();
            $high_answer = $answer->select('id_pertanyaan','answer', DB::raw('count(id) as total'))->groupBy('id_pertanyaan','answer')->orderBy('total', 'desc')->first()->toArray();
            $question[$key]['answer'] = Option::find($high_answer['answer'])->option;
            $question[$key]['percentage'] = round(($high_answer['total']/$count_answer) * 100);
        }
        return $question;
    }

    public function essay($id)
    {
        $wowo = Answer::select('id_pertanyaan','user','answer')->where('id_kuisioner', $id)->where('jenis_pertanyaan', 0)->groupBy('id_pertanyaan','user','answer')->orderBy('id_pertanyaan', 'asc')->orderBy('user', 'asc')->orderBy('answer', 'asc')->get()->toArray();
        $wow = [];
        foreach ($wowo as $key => $value) {
            $wow[$value['id_pertanyaan']][UPT::where('office_id', User::find($value['user'])->upt)->value('office_name')] = $value['answer'];
        }
        return $wow;
    }

    public function option($id)
    {
        $wow = Question::where('id_kuisioner', $id)->where('jenis_pertanyaan', 1)->pluck('id', 'pertanyaan');
        $wiw = Answer::select('id_kuisioner','id_pertanyaan', 'answer', DB::raw('count(id) as total'))->where('jenis_pertanyaan', 1)->whereIn('id_pertanyaan', $wow)->groupBy('id_kuisioner')->groupBy('id_pertanyaan')->groupBy('answer')->get()->toArray();
        $wow2 = [];
        foreach ($wiw as $key => $value) {
            $wow2[$value['id_pertanyaan']][Option::find($value['answer'])->option] = $value['total'];
        }
        $data['questions'] = $wow;
        $data['option_detail'] = $wow2;
        return $data;
    }
}
