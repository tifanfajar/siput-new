<?php

namespace App\Http\Controllers\Core\Inspeksi\InspeksiData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Inspeksi\Inspeksi;
use Illuminate\Support\Facades\Crypt;
use App\Imports\InspeksiDataImport;
use App\Exports\InspeksiDataExport;
use App\Model\Notification\Notification;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Setting\Perusahaan;
use App\Model\Privillage\Role;
use Auth;
use Carbon\Carbon;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class InspeksiDataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {

        if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
             $query_data_sampling = Inspeksi::select(DB::raw('kode_upt, status, SUM(data_sampling) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $data_sampling = [];
        foreach ($query_data_sampling as $key => $value) {
            if (!isset($data_sampling[$value['kode_upt']][$value['status']]) || empty($data_sampling[$value['kode_upt']][$value['status']]) || is_null($data_sampling[$value['kode_upt']][$value['status']])) {
                $data_sampling[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($data_sampling['total'][$value['status']]) || empty($data_sampling['total'][$value['status']]) || is_null($data_sampling['total'][$value['status']])) {
                    $data_sampling['total'][$value['status']] = 0;
                }
                $data_sampling['total'][$value['status']] = $data_sampling['total'][$value['status']] + $value['total'];
            }
        }

        $query_hi_sesuai_isr = Inspeksi::select(DB::raw('kode_upt, status, SUM(hi_sesuai_isr) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $hi_sesuai_isr = [];
        foreach ($query_hi_sesuai_isr as $key => $value) {
            if (!isset($hi_sesuai_isr[$value['kode_upt']][$value['status']]) || empty($hi_sesuai_isr[$value['kode_upt']][$value['status']]) || is_null($hi_sesuai_isr[$value['kode_upt']][$value['status']])) {
                $hi_sesuai_isr[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($hi_sesuai_isr['total'][$value['status']]) || empty($hi_sesuai_isr['total'][$value['status']]) || is_null($hi_sesuai_isr['total'][$value['status']])) {
                    $hi_sesuai_isr['total'][$value['status']] = 0;
                }
                $hi_sesuai_isr['total'][$value['status']] = $hi_sesuai_isr['total'][$value['status']] + $value['total'];
            }
        }
        $query_hi_tidak_sesuai_isr = Inspeksi::select(DB::raw('kode_upt, status, SUM(hi_tidak_sesuai_isr) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $hi_tidak_sesuai_isr = [];
        foreach ($query_hi_tidak_sesuai_isr as $key => $value) {
            if (!isset($hi_tidak_sesuai_isr[$value['kode_upt']][$value['status']]) || empty($hi_tidak_sesuai_isr[$value['kode_upt']][$value['status']]) || is_null($hi_tidak_sesuai_isr[$value['kode_upt']][$value['status']])) {
                $hi_tidak_sesuai_isr[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($hi_tidak_sesuai_isr['total'][$value['status']]) || empty($hi_tidak_sesuai_isr['total'][$value['status']]) || is_null($hi_tidak_sesuai_isr['total'][$value['status']])) {
                    $hi_tidak_sesuai_isr['total'][$value['status']] = 0;
                }
                $hi_tidak_sesuai_isr['total'][$value['status']] = $hi_tidak_sesuai_isr['total'][$value['status']] + $value['total'];
            }
        }
        $query_hi_tidak_aktif = Inspeksi::select(DB::raw('kode_upt, status, SUM(hi_tidak_aktif) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $hi_tidak_aktif = [];
        foreach ($query_hi_tidak_aktif as $key => $value) {
            if (!isset($hi_tidak_aktif[$value['kode_upt']][$value['status']]) || empty($hi_tidak_aktif[$value['kode_upt']][$value['status']]) || is_null($hi_tidak_aktif[$value['kode_upt']][$value['status']])) {
                $hi_tidak_aktif[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($hi_tidak_aktif['total'][$value['status']]) || empty($hi_tidak_aktif['total'][$value['status']]) || is_null($hi_tidak_aktif['total'][$value['status']])) {
                    $hi_tidak_aktif['total'][$value['status']] = 0;
                }
                $hi_tidak_aktif['total'][$value['status']] = $hi_tidak_aktif['total'][$value['status']] + $value['total'];
            }
        }
        $query_hi_proses_isr = Inspeksi::select(DB::raw('kode_upt, status, SUM(hi_proses_isr) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $hi_proses_isr = [];
        foreach ($query_hi_proses_isr as $key => $value) {
            if (!isset($hi_proses_isr[$value['kode_upt']][$value['status']]) || empty($hi_proses_isr[$value['kode_upt']][$value['status']]) || is_null($hi_proses_isr[$value['kode_upt']][$value['status']])) {
                $hi_proses_isr[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($hi_proses_isr['total'][$value['status']]) || empty($hi_proses_isr['total'][$value['status']]) || is_null($hi_proses_isr['total'][$value['status']])) {
                    $hi_proses_isr['total'][$value['status']] = 0;
                }
                $hi_proses_isr['total'][$value['status']] = $hi_proses_isr['total'][$value['status']] + $value['total'];
            }
        }
        $query_tl_sesuai_isr = Inspeksi::select(DB::raw('kode_upt, status, SUM(tl_sesuai_isr) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $tl_sesuai_isr = [];
        foreach ($query_tl_sesuai_isr as $key => $value) {
            if (!isset($tl_sesuai_isr[$value['kode_upt']][$value['status']]) || empty($tl_sesuai_isr[$value['kode_upt']][$value['status']]) || is_null($tl_sesuai_isr[$value['kode_upt']][$value['status']])) {
                $tl_sesuai_isr[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($tl_sesuai_isr['total'][$value['status']]) || empty($tl_sesuai_isr['total'][$value['status']]) || is_null($tl_sesuai_isr['total'][$value['status']])) {
                    $tl_sesuai_isr['total'][$value['status']] = 0;
                }
                $tl_sesuai_isr['total'][$value['status']] = $tl_sesuai_isr['total'][$value['status']] + $value['total'];
            }
        }

        $query_tl_belum_isr = Inspeksi::select(DB::raw('kode_upt, status, SUM(tl_belum_isr) AS total, EXTRACT(MONTH FROM tanggal_lapor) AS bulan, EXTRACT(YEAR FROM tanggal_lapor) AS tahun'))->groupBy(DB::raw('EXTRACT(YEAR FROM tanggal_lapor)'))->groupBy(DB::raw('EXTRACT(MONTH FROM tanggal_lapor)'))->groupBy('status')->groupBy('kode_upt')->orderBy('kode_upt', 'asc')->orderBy('kode_upt', 'asc')->orderBy('status', 'asc')->orderBy('tahun', 'desc')->orderBy('bulan', 'desc')->orderBy('total', 'desc')->get()->toArray();
        $tl_belum_isr = [];
        foreach ($query_tl_belum_isr as $key => $value) {
            if (!isset($tl_belum_isr[$value['kode_upt']][$value['status']]) || empty($tl_belum_isr[$value['kode_upt']][$value['status']]) || is_null($tl_belum_isr[$value['kode_upt']][$value['status']])) {
                $tl_belum_isr[$value['kode_upt']][$value['status']]['total'] = $value['total'];
                if (!isset($tl_belum_isr['total'][$value['status']]) || empty($tl_belum_isr['total'][$value['status']]) || is_null($tl_belum_isr['total'][$value['status']])) {
                    $tl_belum_isr['total'][$value['status']] = 0;
                }
                $tl_belum_isr['total'][$value['status']] = $tl_belum_isr['total'][$value['status']] + $value['total'];
            }
        }


                $data_sampling_request = $data_sampling['total'][0];
                $hi_sesuai_isr_request = $hi_sesuai_isr['total'][0];
                $hi_tidak_sesuai_isr_request = $hi_tidak_sesuai_isr['total'][0];
                $hi_tidak_aktif_request = $hi_tidak_aktif['total'][0];
                $hi_proses_isr_request = $hi_proses_isr['total'][0];
                $tl_sesuai_isr_request = $tl_sesuai_isr['total'][0];
                $tl_belum_isr_request = $tl_belum_isr['total'][0];
                if($data_sampling_request == 0)
                    $capaian_request = 0;
                else{
                    $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request) * 100;
                }
                if ($capaian_request >= 100) {
                    $capaian_request = 100;
                }

                $data_sampling_reject = $data_sampling['total'][3];
                $hi_sesuai_isr_reject = $hi_sesuai_isr['total'][3];
                $hi_tidak_sesuai_isr_reject = $hi_tidak_sesuai_isr['total'][3];
                $hi_tidak_aktif_reject = $hi_tidak_aktif['total'][3];
                $hi_proses_isr_reject = $hi_proses_isr['total'][3];
                $tl_sesuai_isr_reject = $tl_sesuai_isr['total'][3];
                $tl_belum_isr_reject = $tl_belum_isr['total'][3];
                if($data_sampling_reject == 0){
                    $capaian_reject = 0;
                }else{
                    $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject) * 100;
                }

                if ($capaian_reject >= 100) {
                    $capaian_reject = 100;
                }

                $data_sampling = $data_sampling['total'][1];
                $hi_sesuai_isr = $hi_sesuai_isr['total'][1];
                $hi_tidak_sesuai_isr = $hi_tidak_sesuai_isr['total'][1];
                $hi_tidak_aktif = $hi_tidak_aktif['total'][1];
                $hi_proses_isr = $hi_proses_isr['total'][1];
                $tl_sesuai_isr = $tl_sesuai_isr['total'][1];
                $tl_belum_isr = $tl_belum_isr['total'][1];
                if($data_sampling == 0){
                    $capaian = 0;
                }else{
                    $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
                }
                if ($capaian >= 100) {
                    $capaian = 100;
                }

                $inspeksiss = Inspeksi::orderBy('tanggal_lapor','desc')->get();
        }
        else{
            $date = Inspeksi::select('tanggal_lapor')->orderBy('tanggal_lapor','desc')->where('kode_upt',Auth::user()->upt)->first();
        if (!$date) {
           $month=date("m");
           $year=date("Y");
        }else{
           $time=strtotime($date->tanggal_lapor);
           $month=date("m",$time);
           $year=date("Y",$time);
        }
            $inspeksiss = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->orderBy('tanggal_lapor','desc')->get();

            $data_sampling_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
            $hi_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
            $hi_proses_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
            $tl_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
            $tl_belum_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
            if($data_sampling_request == 0){
                $capaian_request = 0;
            }else{
                $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request) * 100;
            }
            if ($capaian_request >= 100) {
                $capaian_request = 100;
            }

            $data_sampling_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
            $hi_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
            $hi_proses_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
            $tl_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
            $tl_belum_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
            if($data_sampling_reject == 0){
                $capaian_reject = 0;
            }else{
                $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject) * 100;
            }

            if ($capaian_reject >= 100) {
                $capaian_reject = 100;
            }

            $data_sampling = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
            $hi_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
            $hi_proses_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
            $tl_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
            $tl_belum_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
            if($data_sampling == 0){
                $capaian = 0;
            }else{
                $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
            }

            if ($capaian >= 100) {
                $capaian = 100;
            }
        }
        return view('templates.inspeksi.inspeksi-data.core.index', compact('inspeksiss','data_sampling_request','hi_sesuai_isr_request','hi_tidak_sesuai_isr_request','hi_tidak_aktif_request','hi_proses_isr_request','tl_sesuai_isr_request','tl_belum_isr_request','capaian_request','data_sampling_reject','hi_sesuai_isr_reject','hi_tidak_sesuai_isr_reject','hi_tidak_aktif_reject','hi_proses_isr_reject','tl_sesuai_isr_reject','tl_belum_isr_reject','capaian_reject','data_sampling','hi_sesuai_isr','hi_tidak_sesuai_isr','hi_tidak_aktif','hi_proses_isr','tl_sesuai_isr','tl_belum_isr','capaian'));
    }

public function save(Request $request)
{
    $inspeksi = new Inspeksi;

        // return Auth::user()->role_id;

    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
      $inspeksi->status = 1;
      $inspeksi->id_prov = $request->upt_provinsi;
      $inspeksi->kode_upt = $request->id_upt;           
    }else{
       $inspeksi->status = 0;
       $inspeksi->id_prov = Auth::user()->province_code;
       $inspeksi->kode_upt = Auth::user()->upt;
    }

    $inspeksi->tanggal_lapor = $request->tanggal_lapor;
    $inspeksi->data_sampling = $request->data_sampling;
    $inspeksi->hi_sesuai_isr = $request->hi_sesuai_isr;
    $inspeksi->hi_tidak_sesuai_isr = $request->hi_tidak_sesuai_isr;
    $inspeksi->hi_tidak_aktif = $request->hi_tidak_aktif;
    $inspeksi->hi_proses_isr = $request->hi_proses_isr;
    $inspeksi->tl_sesuai_isr = $request->tl_sesuai_isr;
    $inspeksi->tl_belum_isr = $request->tl_belum_isr;
    if($request->data_sampling == 0){
        $total_capaian = 0;
    }else{
        $total_capaian = ( $request->hi_sesuai_isr + $request->tl_sesuai_isr ) / ($request->data_sampling ) * 100;
    }
    $inspeksi->capaian_valid = $total_capaian;
    $inspeksi->keterangan = $request->keterangan;
    if ($request->hasFile('lampiran')) {
        $files = $request->file('lampiran');
        $cover = $request->tanggal_lapor."-".$request->data_sampling."-lampiran"."."
        .$files->getClientOriginalExtension();
        $files->move(public_path('lampiran/inspeksi/inspeksi-data'), $cover);
        $inspeksi->lampiran = $cover; 
    }
    $inspeksi->active = $request->active;
    $inspeksi->type = $request->type;
    $inspeksi->created_by = Auth::user()->id;
    $inspeksi->save();
    $notification = new Notification;
    $nama_user = Auth::user()->name;
    $notification->id_module = 15;
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        $notification->id_prov = Auth::user()->province_code;
        $notification->id_upt = Auth::user()->upt;
    }else{
        $notification->id_prov = Auth::user()->province_code;
        $notification->id_upt = Auth::user()->upt;
    }
    $notification->readable = 0;
    $notification->message = $nama_user.' Telah Berhasil Menambah Data Inspeksi';
    $notification->created_at = date('Y-m-d H:m:s');
    $notification->updated_at = date('Y-m-d H:m:s');
    $notification->save();
    Session::flash('info', 'Inspeksi');
    Session::flash('colors', 'green');
    Session::flash('icons', 'fas fa-clipboard-check');
    Session::flash('alert', 'Berhasil Menambah Data!');
    return redirect()->back();
}

public function update(Request $request)
{
 $id = Crypt::decryptString($request->id);
 $inspeksi = Inspeksi::find($id);
 $inspeksi->status = 1;

 if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
   if($request->upt_provinsi == null || $request->upt_provinsi == ''){
    $inspeksi->id_prov = $inspeksi->id_prov;
    $inspeksi->kode_upt = $inspeksi->kode_upt;
}else{
    $inspeksi->id_prov = $request->upt_provinsi;
    $inspeksi->kode_upt = $request->id_upt;
}

}else{
   $inspeksi->id_prov = Auth::user()->province_code;
   $inspeksi->kode_upt = Auth::user()->upt;
}

$inspeksi->tanggal_lapor = $request->tanggal_lapor;
$inspeksi->data_sampling = $request->data_sampling;
$inspeksi->hi_sesuai_isr = $request->hi_sesuai_isr;
$inspeksi->hi_tidak_sesuai_isr = $request->hi_tidak_sesuai_isr;
$inspeksi->hi_tidak_aktif = $request->hi_tidak_aktif;
$inspeksi->hi_proses_isr = $request->hi_proses_isr;
$inspeksi->tl_sesuai_isr = $request->tl_sesuai_isr;
$inspeksi->tl_belum_isr = $request->tl_belum_isr;
if($request->data_sampling == 0){
    $total_capaian = 0;
}else{
   $total_capaian = ( $request->hi_sesuai_isr + $request->tl_sesuai_isr ) / ($request->data_sampling) * 100;
}
$inspeksi->capaian_valid = $total_capaian;
$inspeksi->keterangan = $request->keterangan;
if ($request->hasFile('lampiran')) {
   $files = $request->file('lampiran');
   $cover = $request->type."-".$request->tanggal_lapor."-lampiran"."."
   .$files->getClientOriginalExtension();
   $files->move(public_path('lampiran/inspeksi/inspeksi-data'), $cover);
   $inspeksi->lampiran = $cover;  
}
$inspeksi->active = $request->active;
$inspeksi->type = $request->type;
$inspeksi->created_by = Auth::user()->id;
$inspeksi->save();
$notification = new Notification;
$nama_user = Auth::user()->name;
$notification->id_module = 15;
if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
    $notification->id_prov = Auth::user()->province_code;
    $notification->id_upt = Auth::user()->upt;
}else{
    $notification->id_prov = Auth::user()->province_code;
    $notification->id_upt = Auth::user()->upt;
}
$notification->readable = 0;
$notification->message = $nama_user.' Telah Berhasil Mengubah Data Inspeksi';
$notification->created_at = date('Y-m-d H:m:s');
$notification->updated_at = date('Y-m-d H:m:s');
$notification->save();
Session::flash('info', 'Inspeksi');
Session::flash('colors', 'green');
Session::flash('icons', 'fas fa-clipboard-check');
Session::flash('alert', 'Berhasil Mengubah Data!');
return redirect()->back();
}

public function delete(Request $request){
    $id = Crypt::decryptString($request->id);
    Inspeksi::where('id',$id)->delete();
    $notification = new Notification;
    $nama_user = Auth::user()->name;
    $notification->id_module = 15;
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        $notification->id_prov = Auth::user()->province_code;
        $notification->id_upt = Auth::user()->upt;
    }else{
        $notification->id_prov = Auth::user()->province_code;
        $notification->id_upt = Auth::user()->upt;
    }
    $notification->readable = 0;
    $notification->message = $nama_user.' Telah Berhasil Menghapus Data Inspeksi';
    $notification->created_at = date('Y-m-d H:m:s');
    $notification->updated_at = date('Y-m-d H:m:s');
    $notification->save();
    Session::flash('info', 'Inspeksi');
    Session::flash('colors', 'red');
    Session::flash('icons', 'fas fa-trash');
    Session::flash('alert', 'Berhasil Menghapus Data!');
    return redirect()->back();
}

public function approved(Request $request){
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
        if ($request->getStatus == 0) {
            $ids = $request->id;
            if ($request->id) {
                $action = $request->action;
                Inspeksi::whereIn('id', $ids)
                ->update(['status' => $action]);
                if ($action == 1) {
                    $notification = new Notification;
                    $nama_user = Auth::user()->name;
                    $notification->id_module = 15;
                    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
                        $notification->id_prov = Auth::user()->province_code;
                        $notification->id_upt = Auth::user()->upt;
                    }else{
                        $notification->id_prov = Auth::user()->province_code;
                        $notification->id_upt = Auth::user()->upt;
                    }
                    $notification->readable = 0;
                    $notification->message = $nama_user.' Telah Berhasil Mengapprove Data Inspeksi';
                    $notification->created_at = date('Y-m-d H:m:s');
                    $notification->updated_at = date('Y-m-d H:m:s');
                    $notification->save();
                    Session::flash('info', 'Inspeksi');
                    Session::flash('colors', 'green');
                    Session::flash('icons', 'fas fa-clipboard-check');
                    Session::flash('alert', 'Berhasil Mengapprove Data!');
                }else{
                    $notification = new Notification;
                    $nama_user = Auth::user()->name;
                    $notification->id_module = 15;
                    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
                        $notification->id_prov = Auth::user()->province_code;
                        $notification->id_upt = Auth::user()->upt;
                    }else{
                        $notification->id_prov = Auth::user()->province_code;
                        $notification->id_upt = Auth::user()->upt;
                    }
                    $notification->readable = 0;
                    $notification->message = $nama_user.' Telah Berhasil Merejcet Data Inspeksi';
                    $notification->created_at = date('Y-m-d H:m:s');
                    $notification->updated_at = date('Y-m-d H:m:s');
                    $notification->save();
                    Session::flash('info', 'Inspeksi');
                    Session::flash('colors', 'red');
                    Session::flash('icons', 'fas fa-clipboard-check');
                    Session::flash('alert', 'Berhasil Mereject Data!');
                }
                return redirect()->back();
            }
            else{
                Session::flash('info', 'Inspeksi');
                Session::flash('colors', 'red');
                Session::flash('icons', 'fas fa-clipboard-check');
                Session::flash('alert', 'Belum Memilih Data!');
                return redirect()->back();
            }   
        }
        else{
            Session::flash('info', 'Inspeksi');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Status Salah!');
            return redirect()->back();
        }

    }else{
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Tidak Memiliki Akses!');
        return redirect()->back();
    }
}

public function reupload($id){
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator') {
        $rensos = Inspeksi::find($id);
        $rensos->status = 0;
        $rensos->save();
        $notification = new Notification;
        $nama_user = Auth::user()->name;
        $notification->id_module = 15;
        if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
            $notification->id_prov = Auth::user()->province_code;
            $notification->id_upt = Auth::user()->upt;
        }else{
            $notification->id_prov = Auth::user()->province_code;
            $notification->id_upt = Auth::user()->upt;
        }
        $notification->readable = 0;
        $notification->message = $nama_user.' Telah Berhasil Mereupload Data Loket Pelayanan';
        $notification->created_at = date('Y-m-d H:m:s');
        $notification->updated_at = date('Y-m-d H:m:s');
        $notification->save();
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Berhasil Mengirim Ulang');
        return redirect()->back();
    }
    else{
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Tidak Memiliki Akses!');
        return redirect()->back();
    }
}

public function updateRejected(Request $request)
{
 $id = Crypt::decryptString($request->id);
 $inspeksi = Inspeksi::find($id);
 $inspeksi->status = 3;

 if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
    if($request->upt_provinsi == null || $request->upt_provinsi == ''){
        $inspeksi->id_prov = $inspeksi->id_prov;
        $inspeksi->kode_upt = $inspeksi->kode_upt;
    }else{
        $inspeksi->id_prov = $request->upt_provinsi;
        $inspeksi->kode_upt = $request->id_upt;
    }

}else{
    $inspeksi->id_prov = Auth::user()->province_code;
    $inspeksi->kode_upt = Auth::user()->upt;
}

$inspeksi->tanggal_lapor = $request->tanggal_lapor;
$inspeksi->data_sampling = $request->data_sampling;
$inspeksi->hi_sesuai_isr = $request->hi_sesuai_isr;
$inspeksi->hi_tidak_sesuai_isr = $request->hi_tidak_sesuai_isr;
$inspeksi->hi_tidak_aktif = $request->hi_tidak_aktif;
$inspeksi->hi_proses_isr = $request->hi_proses_isr;
$inspeksi->tl_sesuai_isr = $request->tl_sesuai_isr;
$inspeksi->tl_belum_isr = $request->tl_belum_isr;
$total_capaian = ( $request->hi_sesuai_isr + $request->tl_sesuai_isr ) / ($request->data_sampling) * 100;
$inspeksi->capaian_valid = $total_capaian;
$inspeksi->keterangan = $request->keterangan;
if ($request->hasFile('lampiran')) {
    $files = $request->file('lampiran');
    $cover = $request->type."-".$request->tanggal_lapor."-lampiran"."."
    .$files->getClientOriginalExtension();
    $files->move(public_path('lampiran/inspeksi/inspeksi-data'), $cover);
    $inspeksi->lampiran = $cover;   
}
$inspeksi->type = $request->type;
$inspeksi->created_by = Auth::user()->id;
$inspeksi->save();
$notification = new Notification;
$nama_user = Auth::user()->name;
$notification->id_module = 15;
if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
    $notification->id_prov = Auth::user()->province_code;
    $notification->id_upt = Auth::user()->upt;
}else{
    $notification->id_prov = Auth::user()->province_code;
    $notification->id_upt = Auth::user()->upt;
}
$notification->readable = 0;
$notification->message = $nama_user.' Telah Berhasil Mengubah Data Loket Pelayanan';
$notification->created_at = date('Y-m-d H:m:s');
$notification->updated_at = date('Y-m-d H:m:s');
$notification->save();
Session::flash('info', 'Inspeksi');
Session::flash('colors', 'green');
Session::flash('icons', 'fas fa-clipboard-check');
Session::flash('alert', 'Berhasil Mengubah Data!');
return redirect()->back();
}

public function search(Request $request){
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        $changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
        $changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
        $provinceCode = Inspeksi::where('kode_upt',$changeUPT)->value('id_prov');
        $getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
        $date = Inspeksi::select('tanggal_lapor')->where('kode_upt',$changeUPT)->orderBy('tanggal_lapor','desc')->first();
        $time=strtotime($date->tanggal_lapor);
        $month=date("m",$time);
        $year=date("Y",$time);
        if ($changeUPT) {
            $count = $getUPT->count();
            if ($count > 1) {
                return view('templates.inspeksi.inspeksi-data.search.multiple',compact('getUPT'));
            }
            else{
                $inspeksi = Inspeksi::where('kode_upt',$changeUPT)->get();

                $data_sampling_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
                $hi_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
                $hi_tidak_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
                $hi_tidak_aktif_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
                $hi_proses_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
                $tl_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
                $tl_belum_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
                $jumlah_data_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
                if($data_sampling_request == 0){
                    $capaian_request = 0;
                }else{
                    $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request) * 100;
                }
                if ($capaian_request >= 100) {
                    $capaian_request = 100;
                }

                $data_sampling_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
                $hi_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
                $hi_tidak_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
                $hi_tidak_aktif_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
                $hi_proses_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
                $tl_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
                $tl_belum_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
                $jumlah_data_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
                if($data_sampling_reject == 0){
                    $capaian_reject = 0;
                }else{
                    $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject) * 100;
                }

                if ($capaian_reject >= 100) {
                $capaian_reject = 100;
            }


                $data_sampling = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
                $hi_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
                $hi_tidak_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
                $hi_tidak_aktif = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
                $hi_proses_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
                $tl_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
                $tl_belum_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
                $jumlah_data = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
                if($data_sampling == 0){
                    $capaian = 0;
                }else{
                    $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
                }
                if ($capaian >= 100) {
                    $capaian = 100;
                }
            }
            return view('templates.inspeksi.inspeksi-data.search.index',compact('inspeksi','changeUPT','provinceCode','data_sampling_request','hi_sesuai_isr_request','hi_tidak_sesuai_isr_request','hi_tidak_aktif_request','hi_proses_isr_request','tl_sesuai_isr_request','tl_belum_isr_request','jumlah_data_request','capaian_request','data_sampling_reject','hi_sesuai_isr_reject','hi_tidak_sesuai_isr_reject','hi_tidak_aktif_reject','hi_proses_isr_reject','tl_sesuai_isr_reject','tl_belum_isr_reject','jumlah_data_reject','capaian_reject','data_sampling','hi_sesuai_isr','hi_tidak_sesuai_isr','hi_tidak_aktif','hi_proses_isr','tl_sesuai_isr','tl_belum_isr','jumlah_data','capaian'));
        }
        else{
            Session::flash('info', 'Terjadi Kesalahan');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
            return redirect()->back();

        }
    }
    else{
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Tidak Memiliki Akses!');
        return redirect()->back();
    }
}

public function searchMultiple($id,Request $request){
    $month = date('m');
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        $changeUPT = $id;
        $provinceCode = Inspeksi::where('kode_upt',$changeUPT)->value('id_prov');
        $date = Inspeksi::select('tanggal_lapor')->where('kode_upt',$changeUPT)->orderBy('tanggal_lapor','desc')->first();
        $time=strtotime($date->tanggal_lapor);
        $month=date("m",$time);
        $year=date("Y",$time);
        if ($changeUPT) {
            $inspeksi = Inspeksi::where('kode_upt',$changeUPT)->get();

            $data_sampling_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
            $hi_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
            $hi_proses_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
            $tl_sesuai_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
            $tl_belum_isr_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
            $jumlah_data_request = Inspeksi::where('status',0)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
            if($data_sampling_request == 0){
                $capaian_request = 0;
            }else{
                $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request) * 100;
            }

            $data_sampling_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
            $hi_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
            $hi_proses_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
            $tl_sesuai_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
            $tl_belum_isr_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
            $jumlah_data_reject = Inspeksi::where('status',3)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
            if($data_sampling_reject == 0){
                $capaian_reject = 0;
            }else{
                $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject) * 100;
            }

            $data_sampling = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('data_sampling');
            $hi_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_sesuai_isr');
            $hi_tidak_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_sesuai_isr');
            $hi_tidak_aktif = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_tidak_aktif');
            $hi_proses_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('hi_proses_isr');
            $tl_sesuai_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_sesuai_isr');
            $tl_belum_isr = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->sum('tl_belum_isr');
            $jumlah_data = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$changeUPT)->count();
            if($data_sampling == 0){
                $capaian = 0;
            }else{
                $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
            }

            return view('templates.inspeksi.inspeksi-data.search.index',compact('inspeksi','changeUPT','provinceCode','data_sampling_request','hi_sesuai_isr_request','hi_tidak_sesuai_isr_request','hi_tidak_aktif_request','hi_proses_isr_request','tl_sesuai_isr_request','tl_belum_isr_request','jumlah_data_request','capaian_request','data_sampling_reject','hi_sesuai_isr_reject','hi_tidak_sesuai_isr_reject','hi_tidak_aktif_reject','hi_proses_isr_reject','tl_sesuai_isr_reject','tl_belum_isr_reject','jumlah_data_reject','capaian_reject','data_sampling','hi_sesuai_isr','hi_tidak_sesuai_isr','hi_tidak_aktif','hi_proses_isr','tl_sesuai_isr','tl_belum_isr','jumlah_data','capaian'));
        }
        else{
            Session::flash('info', 'Terjadi Kesalahan');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
            return redirect()->back();

        }
    }
    else{
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Tidak Memiliki Akses!');
        return redirect()->back();
    }
}

public function print(Request $request){
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        // if ($date && $month && $year) {
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $month){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($date){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->get();
        // }elseif($month && $year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($month){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereYear('tanggal_lapor',$year)->get();
        // }else{
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->get();
        // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
            $inspeksi['inspeksi'] = Inspeksi::where('status',1)->get();
        }
    }else{
        // if ($date && $month && $year) {
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($date && $month){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($date){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($month && $year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($date && $year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($month){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereMonth('tanggal_lapor',$month)->where('kode_upt', Auth::user()->upt)->get();
        // }elseif($year){
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->whereYear('tanggal_lapor',$year)->where('kode_upt', Auth::user()->upt)->get();
        // }else{
        //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt', Auth::user()->upt)->get();
        // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi['inspeksi'] = Inspeksi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
           $inspeksi['inspeksi'] = Inspeksi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
       }
   }
   $pdf = PDF::loadView('templates.inspeksi.inspeksi-data.dev.print', $inspeksi)->setPaper('a3','landscape');
   return $pdf->download('Data-Inspeksi-'.date('d-F-Y').'.pdf');
}

public function print_search($id, Request $request){
    if($id == null){
        return redirect()->back();
    }
    else{
    //     $date = $request->date;
    //     $month = $request->month;
    //     $year = $request->year;
    //     if ($date && $month && $year) {
    //      $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
    //  }elseif($date && $month){
    //      $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->get();
    //  }elseif($date){
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereDay('tanggal_lapor',$date)->get();
    // }elseif($month && $year){
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
    // }elseif($date && $year){
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->get();
    // }elseif($month){
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereMonth('tanggal_lapor',$month)->get();
    // }elseif($year){
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereYear('tanggal_lapor',$year)->get();
    // }else{
    //     $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->get();
    // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
            $inspeksi['inspeksi'] = Inspeksi::where('status', 1)->where('kode_upt',$id)->get();
        }
        $getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
        $pdf = PDF::loadView('templates.inspeksi.inspeksi-data.dev.print-search', $inspeksi)->setPaper('a3','landscape');
        return $pdf->download('Monev Sosialisasi Sosialisasi '.$getUptName.'-'.date('d-F-Y').'.pdf');
    }
}

public function export(Request $request)
{
    $push = [];
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        // $date = request()->date;
        // $month = request()->month;
        // $year = request()->year;
        // if ($date && $month && $year) {
        //     $inspeksi = Inspeksi::where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $month){
        //     $inspeksi = Inspeksi::where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($date){
        //     $inspeksi = Inspeksi::where('status',1)->whereDay('tanggal_lapor',$date)->get();
        // }elseif($month && $year){
        //     $inspeksi = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $year){
        //     $inspeksi = Inspeksi::where('status',1)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($month){
        //     $inspeksi = Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($year){
        //     $inspeksi = Inspeksi::where('status',1)->whereYear('tanggal_lapor',$year)->get();
        // }else{
        //     $inspeksi = Inspeksi::where('status',1)->get();
        // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi = Inspeksi::where('status', 1)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
            $inspeksi = Inspeksi::where('status',1)->get();
        }

    }else{
        // $date = request()->date;
        // $month = request()->month;
        // $year = request()->year;
        // if ($date && $month && $year) {
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $month){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($date){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_lapor',$date)->get();
        // }elseif($month && $year){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $year){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($month){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($year){
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereYear('tanggal_lapor',$year)->get();
        // }else{
        //     $inspeksi = Inspeksi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();  
        // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi = Inspeksi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
           $inspeksi = Inspeksi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->where('status', 1)->get();
       }
   }
   $export = new InspeksiDataExport([          
    $push,
]);
   foreach ($inspeksi as $key => $value) {           
    $provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
    $nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
    $dibuat = Auth::user()->where('id', $value->created_by)->value('name');
    $bulan = Carbon::parse($value->tanggal_lapor)->format('F');
    $tahun = Carbon::parse($value->tanggal_lapor)->format('Y');

            // return $perusahaan;

    array_push($push, array(
        $value->id,
        $tahun,
        $bulan,
        $provinsi,
        $nama_upt,
        $value->tanggal_lapor,
        $value->data_sampling,
        $value->hi_sesuai_isr,
        $value->hi_tidak_sesuai_isr,
        $value->hi_tidak_aktif,
        $value->hi_proses_isr,
        $value->tl_sesuai_isr,
        $value->tl_belum_isr,
        $value->capaian_valid,
        $value->keterangan,
        $value->lampiran,
        $value->type,
        $dibuat,
    ));

    $export = new InspeksiDataExport([          
        $push,
    ]);
}
return Excel::download($export, 'Inspeksi-'.date('d-F-Y').'.xlsx');
}

public function export_search(Request $request, $id){
    $changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
    if($id == null){
        return redirect()->back();
    }
    else{
        $push = [];

        // $date = request()->date;
        // $month = request()->month;
        // $year = request()->year;
        // if ($date && $month && $year) {
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $month){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_lapor',$date)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($date){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_lapor',$date)->get();
        // }elseif($month && $year){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($date && $year){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_lapor',$date)->whereYear('tanggal_lapor',$year)->get();
        // }elseif($month){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_lapor',$month)->get();
        // }elseif($year){
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereYear('tanggal_lapor',$year)->get();
        // }else{
        //     $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->get();;
        // }
        $dari = $request->dari;
        $sampai = $request->sampai;
        if ($dari && $sampai) {
            $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->whereBetween('tanggal_lapor', array($dari, $sampai))->get();
        }else{
            $inspeksi = Inspeksi::where('kode_upt',$id)->where('status',1)->get();
        }

        $export = new InspeksiDataExport([          
            $push,
        ]);

        foreach ($inspeksi as $key => $value) {    
            $provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
            $nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
            $dibuat = Auth::user()->where('id', $value->created_by)->value('name');
            $bulan = Carbon::parse($value->tanggal_lapor)->format('F');
            $tahun = Carbon::parse($value->tanggal_lapor)->format('Y');

                // return $perusahaan;

            array_push($push, array(
                $value->id,
                $tahun,
                $bulan,
                $provinsi,
                $nama_upt,
                $value->tanggal_lapor,
                $value->data_sampling,
                $value->hi_sesuai_isr,
                $value->hi_tidak_sesuai_isr,
                $value->hi_tidak_aktif,
                $value->hi_proses_isr,
                $value->tl_sesuai_isr,
                $value->tl_belum_isr,
                $value->capaian_valid,
                $value->keterangan,
                $value->lampiran,
                $value->type,
                $dibuat,
            ));

            $export = new InspeksiDataExport([          
                $push,
            ]);
        }
        return Excel::download($export, 'Inspeksi '.$changeName.'-'.date('d-F-Y').'.xlsx');
    }
}

public function importPreview(Request $request)
{
    $this->validate($request,[
        'Import' => 'required|file|mimes:xls,xlsx',
    ]);

        // Excel::import(new SettleImport, request()->file('Import'));    
    $collection = (new InspeksiDataImport)->toArray(request()->file('Import'));

    $body = $collection[0];

    $header = $body[0];

    $result = array_splice($body, 1);

    // return $header[8];

    if($header[0] == 'Tanggal Lapor' && $header[1] == 'Data Sampling' && $header[2] == 'Sesuai ISR' && $header[3] == 'Tidak Sesuai ISR' && $header[4] == 'Tidak Aktif' && $header[5] == 'Proses ISR' && $header[6] == 'Sudah Tidaklanjut' && $header[7] == 'Belum Tindak Lanjut' && $header[8] == 'Keterangan'){            
        return view('templates.inspeksi.inspeksi-data.import.preview')
        ->with('result', $result);
    }else{
        Session::flash('info', 'Inspeksi');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Data header tidak cocok!');        
        return redirect()->back();
    }
}

public function importPost(Request $request)
{
    $data = $request->all();            
    $tanggal_lapor = $data['tanggal_lapor'];
    $data_sampling = $data['data_sampling'];
    $hi_sesuai_isr = $data['hi_sesuai_isr'];
    $hi_tidak_sesuai_isr = $data['hi_tidak_sesuai_isr'];
    $hi_tidak_aktif = $data['hi_tidak_aktif'];
    $hi_proses_isr = $data['hi_proses_isr'];
    $tl_sesuai_isr = $data['tl_sesuai_isr'];
    $tl_belum_isr = $data['tl_belum_isr'];
    if($request->lampiran){
        $lampiran = $data['lampiran'];
    }else{
        $lampiran = '';
    }
    $keterangan = $data['keterangan'];
    // $type = $data['type'];         
    $created_at = date('Y-m-d H:m');
    $updated_at = date('Y-m-d H:m');
    if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
        $id_prov = $request->upt_provinsi;
        $kode_upt = $request->id_upt;
        $status = 1;
    }
    else{
        $id_prov = Auth::user()->province_code;  
        $kode_upt = Auth::user()->upt;           
        $status = 0;
    }
    $rows = [];
    foreach($tanggal_lapor as $key => $input) {
        $uniq = uniqid();
        if(isset($lampiran[$key])){
            $namaLampiran = 'Inspeksi-data-'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension();
            $lampiran[$key]->move(public_path('lampiran/inspeksi/data'),'Inspeksi-data-'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension());
        }else{
            $namaLampiran = '';
        }
        $total_capaian = 0;
        if(isset($data_sampling[$key])){
            if($data_sampling[$key] == 0){
                $total_capaian = 0;
            }else{
                // $total_capaian = ( $hi_sesuai_isr[$key] + $tl_sesuai_isr[$key] ) / $data_sampling[$key]  * 100;
                $total_capaian = ( $hi_sesuai_isr[$key] + $tl_sesuai_isr[$key] ) / ($data_sampling[$key]) * 100;;
            }
        }else{
            $total_capaian = 0;
        }

        array_push($rows, [
            'tanggal_lapor' => isset($tanggal_lapor[$key]) ? $tanggal_lapor[$key] : '',
            'data_sampling' => isset($data_sampling[$key]) ? $data_sampling[$key] : '',           
            'hi_sesuai_isr' => isset($hi_sesuai_isr[$key]) ? $hi_sesuai_isr[$key] : '',
            'hi_tidak_sesuai_isr' => isset($hi_tidak_sesuai_isr[$key]) ? $hi_tidak_sesuai_isr[$key] : '',
            'hi_tidak_aktif' => isset($hi_tidak_aktif[$key]) ? $hi_tidak_aktif[$key] : '',
            'hi_proses_isr' => isset($hi_proses_isr[$key]) ? $hi_proses_isr[$key] : '',
            'hi_sesuai_isr' => isset($hi_sesuai_isr[$key]) ? $hi_sesuai_isr[$key] : '',
            'tl_sesuai_isr' => isset($tl_sesuai_isr[$key]) ? $tl_sesuai_isr[$key] : '',
            'tl_belum_isr' => isset($tl_belum_isr[$key]) ? $tl_belum_isr[$key] : '',
            'capaian_valid' => $total_capaian,
            'lampiran' => $namaLampiran,
            'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
            // 'type' => isset($type[$key]) ? $type[$key] : '',
            'created_by' => Auth::user()->id,     
            'id_prov' => $id_prov,
            'kode_upt' => $kode_upt,
            'status' => $status,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            
        ]);
    }
    $hasil = Inspeksi::insert($rows);
    Session::flash('info', 'Inspeksi');
    Session::flash('colors', 'green');
    Session::flash('icons', 'fas fa-clipboard-check');
    Session::flash('alert', 'Berhasil Mengimport Data!');       
    return redirect(url('inspeksi/data'));

}


public function downloadSampel(){

    $responses = response()->download(storage_path("app/public/sampelInspeksi.xlsx"));
    ob_end_clean();
    return $responses;

}

public function filterData(Request $request)
{
    if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
        $inspeksiss = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->get();

        $data_sampling_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('data_sampling');
        $hi_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('hi_tidak_aktif');
        $hi_proses_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('hi_proses_isr');
        $tl_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('tl_sesuai_isr');
        $tl_belum_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->sum('tl_belum_isr');
        $jumlah_data_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->count();
        if($data_sampling_request == 0){
            $capaian_request = 0;
        }else{
            $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request ) * 100;
        }

        $data_sampling_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('data_sampling');
        $hi_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('hi_tidak_aktif');
        $hi_proses_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('hi_proses_isr');
        $tl_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('tl_sesuai_isr');
        $tl_belum_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->sum('tl_belum_isr');
        $jumlah_data_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->count();
        if($data_sampling_reject == 0){
            $capaian_reject = 0;
        }else{
            $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject ) * 100;
        }


        $data_sampling = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('data_sampling');
        $hi_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('hi_tidak_aktif');
        $hi_proses_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('hi_proses_isr');
        $tl_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('tl_sesuai_isr');
        $tl_belum_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->sum('tl_belum_isr');
        $jumlah_data = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->count();
        if($data_sampling == 0){
            $capaian = 0;
        }else{
            $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling ) * 100;
        }

    }
    else{
        $inspeksiss = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->get();

        $data_sampling_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
        $hi_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
        $hi_proses_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
        $tl_sesuai_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
        $tl_belum_isr_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
        $jumlah_data_request = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',0)->where('kode_upt',Auth::user()->upt)->count();
        if($data_sampling_request == 0){
            $capaian_request = 0;
        }else{
            $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request ) * 100;
        }

        $data_sampling_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
        $hi_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
        $hi_proses_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
        $tl_sesuai_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
        $tl_belum_isr_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
        $jumlah_data_reject = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',3)->where('kode_upt',Auth::user()->upt)->count();
        if($data_sampling_request == 0){
            $capaian_reject = 0;
        }else{
            $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject ) * 100;
        }

        $data_sampling = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
        $hi_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
        $hi_tidak_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_sesuai_isr');
        $hi_tidak_aktif = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('hi_tidak_aktif');
        $hi_proses_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('hi_proses_isr');
        $tl_sesuai_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
        $tl_belum_isr = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->sum('tl_belum_isr');
        $jumlah_data = Inspeksi::whereBetween('tanggal_lapor', array($request->dari, $request->sampai))->where('status',1)->where('kode_upt',Auth::user()->upt)->count();
        if($data_sampling == 0){
            $capaian = 0;
        }else{
            $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling ) * 100;
        }
    }
    return view('templates.inspeksi.inspeksi-data.core.index', compact('inspeksiss','data_sampling_request','hi_sesuai_isr_request','hi_tidak_sesuai_isr_request','hi_tidak_aktif_request','hi_proses_isr_request','tl_sesuai_isr_request','tl_belum_isr_request','jumlah_data_request','capaian_request','data_sampling_reject','hi_sesuai_isr_reject','hi_tidak_sesuai_isr_reject','hi_tidak_aktif_reject','hi_proses_isr_reject','tl_sesuai_isr_reject','tl_belum_isr_reject','jumlah_data_reject','capaian_reject','data_sampling','hi_sesuai_isr','hi_tidak_sesuai_isr','hi_tidak_aktif','hi_proses_isr','tl_sesuai_isr','tl_belum_isr','jumlah_data','capaian'));

    Session::flash('info', 'Filter Inspeksi Data');
    Session::flash('colors', 'green');
    Session::flash('icons', 'fas fa-clipboard-check');
    Session::flash('alert', 'Data Berhasil Diambil!');
    return view('templates.inspeksi.inspeksi-data.core.index', compact('inspeksi'));
}

public function searchByUpt(Request $request)
{

    $inspeksi = Inspeksi::where('kode_upt', $request->upt)->get();
    $date = Inspeksi::select('tanggal_lapor')->where('kode_upt',$request->upt)->orderBy('tanggal_lapor','desc')->first();
    if (!$date) {
       $month=date("m");
       $year=date("Y");
    }else{
       $time=strtotime($date->tanggal_lapor);
       $month=date("m",$time);
       $year=date("Y",$time);
    }
    

    $data_sampling_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('data_sampling');
    $hi_sesuai_isr_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_sesuai_isr');
    $hi_tidak_sesuai_isr_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_tidak_sesuai_isr');
    $hi_tidak_aktif_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_tidak_aktif');
    $hi_proses_isr_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_proses_isr');
    $tl_sesuai_isr_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_sesuai_isr');
    $tl_belum_isr_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_belum_isr');
    $jumlah_data_request = Inspeksi::where('status',0)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->count();
    if($data_sampling_request == 0){
        $capaian_request = 0;
    }else{
        $capaian_request = ( $hi_sesuai_isr_request + $tl_sesuai_isr_request ) / ($data_sampling_request ) * 100;
    }

    $data_sampling_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('data_sampling');
    $hi_sesuai_isr_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_sesuai_isr');
    $hi_tidak_sesuai_isr_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->sum('hi_tidak_sesuai_isr');
    $hi_tidak_aktif_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_tidak_aktif');
    $hi_proses_isr_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_proses_isr');
    $tl_sesuai_isr_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_sesuai_isr');
    $tl_belum_isr_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_belum_isr');
    $jumlah_data_reject = Inspeksi::where('status',3)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->count();
    if($data_sampling_reject == 0){
        $capaian_reject = 0;
    }else{
        $capaian_reject = ( $hi_sesuai_isr_reject + $tl_sesuai_isr_reject ) / ($data_sampling_reject ) * 100;
    }

    $data_sampling = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('data_sampling');
    $hi_sesuai_isr = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_sesuai_isr');
    $hi_tidak_sesuai_isr = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_tidak_sesuai_isr');
    $hi_tidak_aktif = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_tidak_aktif');
    $hi_proses_isr = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('hi_proses_isr');
    $tl_sesuai_isr = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_sesuai_isr');
    $tl_belum_isr = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->sum('tl_belum_isr');
    $jumlah_data = Inspeksi::where('status',1)->where('kode_upt',$request->upt)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->count();
    if($data_sampling == 0){
        $capaian = 0;
    }else{
        $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling ) * 100;
    }

    if($inspeksi){
        Session::flash('info', 'Inspeksi Data');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Data UPT Berhasil Diambil!');
    }else{
        Session::flash('info', 'Inspeksi Data');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-times');
        Session::flash('alert', 'Data Kosong!');
    }

    return view('templates.inspeksi.inspeksi-data.core.index', compact('inspeksi','data_sampling_request','hi_sesuai_isr_request','hi_tidak_sesuai_isr_request','hi_tidak_aktif_request','hi_proses_isr_request','tl_sesuai_isr_request','tl_belum_isr_request','jumlah_data_request','capaian_request','data_sampling_reject','hi_sesuai_isr_reject','hi_tidak_sesuai_isr_reject','hi_tidak_aktif_reject','hi_proses_isr_reject','tl_sesuai_isr_reject','tl_belum_isr_reject','jumlah_data_reject','capaian_reject','data_sampling','hi_sesuai_isr','hi_tidak_sesuai_isr','hi_tidak_aktif','hi_proses_isr','tl_sesuai_isr','tl_belum_isr','jumlah_data','capaian'));
}
}
