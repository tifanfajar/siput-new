<?php

namespace App\Http\Controllers\Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SosialisasiBimtek\RencanaSosialisasi;
use Illuminate\Support\Facades\Crypt;
use App\Imports\RencanaSosialisasiImport;
use App\Exports\RencanaSosialisasiExport;
use App\Model\Notification\Notification;
use App\Model\Region\Provinsi;
use App\Model\Setting\UPT;
use App\Model\Privillage\Role;
use App\Model\Refrension\Kegiatan;
use Carbon\Carbon;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class RencanaSosialisasiController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$rensos = RencanaSosialisasi::all();
		}
		else{
			$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->get();
		}
		return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.core.index',compact('rensos'));
	}
	public function save(Request $request){
		$rensos = new RencanaSosialisasi;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$rensos->id_prov = $request->upt_provinsi;
			$rensos->kode_upt = $request->id_upt;
		}else{
			$rensos->id_prov = Auth::user()->province_code;
			$rensos->kode_upt = Auth::user()->upt;
		}
		$rensos->tempat = $request->tempat;
		$rensos->jenis_kegiatan = $request->jenis_kegiatan;
		$rensos->tanggal_pelaksanaan = $request->tanggal_pelaksanaan;
		$rensos->tema = $request->tema;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->tema."-".$request->tanggal_pelaksanaan."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/rencanasosialisasi'), $cover);
			$rensos->lampiran = $cover;	
		}
		$rensos->narasumber = $request->narasumber;
		$rensos->target_peserta = $request->target_peserta;
		$rensos->jumlah_peserta = $request->jumlah_peserta;
		$rensos->anggaran = $request->anggaran;
		$rensos->keterangan = $request->keterangan;
		$rensos->created_by = Auth::user()->id;
		$rensos->status = 1;
		$rensos->active = 1;
		$rensos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->upt_provinsi)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 12;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->upt_provinsi;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data Rencana Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();


		Session::flash('info', 'Rencana Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$rensos = RencanaSosialisasi::find($id);
		$rensos->tempat = $request->tempat;
		$rensos->jenis_kegiatan = $request->jenis_kegiatan;
		$rensos->tanggal_pelaksanaan = $request->tanggal_pelaksanaan;
		$rensos->tema = $request->tema;		
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->tema."-".$request->tanggal_pelaksanaan."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/rencanasosialisasi'), $cover);
			$rensos->lampiran = $cover;	
		}
		$rensos->narasumber = $request->narasumber;
		$rensos->target_peserta = $request->target_peserta;
		$rensos->jumlah_peserta = $request->jumlah_peserta;
		$rensos->anggaran = $request->anggaran;
		$rensos->keterangan = $request->keterangan;
		$rensos->created_by = Auth::user()->id;
		if($request->active == null || $request->active == ''){
			$rensos->active = $rensos->active;
		}else{
			$rensos->active = $request->active;
		}
		$rensos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$rensos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 12;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $rensos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Rencana Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Rencana Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		$rensos = RencanaSosialisasi::where('id',$id)->first();
		RencanaSosialisasi::where('id',$id)->delete();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$rensos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 12;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $rensos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Rencana Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Rencana Sosialisasi');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	// public function approved(Request $request){
	// 	if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
	// 		if ($request->getStatus == 0) {
	// 			$ids = $request->id;
	// 			$action = $request->action;
	// 			if ($request->id) {
	// 				$action = $request->action;
	// 				DB::table('rencana_sosialisasis')->whereIn('id', $ids)->update(['status' => $action]);
	// 				if ($action == 1) {
	// 					Session::flash('info', 'Rencana Sosialisasi');
	// 					Session::flash('colors', 'green');
	// 					Session::flash('icons', 'fas fa-clipboard-check');
	// 					Session::flash('alert', 'Berhasil Mengapprove Data!');
	// 				}else{
	// 					Session::flash('info', 'Rencana Sosialisasi');
	// 					Session::flash('colors', 'red');
	// 					Session::flash('icons', 'fas fa-clipboard-check');
	// 					Session::flash('alert', 'Berhasil Mereject Data!');
	// 				}
	// 				return redirect()->back();
	// 			}
	// 			else{
	// 				Session::flash('info', 'Rencana Sosialisasi');
	// 				Session::flash('colors', 'red');
	// 				Session::flash('icons', 'fas fa-clipboard-check');
	// 				Session::flash('alert', 'Belum Memilih Data!');
	// 				return redirect()->back();
	// 			}	
	// 		}
	// 		else{
	// 			Session::flash('info', 'Rencana Sosialisasi');
	// 			Session::flash('colors', 'red');
	// 			Session::flash('icons', 'fas fa-clipboard-check');
	// 			Session::flash('alert', 'Status Salah!');
	// 			return redirect()->back();
	// 		}
	// 	}else{
	// 		Session::flash('info', 'Rencana Sosialisasi');
	// 		Session::flash('colors', 'red');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Tidak Memiliki Akses!');
	// 		return redirect()->back();
	// 	}
	// }
	// public function reupload($id){
	// 	if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator') {
	// 		$rensos = RencanaSosialisasi::find($id);
	// 		$rensos->status = 0;
	// 		$rensos->save();
	// 		Session::flash('info', 'Rencana Sosialisasi');
	// 		Session::flash('colors', 'green');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Berhasil Mengirim Ulang');
	// 		return redirect()->back();
	// 	}
	// 	else{
	// 		Session::flash('info', 'Rencana Sosialisasi');
	// 		Session::flash('colors', 'red');
	// 		Session::flash('icons', 'fas fa-clipboard-check');
	// 		Session::flash('alert', 'Tidak Memiliki Akses!');
	// 		return redirect()->back();
	// 	}
	// }
	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$provinceCode = RencanaSosialisasi::where('kode_upt',$changeUPT)->value('id_prov');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.search.multiple',compact('getUPT'));
				}
				else{
					$rensos = RencanaSosialisasi::where('kode_upt',$changeUPT)->get();
				}
				return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.search.index',compact('rensos','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Rencana Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$provinceCode = RencanaSosialisasi::where('kode_upt',$changeUPT)->value('id_prov');
			if ($changeUPT) {
				$rensos = RencanaSosialisasi::where('kode_upt',$changeUPT)->get();
				return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.search.index',compact('rensos','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Rencana Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function print(Request $request){
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$rensos['rensos'] = RencanaSosialisasi::whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $month){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($date){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereDay('tanggal_pelaksanaan',$date)->get();
			// }elseif($month && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($month){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($year){
			// 	$rensos['rensos'] = RencanaSosialisasi::whereYear('tanggal_pelaksanaan',$year)->get();
			// }else{
			// 	$rensos['rensos'] = RencanaSosialisasi::all();
			// }
			$dari = $request->dari;
		    $sampai = $request->sampai;
		    if ($dari && $sampai) {
	            $rensos['rensos'] = RencanaSosialisasi::whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
	        }else{
	            $rensos['rensos'] = RencanaSosialisasi::all();
	        }
	    }else{
	    	$dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $rensos['rensos'] = RencanaSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
	        }else{
	             $rensos['rensos'] = RencanaSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
	        }
	    } 
		$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.dev.print', $rensos)->setPaper('a3','landscape');
		return $pdf->download('Data-Rencana-Sosialisasi.pdf');
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $month){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($date){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->get();
			// }elseif($month && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($month){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }else{
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->get();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
			}else{
				$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.dev.print-search', $rensos)->setPaper('a3','landscape');
			return $pdf->download('Rencana Sosialisasi '.$getUptName.'.pdf');
		}
	}
	public function export(Request $request)
	{
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->upt == 0) {
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos = RencanaSosialisasi::where('status',1)->get();;
            // }
            $dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $rensos = RencanaSosialisasi::whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
	        }else{
	            $rensos = RencanaSosialisasi::all();
	        }
		}
		else{
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();;
            // }
            $dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $rensos = RencanaSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
	        }else{
	             $rensos = RencanaSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
	        }
		}
		$export = new RencanaSosialisasiExport([          
			$push,
		]);

		foreach ($rensos as $key => $value) {
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$kegiatan = Kegiatan::where('id', $value->jenis_kegiatan)->value('kegiatan');
			$bulan = Carbon::parse($value->tanggal_pelaksanaan)->format('F');
			$tahun = Carbon::parse($value->tanggal_pelaksanaan)->format('Y');
			if ($value->status == 1) {
				$status = 'Open';
			}else{
				$status = 'Close';
			}
			array_push($push, array(
				$value->id,
				$value->created_at,
				$tahun,
				$bulan,
				$value->tanggal_pelaksanaan,
				$nama_upt,
				$kegiatan,
				$value->tempat,
				$value->tema,
				$value->jumlah_peserta,
				$value->target_peserta,
				$value->narasumber,
				$value->anggaran,
				$value->keterangan,
				$value->lampiran,
			));

			$export = new RencanaSosialisasiExport([          
				$push,
			]);
		}
		return Excel::download($export, 'Rencana-Sosialisasi.xlsx');
	}
	public function export_search(Request $request, $id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];

			// $date = request()->date;
	  //       $month = request()->month;
	  //       $year = request()->year;
	  //       if ($date && $month && $year) {
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
	  //       }elseif($date && $month){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
	  //       }elseif($date){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
	  //       }elseif($month && $year){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
	  //       }elseif($date && $year){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
	  //       }elseif($month){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
	  //       }elseif($year){
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
	  //       }else{
	  //           $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->get();;
	  //       }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
			}else{
				$rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->get();
			}

	        $export = new RencanaSosialisasiExport([          
				$push,
			]);

			foreach ($rensos as $key => $value) {			
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				$kegiatan = Kegiatan::where('id', $value->jenis_kegiatan)->value('kegiatan');
				$bulan = Carbon::parse($value->tanggal_pelaksanaan)->format('F');
				$tahun = Carbon::parse($value->tanggal_pelaksanaan)->format('Y');
				if ($value->status == 1) {
					$status = 'Open';
				}else{
					$status = 'Close';
				}
				array_push($push, array(
					$value->id,
					$value->created_at,
					$tahun,
					$bulan,
					$value->tanggal_pelaksanaan,
					$nama_upt,
					$kegiatan,
					$value->tempat,
					$value->tema,
					$value->jumlah_peserta,
					$value->target_peserta,
					$value->narasumber,
					$value->anggaran,
					$value->keterangan,
					$value->lampiran,
				));

				$export = new RencanaSosialisasiExport([          
					$push,
				]);
			}
			return Excel::download($export, 'Rencana Sosialisasi '.$changeName.'.xlsx');
		}
	}
	public function importPreview(Request $request)
    {
        $this->validate($request,[
            'Import' => 'required|file|mimes:xls,xlsx',
        ]);

        // Excel::import(new SettleImport, request()->file('Import'));    
        $collection = (new RencanaSosialisasiImport)->toArray(request()->file('Import'));

        $body = $collection[0];

        $header = $body[0];

        $result = array_splice($body, 1);

        // return $result;

        if($header[0] == 'Tempat' && $header[1] == 'Tanggal Pelaksanaan' && $header[2] == 'Tema' && $header[3] == 'Narasumber' && $header[4] == 'Kategori Peserta' && $header[5] == 'Jumlah Peserta' && $header[6] == 'Anggaran' && $header[7] == 'Keterangan'){

            return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.import.preview')
            ->with('result', $result);
        }else{
            Session::flash('info', 'Rencana Sosialisasi');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Data header tidak cocok!');        
            return redirect()->back();
        }
    }

	public function importPost(Request $request)
	{
		$data = $request->all();			
		$tempat = $data['tempat'];
		$tanggal_pelaksanaan = $data['tanggal_pelaksanaan'];
		$tema = $data['tema'];
		$narasumber = $data['narasumber'];
		$target_peserta = $data['target_peserta'];
		$jumlah_peserta = $data['jumlah_peserta'];
		$anggaran = $data['anggaran'];
		$keterangan = $data['keterangan'];
		$lampiran = $data['lampiran'];
		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$id_prov = $request->id_prov;
			$kode_upt = $request->id_upt;
		}
		else{
			$id_prov = Auth::user()->province_code;	
			$kode_upt = Auth::user()->upt;			
		}			
		$rows = [];
		foreach($tempat as $key => $input) {
			$uniq = uniqid();
			$lampiran[$key]->move(public_path('lampiran/rencanasosialisasi'),'Rencana-Sosialisasi'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension());
			array_push($rows, [
				'tempat' => isset($tempat[$key]) ? $tempat[$key] : '',
				'tanggal_pelaksanaan' => isset($tanggal_pelaksanaan[$key]) ? $tanggal_pelaksanaan[$key] : '',      		
				'tema' => isset($tema[$key]) ? $tema[$key] : '',
				'narasumber' => isset($narasumber[$key]) ? $narasumber[$key] : '',
				'target_peserta' => isset($target_peserta[$key]) ? $target_peserta[$key] : '',
				'jumlah_peserta' => isset($jumlah_peserta[$key]) ? $jumlah_peserta[$key] : '',
				'anggaran' => isset($anggaran[$key]) ? $anggaran[$key] : '',
				'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
				'created_by' => Auth::user()->id,
				'lampiran' => 'Rencana-Sosialisasi'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension(),
				'jenis_kegiatan' => $request->jenis_kegiatan,
				'id_prov' => $id_prov,
				'kode_upt' => $kode_upt,
				'active' => 1,
				'status' => 1,
				'created_at' => $created_at,
				'updated_at' => $updated_at,

			]);
		}

		$nama_user = Auth::user()->name;
		$notif_id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$notif_id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 12;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Mengimport Data Rencana Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		$hasil = RencanaSosialisasi::insert($rows);
		Session::flash('info', 'RencanaSosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');		
		return redirect(url('sosialisasi-bimtek/rencana-sosialisasi'));

	}

    public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelRencanaSosialisasi.xlsx"));
		ob_end_clean();
		return $responses;

	}

	public function filterData(Request $request)
    {
    	if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$rensos = RencanaSosialisasi::whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
		}
		else{
			$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
		}

        Session::flash('info', 'Filter Rencana Sosialisasi');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Data Berhasil Diambil!');
        return view('templates.teknologi-bimbingan-sosialisasi.rencana-sosialisasi.core.index',compact('rensos'));
    }
}
