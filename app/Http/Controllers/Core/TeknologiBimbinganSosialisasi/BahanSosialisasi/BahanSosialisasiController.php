<?php

namespace App\Http\Controllers\Core\TeknologiBimbinganSosialisasi\BahanSosialisasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SosialisasiBimtek\BahanSosialisasi;
use Illuminate\Support\Facades\Crypt;
use App\Model\Region\Provinsi;
use Auth;
use Session;
use App\Exports\BahanSosialisasiExport;
use App\Imports\BahanSosialisasiImport;
use App\Exports\BahanSosialisasiExportId;
use App\Model\Notification\Notification;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Carbon\Carbon;
use App\Model\Setting\UPT;
use App\Model\Refrension\Materi;
use App\Model\Privillage\Role;

class BahanSosialisasiController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
   public function index(){
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$bahsos = BahanSosialisasi::all();
		}
		else{
			$bahsos = BahanSosialisasi::all();
		}
		return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.index',compact('bahsos'));

	}

	public function searchMultiple($id,Request $request){
		// return $request->id_map;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			if ($changeUPT) {
				$bahsos = BahanSosialisasi::where('kode_upt',$changeUPT)->get();
				return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.search.index',compact('bahsos','changeUPT'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Bahan Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	//GetData
	public function print(Request $request){
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
			// }elseif($date){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereDay('created_at',$date)->get();
			// }elseif($month && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
			// }elseif($month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereMonth('created_at',$month)->get();
			// }elseif($year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::whereYear('created_at',$year)->get();
			// }else{
			// 	$bahsos['bahsos'] = BahanSosialisasi::all();
			$dari = $request->dari;
        	$sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $bahsos['bahsos'] = BahanSosialisasi::whereBetween('created_at', array($dari, $sampai))->get();
	        }else{
	            $bahsos['bahsos'] = BahanSosialisasi::all();
			}
		}
		else{
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
			// }elseif($date){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->get();
			// }elseif($month && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereYear('created_at',$year)->get();
			// }elseif($month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereMonth('created_at',$month)->get();
			// }elseif($year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereYear('created_at',$year)->get();
			// }else{
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->get();
			// }
			$dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $bahsos['bahsos'] = BahanSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->whereBetween('created_at', array($dari, $sampai))->get();
	        }else{
	             $bahsos['bahsos'] = BahanSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
	        }
		}
		$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.dev.print-search', $bahsos)->setPaper('a3','landscape');
		return $pdf->download('Bahan Sosialisasi.pdf');
	}
	public function export(Request $request){
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $bahsos = BahanSosialisasi::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
            // }elseif($date && $month){
            //     $bahsos = BahanSosialisasi::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
            // }elseif($date){
            //     $bahsos = BahanSosialisasi::whereDay('created_at',$date)->get();
            // }elseif($month && $year){
            //     $bahsos = BahanSosialisasi::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
            // }elseif($date && $year){
            //     $bahsos = BahanSosialisasi::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
            // }elseif($month){
            //     $bahsos = BahanSosialisasi::whereMonth('created_at',$month)->get();
            // }elseif($year){
            //     $bahsos = BahanSosialisasi::whereYear('created_at',$year)->get();
            // }else{
            //     $bahsos = BahanSosialisasi::get();;
            // }
            $dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $bahsos = BahanSosialisasi::whereBetween('created_at', array($dari, $sampai))->get();
	        }else{
	            $bahsos = BahanSosialisasi::all();
	        }
		}
		else{
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
            // }elseif($date && $month){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
            // }elseif($date){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->get();
            // }elseif($month && $year){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
            // }elseif($date && $year){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereDay('created_at',$date)->whereYear('created_at',$year)->get();
            // }elseif($month){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereMonth('created_at',$month)->get();
            // }elseif($year){
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->whereYear('created_at',$year)->get();
            // }else{
            //     $bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->get();;
            // }
            $dari = $request->dari;
	        $sampai = $request->sampai;
	        if ($dari && $sampai) {
	            $bahsos = BahanSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->whereBetween('created_at', array($dari, $sampai))->get();
	        }else{
	             $bahsos = BahanSosialisasi::where('kode_upt', Auth::user()->upt)->where('id_prov', Auth::user()->province_code)->get();
	        }
		}

		$export = new BahanSosialisasiExport([          
			$push,
		]);

		foreach ($bahsos as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$materi = Materi::where('id', $value->id_materi)->value('jenis_materi');

			$tanggal = Carbon::parse($value->created_at)->format('d, M Y');
			$tahun = Carbon::parse($value->created_at)->format('Y');
			$bulan =Carbon::parse($value->created_at)->format('F');

			array_push($push, array(
				$value->id,
				$tanggal,
				$tahun,
				$bulan,
				$provinsi,
				$nama_upt,
				$materi,
				$value->judul,
				$value->deskripsi,
				$value->keterangan,
				$value->author,
			));

			$export = new BahanSosialisasiExport([          
				$push,
			]);
		}

		return Excel::download($export, 'Bahan Sosialisasi.xlsx');
	}
	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
			// }elseif($date){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereDay('created_at',$date)->get();
			// }elseif($month && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			// }elseif($date && $year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereDay('created_at',$date)->whereYear('created_at',$year)->get();
			// }elseif($month){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereMonth('created_at',$month)->get();
			// }elseif($year){
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereYear('created_at',$year)->get();
			// }else{
			// 	$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->get();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->whereBetween('created_at', array($dari, $sampai))->get();
			}else{
				$bahsos['bahsos'] = BahanSosialisasi::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.dev.print', $bahsos)->setPaper('a3','landscape');
			return $pdf->download('Bahan Sosialisasi '.$getUptName.'.pdf');
		}
	}
	public function export_search(Request $request, $id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		// return $changeName;
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
	            // $date = request()->date;
	            // $month = request()->month;
	            // $year = request()->year;
	            // if ($date && $month && $year) {
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
	            // }elseif($date && $month){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
	            // }elseif($date){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereDay('created_at',$date)->get();
	            // }elseif($month && $year){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
	            // }elseif($date && $year){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereDay('created_at',$date)->whereYear('created_at',$year)->get();
	            // }elseif($month){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereMonth('created_at',$month)->get();
	            // }elseif($year){
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->whereYear('created_at',$year)->get();
	            // }else{
	            //     $bahsos = BahanSosialisasi::where('kode_upt', $id)->get();
	            // }
	            $dari = $request->dari;
	            $sampai = $request->sampai;
	            if ($dari && $sampai) {
	            	$bahsos = BahanSosialisasi::where('kode_upt', $id)->whereBetween('created_at', array($dari, $sampai))->get();
	            }else{
	            	$bahsos = BahanSosialisasi::where('kode_upt', $id)->get();
	            }
			}

			$export = new BahanSosialisasiExport([          
				$push,
			]);

			foreach ($bahsos as $key => $value) {			
				$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				$materi = Materi::where('id', $value->id_materi)->value('jenis_materi');
				$tanggal = Carbon::parse($value->created_at)->format('d, M Y');
				$tahun = Carbon::parse($value->created_at)->format('Y');
				$bulan =Carbon::parse($value->created_at)->format('F');

				array_push($push, array(
					$value->id,
					$tanggal,
					$tahun,
					$bulan,
					$provinsi,
					$nama_upt,
					$materi,
					$value->judul,
					$value->deskripsi,
					$value->keterangan,
					$value->author,
				));

				$export = new BahanSosialisasiExport([          
					$push,
				]);
			}

			return Excel::download($export, 'Bahan Sosialisasi '. $changeName .'.xlsx');
		}
	}

	public function search(Request $request){
		$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
		$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
		$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
		if ($changeUPT) {
			$count = $getUPT->count();
			if ($count > 1) {
				return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.search.multiple',compact('getUPT'));
			}
			else{
				$bahsos = BahanSosialisasi::where('kode_upt',$changeUPT)->get();
			}
			return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.search.index',compact('bahsos','changeUPT'));
		}
		else{
			Session::flash('info', 'Terjadi Kesalahan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-times');
			Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
			return redirect()->back();
			
		}
	}

	public function save(Request $request){
		$bahsos = new BahanSosialisasi;
		$bahsos->status = 1;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$bahsos->id_prov = $request->id_prov;
			$bahsos->kode_upt = $request->kode_upt;
		}else{
			$bahsos->id_prov = Auth::user()->province_code;
			$bahsos->kode_upt = Auth::user()->upt;
		}
		$bahsos->id_materi = $request->input('id_materi');
		$bahsos->judul = $request->input('judul');
		$bahsos->deskripsi = $request->input('deskripsi');
		$bahsos->author = $request->input('kontributor');
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->judul."-".$request->kontributor."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/bahansosialisasi'), $cover);
			$bahsos->lampiran = $cover;	
		}
		$bahsos->keterangan = $request->input('keterangan');
		$bahsos->created_by = Auth::user()->id;
		$bahsos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 11;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data Bahan Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Bahan Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$bahsos = BahanSosialisasi::find($id);
		$bahsos->status = 1;
		$bahsos->id_prov = $bahsos->id_prov;
		$bahsos->kode_upt = $bahsos->kode_upt;
		$bahsos->id_materi = $request->input('id_materi');
		$bahsos->judul = $request->input('judul');
		$bahsos->deskripsi = $request->input('deskripsi');
		$bahsos->author = $request->input('kontributor');
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->judul."-".$request->kontributor."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/bahansosialisasi'), $cover);
			$bahsos->lampiran = $cover;	
		}
		$bahsos->keterangan = $request->input('keterangan');
		$bahsos->created_by = Auth::user()->id;
		$bahsos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$bahsos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 11;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $bahsos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Bahan Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Bahan Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbarui Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		$bahsos = BahanSosialisasi::where('id',$id)->first();
		BahanSosialisasi::where('id',$id)->delete();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$bahsos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 11;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $bahsos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Bahan Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Bahan Sosialisasi');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
	        'Import' => 'required|file|mimes:xls,xlsx',
	    ]);

	    // Excel::import(new SettleImport, request()->file('Import'));    
	    $collection = (new BahanSosialisasiImport)->toArray(request()->file('Import'));

	    $body = $collection[0];

	    $header = $body[0];

	    $result = array_splice($body, 1);

	    if($header[0] == 'judul' && $header[1] == 'deskripsi' && $header[2] == 'kontributor' && $header[3] == 'keterangan'){
		    return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.import.preview')
		    ->with('result', $result);		    
		}else{
			Session::flash('info', 'Bahan Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{			
		$data = $request->all();
		$judul = $data['judul'];
		$deskripsi = $data['deskripsi'];
		$author = $data['author'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('lampiran');
		$status = 1;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$id_prov = $request->upt_provinsi;
			$kode_upt = $request->id_upt;
		}
		else{
			$id_prov = Auth::user()->province_code;	
			$kode_upt = Auth::user()->upt;			
		}
		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');

		// dd($filename);
		$rows = [];
		foreach($judul as $key => $input){
		$uniq = uniqid();
		$filename[$key]->move(public_path('lampiran/bahansosialisasi'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
		array_push($rows, [
			'status' => $status,
			'id_prov' => $id_prov,
     		'kode_upt' => $kode_upt,			
			'id_materi' => $request->id_materi,
			'judul' => isset($judul[$key]) ? $judul[$key] : '',
     		'deskripsi' => isset($deskripsi[$key]) ? $deskripsi[$key] : '',			
     		'author' => isset($author[$key]) ? $author[$key] : '',
     		'lampiran' => 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension(),
     		'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',     		
     		'created_by' => Auth::user()->id,
     		'created_at' => $created_at,
     		'updated_at' => $updated_at,
     	]);
    	}

    	$nama_user = Auth::user()->name;
		$notif_id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$notif_id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 11;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Mengimport Data Bahan Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

    	$hasil = BahanSosialisasi::insert($rows);
		Session::flash('info', 'Bahan Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');
		return redirect(url('sosialisasi-bimtek/bahan-sosialisasi'));
	}

	public function downloadSampel(){

	    $responses = response()->download(storage_path("app/public/sampelBahsos.xlsx"));
	    ob_end_clean();
	    return $responses;

  	}

  	public function filterData(Request $request)
    {
    	if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$bahsos = BahanSosialisasi::whereBetween('created_at', array($request->dari, $request->sampai))->get();
		}
		else{
			$bahsos = BahanSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->whereBetween('created_at', array($request->dari, $request->sampai))->get();
		}

        Session::flash('info', 'Filter Bahan Sosialisasi');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Data Berhasil Diambil!');
        return view('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.index',compact('bahsos'));
    }
}