<?php

namespace App\Http\Controllers\Core\TeknologiBimbinganSosialisasi\MonevSosialisasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SosialisasiBimtek\MonevSosialisasi;
use App\Model\SosialisasiBimtek\RencanaSosialisasi;
use App\Imports\MonevSosialisasiImport;
use App\Exports\MonevSosialisasiExport;
use App\Exports\SampelMonevSosialisasiExport;
use App\Model\Notification\Notification;
use Illuminate\Support\Facades\Crypt;
use App\Model\Region\Provinsi;
use App\Model\Setting\UPT;
use App\Model\Privillage\Role;
use App\Model\Refrension\Kegiatan;
use Carbon\Carbon;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class MonevSosialisasiController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index(){
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$rensos = RencanaSosialisasi::all();
			$monsos = MonevSosialisasi::all();
		}
		else{
			$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->get();
			$monsos = MonevSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->get();
		}
		return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.index',compact('monsos','rensos'));
	}
	public function filterData(Request $request)
    {
    	if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
        	$rensos = RencanaSosialisasi::whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
        	$monsos = MonevSosialisasi::whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
        }else{
        	$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
			$monsos = MonevSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->whereBetween('tanggal_pelaksanaan', array($request->dari, $request->sampai))->get();
        }

        Session::flash('info', 'Filter Monev Sosialisasi');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Data Berhasil Diambil!');
        return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.index',compact('monsos', 'rensos'));
    }
	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			if ($changeUPT) {
				$monsos = MonevSosialisasi::where('kode_upt',$changeUPT)->get();
				return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.search.index',compact('monsos','changeUPT'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Monev Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function save(Request $request)
	{
		$monsos = new MonevSosialisasi;
		$monsos->id_rensos = $request->id_rensos;
		$monsos->id_prov = RencanaSosialisasi::where('id',$request->id_rensos)->value('id_prov');
		$monsos->kode_upt = RencanaSosialisasi::where('id',$request->id_rensos)->value('kode_upt');
		$monsos->tempat = $request->tempat;
		$monsos->jenis_kegiatan = RencanaSosialisasi::where('id',$request->id_rensos)->value('jenis_kegiatan');
		$monsos->tanggal_pelaksanaan = $request->tanggal_pelaksanaan;
		$monsos->tema = $request->tema;
		if($request->lampiran_sebelumnya){
			if ($request->hasFile('lampiran')) {
				$files = $request->file('lampiran');
				$cover = $request->tema."-".$request->tanggal_pelaksanaan."-lampiran"."."
				.$files->getClientOriginalExtension();
				$files->move(public_path('lampiran/monevsosialisasi'), $cover);
				$monsos->lampiran = $cover;	
			}else{
				$file = \File::copy(public_path('lampiran/rencanasosialisasi/'.$request->lampiran_sebelumnya),public_path('lampiran/monevsosialisasi/'.$request->lampiran_sebelumnya));
				$monsos->lampiran = $request->lampiran_sebelumnya;	
			}
		}else{
			if ($request->hasFile('lampiran')) {
				$files = $request->file('lampiran');
				$cover = $request->tema."-".$request->tanggal_pelaksanaan."-lampiran"."."
				.$files->getClientOriginalExtension();
				$files->move(public_path('lampiran/monevsosialisasi'), $cover);
				$monsos->lampiran = $cover;	
			}
		}
		$monsos->narasumber = $request->narasumber;
		$monsos->jumlah_peserta = $request->jumlah_peserta;
		$monsos->anggaran = $request->anggaran;
		$monsos->keterangan = $request->keterangan;
		$monsos->created_by = Auth::user()->id;
		$monsos->status = 1;
		$monsos->active = 1;
		$monsos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 13;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = RencanaSosialisasi::where('id',$request->id_rensos)->value('id_prov');
			$notification->id_upt = RencanaSosialisasi::where('id',$request->id_rensos)->value('kode_upt');
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data Monev Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Monev Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$monsos = MonevSosialisasi::find($id);
		$monsos->tempat = $request->tempat;
		$monsos->tanggal_pelaksanaan = $request->tanggal_pelaksanaan;
		$monsos->tema = $request->tema;
		if ($request->hasFile('lampiran')) {
			$files = $request->file('lampiran');
			$cover = $request->tema."-".$request->tanggal_pelaksanaan."-lampiran"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/monevsosialisasi'), $cover);
			$monsos->lampiran = $cover;	
		}
		$monsos->narasumber = $request->narasumber;
		$monsos->jumlah_peserta = $request->jumlah_peserta;
		$monsos->anggaran = $request->anggaran;
		$monsos->keterangan = $request->keterangan;
		$monsos->created_by = Auth::user()->id;
		$monsos->status = 1;
		$monsos->active = 1;
		$monsos->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$monsos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 13;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $monsos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Monev Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Monev Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}
	

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		$monsos = MonevSosialisasi::where('id',$id)->first();
		MonevSosialisasi::where('id',$id)->delete();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$monsos->id_prov)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 13;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $monsos->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Monev Sosialisasi';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();
		
		Session::flash('info', 'Monev Sosialisasi');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}

	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$provinceCode = RencanaSosialisasi::where('kode_upt',$changeUPT)->value('id_prov');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.search.multiple',compact('getUPT'));
				}
				else{
					$monsos = MonevSosialisasi::where('kode_upt',$changeUPT)->get();
				}				
				return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.search.index',compact('monsos','changeUPT'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();
			}
		}
		else{
			Session::flash('info', 'Monev Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);
	    // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new MonevSosialisasiImport)->toArray(request()->file('Import'));
		$body = $collection[0];
		$header = $body[0];
		$result = array_splice($body, 1);
		// return $header;
		if($header[0] == 'Tanggal Buat' && $header[1] == 'Rencana Sosialisasi' && $header[2] == 'Provinsi' && $header[3] == 'UPT' && $header[4] == 'Jenis Kegiatan' && $header[5] == 'Tempat' && $header[6] == 'Tema' && $header[7] == 'Target Peserta'&& $header[8] == 'Anggaran' && $header[9] == 'Narasumber'&& $header[10] == 'Tanggal Pelaksanaan'&& $header[11] == 'Tempat' && $header[12] == 'Tema' && $header[13] == 'Jumlah Peserta' && $header[14] == 'Realisasi Anggaran' && $header[15] == 'Narasumber' && $header[16] == 'Keterangan'){		    
			return view('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'Monev Sosialisasi');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}
	public function importPost(Request $request)
	{
		$data = $request->all();			
		$tempat = $data['tempat'];
		$tanggal_pelaksanaan = $data['tanggal_pelaksanaan'];
		$tema = $data['tema'];
		$narasumber = $data['narasumber'];
		$jumlah_peserta = $data['jumlah_peserta'];
		$anggaran = $data['anggaran'];
		$lampiran = $data['lampiran'];
		$keterangan = $data['keterangan'];
		$jenis_kegiatan = $data['jenis_kegiatan'];
		$created_at = date('Y-m-d H:m');
		$updated_at = date('Y-m-d H:m');
		$id_rensos = $data['id_rensos'];
		
		$rows = [];
		foreach($tempat as $key => $input) {
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
				$id_prov = RencanaSosialisasi::where('id',$id_rensos[$key])->value('id_prov');
				$kode_upt = RencanaSosialisasi::where('id',$id_rensos[$key])->value('kode_upt');
			}
			else{
				$id_prov = Auth::user()->province_code;	
				$kode_upt = Auth::user()->upt;			
			}
			$uniq = uniqid();
			$lampiran[$key]->move(public_path('lampiran/monevsosialisasi'),'Monev-Sosialisasi'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension());
			array_push($rows, [
				'id_rensos' => isset($id_rensos[$key]) ? $id_rensos[$key] : '',
				'tempat' => isset($tempat[$key]) ? $tempat[$key] : '',
				'tanggal_pelaksanaan' => isset($tanggal_pelaksanaan[$key]) ? $tanggal_pelaksanaan[$key] : '',      		
				'tema' => isset($tema[$key]) ? $tema[$key] : '',
				'jumlah_peserta' => isset($jumlah_peserta[$key]) ? $jumlah_peserta[$key] : '',
				'anggaran' => isset($anggaran[$key]) ? $anggaran[$key] : '',
				'narasumber' => isset($narasumber[$key]) ? $narasumber[$key] : '',
				'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
				'created_by' => Auth::user()->id,
				'lampiran' => 'Monev-Sosialisasi'.$uniq.'-'.'.'.$lampiran[$key]->getClientOriginalExtension(),
				'jenis_kegiatan' => isset($jenis_kegiatan[$key]) ? $jenis_kegiatan[$key] : '',
				'id_prov' => $id_prov,
				'kode_upt' => $kode_upt,
				'active' => 1,
				'status' => 1,
				'created_at' => $created_at,
				'updated_at' => $updated_at,
			]);

			$nama_user = Auth::user()->name;
	  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
			$notification = new Notification;
			$notification->id_module = 13;
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
				$notification->id_prov = $request->id_prov;
				$notification->id_upt = $id_upt;
			}else{
				$notification->id_prov = Auth::user()->province_code;
				$notification->id_upt = Auth::user()->upt;
			}
			$notification->message = $nama_user.' Telah Berhasil Menambahkan Data Monev Sosialisasi';
			$notification->created_at = date('Y-m-d H:m:s');
			$notification->updated_at = date('Y-m-d H:m:s');
			$notification->save();
		}	

		$hasil = MonevSosialisasi::insert($rows);
		Session::flash('info', 'Monev Sosialisasi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');		
		return redirect(url('sosialisasi-bimtek/monev-sosialisasi'));
	}
	public function print(Request $request){
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->upt == 0) {
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos['rensos'] = RencanaSosialisasi::where('status',1)->get();;
            // }
            $dari = $request->dari;
            $sampai = $request->sampai;
            if ($dari && $sampai) {
            	$rensos['rensos'] = RencanaSosialisasi::whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->where('status', 1)->get();
            }else{
            	$rensos['rensos'] = RencanaSosialisasi::where('status', 1)->get();
            }
		}
		else{
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();;
            // }
            $dari = $request->dari;
            $sampai = $request->sampai;
            if ($dari && $sampai) {
            	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
            }else{
            	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();;
            }
		}
		$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.dev.print', $rensos)->setPaper('a3','landscape');
		return $pdf->download('Data-Monitoring-Evaluasi-'.date('d-F-Y').'.pdf');
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			// $date = $request->date;
			// $month = $request->month;
			// $year = $request->year;
			// if ($date && $month && $year) {
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $month){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($date){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->get();
			// }elseif($month && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($date && $year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }elseif($month){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereMonth('tanggal_pelaksanaan',$month)->get();
			// }elseif($year){
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereYear('tanggal_pelaksanaan',$year)->get();
			// }else{
			// 	$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->get();
			// }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
			}else{
				$rensos['rensos'] = RencanaSosialisasi::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.dev.print-search', $rensos)->setPaper('a3','landscape');
			return $pdf->download('Monev Sosialisasi '.$getUptName.'.pdf');
		}
	}

	public function export(Request $request)
	{
		$push = [];
		$dari = $request->dari;
		$sampai = $request->sampai;
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->upt == 0) {
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos = RencanaSosialisasi::where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos = RencanaSosialisasi::where('status',1)->get();;
            // }
            if ($dari && $sampai) {
            	$rensos = RencanaSosialisasi::where('status',1)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
            }else{
            	$rensos = RencanaSosialisasi::where('status',1)->get();;
            }
		}
		else{
            // $date = request()->date;
            // $month = request()->month;
            // $year = request()->year;
            // if ($date && $month && $year) {
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $month){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($date){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
            // }elseif($month && $year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($date && $year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }elseif($month){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
            // }elseif($year){
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
            // }else{
            //     $rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();
            // }
            if ($dari && $sampai) {
            	$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
            }else{
            	$rensos = RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->where('status',1)->get();
            }
		}
		$export = new MonevSosialisasiExport([          
			$push,
		]);

		foreach ($rensos as $key => $value) {
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$monev = MonevSosialisasi::where('id_rensos',$value->id)->first();
			$kegiatan = Kegiatan::where('id', $value->jenis_kegiatan)->value('kegiatan');
			$bulan = Carbon::parse($value->tanggal_pelaksanaan)->format('F');
			$tahun = Carbon::parse($value->tanggal_pelaksanaan)->format('Y');
			if ($value->status == 1) {
				$status = 'Open';
			}else{
				$status = 'Close';
			}
			array_push($push, array(
				$value->created_at,
				$value->tanggal_pelaksanaan,
				$provinsi,
				$nama_upt,
				$kegiatan,
				$value->tempat,
				$value->tema,
				$value->target_peserta,
				$value->anggaran,
				$value->narasumber,
			));
			if($monev){
				array_push($push[$key], 
					$monev->tanggal_pelaksanaan,
					$monev->tempat,
					$monev->tema,
					$monev->jumlah_peserta,
					$monev->anggaran,
					$monev->keterangan
				);
			}

			$export = new MonevSosialisasiExport([          
				$push,
			]);
		}
		return Excel::download($export, 'Monev-Sosialisasi-'.date('d-F-Y').'.xlsx');
	}

	public function export_search(Request $request, $id){
		$push = [];
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			// $date = request()->date;
   //          $month = request()->month;
   //          $year = request()->year;
   //          if ($date && $month && $year) {
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
   //          }elseif($date && $month){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereMonth('tanggal_pelaksanaan',$month)->get();
   //          }elseif($date){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->get();
   //          }elseif($month && $year){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->whereYear('tanggal_pelaksanaan',$year)->get();
   //          }elseif($date && $year){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereDay('tanggal_pelaksanaan',$date)->whereYear('tanggal_pelaksanaan',$year)->get();
   //          }elseif($month){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal_pelaksanaan',$month)->get();
   //          }elseif($year){
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereYear('tanggal_pelaksanaan',$year)->get();
   //          }else{
   //              $rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->get();
   //          }
			$dari = $request->dari;
			$sampai = $request->sampai;
			if ($dari && $sampai) {
				$rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->whereBetween('tanggal_pelaksanaan', array($dari, $sampai))->get();
			}else{
				$rensos = RencanaSosialisasi::where('kode_upt',$id)->where('status',1)->get();
			}

            $export = new MonevSosialisasiExport([          
			$push,
		]);

		foreach ($rensos as $key => $value) {
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$monev = MonevSosialisasi::where('id_rensos',$value->id)->first();
			$kegiatan = Kegiatan::where('id', $value->jenis_kegiatan)->value('kegiatan');
			$bulan = Carbon::parse($value->tanggal_pelaksanaan)->format('F');
			$tahun = Carbon::parse($value->tanggal_pelaksanaan)->format('Y');
			if ($value->status == 1) {
				$status = 'Open';
			}else{
				$status = 'Close';
			}
			array_push($push, array(
				$value->created_at,
				$value->tanggal_pelaksanaan,
				$provinsi,
				$nama_upt,
				$kegiatan,
				$value->tempat,
				$value->tema,
				$value->target_peserta,
				$value->anggaran,
				$value->narasumber,
			));
			if($monev){
				array_push($push[$key], 
					$monev->tanggal_pelaksanaan,
					$monev->tempat,
					$monev->tema,
					$monev->jumlah_peserta,
					$monev->anggaran,
					$monev->keterangan
				);
			}

			$export = new MonevSosialisasiExport([          
				$push,
			]);
		}
		return Excel::download($export, 'MONEV-SOSIALISASI-'.$changeName.'.xlsx');
		}
	}

	public function downloadSampel(){
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->upt == 0) {
			$rensos = RencanaSosialisasi::all();
		}
		else{
			$rensos = RencanaSosialisasi::where('kode_upt', Auth::user()->upt)->get();
		}

		foreach ($rensos as $key => $value) {
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$monev = MonevSosialisasi::where('id_rensos',$value->id)->first();
			$kegiatan = Kegiatan::where('id', $value->jenis_kegiatan)->value('kegiatan');
			$bulan = Carbon::parse($value->tanggal_pelaksanaan)->format('F');
			$tahun = Carbon::parse($value->tanggal_pelaksanaan)->format('Y');
			if ($value->status == 1) {
				$status = 'Open';
			}else{
				$status = 'Close';
			}
			if(!$monev){
				array_push($push, array(
					$value->created_at,
					$value->tanggal_pelaksanaan,
					$provinsi,
					$nama_upt,
					$kegiatan,
					$value->tempat,
					$value->tema,
					$value->target_peserta,
					$value->anggaran,
					$value->narasumber,
				));
			}

			$export = new SampelMonevSosialisasiExport([          
				$push,
			]);
		}
		return Excel::download($export, 'Sampel-Monev-Sosialisasi.xlsx');
		
		// $responses = response()->download(storage_path("app/public/sampelMonevSosialisasi.xlsx"));
		// ob_end_clean();
		// return $responses;
	}	

	public function getRensos($id)
	{
		$data = RencanaSosialisasi::find($id);

		return $data;
	}
}
