<?php

namespace App\Http\Controllers\Core\Setting\Kuisioner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Model\Setting\Kuisioner\Kuisioner as Quiz;
use \App\Model\Setting\Kuisioner\Setting as QuizSetting;
use \App\Model\Setting\Kuisioner\Question;
use \App\Model\Setting\Kuisioner\Option as QuestionOption;
use Illuminate\Support\Facades\Crypt;
use Session;

class KuisionerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$quiz = Quiz::all();
    	
    	return view('templates.pengaturan.kuisioner.core.index',compact('quiz'));
    }

    public function save(Request $request)
    {
    	$quiz = new Quiz;
    	$quiz->title = $request->title;
    	$quiz->description = $request->description;
    	$quiz->save();
    	Session::flash('info', 'Kuisioner');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil Menambah Kuisioner');

    	return redirect()->back();
    }

    public function update(Request $request)
    {
    	$id = Crypt::decryptString($request->id);
    	$quiz = Quiz::find($id);
    	$quiz->title = $request->title;
    	$quiz->description = $request->description;
    	$quiz->save();

    	Session::flash('info', 'Kuisioner');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil Mengubah Kuisioner');
    	return redirect()->back();
    }

    public function delete(Request $request)
    {
    	$id = Crypt::decryptString($request->id);
    	QuizSetting::where('id_kuisioner', $id)->delete();
    	Quiz::where('id', $id)->delete();
        Question::where('id_kuisioner',$id)->delete();
    	Session::flash('info', 'Kuisioner');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Kuisioner');
    	return redirect()->back();
    }

    public function preview($id){
        $id = Crypt::decryptString($id);
        $question = \App\Model\Setting\Kuisioner\Question::where('id_kuisioner',$id)->orderBy('id','asc')->get();
        $kuisioner = \App\Model\Setting\Kuisioner\Kuisioner::where('id',$id)->value('title');
        return view('templates.pengaturan.kuisioner.core.preview',compact('question','kuisioner'));
    }

    public function save_setting(Request $request)
    {
        $data = $request->all();
        $id_upt = isset($data['id_upt']) ? $data['id_upt'] : null;

        if ($id_upt != null) {
            foreach ($id_upt as $key => $value) {
        	    $quiz = new QuizSetting;
            	$quiz->id_kuisioner = Crypt::decryptString($request->id_kusioner);
            	$quiz->id_upt = isset($id_upt[$key]) ? $id_upt[$key] : '';
            	$quiz->start_date = $request->start_date;
            	$quiz->end_date = $request->end_date;
            	$quiz->active = $request->active;
                $quiz->save();
            }
        }else{
            $quiz = new QuizSetting;
                $quiz->id_kuisioner = Crypt::decryptString($request->id_kusioner);
                $quiz->start_date = $request->start_date;
                $quiz->end_date = $request->end_date;
                $quiz->active = $request->active;
                $quiz->save();
        }
        Session::flash('info', 'Kuisioner');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil Menambah Pengaturan Kuisioner');
    	return redirect()->back();
    }

    public function getKuisionersetting($id){
        $id_kuis = Crypt::decryptString($id);
        $quizSetting = QuizSetting::where('id_kuisioner',$id_kuis)->get();
        $active = QuizSetting::where('id_kuisioner',$id_kuis)->distinct('id_kuisioner')->first();
       
        return view('templates.pengaturan.kuisioner.core.get-setting',compact('quizSetting','active'));
    }

    public function update_setting(Request $request)
    {
    	QuizSetting::where('id_kuisioner', $request->id_kuisioner)->delete();
    	$data = $request->all();
        $id_upt = isset($data['id_upt']) ? $data['id_upt'] : null;
        if ($id_upt != null) {
            foreach ($id_upt as $key => $value) {
        	    $quiz = new QuizSetting;
            	$quiz->id_kuisioner = $request->id_kuisioner;
            	$quiz->id_upt = isset($id_upt[$key]) ? $id_upt[$key] : '';
            	$quiz->start_date = $request->start_date;
            	$quiz->end_date = $request->end_date;
                if ($request->active == null) {
                    $active = 0;
                }else{
                    $active = $request->active;
                }
            	$quiz->active = $active;
                $quiz->save();
            }
        }else{
                $quiz = new QuizSetting;
                $quiz->id_kuisioner = Crypt::decryptString($request->id_kusioner);
                $quiz->start_date = $request->start_date;
                $quiz->end_date = $request->end_date;
                if ($request->active == null) {
                    $active = 0;
                }else{
                    $active = $request->active;
                }
                $quiz->active = $active;
                $quiz->save();
        }
        Session::flash('info', 'Kuisioner');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil Mengubah Pengaturan Kuisioner');
    	return redirect('pengaturan/kuisioner');

    }

    public function kuisionerQuestion($id){
        $id_kuisioner = Crypt::decryptString($id);
        $question = Question::where('id_kuisioner',$id_kuisioner)->get();
        return view('templates.pengaturan.kuisioner.question.index',compact('question','id_kuisioner'));
    }

    public function saveQuestion(Request $request){
        $question = new Question;
        $question->id_kuisioner = $request->id_kuisioner;
        $question->pertanyaan = $request->pertanyaan;
        $question->jenis_pertanyaan = $request->jenis_pertanyaan;
        $question->save();
        Session::flash('info', 'Pertanyaan Kuesioner');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-check-circle');
        Session::flash('alert', 'Berhasil Menambah Pertanyaan');
        return redirect()->back();
    }

    public function updateQuestion(Request $request){
        $id = Crypt::decryptString($request->id);
        $question = Question::find($id);
        $question->pertanyaan = $request->pertanyaan;
        $question->save();
        Session::flash('info', 'Pertanyaan Kuesioner');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-check-circle');
        Session::flash('alert', 'Berhasil Mengubah Pertanyaan');
        return redirect()->back();
    }

    public function deleteQuestion($id)
    {
        $id = Crypt::decryptString($id);
        Question::where('id', $id)->delete();
        QuestionOption::where('id_pertanyaan',$id)->delete();
        Session::flash('info', 'Pertanyaan Kuesioner');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-trash');
        Session::flash('alert', 'Berhasil Menghapus Pertanyaan');
        return redirect()->back();
    }

    public function questionOption($id){
        $id_pertanyaan = Crypt::decryptString($id);
        $option = QuestionOption::where('id_pertanyaan',$id_pertanyaan)->where('id_kuisioner',\App\Model\Setting\Kuisioner\Question::where('id',$id_pertanyaan)->value('id_kuisioner'))->get();
        return view('templates.pengaturan.kuisioner.option.index',compact('option','id_pertanyaan'));
    }

    public function saveOption(Request $request){
        $option = new QuestionOption;
        $option->id_pertanyaan = $request->id_pertanyaan;
        $option->id_kuisioner = \App\Model\Setting\Kuisioner\Question::where('id',$request->id_pertanyaan)->value('id_kuisioner');
        $option->option = $request->option;
        $option->value = $request->value;
        $option->save();
        Session::flash('info', 'Option');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-check-circle');
        Session::flash('alert', 'Berhasil Menambah Option');
        return redirect()->back();
    }

    public function updateOption(Request $request){
        $id = Crypt::decryptString($request->id);
        $option = QuestionOption::find($id);
        $option->option = $request->option;
        $option->value = $request->value;
        $option->save();
        Session::flash('info', 'Option');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-check-circle');
        Session::flash('alert', 'Berhasil Mengubah Option');
        return redirect()->back();
    }

    public function deleteOption($id)
    {
        $id = Crypt::decryptString($id);
        QuestionOption::where('id', $id)->delete();
        Session::flash('info', 'Option');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-trash');
        Session::flash('alert', 'Berhasil Menghapus Option');
        return redirect()->back();
    }
}
