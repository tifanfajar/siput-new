<?php

namespace App\Http\Controllers\Core\Setting\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\User;
use App\Model\Setting\UPT;
use App\Model\Region\Provinsi;
use App\Model\Privillage\Role;
use App\Model\Notification\Notification;
use PDF;
use Auth;
use App\Exports\UserExport;
use App\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class UsersController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Read
	public function index(){
		$users = User::all();
		return view('templates.pengaturan.users.core.index',compact('users'));
	}

	//GetData
	public function print(){
		$users['users'] = User::all();
		$pdf = PDF::loadView('templates.pengaturan.users.dev.print', $users)->setPaper('a4','landscape');
		return $pdf->download('Data-Pengguna.pdf');
	}
	public function export(){
		$push = [];
        $date = request()->date;
        $month = request()->month;
        $year = request()->year;
        if ($date && $month && $year) {
            $user = User::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
        }elseif($date && $month){
            $user = User::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
        }elseif($date){
            $user = User::whereDay('created_at',$date)->get();
        }elseif($month && $year){
            $user = User::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
        }elseif($date && $year){
            $user = User::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
        }elseif($month){
            $user = User::whereMonth('created_at',$month)->get();
        }elseif($year){
            $user = User::whereYear('created_at',$year)->get();
        }else{
            $user = User::get();
        }
		$export = new UserExport([          
			$push,
		]);

		foreach ($user as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->province_code)->value('nama');
			$nama_upt = UPT::where('office_id',$value->upt)->select('office_name')->distinct()->value('office_name');
			$hak_akses = Role::where('id',$value->role_id)->value('akses');

			// return $kabupaten;

			array_push($push, array(
				$value->id,
				$value->name,
				$value->email,
				$provinsi,
				$nama_upt,
				$value->no_telp,
				$value->no_hp,
				$value->no_fax,
				$value->alamat,
				$hak_akses,
			));

			$export = new UserExport([          
				$push,
			]);
		}
		return Excel::download($export, 'Data Pengguna.xlsx');
	}

	//Proses
	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		$user = User::where('id',$id)->first();
		User::where('id',$id)->delete();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$user->province_code)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification->id_module = 16;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $user->province_code;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Pengguna');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function save(Request $request){
		$user = new User;
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));

		if ($request->upt_provinsi) {
			$user->province_code = $request->input('upt_provinsi');
		}else{
			$user->province_code = 0;
		}

		if ($request->id_upt) {
			$user->upt = $request->input('id_upt');
		}else{
			$user->upt = 0;
		}
		$user->no_telp = $request->input('telp');
		$user->no_hp = $request->input('nohp');
		$user->no_fax = $request->input('nofax');
		$user->alamat = $request->input('alamat');
		if ($request->hasFile('profilephoto')) {
			$files = $request->file('profilephoto');
			$cover = date("YmdHis")."-user"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('images/user'), $cover);
			$user->profile_photo = $cover;	
		}

		$user->role_id = $request->input('role_id');
		$user->save();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->upt_provinsi)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification->id_module = 16;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->upt_provinsi;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Pengguna');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$user =  User::find($id);
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->province_code = $request->input('upt_provinsi');
		$user->upt = $request->input('id_upt');
		$user->no_telp = $request->input('telp');
		$user->no_hp = $request->input('nohp');
		$user->no_fax = $request->input('nofax');
		$user->alamat = $request->input('alamat');
		
		if ($request->hasFile('profilephoto')) {
			$files = $request->file('profilephoto');
			$cover = date("YmdHis")."-user"."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('images/user'), $cover);
			$user->profile_photo = $cover;	
		}
		if ($request->password) {
			$user->password = bcrypt($request->input('password'));
		}
		$user->role_id = $request->input('role_id');
		$user->save();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$user->province_code)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification->id_module = 16;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $user->province_code;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Pengguna');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function import(Request $request)
	{
		$this->validate($request,[
	        'Import' => 'required|file|mimes:xls,xlsx',
	    ]);
	    
	    $collection = (new UserImport)->toArray(request()->file('Import'));

	    $body = $collection[0];	    

	    $header = $body[0];

	    $result = array_splice($body, 1);

	    if($header[0] == 'No Klien Lisensi' && $header[1] == 'name' && $header[2] == 'alamat' && $header[3] == 'notelp' && $header[4] == 'nohp' ){
		    foreach($result as $value){	    	
		    	$user = new User;
				$user->name = $value[0];
				$user->email = $value[1];
				$user->password = bcrypt($value[2]);
				$user->province_code = $request->kode_upt;
				$user->upt = $request->upt_provinsi;
				$user->no_telp = $value[3];
				$user->no_hp = $value[4];
				$user->no_fax = $value[5];
				$user->alamat = $value[6];

				$user->role_id = $request->role_id;
				$user->save();			
		    }
		    Session::flash('info', 'Perusahaan');
			Session::flash('colors', 'green');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Berhasil Menambah Data!');		
			return redirect()->back();
	    }else{
	    	Session::flash('info', 'Perusahaan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
	    }
	}

  	public function importPreview(Request $request)
	{
		$this->validate($request,[
	        'Import' => 'required|file|mimes:xls,xlsx',
	    ]);

	    // Excel::import(new SettleImport, request()->file('Import'));    
	    $collection = (new UserImport)->toArray(request()->file('Import'));

	    $body = $collection[0];

	    $header = $body[0];

	    $result = array_splice($body, 1);

	    if($header[0] == 'Nama' && $header[1] == 'Email/Username' && $header[2] == 'password' && $header[3] == 'telp' && $header[4] == 'nohp' && $header[5] == 'nofax' && $header[6] == 'alamat'){
	    	return view('templates.pengaturan.users.import.preview')
		    ->with('result', $result);		    
		}else{
			Session::flash('info', 'Pengguna');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
			$data = $request->all();
			$name = $data['name'];
			$email = $data['email'];
			$password = $data['password'];
			$id_upt = $request->id_upt;
			$upt_provinsi = $request->upt_provinsi;
			$telp = $data['telp'];
			$nohp = $data['nohp'];
			$nofax = $data['nofax'];
			$alamat = $data['alamat'];
			$created_at = date('Y-m-d H:m');
            $updated_at = date('Y-m-d H:m');

		$rows = [];
 		 foreach($name as $key => $input) { 		 	
 		 	$bcryptpassword = bcrypt($password[$key]);
    		array_push($rows, [
     		'name' => isset($name[$key]) ? $name[$key] : '',
			'email' => isset($email[$key]) ? $email[$key] : '',
			'password' => isset($bcryptpassword) ? $bcryptpassword : '',
			'upt' => $request->id_upt,
			'province_code' => $request->upt_provinsi,
			'no_telp' => isset($telp[$key]) ? $telp[$key] : '',
			'no_hp' => isset($nohp[$key]) ? $nohp[$key] : '',
			'no_fax' => isset($nofax[$key]) ? $nofax[$key] : '',
			'alamat' => isset($alamat[$key]) ? $alamat[$key] : '',
			'role_id' => $request->role_id,
			'created_at' => $created_at,
			'updated_at' => $updated_at,
     		
   		 ]);
 		 }
  		$hasil = User::insert($rows);

  		$notification = new Notification;
  		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->upt_provinsi)->value('id_row');
  		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification->id_module = 16;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->upt_provinsi;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengimport Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

	    Session::flash('info', 'Pengguna');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');		
		return redirect(url('pengaturan/daftar-pengguna'));
	    
	}

	public function downloadSampel(){

	    $responses = response()->download(storage_path("app/public/sampelUser.xlsx"));
	    ob_end_clean();
	    return $responses;

  	}


}
