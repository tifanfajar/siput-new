<?php

namespace App\Http\Controllers\Core\Setting\BackupDatabase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artisan;
use Alert;
use Session;
use Log;
use Spatie\DbDumper\Databases\PostgreSql;
use Response;
use Storage;
use Carbon\Carbon;

class BackupDatabaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
    	$disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        $files = $disk->files(config('backup.backup.name'));
        $backups = [];
        foreach ($files as $k => $f) {
            if (substr($f, -4) == '.zip' && $disk->exists($f)) {
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => str_replace(config('backup.backup.name') . '/', '', $f),
                    'file_size' => $this->human_filesize($disk->size($f)),
                    'last_modified' => $this->getDate($disk->lastModified($f)),
                ];
            }
        }
        $backups = array_reverse($backups);
        return view("templates.pengaturan.backup-database.core.index")->with(compact('backups'));
    }
    
    public function download($file_name)
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
        $file = config('backup.backup.name') . '/' . $file_name;
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists($file)) {
            $fs = Storage::disk(config('backup.backup.destination.disks')[0])->getDriver();
            $stream = $fs->readStream($file);
            return Response::stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Type" => $fs->getMimetype($file),
                "Content-Length" => $fs->getSize($file),
                "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
            ]);
        } else {
            abort(404, "Database tidak ada !");
        }
    }

    public function create()
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
        try {
            Artisan::call('backup:run',['--only-db'=>true]);
            $output = Artisan::output();
            Log::info("Backpack\BackupManager -- new backup started from admin interface \r\n" . $output);
            Session::flash('info', 'Backup Database');
			Session::flash('colors', 'green');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Berhasil Membackup Database!');
            return redirect()->back();
        } catch (Exception $e) {
            Flash::error($e->getMessage());
            return redirect()->back();
        }
    }

    public function delete($file_name)
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
    	$disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists(config('backup.backup.name') . '/' . $file_name)) {
            $disk->delete(config('backup.backup.name') . '/' . $file_name);
            Session::flash('info', 'Backup Database');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-trash');
            Session::flash('alert', 'Berhasil Menghapus File Backup!');
            return redirect()->back();
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    public function getDate($date_modify)
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
    	return Carbon::createFromTimeStamp($date_modify)->formatLocalized('%d %B %Y %H:%M');
    }

    public function human_filesize($bytes, $decimals = 2)
    {
        ini_set('max_execution_time',3600);
        ini_set('memory_limit', '-1');
    	if($bytes < 1024){
    		return $bytes. ' B';
    	}
    	$factor = floor(log($bytes, 1024));

    	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . [' B', ' KB', ' MB', ' GB', ' TB', ' PB'][$factor];
    }
}
