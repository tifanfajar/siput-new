<?php

namespace App\Http\Controllers\Core\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Setting\Perusahaan;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Notification\Notification;
use App\Model\Privillage\Role;
use App\Model\Setting\UPT;
use PDF;
use App\Exports\PerusahaanExport;
use App\Imports\PerusahaanImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Auth;

class CompanyController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	//Read
	public function index(Request $request){

		$company = new Perusahaan;
		if (Role::where('id',Auth::user()->role_id)->value('akses') != 'administrator') {
			$company = $company->where('id_prov',Auth::user()->province_code);
		}

		$company = $company->get();
		if ($request->filled('type') && $request->type = 'WEB') {
			$company = datatables()->of($company)
			->editColumn('id', function($data) {
				return Crypt::encryptString($data['id']);
			})
			->editColumn('provinsi', function($data) {
				return \App\Model\Region\Provinsi::where('id_row', $data['id_prov'])->value('nama');
			})
			->editColumn('kabupaten', function($data) {
				return \App\Model\Region\KabupatenKota::where('id', $data['id_kabkot'])->value('nama');
			})
			->make(true);
			return $company;
		}
		return view('templates.pengaturan.perusahaan.core.index',compact('company'));
	}

	//Proses
	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		$perusahaan = Perusahaan::where('id',$id)->first();
		Perusahaan::where('id',$id)->delete();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$perusahaan->id_prov)->value('id_row');
		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 21;
		if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$notification->id_prov = $perusahaan->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data Perusahaan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Perusahaan');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	
	public function save(Request $request){
		date_default_timezone_set('Asia/Jakarta');
		$company = new Perusahaan;
		$company->company_id = $request->company_id;
		$company->name = $request->name;
		$company->created_by = 'null';
		$company->last_update_by = 'null';
		$company->alamat = $request->alamat;
		$company->no_telp = $request->notelp;
		$company->no_hp = $request->nohp;
		$company->id_prov = $request->id_prov;
		$company->id_kabkot = $request->id_kabkot;
		$company->save();

		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification = new Notification;
		$notification->id_module = 21;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data Perusahaan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Perusahaan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');		
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$company = Perusahaan::find($id);
		$company->company_id = $request->company_id;
		$company->name = $request->name;
		$company->created_by = $company->created_by;
		$company->last_update_by = Auth::user()->id;
		$company->alamat = $request->alamat;
		$company->no_telp = $request->notelp;
		$company->no_hp = $request->nohp;
		
		$company->id_prov = $request->id_prov;
		$company->id_kabkot = $request->id_kabkot;
		$company->save();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$id_prov = Provinsi::where('id',$company->id_prov)->value('id_row');
		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$notification->id_module = 21;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $company->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data Perusahaan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Perusahaan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();
	}

	//GetData
	public function print(Request $request){
		$date = $request->date;
		$month = $request->month;
		$year = $request->year;
		if ($date && $month && $year) {
			$company['company'] = Perusahaan::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
		}elseif($date && $month){
			$company['company'] = Perusahaan::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
		}elseif($date){
			$company['company'] = Perusahaan::whereDay('created_at',$date)->get();
		}elseif($month && $year){
			$company['company'] = Perusahaan::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
		}elseif($date && $year){
			$company['company'] = Perusahaan::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
		}elseif($month){
			$company['company'] = Perusahaan::whereMonth('created_at',$month)->get();
		}elseif($year){
			$company['company'] = Perusahaan::whereYear('created_at',$year)->get();
		}else{
			$company['company'] = Perusahaan::all();
		}

		$pdf = PDF::loadView('templates.pengaturan.perusahaan.dev.print', $company)->setPaper('a4','landscape');
		return $pdf->download('Data-Perusahaan-'.date('d-F-Y').'.pdf');
	}
	public function export()
	{
		$push = [];
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$date = request()->date;
			$month = request()->month;
			$year = request()->year;
			if ($date && $month && $year) {
				$perusahaan = Perusahaan::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			}elseif($date && $month){
				$perusahaan = Perusahaan::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
			}elseif($date){
				$perusahaan = Perusahaan::whereDay('created_at',$date)->get();
			}elseif($month && $year){
				$perusahaan = Perusahaan::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			}elseif($date && $year){
				$perusahaan = Perusahaan::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
			}elseif($month){
				$perusahaan = Perusahaan::whereMonth('created_at',$month)->get();
			}elseif($year){
				$perusahaan = Perusahaan::whereYear('created_at',$year)->get();
			}else{
				$perusahaan = Perusahaan::get();
			}
		}
		else{
			$date = request()->date;
			$month = request()->month;
			$year = request()->year;
			if ($date && $month && $year) {
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereDay('created_at',$date)->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			}elseif($date && $month){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereDay('created_at',$date)->whereMonth('created_at',$month)->get();
			}elseif($date){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereDay('created_at',$date)->get();
			}elseif($month && $year){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereMonth('created_at',$month)->whereYear('created_at',$year)->get();
			}elseif($date && $year){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereDay('created_at',$date)->whereYear('created_at',$year)->get();
			}elseif($month){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereMonth('created_at',$month)->get();
			}elseif($year){
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::whereYear('created_at',$year)->get();
			}else{
				$perusahaan = Perusahaan::where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)::get();
			}
		}
		$export = new PerusahaanExport([          
			$push,
		]);

		foreach ($perusahaan as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$kabupaten = KabupatenKota::where('id', $value->id_kabkot)->value('nama');

			// return $kabupaten;

			array_push($push, array(
				$value->company_id,
				$value->name,
				$value->alamat,
				$value->no_telp,
				$value->no_hp,
				$provinsi,
				$kabupaten,
			));

			$export = new PerusahaanExport([          
				$push,
			]);
		}

		return Excel::download($export, 'Perusahaan-'.date('d-F-Y').'.xlsx');
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

	    // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new PerusahaanImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);

		if($header[0] == 'No Klien Lisensi' && $header[1] == 'Nama' && $header[2] == 'Alamat' && $header[3] == 'No Telp' && $header[4] == 'No HP'){
			$prov = Provinsi::where('id', Auth::user()->province_code)->first();
			return view('templates.pengaturan.perusahaan.import.preview')
			->with('result', $result)
			->with('prov', $prov);
		}else{
			Session::flash('info', 'Perusahaan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		date_default_timezone_set('Asia/Jakarta');
		$data = $request->all();
		$company_id = $data['company_id'];
		$name = $data['name'];
		$alamat = $data['alamat'];
		$notelp = $data['notelp'];
		$nohp = $data['nohp'];
		if (Auth::user()->province_code == 0 && Auth::user()->upt == 0) {
			$id_prov = $request->id_prov;
			$id_kabkot = $request->upt_kabupaten;
		}
		else{
			$id_prov = Auth::user()->province_code;
			$id_kabkot = $request->upt_kabupaten;
		}

		$rows = [];
		foreach($company_id as $key => $input) {
			array_push($rows, [
				'company_id' => isset($company_id[$key]) ? $company_id[$key] : '',
				'name' => isset($name[$key]) ? $name[$key] : '',
				'created_by' => Auth::user()->id,
				'last_update_by' => Auth::user()->id,
				'alamat' => isset($alamat[$key]) ? $alamat[$key] : '',
				'no_telp' => isset($notelp[$key]) ? $notelp[$key] : '',
				'no_hp' => isset($nohp[$key]) ? $nohp[$key] : '',
				'id_prov' => $id_prov,
				'id_kabkot' => $id_kabkot

			]);
		}
		$hasil = Perusahaan::insert($rows);
		$id_prov = Provinsi::where('id',$request->id_prov)->value('id_row');
		$id_upt = UPT::where('province_code',$id_prov)->select('office_id','office_name')->distinct()->value('office_id');
		$nama_user = Auth::user()->name;
		$notification = new Notification;
		$notification->id_module = 21;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = $request->id_prov;
			$notification->id_upt = $id_upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengimport Data Perusahaan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Perusahaan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengimport Data!');		
		return redirect(url('pengaturan/perusahaan'));

	}

	public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelPerusahaan.xlsx"));
		ob_end_clean();
		return $responses;

	}
}
