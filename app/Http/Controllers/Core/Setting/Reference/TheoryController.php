<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\Materi;
use PDF;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
class TheoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
     public function save(Request $request){
		$materi = new Materi;
		$materi->jenis_materi = $request->input('jenis_materi');
		$materi->created_by = Auth::user()->id;
		$materi->updated_by = Auth::user()->id;
		$materi->save();
		Session::flash('info', 'Materi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Materi::where('id',$id)->delete();
		Session::flash('info', 'Materi');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$materi = Materi::find($id);
		$materi->jenis_materi = $request->input('jenis_materi');
		$materi->updated_by = Auth::user()->id;
		$materi->save();
		Session::flash('info', 'Materi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}
}
