<?php

namespace App\Http\Controllers\Core\Setting\Upt;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Setting\UPT;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UptExport;
use Session;

class UptController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index(Request $request){
		$upt = new UPT;
		$upt = $upt->get();
		if ($request->filled('type') && $request->type = 'WEB') {
			$upt = datatables()->of($upt)->make(true);
			return $upt;
		}
		return view('templates.pengaturan.upt.core.index',compact('upt'));
	}
	public function print(){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$upt['upt'] = UPT::all();
		$pdf = PDF::loadView('templates.pengaturan.upt.dev.print', $upt)->setPaper('a4','landscape');;
		return $pdf->download('Data UPT-'.date('d-F-Y').'.pdf');
	}

	public function export(){
		ini_set('max_execution_time',3600);
		ini_set('memory_limit', '-1');
		$push = [];
        $upt = UPT::all();
		$export = new UptExport([          
			$push,
		]);

		foreach ($upt as $key => $value) {
			array_push($push, array(
				$value->office_id,
				$value->office_name,
				$value->district_name,
				$value->province_name,
				$value->zone,
			));

			$export = new UptExport([          
				$push,
			]);
		}
		return Excel::download($export, 'Data UPT-'.date('d-F-Y').'.xlsx');
	}
}
