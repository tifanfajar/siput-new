<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\Model\Inspeksi\Inspeksi;
use App\Model\ItemProgramModel;
use Auth;
use Session;

class TemplateProgramController extends Controller
{
	public function __construct()
 {
     $this->middleware('auth');
 }
 public function index () {
  if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
   // echo 'tes 2';
   return view('template-program/template-program');
  }else{
    return redirect()->back();
  }
 }
 public function edit($id) {
  $nama = Auth::user()->name;
  $input = ItemProgramModel::INPUT_TYPE_VARIABLE;
  return view('template-program/edit', compact('nama', 'id','input'));
 }
 public function add(Request $request) {
  // echo '<pre>';
  // print_r(Auth::user()->name);
  $nama = Auth::user()->name;
  // die;
  return view('template-program/add', compact('nama'));
 }
}