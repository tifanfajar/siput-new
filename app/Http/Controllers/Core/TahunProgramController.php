<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use App\Model\Inspeksi\Inspeksi;
use Auth;
use Session;

class TahunProgramController extends Controller
{
	public function __construct()
 {
     $this->middleware('auth');
 }
 public function index () {
  if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
   // echo 'tes 2';
   return view('tahun-program/tahun-program');
  }else{
    return redirect()->back();
  }
 }
 public function edit($id) {
  return view('tahun-program/edit', compact('id'));
 }
 public function add() {
  $nama = Auth::user()->name;
  return view('tahun-program/add', compact('nama'));
 }
}