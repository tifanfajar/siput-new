<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TahunProgramServices as service;


class TahunProgramController extends Controller
{
     protected $service;
 
     public function __construct(service $service)
     {
         $this->service = $service;
     }

     public function getData(Request $request) {
       $offset = $request->query('start');
       // if ($offset == 0){
       //   $offset = 1;
       // };
       // print_r($offset);
       $limit = $request->query('length');
       $result = ['status' => 200];
       try{
           $result['data'] = $this->service->repoGetData($offset, $limit);
           $result['recordsTotal'] = 0;
           $result['iTotalRecords'] = $this->service->repoGetCount();;
           $result['iTotalDisplayRecords'] = $this->service->repoGetCount();
       }catch(\Exception $e){
           $result = [
               'status' => 500,
               'error' => $e->getMessage()
           ];
       }

       return response()->json($result, $result['status']);
     }

     public function getCount () {
      $result = ['status' => 200];
      try{
          $result['data'] = $this->service->repoGetCount();
      }catch(\Exception $e){
          $result = [
              'status' => 500,
              'error' => $e->getMessage()
          ];
      }

      return response()->json($result, $result['status']);
     }

     public function store(Request $request){
         $result = ['status' => 200];
         try{
             $result['data'] = $this->service->repoSave($request);
         }catch(\Exception $e){
             $result = [
                 'status' => 500,
                 'error' => $e->getMessage()
             ];
         }

         return response()->json($result, $result['status']);
     }
     public function getById(Request $request){
         $result = ['status' => 200];
         try{
             $result['data'] = $this->service->repoGetDataByID($request->route('id'));
         }catch(\Exception $e){
             $result = [
                 'status' => 500,
                 'error' => $e->getMessage()
             ];
         }

         return response()->json($result, $result['status']);
     }

     public function delete(Request $request) {
        $result = ['status' => 200];
        try{
         $result['data'] = $this->service->repoDeleteById($request->route('id'));
        } catch (\Execption $e) {
          $result = [
              'status' => 500,
              'error' => $e->getMessage()
          ];
        }
        return response()->json($result, $result['status']);
     }
}