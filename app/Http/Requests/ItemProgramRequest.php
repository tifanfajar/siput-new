<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_template' => 'required',
            'kolom' => 'required',
            'tipe_kolom' => 'required',
            'order_by' => 'required'
        ];
    }
}
