<?php

namespace App\Model\Setting\Kuisioner;

use Illuminate\Database\Eloquent\Model;
use App\Model\Setting\Kuisioner\Answer;

class Option extends Model
{
    public function answers()
  	{
    	return $this->hasMany(Answer::class, 'answer');
  	}
}
