<?php

namespace App\Model\Setting\Kuisioner;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Setting extends Model
{
    public static function getKuesionerId($upt)
    {	
    	if (is_null($upt)) {
    		return self::where('id_upt', Auth::user()->upt)->pluck('id_kuisioner');
    	}
    	return self::where('id_upt', $upt)->pluck('id_kuisioner');
    }
}
