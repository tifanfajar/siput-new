<?php

namespace App\Model\Setting\Kuisioner;

use Illuminate\Database\Eloquent\Model;
use App\Model\Setting\Kuisioner\Option;

class Answer extends Model
{
    public function options()
  	{
    	return $this->belongsTo(Option::class, 'answer');
  	}

  	public function max()
  	{
  		return null;
  	}
}
