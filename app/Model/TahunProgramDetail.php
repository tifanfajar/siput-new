<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\TahunProgram;
use App\Model\Privillage\Role;
use Auth;

class TahunProgramDetail extends Model
{
    protected $table = "tahun_program_detail";

    protected $fillable = [
        'id_tahun_program',
        'id_template_program_detail',
        'satuan',
        'unit',
        'status'
    ];

    protected $appends = ['total_periode_active','total_periode'];

    public function templateProgram(){
        return $this->belongsTo('App\Model\TemplateProgram', 'id_template_program_detail');
    }

    public function tahunProgram(){
        return $this->belongsTo('App\Model\TahunProgram', 'id_tahun_program');
    }

    /**
	 * @return mixed
	 */
	public function getTotalPeriodeActiveAttribute()
	{
		$total_periode = 0;

        if(!empty($this->tahunProgram)){
            $total_periode = TahunProgramDetail::where('id_template_program_detail',$this->id_template_program_detail)
            ->where('id_tahun_program',$this->id_tahun_program);
            
            $total_periode = $total_periode->count();
        }

        return $total_periode;
		
	}

    /**
	 * @return mixed
	 */
	public function getTotalPeriodeAttribute()
	{
		$total_periode = 0;

        if(!empty($this->tahunProgram)){
            $total_periode = TahunProgramDetail::where('id_template_program_detail',$this->id_template_program_detail);

            $total_periode = $total_periode->count();
        }

        return $total_periode;
		
	}
}
