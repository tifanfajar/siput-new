<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UPTSModel extends Model
{
    protected $table = 'u_p_t_s';
	protected $primaryKey = 'id';
    protected $connection = 'pgsql';

    protected $fillable = [
		'id',
		'office_id',
        'office_name',
        'district_id',
        'district_name',
        'province_code',
        'province_name',
        'zone'
	];

    public function getData(){
        return DB::table($this->table)->limit(10)->get();
    }
}
