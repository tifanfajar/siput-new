<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetadataProgram extends Model
{
    protected $table = 'metadata_program';
    protected $fillable = [
        'id',
        'id_tb_tahun_program_detail',
        'id_upt',
        'float1',
        'float2',
        'float3',
        'float4',
        'float5',
        'float6',
        'float7',
        'float8',
        'float9',
        'float10',
        'varchar1',
        'varchar2',
        'varchar3',
        'varchar4',
        'varchar5',
        'varchar6',
        'varchar7',
        'varchar8',
        'varchar9',
        'varchar10',
        'date1',
        'date2',
        'date3',
        'date4',
        'date5',
        'date6',
        'date7',
        'date8',
        'date9',
        'date10',
        'created_by',
        'updated_by'
    ];

    public function templateProgram(){
        return $this->belongsTo('App\Model\TemplateProgram', 'id_template_program');
    }

    public function tbTahunProgramDetail(){
        return $this->belongsTo('App\Model\TahunProgramDetail', 'id_tb_tahun_program_detail');
    }
}
