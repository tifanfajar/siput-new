<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Privillage\Role;
use App\Model\TahunProgramDetail;
use Auth;

class TemplateProgram extends Model
{
    protected $table = 'template_program';
    protected $fillable = [
        'nama',
        'status',
        'created_by',
        'modified_by',
        'logo'
    ];


    public function itemProgram(){
        return $this->hasMany('App\Model\ItemProgramModel', 'id_template');
    }

    public function tahunProgram(){
        return $this->belongsToMany('App\Model\TahunProgram', 'tb_tahun_program_detail','id_tahun_program', 'id_template_program_detail');
    }

    public function tahunProgramDetail(){
        return $this->hasMany('App\Model\TahunProgramDetail', 'id_template_program_detail');
    }

    /**
	 * @return mixed
	 */
	public function getTotalPeriodeActiveAttribute()
	{
		$total_periode = 0;

        if(!empty($this->tahunProgramDetail)){
            $total_periode = TahunProgramDetail::where('id_template_program_detail',$this->id);

            $year = date('Y');
            $total_periode = $total_periode->whereHas('tahunProgram',function($q) use($year){
                $q->where('value',$year);
            });
            //if user not admin
            if(!empty(Auth::user())){
                $getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
                if(!empty($getStatus) && $getStatus!='administrator'){
                    $id_upt = Auth::user()->upt;
                    $total_periode = $total_periode->where('unit',$id_upt);
                }
            }

            $total_periode = $total_periode->count();
        }

        return $total_periode;
		
	}

    /**
	 * @return mixed
	 */
	public function getTotalPeriodeAttribute()
	{
		$total_periode = 0;

        if(!empty($this->tahunProgramDetail)){
            $total_periode = $this->tahunProgramDetail;

            //if user not admin
            if(!empty(Auth::user())){
                $getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
                if(!empty($getStatus) && $getStatus!='administrator'){
                    $id_upt = Auth::user()->upt;
                    $total_periode = $total_periode->where('unit',$id_upt);
                }
            }

            $total_periode = $total_periode->count();
        }

        return $total_periode;
		
	}
}
