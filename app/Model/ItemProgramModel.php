<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemProgramModel extends Model
{
    protected $table = 'item_program';

    const INPUT_TYPE_VARIABLE = [
        'varchar1'=>['type' => 'text'],
        'varchar2'=>['type' => 'text'],
        'varchar3'=>['type' => 'text'],
        'varchar4'=>['type' => 'text'],
        'varchar5'=>['type' => 'text'],
        'varchar6'=>['type' => 'text'],
        'varchar7'=>['type' => 'text'],
        'varchar8'=>['type' => 'text'],
        'varchar9'=>['type' => 'text'],
        'varchar10'=>['type' => 'text'],
        'float1'=>['type' => 'number'],
        'float2'=>['type' => 'number'],
        'float3'=>['type' => 'number'],
        'float4'=>['type' => 'number'],
        'float5'=>['type' => 'number'],
        'float6'=>['type' => 'number'],
        'float7'=>['type' => 'number'],
        'float8'=>['type' => 'number'],
        'float9'=>['type' => 'number'],
        'float10'=>['type' => 'number'],
        'date1'=>['type' => 'date'],
        'date2'=>['type' => 'date'],
        'date3'=>['type' => 'date'],
        'date4'=>['type' => 'date'],
        'date5'=>['type' => 'date'],
        'date6'=>['type' => 'date'],
        'date7'=>['type' => 'date'],
        'date8'=>['type' => 'date'],
        'date9'=>['type' => 'date'],
        'date10'=>['type' => 'date'],
    ];

    protected $fillable = [
        'id_template',
        'kolom',
        'tipe_kolom',
        'order_by',
        'status',
        'created_by',
        'modified_by'
    ];

    public function templateProgram(){
        return $this->belongsTo('App\Model\TemplateProgram', 'id_template');
    }
}
