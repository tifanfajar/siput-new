<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TahunProgram extends Model
{
    protected $table = "tahun_program";

    protected $fillable = [
        'label_tahun_program',
        'value',
        'status',
        'created_by',
        'modified_by'
    ];

    public function tahunProgram(){
        return $this->hasMany('App\Model\TahunProgramDetail', 'id_tahun_program');
    }

    public function tahunProgramDetail(){
        return $this->hasMany('App\Model\TahunProgramDetail', 'id_tahun_program');
    }

    /**
	 * @return mixed
	 */
	public function getTotalPeriodeAttribute()
	{
		return $this->tahunProgramDetail->count();
	}
}
