<?php

namespace App\Console;

use Illuminate\Console\Command;

class TemplatesLinkCommand extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'templates:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "templates" path to "public/templates"';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (!file_exists(public_path('templates'))) {
            $this->laravel->make('files')->link(
                base_path('templates'), public_path('templates')
            );
        }


        if (!file_exists(public_path('select2'))) {
            $this->laravel->make('files')->link(
                base_path('select2'), public_path('select2')
            );
        }


        if (!file_exists(public_path('validation'))) {
            $this->laravel->make('files')->link(
                base_path('validation'), public_path('validation')
            );
        }


        if (!file_exists(public_path('css'))) {
            $this->laravel->make('files')->link(
                base_path('css'), public_path('css')
            );
        }

        if (!file_exists(public_path('images'))) {
            $this->laravel->make('files')->link(
                base_path('images'), public_path('images')
            );
        }

        $this->info('The [public/templates] directory has been linked.');
    }
}
