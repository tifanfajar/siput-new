<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::put('item-program/update/{id}', 'ItemProgramController@update');
Route::post('item-program/create', 'ItemProgramController@store');
Route::get('item-program', 'ItemProgramController@getData');

Route::get('template-program', 'TemplateProgramDetailController@getData');
Route::post('template-program/create', 'TemplateProgramDetailController@store');
Route::get('template-program/{id}', 'TemplateProgramDetailController@getById');
Route::delete('template-program/{id}', 'TemplateProgramDetailController@delete');

Route::get('tahun-program', 'TahunProgramController@getData');
Route::get('tahun-program/c', 'TahunProgramController@getCount');
Route::get('tahun-program/{id}', 'TahunProgramController@getById');
Route::post('tahun-program/create', 'TahunProgramController@store');
Route::delete('tahun-program/{id}', 'TahunProgramController@delete');

Route::get('tahun-program-detail', 'TahunProgramDetailController@get');
Route::post('tahun-program-detail/create', 'TahunProgramDetailController@addNew');
Route::delete('tahun-program-detail/{id}', 'TahunProgramDetailController@delete');