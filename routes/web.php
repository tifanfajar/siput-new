<?php

Route::get('1nf0', function() {
	phpinfo();
});
//AUTH
Auth::routes(['register' => false]);
Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('postLogin', 'Auth\LoginController@postLogin')->name('postLogin');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/images/user/{filename}', function ($filename)
{
	$path = public_path('images/user') . '/' . $filename;
	$file = File::get($path);
	$type = File::mimeType($path);
	$response = Response::make($file);
	$response->header("Content-Type", $type);
	return $response;
})->name('loginImages');


//WEB
Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {

		//CORE
		Route::get('surveyLink','Core\DashboardController@surveyLink')->name('surveyLink');

		//KUESIONER FORM
		Route::get('kuesioner','Core\FormKuisionerController@index')->name('kuesioner');
		Route::get('kuesioner/form/{id}','Core\FormKuisionerController@form')->name('kuesioner-form');
		Route::post('kuesioner/post','Core\FormKuisionerController@formPost')->name('kuesioner-form-post');
		Route::group(['prefix' => 'kuesioner'], function () {
			Route::get('dashboard', 'Core\FormKuisionerController@dashboard')->name('kuesioner.dashboard');
			Route::get('detail/{id}', 'Core\FormKuisionerController@detail')->name('kuesioner.detail');
		});

		//template program

		Route::get('template-program', 'Core\TemplateProgramController@index')->name('template-program');
		Route::get('template-program/edit/{id}', 'Core\TemplateProgramController@edit')->name('template-program.edit');
		Route::get('template-program/add', 'Core\TemplateProgramController@add')->name('template-program.add');
		//

		//tahun program
		Route::get('tahun-program', 'Core\TahunProgramController@index')->name('tahun-program');
		Route::get('tahun-program/edit/{id}', 'Core\TahunProgramController@edit')->name('tahun-program.edit');
		Route::get('tahun-program/add', 'Core\TahunProgramController@add')->name('tahun-program.add');
		//endtahun program

				Route::get('/','Core\DashboardController@index');
				Route::get('dash','Core\DashboardController@getUpts');
				Route::get('new-dashboard','Core\DashboardController@newDashboard');
				Route::get('dashboard/month','Core\DashboardController@searchByMonth')->name('dashboard/month');

				Route::get('settingPassword','Core\DashboardController@ubah_password')->name('settingPassword');
				Route::post('postPassword','Core\DashboardController@post_password')->name('postPassword');

				Route::get('provinsi/kabupaten/{id}', 'Dev\CountryController@getKabupaten')->name('admin.cities.get_by_country');
				Route::get('getUpt/{id}', 'Dev\GetUptController@getUpt')->name('admin.upt.getUpt');

				Route::get('getNotification/{id_module}', 'Dev\Notification\NotificationController@getNotification')->name('get-notification');
				Route::get('updateToRead/{id}', 'Dev\Notification\NotificationController@updateToRead')->name('update-to-read-notification');

				//PENGATURAN
				Route::prefix('pengaturan')->group(function(){

					//KUISIONER
					Route::prefix('kuisioner')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/kuisioner'], function () {
							Route::get('/','Core\Setting\Kuisioner\KuisionerController@index')->name('pengaturan/kuisioner')->name('pengaturan/kuisioner');
							Route::get('settingKuisioner/{id}','Core\Setting\Kuisioner\KuisionerController@getKuisionersetting')->name('getQuizSetting');
							Route::get('edit/{id}','Core\Setting\Kuisioner\KuisionerController@edit')->name('kuisioner-edit');
							Route::get('question/{id}','Core\Setting\Kuisioner\KuisionerController@kuisionerQuestion')->name('kuisioner-question');
							Route::get('option/{id}','Core\Setting\Kuisioner\KuisionerController@questionOption')->name('kuisioner-question-option');
							Route::get('preview/{id}','Core\Setting\Kuisioner\KuisionerController@preview')->name('kuisioner-preview');
						});
						Route::group(['middleware' => 'rolec:pengaturan/kuisioner'], function () {
							Route::get('create','Core\Setting\Kuisioner\KuisionerController@create')->name('kuisioner-create');
							Route::post('save','Core\Setting\Kuisioner\KuisionerController@save')->name('kuisioner-save');
							Route::post('save-setting','Core\Setting\Kuisioner\KuisionerController@save_setting')->name('kuisioner-save-setting');
							Route::post('question/save','Core\Setting\Kuisioner\KuisionerController@saveQuestion')->name('kuisioner-question-save');
							Route::post('option/save','Core\Setting\Kuisioner\KuisionerController@saveOption')->name('kuisioner-question-option-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/kuisioner'], function () {
							Route::post('update','Core\Setting\Kuisioner\KuisionerController@update')->name('kuisioner-update');
							Route::post('update-setting','Core\Setting\Kuisioner\KuisionerController@update_setting')->name('kuisioner-update-setting');
							Route::post('question/update','Core\Setting\Kuisioner\KuisionerController@updateQuestion')->name('kuisioner-question-update');
							Route::post('option/update','Core\Setting\Kuisioner\KuisionerController@updateOption')->name('kuisioner-question-option-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/kuisioner'], function () {
							Route::get('delete','Core\Setting\Kuisioner\KuisionerController@delete')->name('kuisioner-delete');
							Route::get('question/delete/{id}','Core\Setting\Kuisioner\KuisionerController@deleteQuestion')->name('kuisioner-question-delete');
							Route::get('option/delete/{id}','Core\Setting\Kuisioner\KuisionerController@deleteOption')->name('kuisioner-question-option-delete');
						});	
					});

					//FREEZE DATE
					Route::prefix('freezeDate')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/freezeDate'], function () {
							Route::get('/','Core\SPP\FreezeDateController@index')->name('pengaturan/kuisioner')->name('pengaturan/freezeDate');
						});
						Route::group(['middleware' => 'roleu:pengaturan/freezeDate'], function () {
							Route::post('update','Core\SPP\FreezeDateController@update')->name('freezeDate-update');
						});
					});

					//HAK AKSES
					Route::prefix('hak-akses')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/hak-akses'], function () {
							Route::get('/','Core\Setting\AccessRights\AccessRightsController@index')->name('pengaturan/hak-akses');
							Route::get('edit/{id}','Core\Setting\AccessRights\AccessRightsController@edit')->name('hak-akses-edit');
						});
						Route::group(['middleware' => 'rolec:pengaturan/hak-akses'], function () {
							Route::get('create','Core\Setting\AccessRights\AccessRightsController@create')->name('hak-akses-create');
							Route::post('save','Core\Setting\AccessRights\AccessRightsController@save')->name('hak-akses-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/hak-akses'], function () {
							Route::post('update/{id}','Core\Setting\AccessRights\AccessRightsController@update')->name('hak-akses-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/hak-akses'], function () {
							Route::get('delete/{id}','Core\Setting\AccessRights\AccessRightsController@delete')->name('hak-akses-delete');
						});	
					});

					Route::prefix('perusahaan')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/perusahaan'], function () {
							Route::get('/','Core\Setting\Company\CompanyController@index')->name('pengaturan/perusahaan');
							Route::get('print','Core\Setting\Company\CompanyController@print')->name('company-print');
							Route::get('download','Core\Setting\Company\CompanyController@export')->name('company-export');
							Route::post('import','Core\Setting\Company\CompanyController@importPost')->name('company-import');
							Route::post('import/previews','Core\Setting\Company\CompanyController@importPreview')->name('company-preview-import');
							Route::get('sampel','Core\Setting\Company\CompanyController@downloadSampel')->name('company-sampel');
						});
						Route::group(['middleware' => 'rolec:pengaturan/perusahaan'], function () {
							Route::post('save','Core\Setting\Company\CompanyController@save')->name('company-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/perusahaan'], function () {
							Route::post('update','Core\Setting\Company\CompanyController@update')->name('company-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/perusahaan'], function () {
							Route::get('delete','Core\Setting\Company\CompanyController@delete')->name('company-delete');
						});		
					});

					//DAFTAR PENGGUNA
					Route::prefix('daftar-pengguna')->group(function()
					{
						Route::group(['middleware' => 'roler:pengaturan/daftar-pengguna'], function () {
							Route::get('/','Core\Setting\Users\UsersController@index')->name('pengaturan/daftar-pengguna');
							Route::get('print','Core\Setting\Users\UsersController@print')->name('users-print');
							Route::get('download','Core\Setting\Users\UsersController@export')->name('users-export');
							
							Route::post('import','Core\Setting\Users\UsersController@importPost')->name('users-import-post');
							Route::post('import/previews','Core\Setting\Users\UsersController@importPreview')->name('users-preview-import');
							Route::get('sampel','Core\Setting\Users\UsersController@downloadSampel')->name('users-sampel');
						});
						Route::group(['middleware' => 'rolec:pengaturan/daftar-pengguna'], function () {
							Route::post('save','Core\Setting\Users\UsersController@save')->name('users-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/daftar-pengguna'], function () {
							Route::post('update','Core\Setting\Users\UsersController@update')->name('users-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/daftar-pengguna'], function () {
							Route::get('delete','Core\Setting\Users\UsersController@delete')->name('users-delete');
						});		
					});

					//REFRENSI
					Route::prefix('refrensi')->group(function(){

						Route::group(['middleware' => 'roler:pengaturan/refrensi'], function () {
							Route::get('/','Core\Setting\Reference\ReferenceController@index')->name('pengaturan/refrensi');
							Route::get('print','Core\Setting\Reference\ReferenceController@print')->name('kategori-spp-print');
							Route::get('download','Core\Setting\Reference\ReferenceController@export')->name('kategori-spp-export');
						});
						// KATEGORI
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save','Core\Setting\Reference\ReferenceController@save')->name('kategori-spp-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update','Core\Setting\Reference\ReferenceController@update')->name('kategori-spp-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete','Core\Setting\Reference\ReferenceController@delete')->name('kategori-spp-delete');
						});
						Route::get('download', 'Core\Setting\Reference\ReferenceController@export')->name('refrensi-export');

						//MATERI
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save-materi','Core\Setting\Reference\TheoryController@save')->name('materi-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update-materi','Core\Setting\Reference\TheoryController@update')->name('materi-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete-materi','Core\Setting\Reference\TheoryController@delete')->name('materi-delete');
						});
						//KEGIATAN
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save-kegiatan','Core\Setting\Reference\ActivityController@save')->name('kegiatan-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update-kegiatan','Core\Setting\Reference\ActivityController@update')->name('kegiatan-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete-kegiatan','Core\Setting\Reference\ActivityController@delete')->name('kegiatan-delete');
						});
						//METODE
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save-metode','Core\Setting\Reference\MethodController@save')->name('metode-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update-metode','Core\Setting\Reference\MethodController@update')->name('metode-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete-metode','Core\Setting\Reference\MethodController@delete')->name('metode-delete');
						});
						//TUJUAN
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save-tujuan','Core\Setting\Reference\PurposeController@save')->name('tujuan-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update-tujuan','Core\Setting\Reference\PurposeController@update')->name('tujuan-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete-tujuan','Core\Setting\Reference\PurposeController@delete')->name('tujuan-delete');
						});
						//GRUP PERIZINAN
						Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
							Route::post('save-group-perizinan','Core\Setting\Reference\LicensingGroupController@save')->name('group-perizinan-save');
						});
						Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
							Route::post('update-group-perizinan','Core\Setting\Reference\LicensingGroupController@update')->name('group-perizinan-update');
						});
						Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
							Route::get('delete-group-perizinan','Core\Setting\Reference\LicensingGroupController@delete')->name('group-perizinan-delete');
						});
					});
					Route::prefix('upt')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/upt'], function () {
							Route::get('/','Core\Setting\Upt\UptController@index')->name('pengaturan/upt');
							Route::get('print','Core\Setting\Upt\UptController@print')->name('upt-print');
							Route::get('download','Core\Setting\Upt\UptController@export')->name('upt-export');
						});
					});
					Route::prefix('backup-database')->group(function(){
						Route::group(['middleware' => 'roler:pengaturan/backup-database'], function(){
							Route::get('/','Core\Setting\BackupDatabase\BackupDatabaseController@index')->name('pengaturan/backup-database');
						});
						Route::group(['middleware' => 'rolec:pengaturan/backup-database'], function () {
							Route::get('create','Core\Setting\BackupDatabase\BackupDatabaseController@create')->name('backup-database-create');
							Route::get('download/{file_name}','Core\Setting\BackupDatabase\BackupDatabaseController@download')->name('backup-database-download');
						});
						Route::group(['middleware' => 'roled:pengaturan/backup-database'], function () {
							Route::get('delete/{file_name}','Core\Setting\BackupDatabase\BackupDatabaseController@delete')->name('backup-database-delete');
						});	
					});
				});

				//LOKET PENGADUAN
				Route::prefix('pelayanan')->group(function(){
					Route::prefix('loket-pengaduan')->group(function(){
						Route::group(['middleware' => 'roler:pelayanan/loket-pengaduan'], function () {
							Route::get('/','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@index')->name('pelayanan/loket-pengaduan');
							Route::get('search','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@search')->name('loket-pengaduan-search');
							Route::get('searchMultiple/{id}','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@searchMultiple')->name('loket-pengaduan-search-multiple');
							Route::get('print','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@print')->name('loket-pengaduan-print');
							Route::get('print/{id}','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@print_search')->name('loket-pengaduan-print-search');
							Route::get('download','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@export')->name('loket-pengaduan-export');
							Route::get('download/{id}','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@export_search')->name('loket-pengaduan-export-search');
							Route::get('sampel','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@downloadSampel')->name('loket-pengaduan-sampel');
							Route::post('import/previews','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@importPreview')->name('loket-pengaduan-import-preview');
							Route::post('import','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@importPost')->name('loket-pengaduan-import-post');
							Route::get('filter-data','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@filterData')->name('loket-pengaduan-filter-data');
							Route::get('/lampiran/{filename}', function ($filename)
							{
								$path = public_path('lampiran/pelayanan/loketpengaduan') . '/' . $filename;
								$file = File::get($path);
								$type = File::mimeType($path);
								$response = Response::make($file);
								$response->header("Content-Type", $type);
								return $response;
							})->name('loket-pengaduan-lampiran');
						});
						Route::group(['middleware' => 'rolec:pelayanan/loket-pengaduan'], function () {
							Route::post('save','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@save')->name('loket-pengaduan-save');	
						});
						Route::group(['middleware' => 'roleu:pelayanan/loket-pengaduan'], function () {
							Route::post('update','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@update')->name('loket-pengaduan-update');
							Route::post('update-rejected','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@updateRejected')->name('loket-pengaduan-reject-update');
							Route::post('approved','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@approved')->name('loket-pengaduan-approved');
							Route::get('reupload/{id}','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@reupload')->name('loket-pengaduan-reupload');
						});	
						Route::group(['middleware' => 'roled:pelayanan/loket-pengaduan'], function () {
							Route::get('delete','Core\Pelayanan\LoketPengaduan\LoketPengaduanController@delete')->name('loket-pengaduan-delete');
						});
					});
					Route::prefix('unar')->group(function(){
						Route::group(['middleware' => 'roler:pelayanan/unar'], function () {
							Route::get('/','Core\Pelayanan\UNAR\UNARController@index')->name('pelayanan/unar');
							Route::get('search','Core\Pelayanan\UNAR\UNARController@search')->name('unar-search');
							Route::get('searchMultiple/{id}','Core\Pelayanan\UNAR\UNARController@searchMultiple')->name('unar-search-multiple');
							Route::get('filter-data','Core\Pelayanan\UNAR\UNARController@filterData')->name('unar-filter-data');
							Route::get('print','Core\Pelayanan\UNAR\UNARController@print')->name('unar-print');
							Route::get('print/{id}','Core\Pelayanan\UNAR\UNARController@print_search')->name('unar-print-search');
							Route::get('download','Core\Pelayanan\UNAR\UNARController@export')->name('unar-export');
							Route::get('download/{id}','Core\Pelayanan\UNAR\UNARController@export_search')->name('unar-export-search');

							Route::get('sampel','Core\Pelayanan\UNAR\UNARController@downloadSampel')->name('unar-sampel');
							Route::post('import/previews','Core\Pelayanan\UNAR\UNARController@importPreview')->name('unar-import-preview');
							Route::post('import','Core\Pelayanan\UNAR\UNARController@importPost')->name('unar-import-post');
							Route::get('/lampiran/{filename}', function ($filename)
							{
								$path = public_path('lampiran/pelayanan/unar') . '/' . $filename;
								$file = File::get($path);
								$type = File::mimeType($path);
								$response = Response::make($file);
								$response->header("Content-Type", $type);
								return $response;
							})->name('unar-lampiran');
						});
						Route::group(['middleware' => 'rolec:pelayanan/unar'], function () {
							Route::post('save','Core\Pelayanan\UNAR\UNARController@save')->name('unar-save');	
						});
						Route::group(['middleware' => 'roleu:pelayanan/unar'], function () {
							Route::post('update','Core\Pelayanan\UNAR\UNARController@update')->name('unar-update');
							Route::post('update-rejected','Core\Pelayanan\UNAR\UNARController@updateRejected')->name('unar-reject-update');
							Route::post('approved','Core\Pelayanan\UNAR\UNARController@approved')->name('unar-approved');
							Route::get('reupload/{id}','Core\Pelayanan\UNAR\UNARController@reupload')->name('unar-reupload');
							Route::post('update/rencanaunar','Core\Pelayanan\UNAR\UNARController@updateRencanaUnar')->name('update-rencana-unar');
						});	
						Route::group(['middleware' => 'roled:pelayanan/unar'], function () {
							Route::get('delete','Core\Pelayanan\UNAR\UNARController@delete')->name('unar-delete');
						});
					});
					Route::prefix('penanganan-piutang')->group(function(){
						Route::group(['middleware' => 'roler:pelayanan/penanganan-piutang'], function () {
							Route::get('/','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@index')->name('pelayanan/penanganan-piutang');
							Route::get('getPerusahaan/{id}', 'Core\Pelayanan\PenangananPiutang\PenangananPiutangController@getPerusahaan')->name('penanganan-piutang-getPerusahaan');

							Route::get('getUpt/{kode_upt}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@getUpt')->name('penanganan-piutang-getUpt');
							Route::get('provinsi/kabupaten/{nama_kpknl}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@getKabKot')->name('penanganan-piutang-getKabKot');

							Route::get('search','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@search')->name('penanganan-piutang-search');
							Route::get('searchMultiple/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@searchMultiple')->name('penanganan-piutang-search-multiple');
							Route::get('print','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@print')->name('penanganan-piutang-print');
							Route::get('print/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@print_search')->name('penanganan-piutang-print-search');
							Route::get('download','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@export')->name('penanganan-piutang-export');
							Route::get('download/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@export_search')->name('penanganan-piutang-export-search');
							Route::post('import/previews','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@importPreview')->name('penanganan-piutang-import-preview');
							Route::post('import','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@importPost')->name('penanganan-piutang-import-post');
							Route::get('sampel','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@downloadSampel')->name('penanganan-piutang-sampel');
							Route::get('filter-data','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@filterData')->name('penanganan-piutang-filter-data');
						});
						Route::group(['middleware' => 'rolec:pelayanan/penanganan-piutang'], function () {
							Route::post('save','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@save')->name('penanganan-piutang-save');	
						});
						Route::group(['middleware' => 'roleu:pelayanan/penanganan-piutang'], function () {
							Route::post('update','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@update')->name('penanganan-piutang-update');
							Route::post('update-unencrypt','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@updateUnencrypt')->name('penanganan-piutang-update-unencrypt');
							Route::post('update-rejected','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@updateRejected')->name('penanganan-piutang-reject-update');
							Route::post('approved','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@approved')->name('penanganan-piutang-approved');
							Route::get('reupload/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@reupload')->name('penanganan-piutang-reupload');
						});	
						Route::group(['middleware' => 'roled:pelayanan/penanganan-piutang'], function () {
							Route::get('delete','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@delete')->name('penanganan-piutang-delete');
						});
					});

				});

				Route::prefix('sosialisasi-bimtek')->group(function(){
					Route::prefix('bahan-sosialisasi')->group(function(){
						Route::group(['middleware' => 'roler:sosialisasi-bimtek/bahan-sosialisasi'], function () {
							Route::get('/','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@index')->name('sosialisasi-bimtek/bahan-sosialisasi');
							Route::get('search','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@search')->name('sosialisasi-bimtek/bahan-sosialisasi');
							Route::get('searchMultiple/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@searchMultiple')->name('bahsos-search-multiple');
							Route::get('/lampiran/{filename}', function ($filename)
							{
								$path = public_path('lampiran/bahansosialisasi') . '/' . $filename;
								$extension = $extension = pathinfo(public_path('lampiran/bahansosialisasi') . '/' . $filename, PATHINFO_EXTENSION);
								$file = File::get($path);
								$type = File::mimeType($path);
								$response = Response::make($file);
								$headers = ["Content-Type" => $type];
								$response->header("Content-Type", $type);
								return Response::download($path, 'Lampiran Bahan Sosialisasi.'.$extension, $headers);
							})->name('lampiran-bahansosialisasi');
							Route::get('print','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@print')->name('bahsos-print');
							Route::get('print/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@print_search')->name('bahsos-print-search');
							Route::get('download','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@export')->name('bahsos-export');
							Route::get('sampel','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@downloadSampel')->name('bahsos-sampel');
							Route::post('import/previews','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@importPreview')->name('bahsos-import-preview');
							Route::post('import','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@importPost')->name('bahsos-import-post');
							Route::get('download/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@export_search')->name('bahsos-export-search');
							Route::get('filter-data','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@filterData')->name('bahsos-filter-data');
						});
						Route::group(['middleware' => 'rolec:sosialisasi-bimtek/bahan-sosialisasi'], function () {
							Route::post('save','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@save')->name('bahsos-save');
						});
						Route::group(['middleware' => 'roleu:sosialisasi-bimtek/bahan-sosialisasi'], function () {
							Route::post('update','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@update')->name('bahsos-update');
						});
						Route::group(['middleware' => 'roled:sosialisasi-bimtek/bahan-sosialisasi'], function () {
							Route::get('delete','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@delete')->name('bahsos-delete');
						});
					});
					Route::prefix('rencana-sosialisasi')->group(function(){
						Route::group(['middleware' => 'roler:sosialisasi-bimtek/rencana-sosialisasi'], function () {
							Route::get('/','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@index')->name('sosialisasi-bimtek/rencana-sosialisasi');
							Route::get('search','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@search')->name('rensos-search');
							Route::get('searchMultiple/{id}','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@searchMultiple')->name('rensos-search-multiple');
							Route::get('print','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@print')->name('rensos-print');
							Route::get('print/{id}','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@print_search')->name('rensos-print-search');
							Route::get('download','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@export')->name('rensos-export');
							Route::get('download/{id}','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@export_search')->name('rensos-export-search');
							Route::get('filter-data','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@filterData')->name('rensos-filter-data');
							Route::get('sampel','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@downloadSampel')->name('rensos-sampel');
							Route::post('import/previews','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@importPreview')->name('rensos-import-preview');
							Route::post('import','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@importPost')->name('rensos-import-post');
							Route::get('/lampiran/{filename}', function ($filename)
							{
								$path = public_path('lampiran/rencanasosialisasi') . '/' . $filename;
								$file = File::get($path);
								$type = File::mimeType($path);
								$response = Response::make($file);
								$response->header("Content-Type", $type);
								return $response;
							})->name('rensos-lampiran');
						});
						Route::group(['middleware' => 'rolec:sosialisasi-bimtek/rencana-sosialisasi'], function () {
							Route::post('save','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@save')->name('rensos-save');	
						});
						Route::group(['middleware' => 'roleu:sosialisasi-bimtek/rencana-sosialisasi'], function () {
							Route::post('update','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@update')->name('rensos-update');
						});	
						Route::group(['middleware' => 'roled:sosialisasi-bimtek/rencana-sosialisasi'], function () {
							Route::get('delete','Core\TeknologiBimbinganSosialisasi\RencanaSosialisasi\RencanaSosialisasiController@delete')->name('rensos-delete');
						});
					});
					Route::prefix('monev-sosialisasi')->group(function(){
					Route::group(['middleware' => 'roler:sosialisasi-bimtek/monev-sosialisasi'], function () {
					Route::get('/','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@index')->name('sosialisasi-bimtek/monev-sosialisasi');
					Route::get('search','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@search')->name('sosialisasi-bimtek/monev-sosialisasi');
					Route::get('get-rensos/{id}','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@getRensos')->name('sosialisasi-bimtek/monev-sosialisasi/get-rensos');
					Route::get('searchMultiple/{id}','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@searchMultiple')->name('monsos-search-multiple');
					Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/monevsosialisasi') . '/' . $filename;
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$response->header("Content-Type", $type);
						return $response;
					})->name('lampiran-monevsosialisasi');
					Route::get('print','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@print')->name('monsos-print');
					Route::get('print/{id}','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@print_search')->name('monsos-print-search');
					Route::get('download','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@export')->name('monsos-export');
					Route::get('sampel','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@downloadSampel')->name('monsos-sampel');
					Route::post('import/previews','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@importPreview')->name('monsos-import-preview');
					Route::post('import','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@importPost')->name('monsos-import-post');
					Route::get('download/{id}','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@export_search')->name('monsos-export-search');
					Route::get('filter-data','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@filterData')->name('monsos-filter-data');
				});
				Route::group(['middleware' => 'rolec:sosialisasi-bimtek/monev-sosialisasi'], function () {
					Route::post('save','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@save')->name('monsos-save');	
				});
				Route::group(['middleware' => 'roleu:sosialisasi-bimtek/monev-sosialisasi'], function () {
					Route::post('update','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@update')->name('monsos-update');
				});	
				Route::group(['middleware' => 'roled:sosialisasi-bimtek/monev-sosialisasi'], function () {
					Route::get('delete','Core\TeknologiBimbinganSosialisasi\MonevSosialisasi\MonevSosialisasiController@delete')->name('monsos-delete');
				});
			});
		});	

		//INSPEKSI		
		Route::prefix('inspeksi')->group(function(){
			Route::prefix('data')->group(function(){
			Route::group(['middleware' => 'roler:inspeksi/data'], function () {
				Route::get('/','Core\Inspeksi\InspeksiData\InspeksiDataController@index')->name('inspeksi/data');
				Route::get('search','Core\Inspeksi\InspeksiData\InspeksiDataController@search')->name('inspeksi-search');
				Route::get('search-by-upt','Core\Inspeksi\InspeksiData\InspeksiDataController@searchByUpt')->name('inspeksi-search-upt');
				Route::get('searchMultiple/{id}','Core\Inspeksi\InspeksiData\InspeksiDataController@searchMultiple')->name('inspeksi-search-multiple');
				Route::get('print','Core\Inspeksi\InspeksiData\InspeksiDataController@print')->name('inspeksi-print');
				Route::get('print/{id}','Core\Inspeksi\InspeksiData\InspeksiDataController@print_search')->name('inspeksi-print-search');
				Route::get('download','Core\Inspeksi\InspeksiData\InspeksiDataController@export')->name('inspeksi-export');
				Route::get('download/{id}','Core\Inspeksi\InspeksiData\InspeksiDataController@export_search')->name('inspeksi-export-search');
				Route::get('sampel','Core\Inspeksi\InspeksiData\InspeksiDataController@downloadSampel')->name('inspeksi-sampel');
				Route::post('import/previews','Core\Inspeksi\InspeksiData\InspeksiDataController@importPreview')->name('inspeksi-import-preview');
				Route::post('import','Core\Inspeksi\InspeksiData\InspeksiDataController@importPost')->name('inspeksi-import-post');
				Route::get('filter-data','Core\Inspeksi\InspeksiData\InspeksiDataController@filterData')->name('inspeksi-filter-data');
				Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/inspeksi/inspeksi-data') . '/' . $filename;
						$extension = $extension = pathinfo(public_path('lampiran/inspeksi/inspeksi-data') . '/' . $filename, PATHINFO_EXTENSION);
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$headers = ["Content-Type" => $type];
						$response->header("Content-Type", $type);
						return Response::download($path, 'Lampiran Inspeksi.'.$extension, $headers);
					})->name('lampiran-inspeksi');
			});
			Route::group(['middleware' => 'rolec:inspeksi/data'], function () {
				Route::post('save','Core\Inspeksi\InspeksiData\InspeksiDataController@save')->name('inspeksi-save');	
			});
			Route::group(['middleware' => 'roleu:inspeksi/data'], function () {
				Route::post('update','Core\Inspeksi\InspeksiData\InspeksiDataController@update')->name('inspeksi-update');
				Route::post('update-rejected','Core\Inspeksi\InspeksiData\InspeksiDataController@updateRejected')->name('inspeksi-reject-update');
				Route::post('approved','Core\Inspeksi\InspeksiData\InspeksiDataController@approved')->name('inspeksi-approved');
				Route::get('reupload/{id}','Core\Inspeksi\InspeksiData\InspeksiDataController@reupload')->name('inspeksi-reupload');
			});	
			Route::group(['middleware' => 'roled:inspeksi/data'], function () {
				Route::get('delete','Core\Inspeksi\InspeksiData\InspeksiDataController@delete')->name('inspeksi-delete');
			});
		});
		});

		Route::prefix('spp')->group(function(){
			Route::prefix('rt')->group(function(){
				Route::group(['middleware' => 'roler:spp/rt'], function () {
					Route::get('/','Core\SPP\RincianTagihanController@index')->name('spp/rt');
					Route::get('search_query','Core\SPP\RincianTagihanController@search_query')->name('spp/rt');
					Route::get('search','Core\SPP\RincianTagihanController@search')->name('spp-rt-search');	
					Route::get('searchMultiple/{id}','Core\SPP\RincianTagihanController@searchMultiple')->name('spp-rt-search-multiple');	
					Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/spp/rt') . '/' . $filename;
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$response->header("Content-Type", $type);
						return $response;
					})->name('bukti-dukung-spp-rt');

					Route::post('preview','Core\SPP\RincianTagihanController@preview')->name('rt-preview');						
					Route::post('preview_search','Core\SPP\RincianTagihanController@preview_search')->name('rt-preview-search');
					Route::post('print','Core\SPP\RincianTagihanController@print')->name('rt-print');						
					Route::post('print_search','Core\SPP\RincianTagihanController@print_search')->name('rt-print-search');
					Route::post('download','Core\SPP\RincianTagihanController@export')->name('rt-export');
					Route::post('download_search','Core\SPP\RincianTagihanController@export_search')->name('rt-export-search');

					Route::post('import/previews','Core\SPP\RincianTagihanController@importPreview')->name('rt-import-preview');
					Route::post('import','Core\SPP\RincianTagihanController@importPost')->name('rt-import-post');
					Route::get('sampel','Core\SPP\RincianTagihanController@downloadSampel')->name('rt-sampel');
				});
				Route::group(['middleware' => 'rolec:spp/rt'], function () {
					Route::post('postORupdate','Core\SPP\RincianTagihanController@postORupdate')->name('spp-rt-postORupdate');	
				});
				Route::group(['middleware' => 'roleu:spp/rt'], function () {
					Route::post('approved','Core\SPP\RincianTagihanController@approved')->name('spp-rt-approved');
					Route::post('ORUpdate','Core\SPP\RincianTagihanController@ORUpdate')->name('spp-rt-orupdate');
				});

			});

			Route::prefix('st')->group(function(){
				Route::group(['middleware' => 'roler:spp/st'], function () {
					Route::get('/','Core\SPP\StatusTagihanController@index')->name('spp/st');
					Route::get('search_query','Core\SPP\StatusTagihanController@search_query')->name('spp/st');
					Route::get('search','Core\SPP\StatusTagihanController@search')->name('spp-st-search');	
					Route::get('searchMultiple/{id}','Core\SPP\StatusTagihanController@searchMultiple')->name('spp-st-search-multiple');	
					Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/spp/st') . '/' . $filename;
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$response->header("Content-Type", $type);
						return $response;
					})->name('bukti-dukung-spp-st');

					Route::post('preview','Core\SPP\StatusTagihanController@preview')->name('st-preview');						
					Route::post('preview_search','Core\SPP\StatusTagihanController@preview_search')->name('st-preview-search');
					Route::post('print','Core\SPP\StatusTagihanController@print')->name('st-print');
					Route::post('print_search','Core\SPP\StatusTagihanController@print_search')->name('st-print-search');
					Route::post('download','Core\SPP\StatusTagihanController@export')->name('st-export');
					Route::post('download_search','Core\SPP\StatusTagihanController@export_search')->name('st-export-search');

					Route::post('import/previews','Core\SPP\StatusTagihanController@importPreview')->name('st-import-preview');
					Route::post('import','Core\SPP\StatusTagihanController@importPost')->name('st-import-post');
					Route::get('sampel','Core\SPP\StatusTagihanController@downloadSampel')->name('st-sampel');
				});
				Route::group(['middleware' => 'rolec:spp/st'], function () {
					Route::post('postORupdate','Core\SPP\StatusTagihanController@postORupdate')->name('spp-st-postORupdate');	
				});
				Route::group(['middleware' => 'roleu:spp/st'], function () {
					Route::post('approved','Core\SPP\StatusTagihanController@approved')->name('spp-st-approved');
					Route::post('ORUpdate','Core\SPP\StatusTagihanController@ORUpdate')->name('spp-st-orupdate');
				});

			});

		});
		

		//ROUTE PROGRAM DETAIL
		Route::prefix('program')->group(function(){
			Route::get('/','Program\ProgramController@index');
			Route::prefix('metadata_program')->group(function(){
				// Route::group(['middleware' => 'roler:program'], function () {
					Route::get('/{id}','Program\MetadataProgramController@index')->name('get-metadata-program');
					Route::post('/','Program\MetadataProgramController@create')->name('create-metadata-program');
					Route::post('/{id}/update','Program\MetadataProgramController@update')->name('update-metadata-program');
					Route::post('/{id}/delete','Program\MetadataProgramController@delete')->name('delete-metadata-program');
					// Route::get('/{id}/search','Program\MetadataProgramController@search')->name('unar-search');
					// Route::get('/{id}/searchMultiple/{id}','Program\MetadataProgramController@searchMultiple')->name('unar-search-multiple');
					// Route::get('/{id}/filter-data','Program\MetadataProgramController@filterData')->name('unar-filter-data');
					// Route::get('/{id}/print','Program\MetadataProgramController@print')->name('unar-print');
					// Route::get('/{id}/print/{id}','Program\MetadataProgramController@print_search')->name('unar-print-search');
					// Route::get('/{id}/download','Program\MetadataProgramController@export')->name('unar-export');
					// Route::get('/{id}/download/{id}','Program\MetadataProgramController@export_search')->name('unar-export-search');

					// Route::get('/{id}/sampel','Program\MetadataProgramController@downloadSampel')->name('unar-sampel');
					// Route::post('/{id}/import/previews','Program\MetadataProgramController@importPreview')->name('unar-import-preview');
					// Route::post('/{id}/import','Program\MetadataProgramController@importPost')->name('unar-import-post');
					// Route::get('/{id}/lampiran/{filename}', function ($filename)
					// {
					// 	$path = public_path('lampiran/pelayanan/unar') . '/' . $filename;
					// 	$file = File::get($path);
					// 	$type = File::mimeType($path);
					// 	$response = Response::make($file);
					// 	$response->header("Content-Type", $type);
					// 	return $response;
					// })->name('unar-lampiran');
				// });
				// Route::group(['middleware' => 'rolec:pelayanan/unar'], function () {
					// Route::post('/{id}/save','Program\MetadataProgramController@save')->name('unar-save');	
				// });
				// Route::group(['middleware' => 'roleu:pelayanan/unar'], function () {
					// Route::post('/{id}/update','Program\MetadataProgramController@update')->name('unar-update');
					// Route::post('/{id}/update-rejected','Program\MetadataProgramController@updateRejected')->name('unar-reject-update');
					// Route::post('/{id}/approved','Program\MetadataProgramController@approved')->name('unar-approved');
					// Route::get('/{id}/reupload/{id}','Program\MetadataProgramController@reupload')->name('unar-reupload');
					// Route::post('/{id}/update/rencanaunar','Program\MetadataProgramController@updateRencanaUnar')->name('update-rencana-unar');
				// });	
				// Route::group(['middleware' => 'roled:pelayanan/unar'], function () {
					// Route::get('/{id}/delete','Program\MetadataProgramController@delete')->name('unar-delete');
				// });
			});
		});
	});
});
