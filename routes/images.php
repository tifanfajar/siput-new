<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {

		Route::get('/images/user/{filename}', function ($filename)
		{
			$path = public_path('assets/images/avatars') . '/' . $filename;
			$file = File::get($path);
			$type = File::mimeType($path);
			$response = Response::make($file);
			$response->header("Content-Type", $type);
			return $response;
		})->name('userLoginImages');

	});
});

