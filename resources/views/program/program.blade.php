<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
	.footer {padding-bottom: 0px !important;}
</style>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0 text-uppercase"> <i class="ni ni-bullet-list-67"></i> &nbsp;&nbsp;Program Perjanjian Kerja dan Non Perjanjian Kerja</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
                @php $i = 0 @endphp
                @foreach($data as $row)

                    @if($i==3)
                    @php $i = 0 @endphp
                        </div>
                    </div>
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                    @endif
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body pt-2" style="max-height:185px">
                                <a href="{{url('program/metadata_program/'.$row->id)}}" class="d-block">
                                    <center>
                                    @if(!empty($row->templateProgram) && !empty($row->templateProgram->logo))
                                        <img src="{{ asset('public/assets/images'.$row->templateProgram->logo) }}" style="width:130px">
                                    @else
                                        <i class="ni ni-tv-2 text-black-50" style="font-size:130px"></i>
                                    @endif
                                    </center>
                                    <center><span class="text-dark font-weight-bold">{{!empty($row->templateProgram)?$row->templateProgram->nama:'-'}}</span></center>
                                </a>
                            </div>
                            <div class="card-header bg-gradient-default">
                                <div class="row">
                                    <div class="col-sm-auto">
                                        <span class="text-xs text-white"><center><small>Tahun Aktif</small><br><small>{{date('Y')}}</small></center></span>
                                    </div>
                                    <div class="col-sm-auto">
                                        <span class="text-xs text-white"><center><small>Periode Berjalan</small><br><small>{{$row->total_periode_active}}</small></center></span>
                                    </div>
                                    <div class="col-sm-auto">
                                        <span class="text-xs text-white"><center><small>Total Periode</small><br><small>{{$row->total_periode}}</small></center></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $i++ @endphp
                @endforeach
                
			</div>
		</div>
	</div>
</div>


@endsection
