<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<?php
$end = explode('/', url()->current());
$end = end($end);
?>
<script type="text/javascript">
	var j = jQuery.noConflict();
	function trimNull(data)
	{
		if (data != null) {
			return data;
		}

		return '';
	}
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ url('program/metadata_program/'.$tahunProgramDetail->id) }}{{ $end == 'filter-data' ? '/filter-data' : null }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					<?php
					if (!empty($_GET['dari'])) {
						?>
						"dari": "{{ $_GET['dari'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['sampai'])) {
						?>
						"sampai": "{{ $_GET['sampai'] }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [
			@foreach($template->itemProgram as $row)
			{
				"data": "{{$row->kolom}}",
			},
			@endforeach
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var attr = '';
					@foreach($template->itemProgram as $opt)
						attr += 'data-{{$opt->kolom}}="'+data['{{$opt->kolom}}']+'"'
					@endforeach
					return '<a href="#" data-toggle="modal" id="btn-edit-'+data.id+'" data-target="#editModal" class="btn btn-warning btn-sm"' +
					'onclick="edit_data(' + trimNull(data.id) + ')"'+
					attr+
					'><i class="fa fa-edit"></i></a>' +
					
					
					'<a  class="btn btn-danger btn-sm"' +
					'id="getDelete"' +
					'data-toggle="modal"' +
					'data-target="#deleteModal"' +
					'data-id="' + trimNull(data.id) + '"' +
					'onclick="delete_data(' + trimNull(data.id) + ')"'+
					'><i class="fa fa-trash"></i>' +
					'</a>';
				}
			},
		],
		"ordering": true
	});
<?php
if (!empty($_GET['dari']) || !empty($_GET['sampai'])) {
	?>
	j('html, body').animate({
		scrollTop: $("#parent_table").offset().top
	}, 2000);
	<?php
}
?>
</script>

<script type="text/javascript">
	function delete_data(id){
		$('#form-delete').attr('action', "{{url('program/metadata_program')}}/"+id+"/delete");
	}

	function edit_data(id){
		@foreach($template->itemProgram as $row)
			var value = $('#btn-edit-'+id).attr('data-{{$row->kolom}}');
			if(value!='-'){
				$('#edit_{{$row->kolom}}').val(value);
			}
		@endforeach
		$('#form-edit').attr('action', "{{url('program/metadata_program')}}/"+id+"/update");
	}
</script>