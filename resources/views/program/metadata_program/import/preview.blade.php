@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('unar-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('pelayanan/unar')}}">    
		@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>			
		@endif	


	<table class="table table-responsive">

	  <thead>
	    <tr>
	      <th scope="col">Tanggal Ujian *</th>
	      <th scope="col">ID Unar *</th>
	      <th scope="col">Lokasi Ujian *</th>
	      <th scope="col">Jumlah Siaga *</th>
	      <th scope="col">Jumlah Penggalang *</th>
	      <th scope="col">Jumlah Penegak *</th>
	      <th scope="col">Lulus Siaga *</th>
	      <th scope="col">Lulus Penggalang *</th>
	      <th scope="col">Lulus Penegak *</th>
	      <th scope="col">Tidak Lulus Siaga *</th>
	      <th scope="col">Tidak Lulus Penggalang *</th>
	      <th scope="col">Tidak Lulus Penegak *</th>
	      <th scope="col">Lampiran</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $unar => $unars)
	    <tr>
	      <td>
	      	<input type="date" class="form-control" id="tanggal_ujian" name="tanggal_ujian[]" value="{{$unars[0]}}" placeholder="Masukan Data Tanggal Ujian" data-validation="date">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="id_unar" name="id_unar[]" value="{{$unars[1]}}" placeholder="Masukan Data ID Unar" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="lokasi_ujian" name="lokasi_ujian[]" value="{{$unars[2]}}" placeholder="Masukan Data Lokasi Ujian" data-validation="message">
	      </td>

	      <td>
	      	<input type="text" class="form-control" id="jumlah_yd" name="jumlah_siaga[]" value="{{$unars[3]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="jumlah_yc" name="jumlah_penggalang[]" value="{{$unars[4]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="jumlah_yb" name="jumlah_penegak[]" value="{{$unars[5]}}" placeholder="Masukan Data" data-validation="number">
	      </td>

	      <td>
	      	<input type="text" class="form-control" id="lulus_yd" name="lulus_siaga[]" value="{{$unars[6]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="lulus_yc" name="lulus_penggalang[]" value="{{$unars[7]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="lulus_yb" name="lulus_penegak[]" value="{{$unars[8]}}" placeholder="Masukan Data" data-validation="number">
	      </td>

	      <td>
	      	<input type="text" class="form-control" id="tdk_lulus_yd" name="tdk_lulus_siaga[]" value="{{$unars[9]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="tdk_lulus_yc" name="tdk_lulus_penggalang[]" value="{{$unars[10]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="tdk_lulus_yb" name="tdk_lulus_penegak[]" value="{{$unars[11]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
			<input type="file" class="form-control" id="lampiran" name="lampiran[]" placeholder="Masukan File" required>
		  </td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
</script>

@endsection

