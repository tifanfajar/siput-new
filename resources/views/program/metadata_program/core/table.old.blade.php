<style>
	#example_wrapper{width: 2634px !important;}
</style>
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th rowspan="2" scope="col">ID UNAR</th>
			<th rowspan="2" scope="col">TAHUN</th>
			<th rowspan="2" scope="col">BULAN</th>
			<th rowspan="2" scope="col">TANGGAL DIBUAT</th>
			<th rowspan="2" scope="col">PROVINSI</th>
			<th rowspan="2" scope="col">UPT</th>
			<th rowspan="2" scope="col">TANGGAL PELAKSANAAN UJIAN</th>
			<th rowspan="2" scope="col">LOKASI UJIAN</th>
			<th rowspan="2" scope="col">TIPE UNAR</th>
			<th style="text-align: center;" colspan="4" scope="col">JUMLAH PESERTA</th>
			<th style="text-align: center;" colspan="4" scope="col">PESERTA LULUS</th>
			<th style="text-align: center;" colspan="4" scope="col">PESERTA TIDAK LULUS</th>
			<th rowspan="2" scope="col">LAMPIRAN</th>
			<th rowspan="2" scope="col">FULL CAT</th>
			<th rowspan="2" scope="col">TINDAKAN</th>
		</tr>
		<tr>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL JUMLAH PESERTA</th>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL PESERTA LULUS</th>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL PESERTA TIDAK LULUS</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($unar as $value)
		<tr>
			<td>{{$value->id_unar}}</td>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('Y')}}</td>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('M')}}</td>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('d,M Y')}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$jumlah_total = $value->jumlah_siaga + $value->jumlah_penggalang + $value->jumlah_penegak;
			$lulus_total = $value->lulus_siaga + $value->lulus_penggalang + $value->lulus_penegak;
			$tidak_lulus_total = $value->tdk_lulus_siaga + $value->tdk_lulus_penggalang + $value->tdk_lulus_penegak;
			?>
			<td>{{$upts}}</td>
			<td>{{\Carbon\Carbon::parse($value->tanggal_ujian)->format('d,M Y')}}</td>
			<td>{{$value->lokasi_ujian}}</td>
			<td>
				@if($value->tipe == 1)
				REGULER
				@elseif($value->tipe == 0)
				NON-REGULER
				@else
				-
				@endif
			</td>
			<td>{{$value->jumlah_siaga}}</td>
			<td>{{$value->jumlah_penggalang}}</td>
			<td>{{$value->jumlah_penegak}}</td>
			<td>{{$jumlah_total}}</td>
			<td>{{$value->lulus_siaga}}</td>
			<td>{{$value->lulus_penggalang}}</td>
			<td>{{$value->lulus_penegak}}</td>
			<td>{{$lulus_total}}</td>
			<td>{{$value->tdk_lulus_siaga}}</i></td>
			<td>{{$value->tdk_lulus_penggalang}}</td>
			<td>{{$value->tdk_lulus_penegak}}</td>
			<td>{{$tidak_lulus_total}}</td>
			<td>
				@if($value->lampiran != null)
				<a href="{{route('unar-lampiran',$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>
				@endif
			</td>
			<td style="text-align: center;">
				@if($value->full_cat == 1)
				<i class="fas fa-check"></i>
				@endif
			</td>
			<td>
				<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"
				data-id="{{$id}}"
				data-id-unar="{{$value->id_unar}}"
				data-tipe="{{$value->tipe}}"
				data-provinsi="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-upt="{{$upts}}"
				data-tanggal-ujian="{{$value->tanggal_ujian}}"
				data-lokasi="{{$value->lokasi_ujian}}"
				data-jumlah-siaga="{{$value->jumlah_siaga}}"
				data-jumlah-penggalang="{{$value->jumlah_penggalang}}"
				data-jumlah-penegak="{{$value->jumlah_penegak}}"
				data-lulus-siaga="{{$value->lulus_siaga}}"
				data-lulus-penggalang="{{$value->lulus_penggalang}}"
				data-lulus-penegak="{{$value->lulus_penegak}}"
				data-tidak-lulus-siaga="{{$value->tdk_lulus_siaga}}"
				data-tidak-lulus-penggalang="{{$value->tdk_lulus_penggalang}}"
				data-tidak-lulus-penegak="{{$value->tdk_lulus_penegak}}"
				data-lampiran="{{$value->lampiran}}"
				data-full-cat="{{$value->full_cat}}"
				data-pembuat="{{Auth::user()->where('id',$value->created_by)->value('name')}}"
				data-dibuat="{{$value->created_at}}"
				><i class="fa fa-file"></i></a>

				<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm" 
				data-id="{{$id}}"
				data-id-unar="{{$value->id_unar}}"
				data-tipe="{{$value->tipe}}"
				data-prov="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-upt="{{$upts}}"
				data-tanggal-ujian="{{$value->tanggal_ujian}}"
				data-lokasi="{{$value->lokasi_ujian}}"
				data-jumlah-siaga="{{$value->jumlah_siaga}}"
				data-jumlah-penggalang="{{$value->jumlah_penggalang}}"
				data-jumlah-penegak="{{$value->jumlah_penegak}}"
				data-lulus-siaga="{{$value->lulus_siaga}}"
				data-lulus-penggalang="{{$value->lulus_penggalang}}"
				data-lulus-penegak="{{$value->lulus_penegak}}"
				data-tidak-lulus-siaga="{{$value->tdk_lulus_siaga}}"
				data-tidak-lulus-penggalang="{{$value->tdk_lulus_penggalang}}"
				data-tidak-lulus-penegak="{{$value->tdk_lulus_penegak}}"
				data-lampiran="{{$value->lampiran}}"
				data-full-cat="{{$value->full_cat}}"
				data-kota="{{\App\Model\Region\KabupatenKota::where('id',$value->id_kabkot)->value('nama')}}"
				><i class="fa fa-edit"></i></a>
				<a  class="btn btn-danger btn-sm" 
					id="getDelete" 
					data-toggle="modal" 
					data-target="#deleteModal" 
					data-id="{{$id}}"><i class="fa fa-trash"></i>
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
