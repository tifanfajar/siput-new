<form action="{{route('create-metadata-program')}}" method="POST" enctype="multipart/form-data">
@csrf
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg " role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH {{$template->nama}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="id_template_program" value="{{$template->id}}">
					<input type="hidden" name="id_tb_tahun_program_detail" value="{{$tahunProgramDetail->id}}">
					<div class="row">
						@foreach($template->itemProgram as $row)
						<div class="col-md-6 form-group">
							<label for="{{$row->kolom}}"> {{$row->label}} </label><label style="color: red;">*</label>
							<input type="{{!empty($type = App\Model\ItemProgramModel::INPUT_TYPE_VARIABLE[$row->kolom]['type']) ? $type:'text'}}" class="form-control" id="create_{{$row->kolom}}" name="value_{{$row->kolom}}" placeholder="Masukan {{$row->label}}" required>
						</div>
						@endforeach
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>