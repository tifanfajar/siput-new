<form action="#" method="POST" enctype="multipart/form-data" id="form-edit">
@csrf
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg " role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white" id="exampleModalLabel">UBAH UNAR</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id_template_program" value="{{$template->id}}">
						<input type="hidden" name="id_tb_tahun_program_detail" value="{{$tahunProgramDetail->id}}">
						@foreach($template->itemProgram as $row)
						<div class="col-md-6 form-group">
							<label for="{{$row->label}}"> {{$row->label}} </label><label style="color: red;">*</label>
							<input type="{{!empty($type = App\Model\ItemProgramModel::INPUT_TYPE_VARIABLE[$row->kolom]['type']) ? $type:'text'}}" class="form-control" id="edit_{{$row->kolom}}" name="value_{{$row->kolom}}" placeholder="Masukan {{$row->label}}" required>
						</div>
						@endforeach
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						@include('dev.helpers.button.btnBatal')
						@include('dev.helpers.button.btnSimpan')
					</div>
				</div>
			</div>
		</div>
	</div>
</form>