<div class="modal fade modalSiteEdit" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLongTitle"><i class="fa fa-pen-square"></i>&nbsp;&nbsp;Edit Site</h5>
                <button id="editClose" type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @yield('editSiteModalForm')
        </div>
    </div>
</div>