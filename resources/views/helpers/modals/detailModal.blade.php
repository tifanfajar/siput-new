<div class="modal fade modalDetail" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLongTitle"><i class="fa fa-desktop"></i>&nbsp;&nbsp;Detail Data</h5>
                <button id="detailClose" type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @yield('detailModalForm')
        </div>
    </div>
</div>