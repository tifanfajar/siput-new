<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fa fa-users icon-gradient bg-primary">
                </i>
            </div>
            <div>User Management
                <div class="page-title-subheading">Pengaturan User Management untuk Aplikasi Portal Layanan ISR QR Code. <br>
                    {{date('l')}}, {{date('d-M-Y')}} ( {{date('h:i A')}} )
                </div>
            </div>
        </div>
    </div>
</div>  