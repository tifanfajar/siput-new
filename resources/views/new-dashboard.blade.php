<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

<br/>
<br/>

<div class="button-row">
    <div class="card card-stats class-button">
        <p class="card-button-text">Tambah Data</p>   
    </div>
    <div class="card card-stats class-button">
        <p class="card-button-text">Download as PDF</p>   
    </div>
    <div class="card card-stats class-button">
        <p class="card-button-text">Download as XLS</p>   
    </div>
    <div class="card card-stats class-button">
        <p class="card-button-text">IMPORT</p>   
    </div>
    <div class="card card-stats class-button">
        <p class="card-button-text">FILTER</p>   
    </div>
</div>

<div class="card card-stats">
    <div class="card-body">
        Cari Data Berdasarkan UPT
    </div>
    
</div>
<table class="table table-bordered" id="table-upt" style="width: 100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama UPT</th>
            <th>Nama Provinsi</th>    
        </tr>
    </thead>
</table>

@endsection