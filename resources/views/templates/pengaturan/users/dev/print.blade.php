@include('templates.helpers.data')
<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Daftar Pengguna</h2>
			<hr>
			<table class="table table-sm table-bordered" width="400">
				<thead class="thead-light">
					<tr>
						<th scope="col">Nama Pengguna / Email</th>
						<th scope="col">Nama</th>
						<th scope="col">Level</th>
						<th scope="col">Provinsi</th>
						<th scope="col">UPT</th>
						<th scope="col">No. Telp</th>
						<th scope="col">No. HP</th>
						<th scope="col">No. Fax</th>
						<th scope="col">Alamat</th>
						<th scope="col">Tanggal Buat</th>
						<th scope="col">Tanggal Diperbaharui</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$user->upt)->select('office_name')->distinct()->value('office_name');
						?>
						<td>{{$user->email}}</td>
						<td>{{$user->name}}</td>
						<td>{{\App\Model\Privillage\Role::where('id',$user->role_id)->value('role_name')}}</td>
						<td>{{\App\Model\Region\Provinsi::where('id',$user->province_code)->value('nama')}}</td>
						<td>{{$upts}}</td>
						<td>{{$user->no_telp}}</td>
						<td>{{$user->no_hp}}</td>
						<td>{{$user->no_fax}}</td>
						<td>{{$user->alamat}}</td>
						<td>{{$user->created_at}}</td>
						<td>{{$user->updated_at}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>


