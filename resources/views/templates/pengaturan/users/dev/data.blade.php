@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var email=$(e.relatedTarget).attr('data-email');
			$('#demail').val(email);
			var name=$(e.relatedTarget).attr('data-name');
			$('#dname').val(name);
			var role=$(e.relatedTarget).attr('data-role');
			$('#dlevel').val(role);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#dtelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-nofax');
			$('#dnofax').val(fax); 
			var nohp=$(e.relatedTarget).attr('data-nohp');
			$('#dnohp').val(nohp); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#dalamat').val(alamat);
			var created=$(e.relatedTarget).attr('data-created');
			$('#dcreated').val(created);
			var updated=$(e.relatedTarget).attr('data-updated');
			$('#dupdated').val(updated);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);
			var upt=$(e.relatedTarget).attr('data-upt');
			$('#dupt').val(upt);
			var photoprofile=$(e.relatedTarget).attr('data-photo');
			$('#dphoto').html('<img width="150" height="150" src="'+ '../images/user/' + photoprofile +'">');

		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var email=$(e.relatedTarget).attr('data-email');
			$('#eemail').val(email);
			var name=$(e.relatedTarget).attr('data-name');
			$('#ename').val(name);
			var role=$(e.relatedTarget).attr('data-role');
			var idrole=$(e.relatedTarget).attr('data-id-role');
			$('#erole_id').append('<option value="'+ idrole +'" selected>'+ role +'</option>');
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#etelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-nofax');
			$('#enofax').val(fax); 
			var nohp=$(e.relatedTarget).attr('data-nohp');
			$('#enohp').val(nohp); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#ealamat').val(alamat);
			var created=$(e.relatedTarget).attr('data-created');
			$('#ecreated').val(created);
			var updated=$(e.relatedTarget).attr('data-updated');
			$('#eupdated').val(updated);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			var idprovinsi=$(e.relatedTarget).attr('data-provinsiid');
			$('#eprovinsi').append('<option value="'+ idprovinsi +'" selected>'+ provinsi +'</option>');
			var upt=$(e.relatedTarget).attr('data-upt');
			var idupt=$(e.relatedTarget).attr('data-idupt');
			$('#eupt').append('<option value="'+ idupt +'" selected>'+ upt +'</option>');
			var photoprofile=$(e.relatedTarget).attr('data-photo');
			$('#ephoto').html('<img width="150" height="150" src="'+ '../images/user/' + photoprofile +'">');

		});
	});
</script>

<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#eupt').html(data.html);
			}
		});
	});
</script>
