<style type="text/css">
	.select2-container--bootstrap {z-index: 1073}
</style>
<div class="modal fade" id="addUser" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH PENGGUNA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('users-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="email">Nama Pengguna </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="email" name="email" placeholder="Masukan Nama Pengguna" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="name">Nama Lengkap</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Lengkap" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="Email">Pilih Level</label>
						<select class="form-control select2" name="role_id" required>
							<option selected disabled>Pilih salah satu</option>
							@foreach(DB::table('roles')->get() as $level)
                            <option value="{{$level->id}}">{{$level->role_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6  form-group">
						<label for="telp">No. Telp</label>
						<input type="number" class="form-control" id="telp" name="telp" placeholder="Masukan No.Telp">
					</div>
					<div class="col-md-6  form-group">
						<label for="nohp">No. HP</label>
						<input type="number" class="form-control" id="nohp" name="nohp" placeholder="Masukan No.HP">
					</div>
					<div class="col-md-6  form-group">
						<label for="nofax">No. Fax</label>
						<input type="number" class="form-control" id="nofax" name="nofax" placeholder="Masukan No.Fax">
					</div>
					<div class="col-md-6  form-group">
						<label for="profilephoto">Photo Profile</label>
						<input type="file" class="form-control" id="profilephoto" name="profilephoto">
					</div>
					<div class="col-md-12 form-group">
						<label for="alamat">Alamat </label><label style="color: red;">*</label>
						<textarea class="form-control" id="alamat" name="alamat" rows="3" placeholder="Masukan Alamat UPT" required></textarea>
					</div>
					<div class="col-md-12 form-group">
						<label for="provinsi">Provinsi</label>
						<select class="form-control" id="upt_provinsi" name="upt_provinsi">
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
                            <option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-12 form-group">
						<label for="city">UPT</label>
						<select name="id_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>