<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detail Pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('users-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="email">Nama Pengguna</label>
						<input type="text" class="form-control" id="demail" name="email" placeholder="Masukan Nama Pengguna" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="name">Nama Lengkap</label>
						<input type="text" class="form-control" id="dname" name="name" placeholder="Masukan Nama Lengkap" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="level">Level</label>
						<input type="text" class="form-control" id="dlevel" name="level" placeholder="Masukan Nama Lengkap" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="telp">No. Telp</label>
						<input type="number" class="form-control" id="dtelp" name="telp" placeholder="Masukan No.Telp" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="nohp">No. HP</label>
						<input type="number" class="form-control" id="dnohp" name="nohp" placeholder="Masukan No.HP" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="nofax">No. Fax</label>
						<input type="number" class="form-control" id="dnofax" name="nofax" placeholder="Masukan No.Fax" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="profilephoto">Photo Profile</label>
						<div id="dphoto"></div>
					</div>
					<div class="col-md-6 form-group">
						<label for="alamat">Alamat *</label>
						<textarea class="form-control" id="dalamat" name="alamat" rows="3" placeholder="Masukan Alamat UPT" disabled></textarea>
					</div>
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" class="form-control" id="dprovinsi" name="provinsi" placeholder="Masukan Nama Lengkap" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<input type="text" class="form-control" id="dupt" name="upt" placeholder="Masukan Nama Lengkap" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>