@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('users-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('pengaturan/daftar-pengguna')}}">    		
	<label for="provinsi">Provinsi</label>
	<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
		<option selected disabled>Provinsi</option>
		@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
		<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
		@endforeach
	</select>
	
	<label for="city">UPT</label>
	<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
		<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
	</select>

	<label for="city">Role</label>
	<select name="role_id" id="role_id" style="margin-bottom: 20px" class="form-control" required>
		<option value="" selected disabled>Pilih Role</option>
		@foreach(\App\Model\Privillage\Role::all() as $role)
		<option value="{{$role->id}}">{{$role->role_name}}</option>
		@endforeach
	</select>
			
	<table class="table table-responsive">

	  <thead>
	    <tr>
	      <th scope="col">Nama *</th>
	      <th scope="col">Email *</th>
	      <th scope="col">Password *</th>	      
	      <th scope="col">No Telp *</th>	      
	      <th scope="col">No Hp *</th>	      
	      <th scope="col">No Fax *</th>	      
	      <th scope="col">Alamat *</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $user => $users)
	    <tr>
	      <td>
	      	<input type="text" class="form-control" id="name" name="name[]" placeholder="Masukan Tempat" value="{{$users[0]}}" data-validation="name" required>
	      </td>
	      <td><input type="text" class="form-control" id="email" name="email[]" value="{{$users[1]}}" data-validation="email" required></td>
	      <td><input type="password" class="form-control" id="password" name="password[]" value="{{$users[2]}}" placeholder="Masukan Password" data-validation="message"  required></td>
	      <td><input type="text" class="form-control" id="telp" name="telp[]" value="{{$users[3]}}" placeholder="Masukan Narasumber" data-validation="phone" required></td>
	      <td><input type="text" class="form-control" id="nohp" name="nohp[]" value="{{$users[4]}}" placeholder="Masukan Target Peserta" data-validation="phone" data-validation="message" required></td>
	      <td><input type="text" class="form-control" id="nofax" name="nofax[]" value="{{$users[5]}}" placeholder="Masukan Jumlah Peserta" data-validation="phone" required></td>	      
	      <td><input type="text" class="form-control" id="alamat" name="alamat[]" value="{{$users[6]}}" placeholder="Masukan Keterangan" data-validation="message" required></td>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				// console.log(data.html);
			}
		});
	});
</script>

@endsection

