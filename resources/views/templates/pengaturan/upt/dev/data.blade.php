@include('dev.helpers.jquery')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var j = jQuery.noConflict();
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pengaturan/upt') }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
				});
			},
		},
		"columns": [{
			"data": "office_id",
		},
		{
			"data": "office_name",
		},
		{
			"data": "district_name",
		},
		{
			"data": "province_name",
		},
		{
			"data": "zone",
		},
		],
		"ordering": true
	});
</script>