<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}
		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data UPT</h2>
			<hr>
			<table id="example" class="table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col">Office ID</th>
						<th scope="col">Office Name</th>
						<th scope="col">District Name</th>
						<th scope="col">Province Name</th>
						<th scope="col">Zone</th>
					</tr>
				</thead>
				<tbody>
					@foreach($upt as $upts)
					<tr>
						<td>{{$upts->office_id}}</td>
						<td>{{$upts->office_name}}</td>
						<td>{{$upts->district_name}}</td>
						<td>{{$upts->province_name}}</td>
						<td>{{$upts->zone}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>


