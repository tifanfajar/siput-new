@extends('dev.core.using')
@section('content')


<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	div.dataTables_wrapper div.dataTables_paginate {margin:-16px;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
</style>
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<a href="{{route('upt-print')}}" class="btn btn-sm btn-info" style="color: white;" id="getPrint"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
		<a href="{{route('upt-export')}}" class="btn btn-sm btn-danger" style="color: white;" id="getDownload"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS </a>
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DAFTAR UPT</h3>
					</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.pengaturan.upt.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.pengaturan.upt.dev.data')
@endsection