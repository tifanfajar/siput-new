@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
  	<form class="row" action="{{route('company-import')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{route('pengaturan/perusahaan')}}">
	@if($getStatus == 'administrator')
    <tr>
    	<td>
    		<label for="city">Provinsi</label>
    		<select class="form-control" style="margin-bottom: 20px; margin-top:20px; border: 2; border-radius: 6px;" id="id_prov" name="id_prov">
				<option selected disabled>Pilih Kabupaten</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>

			<label for="city">Kabupaten / Kota</label>
			<select name="upt_kabupaten" id="upt_kabupaten" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Kabupaten/Kota</option>
			</select>
    	</td>    
    </tr>
    @else
    <tr>
    	<td>
    		<select class="form-control" style="margin-bottom: 20px; margin-top:50px; border: 2; border-radius: 6px;" id="upt_kabupaten" name="upt_kabupaten">
				<option selected disabled>Pilih Kabupaten</option>
				@foreach(\App\Model\Region\KabupatenKota::where('id_prov', Auth::user()->province_code)->get() as $kabupaten)
				<option value="{{$kabupaten->id}}">{{$kabupaten->nama}}</option>
				@endforeach
			</select>
    	</td>
    </tr>

    @endif
	<table class="table">

	  <thead>
	    <tr>
	      <th scope="col">No Klien Lisensi *</th>
	      <th scope="col">Nama Perusahaan *</th>
	      <th scope="col">No. Telp</th>
	      <th scope="col">No. HP</th>
	      <th scope="col">Alamat *</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $company => $companys)
	    <tr>
	      <td><input type="number" class="form-control" id="company_id" name="company_id[]" placeholder="Masukan No Lisensi Perusahaan" value="{{$companys[0]}}" required></td>
	      <td><input type="text" class="form-control" id="namaperusahaan" name="name[]" placeholder="Masukan Nama Perusahaan" value="{{$companys[1]}}" data-validation="companyName" required></td>
	      <td><input type="number" class="form-control" id="telp" name="notelp[]" value="{{$companys[3]}}" placeholder="Masukan No.Telp Perusahaan" data-validation="number" required></td>
	      <td><input type="number" class="form-control" id="nohp" name="nohp[]" value="{{$companys[4]}}" placeholder="Masukan No.HP Perusahaan" data-validation="number" required></td>
	      <td><input type="text" class="form-control" id="alamat" name="alamat[]" value="{{$companys[2]}}" placeholder="Masukan Alamat" data-validation="message"></td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#upt_kabupaten').html(data.html);
				console.log(data.html);
			}
		});
	});
</script>

@endsection

