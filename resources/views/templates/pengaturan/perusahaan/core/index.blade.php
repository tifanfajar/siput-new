@extends('dev.core.using')
@section('content')

@include('templates.pengaturan.perusahaan.core.add')
@include('templates.pengaturan.perusahaan.core.detail')
@include('templates.pengaturan.perusahaan.core.edit')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')



<style>
	#example_filter{position: absolute; right: 20px;}
	/*tambahan css*/
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
	div.dataTables_wrapper div.dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 160px;
		margin-left: -100px;
		margin-top: -26px;
		text-align: center;
		color: #fff;
		padding: 1em 0;
		background-color: #17457beb;
		/* border: 1px solid #000; */
		border-radius: 59px;
	}
</style>
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addCompany"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DAFTAR PERUSAHAAN</h3>
					</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="">
				<!-- Projects table -->
				<div class="">
					@include('templates.pengaturan.perusahaan.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.pengaturan.perusahaan.dev.data')

@endsection