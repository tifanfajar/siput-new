<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH PERUSAHAAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('company-update')}}" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<input type="hidden" id="eid" name="id">
					<div class="col-md-12 form-group">
						<label for="lisensi">No. Lisensi</label><label style="color: red;">*</label>
						<input type="number" id="elisensi" class="form-control" name="company_id" placeholder="Masukan Lisensi Perusahaan" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="namaupt">Nama Perusahaan</label><label style="color: red;">*</label>
						<input type="text" id="ename" class="form-control" name="name" placeholder="Masukan Nama Perusahaan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="telp">No. Telp</label>
						<input type="text" class="form-control" id="etelp" name="notelp" placeholder="Masukan No.Telp Perusahaan">
					</div>
					<div class="col-md-6 form-group">
						<label for="nohp">No. HP</label>
						<input type="text" class="form-control" id="ehp" name="nohp" placeholder="Masukan Np.HP Perusahaan">
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
						
						<div class="col-md-6 form-group">
							<label for="provinsi">Provinsi </label><label style="color: red;">*</label>
							<select class="form-control" name="id_prov" id="eprovinsi" required>
								<option selected disabled>Pilih Salah Satu</option>
								@foreach(\App\Model\Region\Provinsi::all() as $provinsis)
								<option value="{{ $provinsis->id_row }}">
									{{ $provinsis->nama }}
								</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-6 form-group">
							<label for="city">Kabupaten/Kota</label><label style="color: red;">*</label>
							<select name="id_kabkot" id="ekabupaten" class="form-control" required>
								<option value="" selected disabled>Pilih Salah Satu</option>
							</select>
						</div>
					@else

						<?php
							$provinsi = \App\Model\Region\Provinsi::where('id_row', Auth::user()->province_code)->first();
						?>
						<input type="hidden" name="id_prov" value="{{Auth::user()->province_code}}">

						<div class="col-md-12 form-group">
							<label for="city">Kabupaten/Kota</label>
							<select class="form-control" id="ekabupaten" name="id_kabkot" class="form-control" required="">
								<option selected disabled>Pilih Kabupaten</option>
								@foreach(\App\Model\Region\KabupatenKota::where('id_prov', $provinsi->id)->get() as $kabupaten)
								<option value="{{$kabupaten->id}}">{{$kabupaten->nama}}</option>
								@endforeach
							</select>
						</div>

					@endif
					<div class="col-md-12 form-group">
						<label for="alamat">Alamat *</label>
						<textarea class="form-control" id="ealamat" name="alamat" rows="3" placeholder="Masukan Alamat Perusahaan" required></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>