<!-- Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL PERUSAHAAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="namaupt">Nama Perusahaan *</label>
						<input type="text" id="dname" class="form-control" name="name" placeholder="Masukan Nama Perusahaan" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="lisensi">No. Lisensi *</label>
						<input type="text" id="dlisensi" class="form-control" name="company_id" placeholder="Masukan Lisensi Perusahaan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="telp">No. Telp</label>
						<input type="text" class="form-control" id="dtelp" name="notelp" placeholder="Masukan No.Telp Perusahaan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="nohp">No. HP</label>
						<input type="text" class="form-control" id="dhp" name="nohp" placeholder="Masukan Np.HP Perusahaan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" class="form-control" id="dprovinsi" name="provinsi" placeholder="Masukan Provinsi Perusahaan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="kabupaten">Kabupaten</label>
						<input type="text" class="form-control" id="dkabupaten" name="kabupaten" placeholder="Masukan Provinsi Perusahaan" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="alamat">Alamat *</label>
						<textarea class="form-control" id="dalamat" name="alamat" rows="3" placeholder="Masukan Alamat Perusahaan" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>