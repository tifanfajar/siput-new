<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Perusahaan</h2>
			<hr>
			<table id="example" class="table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col">No.Lisensi</th>
						<th scope="col">Nama</th>
						<th scope="col">No.Telp</th>
						<th scope="col">No.HP</th>
						<th scope="col">Provinsi</th>
						<th scope="col">Kota/Kabupaten</th>
						<th scope="col">Alamat</th>
					</tr>
				</thead>
				<tbody>
					@foreach($company as $companys)
					<tr>
						<td>{{$companys->company_id}}</td>
						<td>{{$companys->name}}</td>
						<td>{{$companys->no_telp}}</td>
						<td>{{$companys->no_hp}}</td>
						<td>{{\App\Model\Region\Provinsi::where('id',$companys->id_prov)->value('nama')}}</td>
						<td>{{\App\Model\Region\KabupatenKota::where('id',$companys->id_kabkot)->value('nama')}}</td>
						<td>{{$companys->alamat}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>