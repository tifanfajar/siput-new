@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var lisensi=$(e.relatedTarget).attr('data-lisensi');
			$('#dlisensi').val(lisensi);
			var name=$(e.relatedTarget).attr('data-name');
			$('#dname').val(name);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#dtelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-hp');
			$('#dhp').val(fax); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#dalamat').val(alamat);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);
			var kabupaten=$(e.relatedTarget).attr('data-kabupaten');
			$('#dkabupaten').val(kabupaten);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var lisensi=$(e.relatedTarget).attr('data-lisensi');
			$('#elisensi').val(lisensi);
			var name=$(e.relatedTarget).attr('data-name');
			$('#ename').val(name);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#etelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-hp');
			$('#ehp').val(fax); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#ealamat').val(alamat);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			var idprovinsi=$(e.relatedTarget).attr('data-provinsiid');
			$('#eprovinsi').append('<option value="'+ idprovinsi +'" selected>'+ provinsi +'</option>');
			var kabupaten=$(e.relatedTarget).attr('data-kabupaten');
			var idkabupaten=$(e.relatedTarget).attr('data-kabupatenid');
			$('#ekabupaten').append('<option value="'+ idkabupaten +'" selected>'+ kabupaten +'</option>');
			
			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$("#provinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var j = jQuery.noConflict();
	function trimNull(data)
	{
		if (data != null) {
			return data;
		}

		return '';
	}
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pengaturan/perusahaan') }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
				});
			},
		},
		"columns": [
		{
			"data": "company_id",
		},
		{
			"data": "name",
		},
		{
			"data": "no_telp",
		},
		{
			"data": "no_hp",
		},
		{
			"data": "provinsi",
		},
		{
			"data": "kabupaten",
		},
		{
			"data": "alamat",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
				'data-lisensi = "' + trimNull(data.company_id) + '"' +
				'data-name = "' + trimNull(data.name) + '"' +
				'data-telp = "' + trimNull(data.no_telp) + '"' +
				'data-hp = "' + trimNull(data.no_hp) + '"' +
				'data-alamat = "' + trimNull(data.alamat) + '"' +
				'data-provinsi = "' + trimNull(data.provinsi) + '"' +
				'data-kabupaten = "' + trimNull(data.kabupaten) + '"' +
				'><i class="fa fa-file"></i></a>' +
				'<a href="#" data-toggle="modal" data-target="#editModal"' +
				'data-id = "' + trimNull(data.id) + '" id="getEdit"' +
				'data-name = "' + trimNull(data.name) + '"' +
				'data-lisensi = "' + trimNull(data.company_id) + '"' +
				'data-telp = "' + trimNull(data.no_telp) + '"' +
				'data-hp = "' + trimNull(data.no_hp) + '"' +
				'data-alamat = "' + trimNull(data.alamat) + '"' +
				'data-provinsi = "' + trimNull(data.provinsi) + '"' +
				'data-kabupaten = "' + trimNull(data.kabupaten) + '"' +
				'data-provinsiid = "' + trimNull(data.id_prov) + '"' +
				'data-kabupatenid = "' + trimNull(data.id_kabkot) + '"' +
				'class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>' +
				'<a class="btn btn-danger btn-sm" id="getDelete" data-toggle="modal" data-target="#deleteModal"' +
				'data-id="' + trimNull(data.id) + '"><i class="fa fa-trash"></i>';
			}
		},
		],
		"ordering": true
	});
</script>
