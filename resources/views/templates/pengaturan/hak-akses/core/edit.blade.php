@extends('dev.core.using')
@section('content')
<br>
<br>
<form action="{{route('hak-akses-update',$role->id)}}" class="row" method="POST" enctype="multipart/enctype">
	@csrf
	<div class="col-md-4 form-group">
		<label for="role_name">Hak Akses *</label>
		<input type="text" class="form-control" id="role_name" name="role_name" placeholder="Masukan Nama Hak Akses" value="{{$role->role_name}}" required>
	</div>
	<div class="col-md-4 form-group">
		<label for="description">Deskripsi *</label>
		<input type="text" class="form-control" id="description" name="description" placeholder="Masukan Deskripsi" value="{{$role->description}}" required>
	</div>
	<div class="col-md-4 form-group">
		<label for="description">Tipe Akses *</label>
		<select class="form-control" name="akses" required>
			<option selected value="{{$role->akses}}">{{ucfirst($role->akses)}}</option>
			<option value="administrator">Administrator</option>
			<option value="kepala-upt">Kepala UPT</option>
			<option value="operator">Operator</option>
		</select>
	</div>
	
	<table class="table table-hover align-items-center">
		<thead class="thead">
			<tr>
				<th scope="col">Module</th>
				<th scope="col">Create</th>
				<th scope="col">Read</th>
				<th scope="col">Update</th>
				<th scope="col">Delete</th>
				<th scope="col">Check All</th>
			</tr>
		</thead>
		<tbody>
			<?php $pengaturan10001 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10001')->first() ;?>
			<tr id='minimal-checkbox-10001'>
				<td>Pelayanan - Loket Pengaduan</td>
				<td><input name="10001_create" type="checkbox" id="minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->create_acl == '10001' ? 'checked' : ''}}></td>
				<td><input name="10001_read" type="checkbox" id="minimal-checkbox-10001" value="10001" {{$pengaturan10001 != null && $pengaturan10001->read_acl == '10001' ? 'checked' : ''}}></td>
				<td><input name="10001_update" type="checkbox" id="minimal-checkbox-999999999" value="10001" {{$pengaturan10001 != null && $pengaturan10001->update_acl == '10001' ? 'checked' : ''}}></td>
				<td><input name="10001_delete" type="checkbox" id="minimal-checkbox-999999999" value="10001" {{$pengaturan10001 != null && $pengaturan10001->delete_acl == '10001' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10001" value="all" name="all" onChange="check(10001)" /></td>
			</tr>
			<?php $pengaturan10006 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10006')->first() ;?>
			<tr id='minimal-checkbox-10006'>
				<td>Pelayanan - UNAR</td>
				<td><input name="10006_create" type="checkbox" id="minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->create_acl == '10006' ? 'checked' : ''}}></td>
				<td><input name="10006_read" type="checkbox" id="minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->read_acl == '10006' ? 'checked' : ''}}></td>
				<td><input name="10006_update" type="checkbox" id="minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->update_acl == '10006' ? 'checked' : ''}}></td>
				<td><input name="10006_delete" type="checkbox" id="minimal-checkbox-10006" value="10006" {{$pengaturan10006 != null && $pengaturan10006->delete_acl == '10006' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10006" value="all" name="all" onChange="check(10006)" /></td>
			</tr>
			<?php $pengaturan10004 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10004')->first() ;?>
			<tr id='minimal-checkbox-10004'>
				<td>SPP - ST</td>
				<td><input name="10004_create" type="checkbox" id="minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->create_acl == '10004' ? 'checked' : ''}}></td>
				<td><input name="10004_read" type="checkbox" id="minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->read_acl == '10004' ? 'checked' : ''}}></td>
				<td><input name="10004_update" type="checkbox" id="minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->update_acl == '10004' ? 'checked' : ''}}></td>
				<td><input name="10004_delete" type="checkbox" id="minimal-checkbox-10004" value="10004" {{$pengaturan10004 != null && $pengaturan10004->delete_acl == '10004' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10004" value="all" name="all" onChange="check(10004)" /></td>
			</tr>
			<?php $pengaturan10003 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10003')->first() ;?>
			<tr id='minimal-checkbox-10003'>
				<td>SPP - RT</td>
				<td><input name="10003_create" type="checkbox" id="minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->create_acl == '10003' ? 'checked' : ''}}></td>
				<td><input name="10003_read" type="checkbox" id="minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->read_acl == '10003' ? 'checked' : ''}}></td>
				<td><input name="10003_update" type="checkbox" id="minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->update_acl == '10003' ? 'checked' : ''}}></td>
				<td><input name="10003_delete" type="checkbox" id="minimal-checkbox-10003" value="10003" {{$pengaturan10003 != null && $pengaturan10003->delete_acl == '10003' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10003" value="all" name="all" onChange="check(10003)" /></td>
			</tr>
			<?php $pengaturan10005 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10005')->first() ;?>
			<tr id='minimal-checkbox-10005'>
				<td>Pelayanan - Penanganan Piutang</td>
				<td><input name="10005_create" type="checkbox" id="minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->create_acl == '10005' ? 'checked' : ''}}></td>
				<td><input name="10005_read" type="checkbox" id="minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->read_acl == '10005' ? 'checked' : ''}}></td>
				<td><input name="10005_update" type="checkbox" id="minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->update_acl == '10005' ? 'checked' : ''}}></td>
				<td><input name="10005_delete" type="checkbox" id="minimal-checkbox-10005" value="10005" {{$pengaturan10005 != null && $pengaturan10005->delete_acl == '10005' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10005" value="all" name="all" onChange="check(10005)" /></td>
			</tr>
			<?php $pengaturan10007 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10007')->first() ;?>
			<tr id='minimal-checkbox-10007'>
				<td>Sosialisasi & Bimtek - Bahan Sosialisasi</td>
				<td><input name="10007_create" type="checkbox" id="minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->create_acl == '10007' ? 'checked' : ''}}></td>
				<td><input name="10007_read" type="checkbox" id="minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->read_acl == '10007' ? 'checked' : ''}}></td>
				<td><input name="10007_update" type="checkbox" id="minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->update_acl == '10007' ? 'checked' : ''}}></td>
				<td><input name="10007_delete" type="checkbox" id="minimal-checkbox-10007" value="10007" {{$pengaturan10007 != null && $pengaturan10007->delete_acl == '10007' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10007" value="all" name="all" onChange="check(10007)" /></td>
			</tr>
			<?php $pengaturan10008 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10008')->first() ;?>
			<tr id='minimal-checkbox-10008'>
				<td>Sosialisasi & Bimtek - Rencana Sosialisasi</td>
				<td><input name="10008_create" type="checkbox" id="minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->create_acl == '10008' ? 'checked' : ''}}></td>
				<td><input name="10008_read" type="checkbox" id="minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->read_acl == '10008' ? 'checked' : ''}}></td>
				<td><input name="10008_update" type="checkbox" id="minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->update_acl == '10008' ? 'checked' : ''}}></td>
				<td><input name="10008_delete" type="checkbox" id="minimal-checkbox-10008" value="10008" {{$pengaturan10008 != null && $pengaturan10008->delete_acl == '10008' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10008" value="all" name="all" onChange="check(10008)" /></td>
			</tr>
			<?php $pengaturan10009 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '10009')->first() ;?>
			<tr id='minimal-checkbox-10009'>
				<td>Sosialisasi & Bimtek - Monev Sosialisasi</td>
				<td><input name="10009_create" type="checkbox" id="minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->create_acl == '10009' ? 'checked' : ''}}></td>
				<td><input name="10009_read" type="checkbox" id="minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->read_acl == '10009' ? 'checked' : ''}}></td>
				<td><input name="10009_update" type="checkbox" id="minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->update_acl == '10009' ? 'checked' : ''}}></td>
				<td><input name="10009_delete" type="checkbox" id="minimal-checkbox-10009" value="10009" {{$pengaturan10009 != null && $pengaturan10009->delete_acl == '10009' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-10009" value="all" name="all" onChange="check(10009)" /></td>
			</tr>
			<?php $pengaturan100011 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100011')->first() ;?>
			<tr id='minimal-checkbox-100011'>
				<td>Inspeksi - Inspeksi Data</td>
				<td><input name="100011_create" type="checkbox" id="minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->create_acl == '100011' ? 'checked' : ''}}></td>
				<td><input name="100011_read" type="checkbox" id="minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->read_acl == '100011' ? 'checked' : ''}}></td>
				<td><input name="100011_update" type="checkbox" id="minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->update_acl == '100011' ? 'checked' : ''}}></td>
				<td><input name="100011_delete" type="checkbox" id="minimal-checkbox-100011" value="100011" {{$pengaturan100011 != null && $pengaturan100011->delete_acl == '100011' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100011" value="all" name="all" onChange="check(100011)" /></td>
			</tr>
			<?php $pengaturan100022 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100022')->first() ;?>
			<tr id='minimal-checkbox-100022'>
				<td>Pengaturan - Kuisioner</td>
				<td><input name="100022_create" type="checkbox" id="minimal-checkbox-100022" value="100022" {{$pengaturan100022 != null && $pengaturan100022->create_acl == '100022' ? 'checked' : ''}}></td>
				<td><input name="100022_read" type="checkbox" id="minimal-checkbox-100022" value="100022" {{$pengaturan100022 != null && $pengaturan100022->read_acl == '100022' ? 'checked' : ''}}></td>
				<td><input name="100022_update" type="checkbox" id="minimal-checkbox-100022" value="100022" {{$pengaturan100022 != null && $pengaturan100022->update_acl == '100022' ? 'checked' : ''}}></td>
				<td><input name="100022_delete" type="checkbox" id="minimal-checkbox-100022" value="100022" {{$pengaturan100022 != null && $pengaturan100022->delete_acl == '100022' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100022" value="all" name="all" onChange="check(100022)" /></td>
			</tr>
			<?php $pengaturan100023 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100023')->first() ;?>
			<tr id='minimal-checkbox-100023'>
				<td>Pengaturan - Freeze Date</td>
				<td><input name="100023_create" type="checkbox" id="minimal-checkbox-100023" value="100023" {{$pengaturan100023 != null && $pengaturan100023->create_acl == '100023' ? 'checked' : ''}}></td>
				<td><input name="100023_read" type="checkbox" id="minimal-checkbox-100023" value="100023" {{$pengaturan100023 != null && $pengaturan100023->read_acl == '100023' ? 'checked' : ''}}></td>
				<td><input name="100023_update" type="checkbox" id="minimal-checkbox-100023" value="100023" {{$pengaturan100023 != null && $pengaturan100023->update_acl == '100023' ? 'checked' : ''}}></td>
				<td><input name="100023_delete" type="checkbox" id="minimal-checkbox-100023" value="100023" {{$pengaturan100023 != null && $pengaturan100023->delete_acl == '100023' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100023" value="all" name="all" onChange="check(100023)" /></td>
			</tr>
			<?php $pengaturan100012 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100012')->first() ;?>
			<tr id='minimal-checkbox-100012'>
				<td>Pengaturan - Daftar Pengguna</td>
				<td><input name="100012_create" type="checkbox" id="minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->create_acl == '100012' ? 'checked' : ''}}></td>
				<td><input name="100012_read" type="checkbox" id="minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->read_acl == '100012' ? 'checked' : ''}}></td>
				<td><input name="100012_update" type="checkbox" id="minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->update_acl == '100012' ? 'checked' : ''}}></td>
				<td><input name="100012_delete" type="checkbox" id="minimal-checkbox-100012" value="100012" {{$pengaturan100012 != null && $pengaturan100012->delete_acl == '100012' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100012" value="all" name="all" onChange="check(100012)" /></td>
			</tr>
			<?php $pengaturan100013 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100013')->first() ;?>
			<tr id='minimal-checkbox-100013'>
				<td>Pengaturan - Hak Akses</td>
				<td><input name="100013_create" type="checkbox" id="minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->create_acl == '100013' ? 'checked' : ''}}></td>
				<td><input name="100013_read" type="checkbox" id="minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->read_acl == '100013' ? 'checked' : ''}}></td>
				<td><input name="100013_update" type="checkbox" id="minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->update_acl == '100013' ? 'checked' : ''}}></td>
				<td><input name="100013_delete" type="checkbox" id="minimal-checkbox-100013" value="100013" {{$pengaturan100013 != null && $pengaturan100013->delete_acl == '100013' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100013" value="all" name="all" onChange="check(100013)" /></td>
			</tr>
			<?php $pengaturan100014 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100014')->first() ;?>
			<tr id='minimal-checkbox-100014'>
				<td>Pengaturan - UPT</td>
				<td><input name="100014_create" type="checkbox" id="minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->create_acl == '100014' ? 'checked' : ''}}></td>
				<td><input name="100014_read" type="checkbox" id="minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->read_acl == '100014' ? 'checked' : ''}}></td>
				<td><input name="100014_update" type="checkbox" id="minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->update_acl == '100014' ? 'checked' : ''}}></td>
				<td><input name="100014_delete" type="checkbox" id="minimal-checkbox-100014" value="100014" {{$pengaturan100014 != null && $pengaturan100014->delete_acl == '100014' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100014" value="all" name="all" onChange="check(100014)" /></td>
			</tr>
			<?php $pengaturan100015 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100015')->first() ;?>
			<tr id='minimal-checkbox-100015'>
				<td>Pengaturan - Refrensi</td>
				<td><input name="100015_create" type="checkbox" id="minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->create_acl == '100015' ? 'checked' : ''}}></td>
				<td><input name="100015_read" type="checkbox" id="minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->read_acl == '100015' ? 'checked' : ''}}></td>
				<td><input name="100015_update" type="checkbox" id="minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->update_acl == '100015' ? 'checked' : ''}}></td>
				<td><input name="100015_delete" type="checkbox" id="minimal-checkbox-100015" value="100015" {{$pengaturan100015 != null && $pengaturan100015->delete_acl == '100015' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100015" value="all" name="all" onChange="check(100015)" /></td>
			</tr>
			<?php $pengaturan100016 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100016')->first() ;?>
			<tr id='minimal-checkbox-100016'>
				<td>Pengaturan - Backup Database</td>
				<td><input name="100016_create" type="checkbox" id="minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->create_acl == '100016' ? 'checked' : ''}}></td>
				<td><input name="100016_read" type="checkbox" id="minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->read_acl == '100016' ? 'checked' : ''}}></td>
				<td><input name="100016_update" type="checkbox" id="minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->update_acl == '100016' ? 'checked' : ''}}></td>
				<td><input name="100016_delete" type="checkbox" id="minimal-checkbox-100016" value="100016" {{$pengaturan100016 != null && $pengaturan100016->delete_acl == '100016' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100016" value="all" name="all" onChange="check(100016)" /></td>
			</tr>
			<?php $pengaturan100017 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100017')->first() ;?>
			<tr id='minimal-checkbox-100017'>
				<td>Pengaturan - Perusahaan</td>
				<td><input name="100017_create" type="checkbox" id="minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->create_acl == '100017' ? 'checked' : ''}}></td>
				<td><input name="100017_read" type="checkbox" id="minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->read_acl == '100017' ? 'checked' : ''}}></td>
				<td><input name="100017_update" type="checkbox" id="minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->update_acl == '100017' ? 'checked' : ''}}></td>
				<td><input name="100017_delete" type="checkbox" id="minimal-checkbox-100017" value="100017" {{$pengaturan100017 != null && $pengaturan100017->delete_acl == '100017' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100017" value="all" name="all" onChange="check(100017)" /></td>
			</tr>
			<?php $pengaturan100024 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100024')->first() ;?>
			<tr id='minimal-checkbox-100024'>
				<td>Pengaturan - Template Program</td>
				<td><input name="100024_create" type="checkbox" id="minimal-checkbox-100024" value="100024" {{$pengaturan100024 != null && $pengaturan100024->create_acl == '100024' ? 'checked' : ''}}></td>
				<td><input name="100024_read" type="checkbox" id="minimal-checkbox-100024" value="100024" {{$pengaturan100024 != null && $pengaturan100024->read_acl == '100024' ? 'checked' : ''}}></td>
				<td><input name="100024_update" type="checkbox" id="minimal-checkbox-100024" value="100024" {{$pengaturan100024 != null && $pengaturan100024->update_acl == '100024' ? 'checked' : ''}}></td>
				<td><input name="100024_delete" type="checkbox" id="minimal-checkbox-100024" value="100024" {{$pengaturan100024 != null && $pengaturan100024->delete_acl == '100024' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100024" value="all" name="all" onChange="check(100024)" /></td>
			</tr>
			<?php $pengaturan100025 = \App\Model\Privillage\Roleacl::where('role_id', $role->id)->where('module_id', '100025')->first() ;?>
			<tr id='minimal-checkbox-100025'>
				<td>Pengaturan - Tahun Program</td>
				<td><input name="100025_create" type="checkbox" id="minimal-checkbox-100025" value="100025" {{$pengaturan100025 != null && $pengaturan100025->create_acl == '100025' ? 'checked' : ''}}></td>
				<td><input name="100025_read" type="checkbox" id="minimal-checkbox-100025" value="100025" {{$pengaturan100025 != null && $pengaturan100025->read_acl == '100025' ? 'checked' : ''}}></td>
				<td><input name="100025_update" type="checkbox" id="minimal-checkbox-100025" value="100025" {{$pengaturan100025 != null && $pengaturan100025->update_acl == '100025' ? 'checked' : ''}}></td>
				<td><input name="100025_delete" type="checkbox" id="minimal-checkbox-100025" value="100025" {{$pengaturan100025 != null && $pengaturan100025->delete_acl == '100025' ? 'checked' : ''}}></td>
				<td><input type="checkbox" id="all-100025" value="all" name="all" onChange="check(100025)" /></td>
			</tr>
		</tbody>
	</table>
	@include('dev.helpers.button.btnGroupFormAction')
</form>
<script>

$('input[type="checkbox"]').css('cursor','pointer');

function check(no) {
            document.getElementById('minimal-checkbox-'+no).addEventListener('change', function(e) {
                var el = e.target;
                var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

                if (el.id === 'all-'+no) {
                    for (var i = 0, input; input = inputs[i++]; ) {
                        input.checked = el.checked;
                    }
                } else {
                    var numChecked = 0;

                    for (var i = 1, input; input = inputs[i++]; ) {
                        if (input.checked) {
                            numChecked++;
                        }
                    }
                    inputs[0].checked = numChecked === inputs.length - 1;
                }
            }, false);
        }
</script>
@endsection