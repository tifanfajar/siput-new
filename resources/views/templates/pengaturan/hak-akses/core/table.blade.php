<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Hak Akses</th>
			<th scope="col">Deskripsi</th>
			<th scope="col">Tanggal Buat</th>
			<th scope="col">Tanggal Ubah</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($roles as $role)
		<tr>
			<td>{{$role->role_name}}</td>
			<td>{{$role->description}}</td>
			<td>{{$role->created_at}}</td>
			<td>{{$role->updated_at}}</td>
			<td>
				<?php $id = Crypt::encryptString($role->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
				$getEdit = DB::table('role_acl')
				->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = DB::table('role_acl')
				->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				@if($getEdit == $getKdModule)
				<a href="{{route('hak-akses-edit',$id)}}"  class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
				@endif
				<a  class="btn  btn-danger btn-sm" 
				href="{{route('hak-akses-delete',$id)}}"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>