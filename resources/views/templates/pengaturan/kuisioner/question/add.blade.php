<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH PERTANYAAN</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kuisioner-question-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id_kuisioner" value="{{$id_kuisioner}}">
					<div class="col-md-12 form-group">
						<label for="jenis_pertanyaan">Jenis Pertanyaan</label>
						<select class="form-control select2" id="jenis_pertanyaan" name="jenis_pertanyaan" required>
							<option selected disabled>PILIH SALAH SATU</option>
							<option value="0">ESSAY</option>
							<option value="1">PILIHAN GANDA</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<label for="pertanyaan">Pertanyaan</label>
						<input type="text" class="form-control" name="pertanyaan" placeholder="Masukan Pertanyaan">
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

