<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Kuesioner</th>
			<th scope="col">Pertanyaan</th>
			<th scope="col">Jenis</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($question as $key =>$value)
		<tr>
			<td>{{\App\Model\Setting\Kuisioner\Kuisioner::where('id',$id_kuisioner)->value('title')}}</td>
			<td>{{$value->pertanyaan}}</td>
			<td>@if($value->jenis_pertanyaan == 1) PILIHAN GANDA @else ESSAY @endif</td>
			<td>
				<?php $id = Crypt::encryptString($value->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-pertanyaan="{{$value->pertanyaan}}" data-jenis="{{$value->jenis_pertanyaan}}"><i class="fa fa-file"></i></a>
				@if($getEdit == $getKdModule)
				<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-pertanyaan="{{$value->pertanyaan}}" data-jenis="{{$value->jenis_pertanyaan}}" data-id="{{$id}}"><i class="fa fa-edit"></i></a>
				@endif
				@if($getDelete == $getKdModule)
				<a  class="btn btn-danger btn-sm" href="{{route('kuisioner-question-delete',$id)}}" onClick="confirm('Yakin menghapus pertanyaan?')"><i class="fa fa-trash"></i></a>
				@endif
				@if($value->jenis_pertanyaan == 1)
				<a class="btn btn-primary btn-sm" href="{{route('kuisioner-question-option',$id)}}" style="color: white;"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH OPSI</a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>