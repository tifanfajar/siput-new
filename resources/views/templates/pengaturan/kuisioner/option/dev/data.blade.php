@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var option=$(e.relatedTarget).attr('data-option');
			$('#doption').val(option);
			var value=$(e.relatedTarget).attr('data-value');
			$('#dvalue').val(value);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var option=$(e.relatedTarget).attr('data-option');
			$('#eoption').val(option);
			var value=$(e.relatedTarget).attr('data-value');
			$('#evalue').val(value);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>
