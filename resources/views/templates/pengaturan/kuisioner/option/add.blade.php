<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH OPSI</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kuisioner-question-option-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id_pertanyaan" value="{{$id_pertanyaan}}">
					<div class="col-md-12 form-group">
						<label for="option">Option</label>
						<input type="text" class="form-control" name="option" placeholder="Masukan Opsi">
					</div>
					<div class="col-md-12 form-group">
						<label for="value">Value</label>
						<input type="number" class="form-control" name="value" placeholder="Masukan Nilai Value">
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

