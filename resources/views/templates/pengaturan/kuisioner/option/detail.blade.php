<div class="modal fade" id="detailModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL OPSI</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="title">OPSI</label>
						<input type="text" id="doption" class="form-control" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="value">Value</label>
						<input type="number" class="form-control" id="dvalue" name="value" placeholder="Masukan Nilai Value" disable>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>