<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Kuesioner</th>
			<th scope="col">Pertanyaan</th>
			<th scope="col">Option</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($option as $key =>$value)
		<tr>
			<td>{{\App\Model\Setting\Kuisioner\Kuisioner::where('id',$value->id_kuisioner)->value('title')}}</td>
			<td>{{\App\Model\Setting\Kuisioner\Question::where('id',$id_pertanyaan)->value('pertanyaan')}}</td>
			<td>{{$value->option}}</td>
			<td>
				<?php $id = Crypt::encryptString($value->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-value="{{$value->value}}" data-option="{{$value->option}}"><i class="fa fa-file"></i></a>
				@if($getEdit == $getKdModule)
				<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-value="{{$value->value}}" data-option="{{$value->option}}" data-id="{{$id}}"><i class="fa fa-edit"></i></a>
				@endif
				@if($getDelete == $getKdModule)
				<a  class="btn btn-danger btn-sm" href="{{route('kuisioner-question-option-delete',$id)}}" onClick="confirm('Yakin menghapus option?')"><i class="fa fa-trash"></i></a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>