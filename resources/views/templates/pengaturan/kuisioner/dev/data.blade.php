@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var title=$(e.relatedTarget).attr('data-title');
			$('#dtitle').val(title);
			var description=$(e.relatedTarget).attr('data-description');
			$('#ddescription').val(description);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#settingModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#sid').val(id);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var title=$(e.relatedTarget).attr('data-title');
			$('#etitle').val(title);
			var description=$(e.relatedTarget).attr('data-description');
			$('#edescription').val(description);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>