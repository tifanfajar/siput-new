<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<form>
<br>
<br>
<br>
<div class="card-shadow">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col">
				<center>
					<h3 class="mb-0 text-white"><i class="fas fa-question"></i>&nbsp;&nbsp;{{$kuisioner}}</h4>
					</center>
				</div>
			</div>
		</div>
		<?php 
		$i = 1;
		?>
		@foreach($question as $key => $value)
		@if($value->jenis_pertanyaan == 1)
		<div class="card-body">
			<div class="card">
				<div class="card-body text-black">
					<h2 class="card-title">&nbsp;{{$i++}}.&nbsp;{{$value->pertanyaan}}</h2>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-sm-10">
								@foreach(\App\Model\Setting\Kuisioner\Option::where('id_pertanyaan',$value->id)->get()  as $key => $option)
								<div class="radio">
									<label><input  type="radio" id="{{$option->id}}" name="option[{{$value->id}}]"  value="{{$option->id}}" required>&nbsp;&nbsp;{{$option->option}}</label>
								</div>
								@endforeach
							</div>
						</div>
					</fieldset>
				</div>	
			</div>
		</div>
		@else
		<div class="card-body">
			<div class="card">
				<div class="card-body text-black">
					<h2 class="card-title">&nbsp;{{$i++}}.&nbsp;{{$value->pertanyaan}}</h2>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="option[]" placeholder="Silahkan Mengisi Disini"></textarea>
								</div>
							</div>
						</div>
					</fieldset>
				</div>	
			</div>
		</div>
		@endif
		@endforeach
	</div>
	</form>
	@endsection
