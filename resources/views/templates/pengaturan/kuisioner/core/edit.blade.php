<div class="modal fade" id="editModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH KUISIONER</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kuisioner-update')}}" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="title">Nama Kuisioner</label>
						<input type="text" id="etitle" name="title" class="form-control">
					</div>
					<div class="col-md-12 form-group">
						<label for="description">Deskripsi</label>
						<input type="text" id="edescription" name="description" class="form-control">
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>