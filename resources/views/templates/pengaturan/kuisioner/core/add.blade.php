<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH KUISIONER</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kuisioner-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="title">Nama Kuisioner</label><label style="color: red;">*</label>
						<input type="text" class="form-control" name="title" placeholder="Masukan Nama Kuisioner" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="description">Deskripsi </label>
						<input type="text" class="form-control" name="description" placeholder="Masukan Deskripsi">
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

