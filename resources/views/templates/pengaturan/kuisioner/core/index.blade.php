@extends('dev.core.using')
@section('content')

@include('templates.pengaturan.kuisioner.core.add')
@include('templates.pengaturan.kuisioner.core.detail')
@include('templates.pengaturan.kuisioner.core.setting')
@include('templates.pengaturan.kuisioner.core.edit')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.pengaturan.kuisioner.dev.data')


<style>
	#example_filter{position: absolute; right: 20px;}
</style>
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA KUISIONER</a>
		@endif
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DAFTAR KUISIONER</h3>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<div class="widest">
					@include('templates.pengaturan.kuisioner.core.table')
				</div>
			</div>
		</div>
	</div>
	
</div>



@endsection