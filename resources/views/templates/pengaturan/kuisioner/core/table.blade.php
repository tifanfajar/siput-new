<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Nama Kuisioner</th>
			<th scope="col">Deskripsi</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($quiz as $key =>$value)
		<tr>
			<td>{{$value->title}}</td>
			<td>{{$value->description}}</td>
			<td>
				<?php $id = Crypt::encryptString($value->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-primary btn-sm" href="{{route('kuisioner-question',$id)}}" style="color: white;"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH PERTANYAAN</a>
				@if(\App\Model\Setting\Kuisioner\Setting::where('id_kuisioner', $value->id)->first())
				<a class="btn btn-info btn-sm" href="{{route('getQuizSetting',$id)}}"><i class="fa fa-cog"></i></a>
				@else
				<a class="btn btn-info btn-sm" id="getSetting" data-toggle="modal" data-target="#settingModal" data-id="{{$id}}"><i class="fa fa-cog"></i></a>
				@endif
				<a class="btn btn-warning btn-sm" href="{{route('kuisioner-preview',$id)}}" style="color: white;"><i class="fa fa-eye"></i>&nbsp;PREVIEW</a>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-title="{{$value->title}}" data-description="{{$value->description}}" data-link="{{$value->link}}"><i class="fa fa-file"></i></a>
				@if($getEdit == $getKdModule)
				<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-title="{{$value->title}}" data-description="{{$value->description}}" data-link="{{$value->link}}" data-id="{{$id}}"><i class="fa fa-edit"></i></a>
				@endif
				@if($getDelete == $getKdModule)
				<a  class="btn  btn-danger btn-sm" id="getDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="fa fa-trash"></i></a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>