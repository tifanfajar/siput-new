<div class="modal fade" id="settingModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">PENGATURAN KUISIONER</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kuisioner-save-setting')}}" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id_kusioner" id="sid">
					<div class="col-md-12 form-group">
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" value="1" name="active" class="custom-control-input" id="customCheck1">
					  <label class="custom-control-label" for="customCheck1">Kuesioner Aktif</label>
					</div>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggalaktif">Tanggal Aktif</label>
						<input type="date" name="start_date" id="tanggalaktif" class="form-control subCheck" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggalnonaktif">Tanggal Non-Aktif</label>
						<input type="date" name="end_date" id="tanggalnonaktif" class="form-control subCheck" disabled>
					</div>
					<table class="table table-bordered">
					  <thead>
					    <tr>
					      <th class="text-center" scope="col">Pilih</th>
					      <th scope="col">UPT</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach(\App\Model\Setting\UPT::distinct('office_id')->get() as $upts)
					    <tr>
					      <td class="text-center">
					      	<input type="checkbox" name="id_upt[]" value="{{$upts->office_id}}" id="defaultCheck1" class="subCheck" disabled>
					      </td>
					      <td>{{$upts->office_name}}</td>
					    </tr>
					    @endforeach
					  </tbody>
					</table>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary subCheck" disabled><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan Data</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function () {
		$("#customCheck1").click(function(){
			if (!$(this).prop("checked")){
				$(".subCheck").prop("disabled",true);
			}else{
				$(".subCheck").prop("disabled",false);
			}
		});
	});
</script>