<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<br>
<br>
<br>
<form action="{{route('kuisioner-update-setting')}}" id="editSetting" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<input type="hidden" value="{{$active->id_kuisioner}}" name="id_kuisioner" id="sid">
					<div class="col-md-12 form-group">
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" value="1" name="active" class="custom-control-input" id="customCheck2" @if($active->active == 1) checked @endif>
					  <label class="custom-control-label" for="customCheck2">Kuesioner Aktif</label>
					</div>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggalaktif">Tanggal Aktif</label>
						<input type="date" name="start_date" id="tanggalaktif" class="form-control" value="{{$active->start_date}}">
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggalnonaktif">Tanggal Non-Aktif</label>
						<input type="date" name="end_date" id="tanggalnonaktif" class="form-control" value="{{$active->end_date}}">
					</div>
					<table class="table table-bordered">
					  <thead>
					    <tr>
					      <th class="text-center" scope="col">Pilih</th>
					      <th scope="col">UPT</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach(\App\Model\Setting\UPT::distinct('office_id')->get() as $upts)
					    <tr>
					      <td class="text-center">
					      	<input type="checkbox" name="id_upt[]" value="{{$upts->office_id}}" id="defaultCheck1"
					      	@foreach($quizSetting as $value)
						      	@if($upts->office_id == $value->id_upt)
						      	checked
						      	@endif
					      	@endforeach
					      	>
					      </td>
					      <td>{{$upts->office_name}}</td>
					    </tr>
					    @endforeach
					  </tbody>
					</table>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupFormAction')
					</div>
				</form>
@include('dev.helpers.jquery')
@endsection