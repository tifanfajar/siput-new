@if (count($backups))
<table class="table table-striped table-bordered" style="text-align: center;">
    <thead>
        <tr>
            <th>No. </th>
            <th>Nama File</th>
            <th>Ukuran File</th>
            <th>Tanggal dibackup</th>
            <th colspan="2">Tindakan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($backups as $key => $backup)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{ $backup['file_name'] }}</td>
            <td>{{ $backup['file_size'] }}</td>
            <td>{{ $backup['last_modified'] }}</td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{ url('pengaturan/backup-database/download/'.$backup['file_name']) }}"><i class="fa fa-download"></i> Unduh</a>
            </td>
            <td>
                <a class="btn btn-danger btn-sm" href="{{ url('pengaturan/backup-database/delete/'.$backup['file_name']) }}"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div style="text-align: center; padding: 5px;">
    <h4 style="font-size: 16pt">Tidak ada Backup</h4>
</div>
@endif