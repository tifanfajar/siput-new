@extends('dev.core.using')
@section('content')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;Backup Database</h3>
					</div>
					@if($getCreate == $getKdModule)
					<div class="col text-right">
						<a href="{{ route('backup-database-create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i>&nbsp;Tambah Backup Database Baru</a>
					</div>
					@endif
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="">
					@include('templates.pengaturan.backup-database.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>



@endsection