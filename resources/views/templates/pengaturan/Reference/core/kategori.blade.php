<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addRefrensi"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DAFTAR KATEGORI</h3>
					</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
						<thead class="thead-light">
							<tr>
								<th scope="col">Kategori SPP</th>
								<th scope="col">Status</th>
								<th scope="col">Dibuat oleh</th>
								<th scope="col">Diubah oleh</th>
								<th scope="col">Tanggal Buat</th>
								<th scope="col">Tanggal Ubah</th>
								<th scope="col">Tindakan</th>
							</tr>
						</thead>
						<tbody>
							@foreach($refrensi as $refrensis)
							<tr>
								<td>{{$refrensis->kategori_spp}}</td>
								<td>@if($refrensis->status == 1)Aktif @else Tidak Aktif @endif</td>
								<td>{{\App\User::where('id', $refrensis->created_by)->value('name')}}</td>
								<td>{{\App\User::where('id', $refrensis->updated_by)->value('name')}}</td>
								<td>{{$refrensis->created_at}}</td>
								<td>{{$refrensis->updated_at}}</td>
								<td>
									<?php $id = Crypt::encryptString($refrensis->id) ?>
									<?php
									$url = \Request::route()->getName();
									$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
									$getEdit = DB::table('role_acl')
									->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
									->value('update_acl');
									$getDelete = DB::table('role_acl')
									->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
									->value('delete_acl');
									?>
									<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-kategori-spp="{{$refrensis->kategori_spp}}" data-created="{{$refrensis->created_at}}" data-updated="{{$refrensis->updated_at}}"><i class="fa fa-file"></i></a>
									@if($getEdit == $getKdModule)
									<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-id="{{$id}}" data-kategori-spp="{{$refrensis->kategori_spp}}" ><i class="fa fa-edit"></i></a>
									@endif
									@if($getDelete == $getKdModule)
									<a  class="btn btn-danger btn-sm" 
									id="getDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="fa fa-trash"></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>