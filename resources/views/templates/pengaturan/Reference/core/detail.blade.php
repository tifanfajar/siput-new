<!-- Kategori -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL KATEGORI SPP</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="kategori_spp">Kategori SPP *</label>
						<input type="text" id="dkategori_spp" class="form-control" name="kategori_spp" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Materi -->
<div class="modal fade" id="detailmateri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL MATERI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="jenis_materi">Jenis Materi *</label>
						<input type="text" id="djenis_materi" class="form-control" name="jenis_materi" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at_materi" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at_materi" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Group Perizinan -->
<div class="modal fade" id="detailGroupPerizinan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL GROUP PERIZINAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="jenis">jenis *</label>
						<input type="text" id="dGroupPerizinan" class="form-control" name="jenis" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at_perizinan" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at_perizinan" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Kegiatan -->
<div class="modal fade" id="detailkegiatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL KEGIATAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="kegiatan">Jenis Kegiatan *</label>
						<input type="text" id="dkegiatan" class="form-control" name="kegiatan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at_kegiatan" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at_kegiatan" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Metode -->
<div class="modal fade" id="detailmetode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL METODE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="metode">Metode *</label>
						<input type="text" id="dmetode" class="form-control" name="metode" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at_metode" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at_metode" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Tujuan -->
<div class="modal fade" id="detailtujuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL TUJUAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="tujuan">Tujuan *</label>
						<input type="text" id="dtujuan" class="form-control" name="tujuan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="created_at">Dibuat pada</label>
						<input type="text" class="form-control" id="dcreated_at_tujuan" name="created_at" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated_at">Diubah Pada</label>
						<input type="text" class="form-control" id="dupdated_at_tujuan" name="updated_at" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>