@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var kategori_spp=$(e.relatedTarget).attr('data-kategori-spp');
			$('#dkategori_spp').val(kategori_spp);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
		$('#detailkegiatan').on('show.bs.modal', function (e) {
			var kegiatan=$(e.relatedTarget).attr('data-kegiatan');
			$('#dkegiatan').val(kegiatan);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at_kegiatan').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at_kegiatan').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
		$('#detailGroupPerizinan').on('show.bs.modal', function (e) {
			var jenis=$(e.relatedTarget).attr('data-group-perizinan');
			$('#dGroupPerizinan').val(jenis);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at_perizinan').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at_perizinan').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
		$('#detailmateri').on('show.bs.modal', function (e) {
			var jenis_materi=$(e.relatedTarget).attr('data-materi');
			$('#djenis_materi').val(jenis_materi);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at_materi').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at_materi').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
		$('#detailmetode').on('show.bs.modal', function (e) {
			var metode=$(e.relatedTarget).attr('data-metode');
			$('#dmetode').val(metode);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at_metode').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at_metode').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
		$('#detailtujuan').on('show.bs.modal', function (e) {
			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			$('#dtujuan').val(tujuan);
			var created_at=$(e.relatedTarget).attr('data-created');
			$('#dcreated_at_tujuan').val(created_at);
			var updated_at=$(e.relatedTarget).attr('data-updated');
			$('#dupdated_at_tujuan').val(updated_at);
			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);
			var updated_by=$(e.relatedTarget).attr('data-updated-by');
			$('#dupdated_by').val(updated_by);

		});
	})
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var kategori_spp=$(e.relatedTarget).attr('data-kategori-spp');
			$('#ekategori_spp').val(kategori_spp);
		});
		$('#eKegiatan').on('show.bs.modal', function(e){
			var idkegiatan=$(e.relatedTarget).attr('data-kegiatan-id');
			$('#eidkegiatan').val(idkegiatan);
			var kegiatan=$(e.relatedTarget).attr('data-kegiatan');
			$('#ekegiatan').val(kegiatan);
		});
		$('#eGroupPerizinan').on('show.bs.modal', function(e){
			var idGroupPerizinan=$(e.relatedTarget).attr('data-group-perizinan-id');
			$('#eidGroupPerizinan').val(idGroupPerizinan);
			var jenis=$(e.relatedTarget).attr('data-group-perizinan');
			$('#ejenis').val(jenis);
		});
		$('#emateri').on('show.bs.modal', function(e){
			var idjenis_materi=$(e.relatedTarget).attr('data-jenis-materi-id');
			$('#eidjenis_materi').val(idjenis_materi);
			var jenis_materi=$(e.relatedTarget).attr('data-jenis-materi');
			$('#ejenis_materi').val(jenis_materi);
		});
		$('#eMetode').on('show.bs.modal', function(e){
			var idmetode=$(e.relatedTarget).attr('data-metode-id');
			$('#eidmetode').val(idmetode);
			var metode=$(e.relatedTarget).attr('data-metode');
			$('#emetode').val(metode);
		});
		$('#eTujuan').on('show.bs.modal', function(e){
			var idtujuan=$(e.relatedTarget).attr('data-tujuan-id');
			$('#eidtujuan').val(idtujuan);
			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			$('#etujuan').val(tujuan);
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>