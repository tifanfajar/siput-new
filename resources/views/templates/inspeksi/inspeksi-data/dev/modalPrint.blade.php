<!-- Modal -->
<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DOWNLOAD AS PDF</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
			<center>
				<form action="{{route('inspeksi-print')}}" method="GET" enctype="multipart/enctype">
					@csrf
					<center>
						<label>Filter Data :</label>
						<hr>
						<div class="row">
						<div class="col-md-6 form-group">
							<label for="date">Dari</label>
							<input type="date" class="form-control" id="fromPrint" name="dari">
						</div>
						<div class="col-md-6 form-group">
							<label for="date">Sampai</label>
							<input type="date" class="form-control" id="toPrint" name="sampai">
						</div>
						</div>
						<label>Kosongkan Input jika ingin menarik semua data</label>
						<hr>
					</center>
					<br>
					<center>
						@include('dev.helpers.button.btnGroupCetak')
					</center>
				</form>
			</center>
		</div>
	</div>
</div>
</div>
@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#fromPrint")
  .on('input', function() {
    var activeFee = (this.value === '0' || this.value === '') ? false : true;
    $('#toPrint').prop('required', activeFee);
  })
  .trigger('input');
</script>

<script type="text/javascript">
	$("#toPrint")
  .on('input', function() {
    var activeFee = (this.value === '0' || this.value === '') ? false : true;
    $('#fromPrint').prop('required', activeFee);
  })
  .trigger('input');
</script>