<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Inspeksi</h2>
			<hr>
			<table id="example" border="1" style="width: 100%;">
				<tr>
					<th scope="col" rowspan="3">Tahun</th>
					<th scope="col" rowspan="3">Bulan</th>
					<th scope="col" rowspan="3">Tanggal</th>
					<th scope="col" rowspan="3">Nama UPT</th>
					<th scope="col" rowspan="3">Data Sampling</th>
					<th scope="col" colspan="4" style="text-align: center; background-color: #ffc107; color: #fff;">Hasil Inspeksi</th>
					<th scope="col" colspan="2" style="text-align: center; background-color: #172B4D; color: #fff;">Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</th>
					<th scope="col" rowspan="3">Capaian(% valid)</th>
					<th scope="col" rowspan="3">Keterangan</th>
					<th scope="col" rowspan="3">Lampiran</th>
					<th scope="col" rowspan="3">Tipe</th>
					<th scope="col" rowspan="3">Status</th>
				</tr>
				<tr>
					<td scope="col" bgcolor="#ffc107" colspan="2" style="border-top: 0px;">Stasiun Radio Aktif</td>
					<td scope="col" bgcolor="#ffc107" rowspan="2" style="border-top: 0px;">Stasiun Radio Tidak Aktif</td>
					<td scope="col" bgcolor="#ffc107" rowspan="2" style="border-top: 0px;">Proses ISR</td>
					<td scope="col" bgcolor="#172B4D" rowspan="2" class="text-white" style="border-top: 0px;">Sudah Ditindaklanjuti</td>
					<td scope="col" bgcolor="#172B4D" rowspan="2" class="text-white" style="border-top: 0px;">Belum Ditindaklanjuti</td>
				</tr>
				<tr>
					<td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Sesuai ISR</td>
					<td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tidak Sesuai ISR</td>			
				</tr>
				<tbody>
					@foreach($inspeksi as $key =>$value)		
					<tr>
							<?php $id = Crypt::encryptString($value->id) ?>
							<?php  
							$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
							?>
							<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
										->format('Y')}}</td>
							<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
										->format('M')}}</td>
							<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
										->format('d, M Y')}}</td>
							<td>{{$upts}}</td>
							<td>{{$value->data_sampling}}</td>
							<td>{{$value->hi_sesuai_isr}}</td>
							<td>{{$value->hi_tidak_sesuai_isr}}</td>
							<td>{{$value->hi_tidak_aktif}}</td>
							<td>{{$value->hi_proses_isr}}</td>
							<td>{{$value->tl_sesuai_isr}}</td>
							<td>{{$value->tl_belum_isr}}</td>
							<td>{{$value->capaian_valid}}%</td>			
							<td>{{$value->keterangan}}</td>
							<td>{{$value->lampiran}}</td>
							<td>{{$value->type}}</td>	
							<td>@if($value->active == 1) Approved @else Draft @endif</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>