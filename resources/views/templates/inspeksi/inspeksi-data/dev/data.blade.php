@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);				
			}
		});
	});
</script>
<script type="text/javascript">
	$("#eupt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#eid_upt').html(data.html);				
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>
<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
function calculateAdd() {
		var total = parseInt($("#add_hi_sesuai_isr").val()) + parseInt($("#add_hi_tidak_sesuai_isr").val()) + parseInt($("#add_hi_tidak_aktif").val()) + parseInt($("#add_hi_proses_isr").val());
		$('#add_data_sampling').val(total);	
}
function calculateEdit() {
	var total = parseInt($("#ehi_sesuai_isr").val()) + parseInt($("#ehi_tidak_sesuai_isr").val()) + parseInt($("#ehi_tidak_aktif").val()) + parseInt($("#ehi_proses_isr").val());
	$('#edata_sampling').val(total);	
}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			
			var provinsi=$(e.relatedTarget).attr('data-id_prov');
			$('#eprovinsi').val(provinsi);			

			var upt=$(e.relatedTarget).attr('data-kode_upt');
			$('#eupt').val(upt);

			var tanggal_lapor=$(e.relatedTarget).attr('data-tanggal_lapor');
			$('#etanggal_lapor').val(tanggal_lapor);

			var data_sampling=$(e.relatedTarget).attr('data-data_sampling');
			$('#edata_sampling').val(data_sampling);

			var hi_sesuai_isr=$(e.relatedTarget).attr('data-hi_sesuai_isr');
			$('#ehi_sesuai_isr').val(hi_sesuai_isr);

			var hi_tidak_sesuai_isr=$(e.relatedTarget).attr('data-hi_tidak_sesuai_isr');
			$('#ehi_tidak_sesuai_isr').val(hi_tidak_sesuai_isr);

			var hi_tidak_aktif=$(e.relatedTarget).attr('data-hi_tidak_aktif');
			$('#ehi_tidak_aktif').val(hi_tidak_aktif);

			var hi_proses_isr=$(e.relatedTarget).attr('data-hi_proses_isr');
			$('#ehi_proses_isr').val(hi_proses_isr);

			var tl_sesuai_isr=$(e.relatedTarget).attr('data-tl_sesuai_isr');
			$('#etl_sesuai_isr').val(tl_sesuai_isr);

			var tl_belum_isr=$(e.relatedTarget).attr('data-tl_belum_isr');
			$('#etl_belum_isr').val(tl_belum_isr);

			var capaian_valid=$(e.relatedTarget).attr('data-capaian_valid');
			$('#ecapaian_valid').val(capaian_valid);

			var type=$(e.relatedTarget).attr('data-type');
			$('#etype').val(type);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#elampiran').val(lampiran);
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#eketerangan').val(keterangan);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#editRejectedModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#erid').val(id);
			
			var provinsi=$(e.relatedTarget).attr('data-id_prov');
			$('#erprovinsi').val(provinsi);			

			var upt=$(e.relatedTarget).attr('data-kode_upt');
			$('#erupt').val(upt);

			var tanggal_lapor=$(e.relatedTarget).attr('data-tanggal_lapor');
			$('#ertanggal_lapor').val(tanggal_lapor);

			var data_sampling=$(e.relatedTarget).attr('data-data_sampling');
			$('#erdata_sampling').val(data_sampling);

			var hi_sesuai_isr=$(e.relatedTarget).attr('data-hi_sesuai_isr');
			$('#erhi_sesuai_isr').val(hi_sesuai_isr);

			var hi_tidak_sesuai_isr=$(e.relatedTarget).attr('data-hi_tidak_sesuai_isr');
			$('#erhi_tidak_sesuai_isr').val(hi_tidak_sesuai_isr);

			var hi_tidak_aktif=$(e.relatedTarget).attr('data-hi_tidak_aktif');
			$('#erhi_tidak_aktif').val(hi_tidak_aktif);

			var hi_proses_isr=$(e.relatedTarget).attr('data-hi_proses_isr');
			$('#erhi_proses_isr').val(hi_proses_isr);

			var tl_sesuai_isr=$(e.relatedTarget).attr('data-tl_sesuai_isr');
			$('#ertl_sesuai_isr').val(tl_sesuai_isr);

			var tl_belum_isr=$(e.relatedTarget).attr('data-tl_belum_isr');
			$('#ertl_belum_isr').val(tl_belum_isr);

			var capaian_valid=$(e.relatedTarget).attr('data-capaian_valid');
			$('#ercapaian_valid').val(capaian_valid);

			var type=$(e.relatedTarget).attr('data-type');
			$('#ertype').val(type);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#erlampiran').val(lampiran);
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erketerangan').val(keterangan);
		});
	})
</script>


<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#did').val(id);
			
			var provinsi=$(e.relatedTarget).attr('data-id_prov');
			$('#dprovinsi').val(provinsi);			

			var upt=$(e.relatedTarget).attr('data-kode_upt');
			$('#dupt').val(upt);

			var tanggal_lapor=$(e.relatedTarget).attr('data-tanggal_lapor');
			$('#dtanggal_lapor').val(tanggal_lapor);

			var data_sampling=$(e.relatedTarget).attr('data-data_sampling');
			$('#ddata_sampling').val(data_sampling);

			var hi_sesuai_isr=$(e.relatedTarget).attr('data-hi_sesuai_isr');
			$('#dhi_sesuai_isr').val(hi_sesuai_isr);

			var hi_tidak_sesuai_isr=$(e.relatedTarget).attr('data-hi_tidak_sesuai_isr');
			$('#dhi_tidak_sesuai_isr').val(hi_tidak_sesuai_isr);

			var hi_tidak_aktif=$(e.relatedTarget).attr('data-hi_tidak_aktif');
			$('#dhi_tidak_aktif').val(hi_tidak_aktif);

			var hi_proses_isr=$(e.relatedTarget).attr('data-hi_proses_isr');
			$('#dhi_proses_isr').val(hi_proses_isr);

			var tl_sesuai_isr=$(e.relatedTarget).attr('data-tl_sesuai_isr');
			$('#dtl_sesuai_isr').val(tl_sesuai_isr);

			var tl_belum_isr=$(e.relatedTarget).attr('data-tl_belum_isr');
			$('#dtl_belum_isr').val(tl_belum_isr);

			var capaian_valid=$(e.relatedTarget).attr('data-capaian_valid');
			$('#dcapaian_valid').val(capaian_valid);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dlampiran').val(lampiran);

			var type=$(e.relatedTarget).attr('data-type');
			$('#dtype').val(type);
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailApprovalModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#daid').val(id);
			
			var provinsi=$(e.relatedTarget).attr('data-id_prov');
			$('#daprovinsi').val(provinsi);			

			var upt=$(e.relatedTarget).attr('data-kode_upt');
			$('#daupt').val(upt);

			var tanggal_lapor=$(e.relatedTarget).attr('data-tanggal_lapor');
			$('#datanggal_lapor').val(tanggal_lapor);

			var data_sampling=$(e.relatedTarget).attr('data-data_sampling');
			$('#dadata_sampling').val(data_sampling);

			var hi_sesuai_isr=$(e.relatedTarget).attr('data-hi_sesuai_isr');
			$('#dahi_sesuai_isr').val(hi_sesuai_isr);

			var hi_tidak_sesuai_isr=$(e.relatedTarget).attr('data-hi_tidak_sesuai_isr');
			$('#dahi_tidak_sesuai_isr').val(hi_tidak_sesuai_isr);

			var hi_tidak_aktif=$(e.relatedTarget).attr('data-hi_tidak_aktif');
			$('#dahi_tidak_aktif').val(hi_tidak_aktif);

			var hi_proses_isr=$(e.relatedTarget).attr('data-hi_proses_isr');
			$('#dahi_proses_isr').val(hi_proses_isr);

			var tl_sesuai_isr=$(e.relatedTarget).attr('data-tl_sesuai_isr');
			$('#datl_sesuai_isr').val(tl_sesuai_isr);

			var tl_belum_isr=$(e.relatedTarget).attr('data-tl_belum_isr');
			$('#datl_belum_isr').val(tl_belum_isr);

			var capaian_valid=$(e.relatedTarget).attr('data-capaian_valid');
			$('#dacapaian_valid').val(capaian_valid);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dalampiran').val(lampiran);

			var type=$(e.relatedTarget).attr('data-type');
			$('#datype').val(type);
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailRejectedModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);
			
			var provinsi=$(e.relatedTarget).attr('data-id_prov');
			$('#deprovinsi').val(provinsi);			

			var upt=$(e.relatedTarget).attr('data-kode_upt');
			$('#deupt').val(upt);

			var tanggal_lapor=$(e.relatedTarget).attr('data-tanggal_lapor');
			$('#detanggal_lapor').val(tanggal_lapor);

			var data_sampling=$(e.relatedTarget).attr('data-data_sampling');
			$('#dedata_sampling').val(data_sampling);

			var hi_sesuai_isr=$(e.relatedTarget).attr('data-hi_sesuai_isr');
			$('#dehi_sesuai_isr').val(hi_sesuai_isr);

			var hi_tidak_sesuai_isr=$(e.relatedTarget).attr('data-hi_tidak_sesuai_isr');
			$('#dehi_tidak_sesuai_isr').val(hi_tidak_sesuai_isr);

			var hi_tidak_aktif=$(e.relatedTarget).attr('data-hi_tidak_aktif');
			$('#dehi_tidak_aktif').val(hi_tidak_aktif);

			var hi_proses_isr=$(e.relatedTarget).attr('data-hi_proses_isr');
			$('#dehi_proses_isr').val(hi_proses_isr);

			var tl_sesuai_isr=$(e.relatedTarget).attr('data-tl_sesuai_isr');
			$('#detl_sesuai_isr').val(tl_sesuai_isr);

			var tl_belum_isr=$(e.relatedTarget).attr('data-tl_belum_isr');
			$('#detl_belum_isr').val(tl_belum_isr);

			var capaian_valid=$(e.relatedTarget).attr('data-capaian_valid');
			$('#decapaian_valid').val(capaian_valid);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#delampiran').val(lampiran);

			var type=$(e.relatedTarget).attr('data-type');
			$('#detype').val(type);
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#deketerangan').val(keterangan);
		});
	})
</script>