@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('inspeksi-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('inspeksi/data')}}">    
		@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')			
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>
		@endif
			
	<table class="table table-responsive">

	  <thead>
	    <tr>
	      <th scope="col">Tanggal Lapor *</th>
	      <th scope="col">Data Sampling *</th>
	      <th scope="col">Sesuai ISR *</th>
	      <th scope="col">Tidak Sesuai ISR *</th>
	      <th scope="col">Tidak Aktif *</th>
	      <th scope="col">Proses ISR *</th>
	      <th scope="col">Sudah Ditindaklanjuti *</th>
	      <th scope="col">Belum Ditindaklanjuti *</th>
	      <!-- <th scope="col">Capaian Valid (%) *</th>    -->
	      <th scope="col">Lampiran *</th>
	      <th scope="col">Keterangan</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $inspeksi => $inspeksis)
	    <tr>
	      <td><input type="date" class="form-control" id="tanggal_lapor" name="tanggal_lapor[]" value="{{$inspeksis[0]}}" data-validation="date" data-validation="date"></td>
	      <td><input type="text" class="form-control" id="data_sampling" name="data_sampling[]" value="{{$inspeksis[1]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="hi_sesuai_isr" name="hi_sesuai_isr[]" value="{{$inspeksis[2]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="hi_tidak_sesuai_isr" name="hi_tidak_sesuai_isr[]" value="{{$inspeksis[3]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="hi_tidak_aktif" name="hi_tidak_aktif[]" value="{{$inspeksis[4]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="hi_proses_isr" name="hi_proses_isr[]" value="{{$inspeksis[5]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="tl_sesuai_isr" name="tl_sesuai_isr[]" value="{{$inspeksis[6]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <td><input type="text" class="form-control" id="tl_belum_isr" name="tl_belum_isr[]" value="{{$inspeksis[7]}}" placeholder="Masukan Data Sampling" data-validation="number" ></td>
	      <!-- <td><input type="text" class="form-control" id="capaian_valid" name="capaian_valid[]" value="{{$inspeksis[8]}}" placeholder="Masukan Data Sampling" data-validation="decimal" ></td> -->
	      <td><input type="file" id="lampiran" name="lampiran[]"></td>
	      <td><input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$inspeksis[8]}}" placeholder="Masukan Keterangan"></td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
	});
</script>

@endsection