<div class="modal fade" id="multiPrint{{$upt->office_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="multiPrint">DOWNLOAD AS PDF</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<center>
					<a class="btn btn-success" style="color: white;" id="multiPrint" href="{{route('inspeksi-print-search',$upt->office_id)}}"><i class="fa fa-download"></i>&nbsp;&nbsp;DOWNLOAD AS PDF</a>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;BATAL</button>
				</center>
			</div>
		</div>
	</div>
</div>