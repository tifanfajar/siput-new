<div class="modal fade" id="downloadMulti{{$upt->office_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="downloadMultiSearch">DOWNLOAD AS PDF</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>			
			<div class="modal-body">
				<center> 
				<a class="btn btn-success" style="color: white;" href="{{route('inspeksi-export-search',$upt->office_id)}}"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;BATAL</button>
				</center>
		</div>
	</div>
</div>
</div>