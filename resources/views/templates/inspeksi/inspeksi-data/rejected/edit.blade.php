<div class="modal fade" id="editRejectedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH INSPEKSI DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('inspeksi-reject-update')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" id="erid">
					<div class="col-md-6 form-group">
						<label for="tanggal_lapor">Tanggal Lapor *</label>
						<input type="date" class="form-control" id="ertanggal_lapor" name="tanggal_lapor" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="type">Type *</label>
						<input type="text" class="form-control" id="ertype" name="type" placeholder="Masukan Tipe" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="data_sampling">Data Sampling *</label>
						<input type="number" class="form-control" id="erdata_sampling" name="data_sampling" placeholder="Masukan Data Sampling" required>
					</div>
					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px;">
						<h5>Hasil Inspeksi</h5>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_sesuai_isr">Sesuai ISR *</label>
						<input type="number" class="form-control" id="erhi_sesuai_isr" name="hi_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_sesuai_isr">Tidak Sesuai ISR *</label>
						<input type="number" class="form-control" id="erhi_tidak_sesuai_isr" name="hi_tidak_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_aktif">Tidak Aktif *</label>
						<input type="number" class="form-control" id="erhi_tidak_aktif" name="hi_tidak_aktif" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_proses_isr">Proses *</label>
						<input type="number" class="form-control" id="erhi_proses_isr" name="hi_proses_isr" placeholder="Masukan Data" required>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
						<h5>Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</h5>
					</div>

					<div class="col-md-6 form-group">
						<label for="tl_sesuai_isr">Sudah Ditindaklanjuti *</label>
						<input type="number" class="form-control" id="ertl_sesuai_isr" name="tl_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tl_belum_isr">Belum Ditindaklanjuti *</label>
						<input type="number" class="form-control" id="ertl_belum_isr" name="tl_belum_isr" placeholder="Masukan Data" required>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
					</div>

					<div class="col-md-6 form-group">
						<label for="capaian_valid">Capaian (% Valid) *</label>
						<input type="number" class="form-control" step='0.01' id="ercapaian_valid" name="capaian_valid" placeholder="0.00" required maxlength="2">
					</div>

					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="file" class="form-control" name="lampiran">
					</div>

					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran Sebelumnya *</label>
						<input type="text" class="form-control" id="erlampiran" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="erketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<select class="form-control" id="erupt_provinsi" name="upt_provinsi">
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<select name="id_upt" id="erid_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

