<div class="modal fade" id="detailRejectedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL INSPEKSI DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('inspeksi-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="tanggal_lapor">Tanggal Lapor *</label>
						<input type="date" class="form-control" id="detanggal_lapor" name="tanggal_lapor" placeholder="Masukan Tanggal" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="type">Type *</label>
						<input type="text" class="form-control" id="detype" name="type" placeholder="Masukan Tipe" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="data_sampling">Data Sampling *</label>
						<input type="number" class="form-control" id="dedata_sampling" name="data_sampling" placeholder="Masukan Data Sampling" disabled>
					</div>
					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px;">
						<h5>Hasil Inspeksi</h5>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_sesuai_isr">Sudah Ditindaklanjuti ISR *</label>
						<input type="number" class="form-control" id="dehi_sesuai_isr" name="hi_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_sesuai_isr">Belum Ditindaklanjuti ISR *</label>
						<input type="number" class="form-control" id="dehi_tidak_sesuai_isr" name="hi_tidak_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_aktif">Tidak Aktif *</label>
						<input type="number" class="form-control" id="dehi_tidak_aktif" name="hi_tidak_aktif" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_proses_isr">Proses *</label>
						<input type="number" class="form-control" id="dehi_proses_isr" name="hi_proses_isr" placeholder="Masukan Data" disabled>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
						<h5>Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</h5>
					</div>

					<div class="col-md-6 form-group">
						<label for="tl_sesuai_isr">Sesuai ISR *</label>
						<input type="number" class="form-control" id="detl_sesuai_isr" name="tl_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tl_belum_isr">Belum Sesuai ISR *</label>
						<input type="number" class="form-control" id="detl_belum_isr" name="tl_belum_isr" placeholder="Masukan Data" disabled>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
					</div>

					<div class="col-md-6 form-group">
						<label for="capaian_valid">Capaian (% Valid) *</label>
						<input type="number" class="form-control" step='0.01' id="decapaian_valid" name="capaian_valid" placeholder="0.00" disabled>
					</div>

					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="text" class="form-control" id="delampiran" name="lampiran" disabled>
					</div>					
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="deketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

