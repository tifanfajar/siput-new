@extends('dev.core.using')
@section('content')

@include('templates.inspeksi.inspeksi-data.core.add')
@include('templates.inspeksi.inspeksi-data.core.detail')
@include('templates.inspeksi.inspeksi-data.core.edit')
@include('templates.inspeksi.inspeksi-data.approval.detail')
@include('templates.inspeksi.inspeksi-data.rejected.detail')
@include('templates.inspeksi.inspeksi-data.rejected.edit')
@include('templates.inspeksi.inspeksi-data.dev.modalPrint')
@include('templates.inspeksi.inspeksi-data.dev.modalDownload')
@include('templates.helpers.delete')
@include('templates.helpers.preview')
@include('templates.helpers.import')
@include('templates.helpers.filter')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>


<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
	.footer{width: 100% !important;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.inspeksi.inspeksi-data.maps')
<div class="container" style="margin-top: 40px;">
	<br>
@if($getStatus == 'administrator')
	@include('templates.helpers.inspeksi.inspeksi-data.chart')
	@else
	@include('templates.helpers.inspeksi.inspeksi-data.chart-no-admin')
@endif
</div>
<div class="row mt-5" id="parent_table">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php ?>
		<?php
			$url = \Request::route()->getName();
			$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
			$getCreate = DB::table('role_acl')
			->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
			->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
		@include('dev.helpers.button.btnFilterData')
		<a href="{{route('inspeksi/data')}}" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i>&nbsp;SHOW ALL</a>
	</div>

	@if($getStatus == 'administrator')
		<div class="col-xl-12 mb-5 mb-5">
			<div class="card shadow" style="background-color: #f3f3f3;" >
			  <div class="card-header">
			    <div class="col-md-12">
			      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-search"></i>&nbsp;&nbsp;CARI DATA BERDASARKAN UPT</h4>
			      <!-- Query Tahunan -->
			      <form name="search_form" method="GET" action="{{route('inspeksi-search-upt')}}">
			        <div class="dropdown" style="margin-left: 10px;padding-top: 5px; float: right;">
			          <button class="btn btn-primary text-white" type="submit"><i class="fa fa-search"></i></button>
			        </div>
			        @if($getStatus == 'administrator')
			        <?php
			        $upt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get(); 
			        ?>
			        <div class="dropdown" style="float: right; float: right; width: 193px; margin-top: 5px; z-index: 1;">
			          <select class="form-control select2" name="upt">
			            <option selected disabled>Pilih UPT</option>
			            @foreach($upt as $value)
			            <option value="{{$value->office_id}}">{{$value->office_name}}</option>
			            @endforeach
			          </select>
			        </div>
			        @else
			        <input type="hidden" name="upt" value="{{$getUptName}}">
			        @endif
		      	</form>
			    </div>
			  </div>
			</div>
		</div>
	@endif

	<div class="col-xl-12 mb-5 mb-xl-0">
		<form method="POST" action="{{route('inspeksi-approved')}}" enctype="multipart/form-data" onsubmit="if($('.box-check:checkbox:checked').length <= 0) { alert('Silahkan memilih data terlebih dahulu'); return false;}">
			@csrf
			<div class="card shadow" style="border:1px solid #dedede;">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;MENUNGGU PERSETUJUAN</h3>
						</div>
				</div>
			</div>
			<div class="">
				<!-- Projects table -->
				<div class="">
					@include('templates.inspeksi.inspeksi-data.core.approval-table')
				</div>
			</div>
			</div>
			@if($getStatus == 'kepala-upt')
			<div class="col text-right" style="padding-top: 10px;">
				<button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
				&nbsp;
				<button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
			</div>
			@endif
		</form>
	</div>	

	@if($getStatus == 'operator')
	<div class="col-xl-12 mb-5 mb-xl-0">
		<br>
		<br>
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;BERKAS DITOLAK</h3>
					</div>
					</div>
				</div>
				<div class="">
					<!-- Projects table -->
					<div class="">
						@include('templates.inspeksi.inspeksi-data.core.reject-table')
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="col-xl-12 mb-5 mb-xl-0">
		<br>
		<br>
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DATA INSPEKSI</h3>
					</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.inspeksi.inspeksi-data.core.table')
				</div>
			</div>
		</div>
	</div>

@include('templates.inspeksi.inspeksi-data.dev.data')
@include('templates.helpers.inspeksi.inspeksi-data.js')


@endsection