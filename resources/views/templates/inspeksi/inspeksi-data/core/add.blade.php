<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">INPUT DATA INSPEKSI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('inspeksi-save')}}" id="inspeksi-save" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="tanggal_lapor">Tanggal Lapor</label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="tanggal_lapor" name="tanggal_lapor" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="data_sampling">Data Sampling </label><label style="color: red;">*</label>
						<input type="number" class="form-control"  id="add_data_sampling" name="data_sampling" placeholder="Masukan Jumlah Data Sampling" required readonly>
						<div class="invalid-feedback">
							Hasil inspeksi tidak boleh melebihi jumlah data sampling.
						</div>
					</div>
					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px;">
						<h5>Hasil Inspeksi</h5>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_sesuai_isr">Sesuai ISR </label><label style="color: red;">*</label>
						<input type="number" class="form-control" onchange="calculateAdd();" id="add_hi_sesuai_isr" name="hi_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_sesuai_isr">Tidak Sesuai ISR</label><label style="color: red;">*</label>
						<input type="number" class="form-control" onchange="calculateAdd();" id="add_hi_tidak_sesuai_isr" name="hi_tidak_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_aktif">Tidak Aktif </label><label style="color: red;">*</label>
						<input type="number" class="form-control" onchange="calculateAdd();" id="add_hi_tidak_aktif" name="hi_tidak_aktif" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_proses_isr">Proses ISR</label><label style="color: red;">*</label>
						<input type="number" class="form-control" onchange="calculateAdd();" id="add_hi_proses_isr" name="hi_proses_isr" placeholder="Masukan Data" required>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
						<h5>Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</h5>
						<h5>( Termasuk Data Tidak Aktif dan Proses ISR )</h5>
						<br>
					</div>

					<div class="col-md-6 form-group">
						<label for="tl_sesuai_isr">Sudah Ditindaklanjuti </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="tl_sesuai_isr" name="tl_sesuai_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tl_belum_isr">Belum Ditindaklanjuti </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="tl_belum_isr" name="tl_belum_isr" placeholder="Masukan Data" required>
					</div>
					<div class="col-md-12  form-group">
						<label for="lampiran">Lampiran</label><label style="color: red;">*</label>
						<input type="file" class="form-control" id="lampiran" name="lampiran">
					</div>		
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label><label style="color: red;">*</label>
						<textarea class="form-control" id="keterangan" name="keterangan" rows="3" placeholder="No Dinas/Perihal/Tanggal No Dinas"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label><label style="color: red;">*</label>
						<select class="form-control" id="upt_provinsi" name="upt_provinsi">
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label><label style="color: red;">*</label>
						<select name="id_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

