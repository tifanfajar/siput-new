<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL INSPEKSI DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('inspeksi-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="tanggal_lapor">Tanggal Lapor *</label>
						<input type="date" class="form-control" id="dtanggal_lapor" name="tanggal_lapor" placeholder="Masukan Tanggal" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="data_sampling">Data Sampling *</label>
						<input type="number" class="form-control" id="ddata_sampling" name="data_sampling" placeholder="Masukan Data Sampling" disabled>
					</div>
					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px;">
						<h5>Hasil Inspeksi</h5>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_sesuai_isr">Sesuai ISR *</label>
						<input type="number" class="form-control" id="dhi_sesuai_isr" name="hi_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_sesuai_isr">Tidak Sesuai ISR *</label>
						<input type="number" class="form-control" id="dhi_tidak_sesuai_isr" name="hi_tidak_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_tidak_aktif">Tidak Aktif *</label>
						<input type="number" class="form-control" id="dhi_tidak_aktif" name="hi_tidak_aktif" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="hi_proses_isr">Proses ISR *</label>
						<input type="number" class="form-control" id="dhi_proses_isr" name="hi_proses_isr" placeholder="Masukan Data" disabled>
					</div>

					<div class="col-md-12 text-center" style="margin-bottom: 10px;margin-top: 30px">
						<h5>Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</h5>
						<h5>( Termasuk Data Tidak Aktif dan Proses ISR )</h5>
						<br>
					</div>

					<div class="col-md-6 form-group">
						<label for="tl_sesuai_isr">Sudah Ditindaklanjuti *</label>
						<input type="number" class="form-control" id="dtl_sesuai_isr" name="tl_sesuai_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tl_belum_isr">Belum Ditindaklanjuti *</label>
						<input type="number" class="form-control" id="dtl_belum_isr" name="tl_belum_isr" placeholder="Masukan Data" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="capaian_valid">Capaian (% Valid) *</label>
						<input type="number" class="form-control" step='0.01' value='0.00' id="dcapaian_valid" name="capaian_valid" placeholder="0.00" disabled>
					</div>

					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="text" class="form-control" id="dlampiran" name="lampiran" disabled>
					</div>					
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="dketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

