<table id="example" class="table table-striped table-dark align-items-center table-flush data-table-inspeksi dataTable no-footer">
	<thead class="thead-light">
		<tr>
			@if($getStatus == 'kepala-upt')
			<th scope="col" rowspan="3"><input type="checkbox" id="selectAll" /></th>
			@endif
			<th scope="col" rowspan="3" style="border-right:1px solid #dedede; padding: 1%;">Tahun</th>
			<th scope="col" rowspan="3" style="border-right:1px solid #dedede; padding: 1%;">Bulan</th>
			<th scope="col" rowspan="3" style="border-right:1px solid #dedede; padding: 1%;">Tanggal</th>
			<th scope="col" rowspan="3" style="border-right:1px solid #dedede; padding: 1%;">Nama UPT</th>
			<th scope="col" rowspan="3" style="border-right:1px solid #dedede; padding: 1%;">Data Sampling</th>
			<th scope="col" colspan="4" style="text-align: center; background-color: #5f74e3; color: #fff;">Hasil Inspeksi</th>
			<th scope="col" colspan="2" style="text-align: center; background-color: #172B4D; color: #fff;">Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</th>
			<th scope="col" rowspan="3" style="padding: 1%;">Capaian(% valid)</th>
			<th scope="col" rowspan="3" style="padding: 1%;">Keterangan</th>
			<th scope="col" rowspan="3" style="padding: 1%;">Lampiran</th>
			<th scope="col" rowspan="3" style="padding: 1%;">Status</th>			
			<th scope="col" rowspan="3" style="padding: 1%;">Tindakan</th>			
		</tr>
		<tr>
			<td scope="col" bgcolor="#5f74e3" colspan="2" style="border-top: 0px; text-align: center;">Stasiun Radio Aktif</td>
			<td scope="col" bgcolor="#5f74e3" rowspan="2" style="border-top: 0px; padding: 1%;">Stasiun Radio Tidak Aktif</td>
			<td scope="col" bgcolor="#5f74e3" rowspan="2" style="border-top: 0px; padding: 1%;">Proses ISR</td>
			<td scope="col" bgcolor="#172B4D" rowspan="2" style="border-top: 0px; padding: 1%;">Sudah Ditindaklanjuti</td>
			<td scope="col" bgcolor="#172B4D" rowspan="2" style="border-top: 0px; padding: 1%;">Belum Ditindaklanjuti</td>
		</tr>
		<tr>
			<td scope="col" bgcolor="#5f74e3" style="border-top: 0px; padding: 1%;">Sesuai ISR</td>
			<td scope="col" bgcolor="#5f74e3" style="border-top: 0px; ">Tidak Sesuai ISR</td>			
		</tr>
	</thead>
	<tbody>		
		@foreach($inspeksiss as $value)
		@if($value->status == 0)		
		<input type="hidden" name="getStatus" value="{{$value->status}}">
		<tr>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			@if($getStatus == 'kepala-upt')
			<td><input class="checkBoxClass box-check" type="checkbox" name="id[]" value="{{ $value->id }}"></td>
			@endif
			<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
				->format('Y')}}</td>
				<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
					->format('M')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
						->format('d, M Y')}}</td>
						<td>{{$upts}}</td>
						<td>{{$value->data_sampling}}</td>
						<td>{{$value->hi_sesuai_isr}}</td>
						<td>{{$value->hi_tidak_sesuai_isr}}</td>
						<td>{{$value->hi_tidak_aktif}}</td>
						<td>{{$value->hi_proses_isr}}</td>
						<td>{{$value->tl_sesuai_isr}}</td>
						<td>{{$value->tl_belum_isr}}</td>
						<td>{{ number_format($value->capaian_valid, 2) }}%</td>			
						<td>{{$value->keterangan}}</td>
						<td>
							@if($value->lampiran != null)
							<a href="{{url('inspeksi/data/lampiran/'.$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>
							@endif
						</td>		
						<td>@if($value->stastus == 1) APPROVED @else DRAFT @endif</td>						
						<td>
							<a href="javascript:void(0)" data-toggle="modal" 
							data-target="#detailApprovalModal"
							data-id = "{{$id}}"
							data-id_prov = "{{$value->id_prov}}"
							data-kode_upt = "{{$value->kode_upt}}"
							data-tanggal_lapor = "{{$value->tanggal_lapor}}"
							data-data_sampling = "{{$value->data_sampling}}"
							data-hi_sesuai_isr = "{{$value->hi_sesuai_isr}}"
							data-hi_tidak_sesuai_isr = "{{$value->hi_tidak_sesuai_isr}}"
							data-hi_tidak_aktif = "{{$value->hi_tidak_aktif}}"
							data-hi_proses_isr = "{{$value->hi_proses_isr}}"
							data-tl_sesuai_isr = "{{$value->tl_sesuai_isr}}"
							data-tl_belum_isr = "{{$value->tl_belum_isr}}"
							data-capaian_valid = "{{$value->capaian_valid}}"
							data-keterangan = "{{$value->keterangan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-type = "{{$value->type}}"
							class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>

							<!-- <a href="#" data-modal="modal" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a> -->				
						</td>
					</tr>
					@endif
					@endforeach		
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align: right;font-weight: bold;">TOTAL</td>
						<td>{{$data_sampling_request}}</td>
						<td>{{$hi_sesuai_isr_request}}</td>
						<td>{{$hi_tidak_sesuai_isr_request}}</td>
						<td>{{$hi_tidak_aktif_request}}</td>
						<td>{{$hi_proses_isr_request}}</td>
						<td>{{$tl_sesuai_isr_request}}</td>
						<td>{{$tl_belum_isr_request}}</td>
						<td>{{number_format($capaian_request, 2)}} %</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>


			@include('dev.helpers.jquery')
			<script type="text/javascript">
				$(document).ready(function () {
					$("#selectAll").click(function () {
						$(".checkBoxClass").prop('checked', $(this).prop('checked'));
					});

					$(".checkBoxClass").change(function(){
						if (!$(this).prop("checked")){
							$("#selectAll").prop("checked",false);
						}
					});
				});</script>