<table id="example" class="table table-striped table-dark align-items-center table-flush data-table-inspeksi dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col" rowspan="3">Tahun</th>
			<th scope="col" rowspan="3">Bulan</th>
			<th scope="col" rowspan="3">Tanggal</th>
			<th scope="col" rowspan="3">Nama UPT</th>
			<th scope="col" rowspan="3">Data Sampling</th>
			<th scope="col" colspan="4" style="text-align: center; background-color: #ffc107; color: #fff;">Hasil Inspeksi</th>
			<th scope="col" colspan="2" style="text-align: center; background-color: #172B4D; color: #fff;">Tindak Lanjut Hasil Inspeksi Yang Tidak Sesuai ISR</th>
			<th scope="col" rowspan="3">Capaian(% valid)</th>
			<th scope="col" rowspan="3">Keterangan</th>
			<th scope="col" rowspan="3">Lampiran</th>
			<th scope="col" rowspan="3">Status</th>			
			<th scope="col" rowspan="3">Tindakan</th>			
		</tr>
		<tr>
			<td scope="col" bgcolor="#ffc107" colspan="2" style="border-top: 0px;">Stasiun Radio Aktif</td>
			<td scope="col" bgcolor="#ffc107" rowspan="2" style="border-top: 0px;">Stasiun Radio Tidak Aktif</td>
			<td scope="col" bgcolor="#ffc107" rowspan="2" style="border-top: 0px;">Proses ISR</td>
			<td scope="col" bgcolor="#172B4D" rowspan="2" style="border-top: 0px;">Sudah Ditindaklanjuti</td>
			<td scope="col" bgcolor="#172B4D" rowspan="2" style="border-top: 0px;">Belum Ditindaklanjuti</td>
		</tr>
		<tr>
			<td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Sesuai ISR</td>
			<td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tidak Sesuai ISR</td>			
		</tr>
	</thead>
	<tbody>
		@foreach($inspeksiss as $value)
		@if($value->status == 3)		
		<tr>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
				->format('Y')}}</td>
				<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
					->format('M')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_lapor)
						->format('d, M Y')}}</td>
						<td>{{$upts}}</td>
						<td>{{$value->data_sampling}}</td>
						<td>{{$value->hi_sesuai_isr}}</td>
						<td>{{$value->hi_tidak_sesuai_isr}}</td>
						<td>{{$value->hi_tidak_aktif}}</td>
						<td>{{$value->hi_proses_isr}}</td>
						<td>{{$value->tl_sesuai_isr}}</td>
						<td>{{$value->tl_belum_isr}}</td>
						<td>{{ number_format($value->capaian_valid, 2) }}%</td>			
						<td>{{$value->keterangan}}</td>
						<td>
							@if($value->lampiran != null)
							<a href="{{url('inspeksi/data/lampiran/'.$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>
							@endif
						</td>						
						<td>@if($value->status == 1) APPROVED @else DRAFT @endif</td>	
						<td>
							<a href="javascript:void(0)" data-toggle="modal" 
							data-target="#detailRejectedModal"
							data-id = "{{$id}}"
							data-id_prov = "{{$value->id_prov}}"
							data-kode_upt = "{{$value->kode_upt}}"
							data-tanggal_lapor = "{{$value->tanggal_lapor}}"
							data-data_sampling = "{{$value->data_sampling}}"
							data-hi_sesuai_isr = "{{$value->hi_sesuai_isr}}"
							data-hi_tidak_sesuai_isr = "{{$value->hi_tidak_sesuai_isr}}"
							data-hi_tidak_aktif = "{{$value->hi_tidak_aktif}}"
							data-hi_proses_isr = "{{$value->hi_proses_isr}}"
							data-tl_sesuai_isr = "{{$value->tl_sesuai_isr}}"
							data-tl_belum_isr = "{{$value->tl_belum_isr}}"
							data-capaian_valid = "{{$value->capaian_valid}}"
							data-keterangan = "{{$value->keterangan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-type = "{{$value->type}}"
							class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>

							<!-- <a href="#" data-modal="modal" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a> -->

							<a href="javascript:void(0)" data-toggle="modal" 
							data-target="#editRejectedModal"
							data-id = "{{$id}}"
							data-id_prov = "{{$value->id_prov}}"
							data-kode_upt = "{{$value->kode_upt}}"
							data-tanggal_lapor = "{{$value->tanggal_lapor}}"
							data-data_sampling = "{{$value->data_sampling}}"
							data-hi_sesuai_isr = "{{$value->hi_sesuai_isr}}"
							data-hi_tidak_sesuai_isr = "{{$value->hi_tidak_sesuai_isr}}"
							data-hi_tidak_aktif = "{{$value->hi_tidak_aktif}}"
							data-hi_proses_isr = "{{$value->hi_proses_isr}}"
							data-tl_sesuai_isr = "{{$value->tl_sesuai_isr}}"
							data-tl_belum_isr = "{{$value->tl_belum_isr}}"
							data-capaian_valid = "{{$value->capaian_valid}}"
							data-keterangan = "{{$value->keterangan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-type = "{{$value->type}}"
							class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>				

							<a  class="btn btn-danger btn-sm" 
							id="getDelete" 
							data-toggle="modal" 
							data-target="#deleteModal" 
							data-id="{{$id}}"><i class="fa fa-trash"></i>
						</a>

						<a href="{{route('inspeksi-reupload',$value->id)}}" class="btn btn-primary btn-sm text-white"  onclick="return confirm('Yakin Mengupload Ulang?');"><i class="fa fa-check-circle"></i>&nbsp;Ajukan Ulang</a>
					</td>
				</tr>
				@endif
				@endforeach	
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align: right;font-weight: bold;">TOTAL</td>
					<td>{{$data_sampling_reject}}</td>
					<td>{{$hi_sesuai_isr_reject}}</td>
					<td>{{$hi_tidak_sesuai_isr_reject}}</td>
					<td>{{$hi_tidak_aktif_reject}}</td>
					<td>{{$hi_proses_isr_reject}}</td>
					<td>{{$tl_sesuai_isr_reject}}</td>
					<td>{{$tl_belum_isr_reject}}</td>
					<td>{{number_format($capaian_reject, 2)}} %</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
