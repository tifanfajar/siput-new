<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.inspeksi.inspeksi-data.core.add')
@include('templates.inspeksi.inspeksi-data.core.detail')
@include('templates.inspeksi.inspeksi-data.rejected.detail')
@include('templates.inspeksi.inspeksi-data.rejected.edit')
@include('templates.inspeksi.inspeksi-data.approval.detail')
@include('templates.inspeksi.inspeksi-data.core.edit')
@include('templates.inspeksi.inspeksi-data.helpers.delete')
@include('templates.inspeksi.inspeksi-data.helpers.print-search')
@include('templates.inspeksi.inspeksi-data.helpers.unduh-search')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.inspeksi.inspeksi-data.dev.data')


<style>
	.widest div{width: 1530px !important;}
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.inspeksi.inspeksi-data.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@include('templates.helpers.inspeksi.inspeksi-data.chart-search')
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		$getRead = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('read_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@if($getRead == $getKdModule)
		<a class="btn btn-sm btn-info" style="color: white;" data-toggle="modal" data-target="#searchIndexPrint"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
		<a class="btn btn-sm btn-danger" style="color: white;" data-toggle="modal" data-target="#downloadIndexSearch"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
		@endif
		<a href="{{route('inspeksi/data')}}" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i>&nbsp;SHOW ALL</a>
	</div>
	<div class="col-xl-12 mb-5 mb-xl-0">
		<form method="POST" action="{{route('inspeksi-approved')}}" enctype="multipart/form-data" onsubmit="if($('.box-check:checkbox:checked').length <= 0) { alert('Silahkan memilih data terlebih dahulu'); return false;}">
			@csrf
			<div class="card shadow" style="border:1px solid #dedede;">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;MENUNGGU PERSETUJUAN</h3>
						</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.inspeksi.inspeksi-data.core.approval-table')
				</div>
			</div>
		</div>
		@if($getStatus == 'kepala-upt')
		<div class="col text-right" style="padding-top: 10px;">
			<button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
			&nbsp;
			<button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
		</div>
		@endif
	</form>
</div>

<div class="col-xl-12 mb-5 mb-xl-0">
	<br>
	<br>
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header border-0" style="background-color: #5f5f5f;">
			<div class="row align-items-center">
				<div class="col">
					<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;DATA INSPEKSI</h3>
				</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.inspeksi.inspeksi-data.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.inspeksi.inspeksi-data.js-search')

@endsection