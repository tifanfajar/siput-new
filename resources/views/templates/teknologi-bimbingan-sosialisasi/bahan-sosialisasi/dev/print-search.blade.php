<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Bahan Sosialisasi</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
				<thead class="thead-light">
					<tr>
						<th scope="col">TAHUN</th>
						<th scope="col">BULAN</th>
						<th scope="col">TANGGAL</th>
						<th scope="col">PROVINSI</th>
						<th scope="col">UPT</th>
						<th scope="col">MATERI</th>
						<th scope="col">JUDUL</th>
						<th scope="col">DESKRIPSI</th>
						<th scope="col">KETERANGAN</th>
						<th scope="col">LAMPIRAN</th>
						<th scope="col">KONTRIBUTOR</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bahsos as $key => $value)
					<tr>
						<td>{{\Carbon\Carbon::parse($value->created_at)->format('Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->created_at)->format('M')}}</td>
						<td>{{\Carbon\Carbon::parse($value->created_at)->format('d, M Y')}}</td>
						@if(\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama'))
						<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						@else
						<td >-</td>
						@endif
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						@if($upts)
						<td>{{$upts}}</td>
						@else
						<td>-</td>
						@endif
						<td>{{\App\Model\Refrension\Materi::where('id',$value->id_materi)->value('jenis_materi')}}</td>
						<td>{{$value->judul}}</td>
						<td>{{$value->deskripsi}}</td>
						<td>{{$value->keterangan}}</td>
						<td>{{$value->lampiran}}</td>
						<td>{{$value->author}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>
