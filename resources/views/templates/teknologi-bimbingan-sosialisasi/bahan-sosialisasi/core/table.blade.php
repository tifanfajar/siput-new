<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">TAHUN</th>
			<th scope="col">BULAN</th>
			<th scope="col">TANGGAL</th>
			<th scope="col">PROVINSI</th>
			<th scope="col">UPT</th>
			<th scope="col">MATERI</th>
			<th scope="col">JUDUL</th>
			<th scope="col">DESKRIPSI</th>
			<th scope="col">KETERANGAN</th>
			<th scope="col">LAMPIRAN</th>
			<th scope="col">KONTRIBUTOR</th>
			<th scope="col">TINDAKAN</th>
		</tr>
	</thead>
	<tbody>
		@foreach($bahsos as $value)
		<tr>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('Y')}}</td>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('M')}}</td>
			<td>{{\Carbon\Carbon::parse($value->created_at)->format('d, M Y')}}</td>
			@if(\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama'))
			<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
			@else
			<td >-</td>
			@endif
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			@if($upts)
			<td>{{$upts}}</td>
			@else
			<td>-</td>
			@endif
			<td>{{\App\Model\Refrension\Materi::where('id',$value->id_materi)->value('jenis_materi')}}</td>
			<td>{{$value->judul}}</td>
			<td>{{$value->deskripsi}}</td>
			<td>{{$value->keterangan}}</td>
			<td>
				@if($value->lampiran != null)
				<a class="btn btn-info btn-sm" onclick="return confirm('Download Lampiran ?')" href="{{url('sosialisasi-bimtek/bahan-sosialisasi/lampiran/'.$value->lampiran)}}"><i class="fa fa-download"></i></a>
				@endif
			</td>
			<td>{{$value->author}}</td>
			<td>
				<?php $id = Crypt::encryptString($value->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" 
				data-provinsi="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}" data-upt="{{$upts}}" data-tanggal="{{$value->created_at}}" data-materi="{{\App\Model\Refrension\Materi::where('id',$value->id_materi)->value('jenis_materi')}}" data-judul="{{$value->judul}}" data-deskripsi="{{$value->deskripsi}}" data-keterangan="{{$value->keterangan}}" data-lampiran="{{$value->lampiran}}" data-author="{{$value->author}}"><i class="fa fa-file"></i></a>
				@if($getEdit == $getKdModule)
				<a class="btn btn-warning btn-sm" data-id="{{$id}}" id="getEdit" data-toggle="modal" 
				data-materi="{{\App\Model\Refrension\Materi::where('id',$value->id_materi)->value('jenis_materi')}}"
				data-target="#editModal" 
				data-judul="{{$value->judul}}" 
				data-author="{{$value->author}}" 
				data-deskripsi="{{$value->deskripsi}}"
				data-materi="{{$value->materi}}"
				data-id-materi="{{$value->id_materi}}"
				data-keterangan="{{$value->keterangan}}" 
				data-lampiran="{{$value->lampiran}}"><i class="fa fa-edit"></i></a>
				@endif
				@if($getDelete == $getKdModule)
				<a  class="btn  btn-danger btn-sm" 
				id="getDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="fa fa-trash"></i></a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
