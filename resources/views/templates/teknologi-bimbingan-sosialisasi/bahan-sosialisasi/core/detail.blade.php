<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL BAHAN SOSIALISASI</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('bahsos-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="dtanggal">Materi *</label>
						<input type="text" class="form-control" id="dmateri" name="dmateri" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="dtanggal">Tanggal *</label>
						<input type="text" class="form-control" id="dtanggal" name="dtanggal" placeholder="Masukan dtanggal" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="judul">Judul *</label>
						<input type="text" class="form-control" id="djudul" name="judul" placeholder="Masukan Judul" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="kontributor">Kontributor *</label>
						<input type="text" class="form-control" id="dauthor" name="kontributor" placeholder="Masukan Kontributor" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="kontributor">UPT *</label>
						<input type="text" class="form-control" id="dupt" name="dupt" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="kontributor">Provinsi *</label>
						<input type="text" class="form-control" id="dprovinsi" name="dprovinsi" placeholder="Admin" disabled>
					</div>					
					<div class="col-md-6 form-group">
						<label for="deskripsi">Deskripsi</label>
						<textarea class="form-control" id="ddeskripsi" name="ddeskripsi" rows="3" placeholder="Masukan Deskripsi" disabled></textarea>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="dketerangan" name="dketerangan" rows="3" placeholder="Masukan Keterangan" disabled></textarea>
					</div>
					<div class="col-md-12  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="text" class="form-control" id="dlampiran" name="dprovinsi" placeholder="Masukan dprovinsi" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>