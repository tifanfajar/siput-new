@extends('dev.core.using')
@section('content')
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('monsos-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('sosialisasi-bimtek/monev-sosialisasi')}}">
		<!-- <label for="id_materi">Pilih Rencana Sosialisasi</label>
		<label style="color: red;">*</label>						
		<select class="form-control select2" name="id_rensos" required>
			<option selected disabled>Pilih salah satu</option>
			@foreach(\App\Model\SosialisasiBimtek\RencanaSosialisasi::all() as $value)
			<option value="{{$value->id}}">{{$value->tema}} | {{$value->tanggal_pelaksanaan}}</option>
			@endforeach
		</select> -->
	<table class="table table-responsive">

	  <thead>
	    <tr>
			<th scope="col" style="background-color: #C6EFCE">TANGGAL BUAT</th>
			<th scope="col" style="background-color: #C6EFCE">RENCANA SOSIALISASI</th>
			<th scope="col" style="background-color: #C6EFCE">PROVINSI</th>
			<th scope="col" style="background-color: #C6EFCE">UPT</th>
			<th scope="col" style="background-color: #C6EFCE">JENIS KEGIATAN</th>
			<th scope="col" style="background-color: #C6EFCE">TEMPAT</th>
			<th scope="col" style="background-color: #C6EFCE">TEMA</th>
			<th scope="col" style="background-color: #C6EFCE">TARGET PESERTA</th>
			<th scope="col" style="background-color: #C6EFCE">ANGGARAN</th>
			<th scope="col" style="background-color: #C6EFCE">NARASUMBER</th>

			<th scope="col" style="background-color: #FFEB9C">TANGGAL PELAKSANAAN</th>
			<th scope="col" style="background-color: #FFEB9C">TEMPAT</th>
			<th scope="col" style="background-color: #FFEB9C">TEMA</th>
			<th scope="col" style="background-color: #FFEB9C">JUMLAH PESERTA</th>
			<th scope="col" style="background-color: #FFEB9C">REALISASI ANGGARAN</th>
			<th scope="col" style="background-color: #FFEB9C">NARASUMBER</th>
			<th scope="col" style="background-color: #FFEB9C">KETERANGAN</th>
			<th scope="col" style="background-color: #FFEB9C">LAMPIRAN</th>
		</tr>
	  </thead>
	  <tbody>
		@foreach($result as $key => $monsoss)
		<?php
			$upt = \App\Model\Setting\UPT::where('office_name', $monsoss[3])->distinct()->value('office_id');
			$rensos = \App\Model\SosialisasiBimtek\RencanaSosialisasi::where('tanggal_pelaksanaan', $monsoss[1])->where('kode_upt', $upt)->where('tempat', $monsoss[5])->where('tema', $monsoss[6])->where('target_peserta', $monsoss[7])->where('anggaran', $monsoss[8])->where('narasumber', $monsoss[9])->value('id');
			$jenis_kegiatan = \App\Model\SosialisasiBimtek\RencanaSosialisasi::where('tanggal_pelaksanaan', $monsoss[1])->where('kode_upt', $upt)->where('tempat', $monsoss[5])->where('tema', $monsoss[6])->where('target_peserta', $monsoss[7])->where('anggaran', $monsoss[8])->where('narasumber', $monsoss[9])->value('jenis_kegiatan');
		?>
		<tr>
			<td scope="col">{{$monsoss[0]}}</td>
			<td scope="col">{{$monsoss[1]}}</td>
			<td scope="col">{{$monsoss[2]}}</td>
			<td scope="col">{{$monsoss[3]}}</td>
			<td scope="col">{{$monsoss[4]}}</td>
			<td scope="col">{{$monsoss[5]}}</td>
			<td scope="col">{{$monsoss[6]}}</td>
			<td scope="col">{{$monsoss[7]}}</td>
			<td scope="col">{{$monsoss[8]}}</td>
			<td scope="col">{{$monsoss[9]}}</td>
			<td><input type="date" class="form-control" id="tanggal_pelaksanaan" name="tanggal_pelaksanaan[]" value="{{$monsoss[10]}}" data-validation="date" required></td>
			<td><input type="text" class="form-control" id="tempat" name="tempat[]" placeholder="Masukan Tempat" value="{{$monsoss[11]}}" data-validation="message" required></td>
			<td><input type="text" class="form-control" id="tema" name="tema[]" placeholder="Masukan Tema" value="{{$monsoss[12]}}" data-validation="message"  required></td>
			<td><input type="text" class="form-control" id="jumlah_peserta" name="jumlah_peserta[]" placeholder="Masukan Jumlah Peserta" value="{{$monsoss[13]}}" data-validation="number" required></td>
			<td><input type="text" class="form-control" id="anggaran" name="anggaran[]" placeholder="Masukan Anggatan" value="{{$monsoss[14]}}" data-validation="number" data-validation="message" required></td>
			<td><input type="text" class="form-control" id="narasumber" name="narasumber[]" placeholder="Masukan Anggatan" value="{{$monsoss[15]}}" data-validation="message" data-validation="message" required></td>
			<td><input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$monsoss[16]}}" placeholder="Masukan Keterangan" data-validation="message" required></td>
			<td><input type="file" id="lampiran" name="lampiran[]" required></td>
	    </tr>
	    <input type="hidden" name="id_rensos[]" id="id_rensos" value="{{$rensos}}">
	    <input type="hidden" name="jenis_kegiatan[]" id="jenis_kegiatan" value="{{$jenis_kegiatan}}">
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
	});
</script>

@endsection

