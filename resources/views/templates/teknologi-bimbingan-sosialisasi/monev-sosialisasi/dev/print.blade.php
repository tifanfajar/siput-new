<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Monev Sosialisasi</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 10px;">
				<thead class="thead-light">
				<tr>
					<th scope="col" style="background-color: #C6EFCE">NO .</th>
					<th scope="col" style="background-color: #C6EFCE">TANGGAL BUAT</th>
					<th scope="col" style="background-color: #C6EFCE">RENCANA SOSIALISASI</th>
					<th scope="col" style="background-color: #C6EFCE">PROVINSI</th>
					<th scope="col" style="background-color: #C6EFCE">UPT</th>
					<th scope="col" style="background-color: #C6EFCE">JENIS KEGIATAN</th>
					<th scope="col" style="background-color: #C6EFCE">TEMPAT</th>
					<th scope="col" style="background-color: #C6EFCE">TEMA</th>
					<th scope="col" style="background-color: #C6EFCE">JUMLAH PESERTA</th>
					<th scope="col" style="background-color: #C6EFCE">ANGGARAN</th>
					<th scope="col" style="background-color: #C6EFCE">NARASUMBER</th>

					<th scope="col" style="background-color: #FFEB9C">TANGGAL PELAKSANAAN</th>
					<th scope="col" style="background-color: #FFEB9C">TEMPAT</th>
					<th scope="col" style="background-color: #FFEB9C">TEMA</th>
					<th scope="col" style="background-color: #FFEB9C">JUMLAH PESERTA</th>
					<th scope="col" style="background-color: #FFEB9C">REALISASI ANGGARAN</th>
					<th scope="col" style="background-color: #FFEB9C">NARASUMBER</th>
					<th scope="col" style="background-color: #FFEB9C">KETERANGAN</th>
				</tr>
			</thead>
			<tbody>
				@foreach($rensos as $key => $value)
				@if($value->status == 1)
				<?php
					$monsos = \App\Model\SosialisasiBimtek\MonevSosialisasi::where('id_rensos', $value->id)->first();
				?>
				<tr>
					<td scope="col">{{$key+1}}</td>
					<td scope="col">{{$value->created_at}}</td>
					<td scope="col">{{$value->tanggal_pelaksanaan}}</td>
					<td scope="col">{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
					<td scope="col">{{\App\Model\Setting\UPT::where('office_id',$value->kode_upt)->distinct()->value('office_name')}}</td>
					<td scope="col">{{\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan')}}</td>
					<td scope="col">{{$value->tempat}}</td>
					<td scope="col">{{$value->tema}}</td>
					<td scope="col">{{$value->target_peserta}}</td>
					<td scope="col">{{$value->anggaran}}</td>
					<td scope="col">{{$value->narasumber}}</td>

					@if($monsos)
						<td scope="col">{{$monsos->tanggal_pelaksanaan}}</td>
						<td scope="col">{{$monsos->tempat}}</td>
						<td scope="col">{{$monsos->tema}}</td>
						<td scope="col">{{$monsos->jumlah_peserta}}</td>
						<td scope="col">{{$monsos->anggaran}}</td>
						<td scope="col">{{$monsos->narasumber}}</td>
						<td scope="col">{{$monsos->keterangan}}</td>
					@else
						<td scope="col"></td>
						<td scope="col"></td>
						<td scope="col"></td>
						<td scope="col"></td>
						<td scope="col"></td>
						<td scope="col"></td>
						<td scope="col"></td>
					@endif
				</tr>
				@endif
				@endforeach
			<hr>
		</div>
	</center>
</body>
</html>