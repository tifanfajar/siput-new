@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailReject').on('show.bs.modal', function (e) {
			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			$('#drjenis_kegiatan').val(jenis_kegiatan);
			var tanggal_pelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#drtanggal_pelaksanaan').val(tanggal_pelaksanaan); 
			var tema=$(e.relatedTarget).attr('data-tema');
			$('#drtema').val(tema); 
			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#drtempat').val(tempat);
			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#drkategori_peserta').val(kategori_peserta);
			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#drjumlah_peserta').val(jumlah_peserta);
			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#dranggaran').val(anggaran);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#drlampiran').val(lampiran);
			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#drnarasumber').val(narasumber);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#drketerangan').val(keterangan);
			var status=$(e.relatedTarget).attr('data-status');
			var status=$(e.relatedTarget).attr('data-status');
			if(status == 0){
				$('#drstatus').val('Open');
			}else{
				$('#drstatus').val('Close');
			}			

		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			$('#djenis_kegiatan').val(jenis_kegiatan);
			var tanggal_pelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#dtanggal_pelaksanaan').val(tanggal_pelaksanaan); 
			var tema=$(e.relatedTarget).attr('data-tema');
			$('#dtema').val(tema); 
			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#dtempat').val(tempat);
			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#dkategori_peserta').val(kategori_peserta);
			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#djumlah_peserta').val(jumlah_peserta);
			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#danggaran').val(anggaran);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dlampiran').val(lampiran);
			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#dnarasumber').val(narasumber);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
			var status=$(e.relatedTarget).attr('data-status');
			var status=$(e.relatedTarget).attr('data-status');
			if(status == 0){
				$('#dstatus').val('Open');
			}else{
				$('#dstatus').val('Close');
			}			

		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailApprovalModal').on('show.bs.modal', function (e) {
			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			$('#dajenis_kegiatan').val(jenis_kegiatan);
			var tanggal_pelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#datanggal_pelaksanaan').val(tanggal_pelaksanaan); 
			var tema=$(e.relatedTarget).attr('data-tema');
			$('#datema').val(tema); 
			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#datempat').val(tempat);
			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#dakategori_peserta').val(kategori_peserta);
			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#dajumlah_peserta').val(jumlah_peserta);
			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#daanggaran').val(anggaran);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dalampiran').val(lampiran);
			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#danarasumber').val(narasumber);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);
			var status=$(e.relatedTarget).attr('data-status');
			var status=$(e.relatedTarget).attr('data-status');
			if(status == 0){
				$('#dastatus').val('Open');
			}else{
				$('#dastatus').val('Close');
			}	
			

		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var id_rensos=$(e.relatedTarget).attr('data-id-rensos');
			$('#eid_rensos').val(id_rensos);
			var tanggal_pelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#etanggal_pelaksanaan').val(tanggal_pelaksanaan); 
			var target_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#etarget_peserta').val(target_peserta); 
			var tema=$(e.relatedTarget).attr('data-tema');
			$('#etema').val(tema); 
			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#etempat').val(tempat);
			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#ekategori_peserta').val(kategori_peserta);
			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#ejumlah_peserta').val(jumlah_peserta);
			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#eanggaran').val(anggaran);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#elampiran').val(lampiran);
			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#enarasumber').val(narasumber);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#eketerangan').val(keterangan);
			var status=$(e.relatedTarget).attr('data-status');
			var status=$(e.relatedTarget).attr('data-status');
			if(status == 0){
				$('#estatus').val('Open');
			}else{
				$('#estatus').val('Close');
			}			

		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#erid').val(id);
			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			$('#erjenis_kegiatan').val(jenis_kegiatan);
			var tanggal_pelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#ertanggal_pelaksanaan').val(tanggal_pelaksanaan); 
			var target_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#ertarget_peserta').val(target_peserta); 
			var tema=$(e.relatedTarget).attr('data-tema');
			$('#ertema').val(tema); 
			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#ertempat').val(tempat);
			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#erkategori_peserta').val(kategori_peserta);
			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#erjumlah_peserta').val(jumlah_peserta);
			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#eranggaran').val(anggaran);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#erlampiran').val(lampiran);
			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#ernarasumber').val(narasumber);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erketerangan').val(keterangan);
			var status=$(e.relatedTarget).attr('data-status');
			var status=$(e.relatedTarget).attr('data-status');
			if(status == 0){
				$('#erstatus').val('Open');
			}else{
				$('#erstatus').val('Close');
			}			

		});
	})
</script>