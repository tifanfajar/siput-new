@extends('dev.core.using')
@section('content')

@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.add')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.detail')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.edit')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.helpers.delete')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.helpers.print-search')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.helpers.unduh-search')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.chart-search')
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		$getRead = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('read_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@if($getRead == $getKdModule)
		<a class="btn btn-sm btn-info" style="color: white;" data-toggle="modal" data-target="#searchIndexPrint"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
		<a class="btn btn-sm btn-danger" style="color: white;" data-toggle="modal" data-target="#downloadIndexSearch"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
		@endif
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;EVALUASI CAPAIAN PELAKSANAAN SOSIALISASI DAN BIMTEK PERIZINAN SFR DAN SOR</h3>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<div class="widest">
					@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.js-search')

@endsection