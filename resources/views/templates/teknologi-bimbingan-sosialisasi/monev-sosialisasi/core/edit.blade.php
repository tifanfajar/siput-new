<div class="modal fade" id="editModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH MONEV SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('monsos-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-6 form-group">
						<label for="tanggal_pelaksanaan">Tanggal Pelaksanaan *</label>
						<input type="date" class="form-control" id="etanggal_pelaksanaan" name="tanggal_pelaksanaan" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="tema">Tema *</label>
						<input type="text" class="form-control" id="etema" name="tema" placeholder="Masukan Tema" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="tempat">Tempat *</label>
						<input type="text" class="form-control" id="etempat" name="tempat" placeholder="Masukan Tempat" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="jumlah_peserta">Jumlah Peserta *</label>
						<input type="number" class="form-control" id="ejumlah_peserta" name="jumlah_peserta" placeholder="Masukan Jumlah" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="anggaran">Anggaran *</label>
						<input type="number" class="form-control" id="eanggaran" name="anggaran" placeholder="Masukan Anggaran" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="file" class="form-control" name="lampiran">
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran Sebelumnya</label>
						<input type="text" class="form-control" id="elampiran" name="lampiran_sebelumnnya" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="narasumber">Narasumber *</label>
						<input type="text" class="form-control" id="enarasumber" name="narasumber" placeholder="Masukan Narasumber" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="eketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>