<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH MONITOR EVALUASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('monsos-save')}}" class="row" method="POST" id="form-add-monev" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="id_materi">Pilih Rencana Sosialisasi</label>
						<label style="color: red;">*</label>
						@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')			
						<select class="form-control select2" id="id_rensos" name="id_rensos" required>
							<option selected disabled>Pilih salah satu</option>
							<?php
							$monev = \App\Model\SosialisasiBimtek\MonevSosialisasi::select('id_rensos')->get();
							$id_rensos = [];
							foreach ($monev as $key => $value) {
								$id_rensos[] = $value->id_rensos;
							}
							?>
							@foreach(\App\Model\SosialisasiBimtek\RencanaSosialisasi::whereNotIn('id',$id_rensos)->get() as $value)
							<option value="{{$value->id}}">{{$value->tema}} | {{$value->tanggal_pelaksanaan}}</option>
							@endforeach
						</select>
						@else
						<?php
						$monev_user = \App\Model\SosialisasiBimtek\MonevSosialisasi::where('kode_upt',Auth::user()->upt)->select('id_rensos')->get();
						$id_rensos_user = [];
						foreach ($monev_user as $key => $value) {
							$id_rensos_user[] = $value->id_rensos;
						}
						?>
						<select class="form-control select2" id="id_rensos" name="id_rensos" required>
							<option selected disabled>Pilih salah satu</option>
							@foreach(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('kode_upt',Auth::user()->upt)->whereNotIn('id',$id_rensos_user)->get() as $value)
							<option value="{{$value->id}}">{{$value->tema}} | {{$value->tanggal_pelaksanaan}}</option>
							@endforeach
						</select>
						@endif
						<a href="{{route('sosialisasi-bimtek/rencana-sosialisasi')}}">➤ Daftar Rencana Sosial Disini</a>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal_pelaksanaan">Tanggal Pelaksanaan</label>
						<label style="color: red;">*</label>						
						<input type="date" class="form-control" id="tanggal_pelaksanaan" name="tanggal_pelaksanaan" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="tema">Tema </label>
						<label style="color: red;">*</label>						
						<input type="text" class="form-control" id="tema" name="tema" placeholder="Masukan Tema" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="tempat">Tempat </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Masukan Tempat" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="jumlah_peserta">Jumlah Peserta </label>
						<label style="color: red;">*</label>						
						<input type="number" class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="Masukan Jumlah" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="anggaran">Anggaran</label>
						<label style="color: red;">*</label>					
						<input type="number" class="form-control" id="anggaran" name="anggaran" placeholder="Masukan Anggaran" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran</label>
						<label style="color: red;">*</label>					
						<input type="file" class="form-control" id="lampiran" name="lampiran">
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran-sebelumnya">Lampiran Sebelumnya</label>
						<input type="text" class="form-control" id="lampiran-sebelumnya" name="lampiran_sebelumnya" readonly>
					</div>
					<div class="col-md-12  form-group">
						<label for="narasumber">Narasumber</label>
						<label style="color: red;">*</label>						
						<input type="text" class="form-control" id="narasumber" name="narasumber" placeholder="Masukan Narasumber" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="keterangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>