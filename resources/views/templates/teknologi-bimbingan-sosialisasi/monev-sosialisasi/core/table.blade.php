<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">TANGGAL DIBUAT</th>
			<th scope="col">TAHUN</th>
			<th scope="col">BULAN</th>
			<th scope="col">TANGGAL PELAKSANAAN</th>
			<th scope="col">NAMA UPT</th>
			<th scope="col">JENIS KEGIATAN</th>
			<th scope="col">TEMPAT</th>
			<th scope="col">TEMA</th>
			<th scope="col">JUMLAH PESERTA</th>
			<th scope="col">KATEGORI PESERTA</th>
			<th scope="col">NARASUMBER</th>
			<th scope="col">ANGGARAN</th>
			<th scope="col">KETERANGAN</th>
			<th scope="col">LAMPIRAN</th>
			<th scope="col">TINDAKAN</th>
		</tr>
	</thead>
	<tbody>
		@foreach($monsos as $value)
		@if($value->status == 1)
		<tr>
			@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('created_at') != $value->created_at)
			<td style="color: red;">
				{{\Carbon\Carbon::parse($value->created_at)
					->format('d, M Y')}}
				</td>
				@else
				<td>
					{{\Carbon\Carbon::parse($value->created_at)
						->format('d, M Y')}}
					</td>
					@endif

					@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('tanggal_pelaksanaan') != $value->tanggal_pelaksanaan)
					<td style="color: red;">
						{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
							->format('Y')}}
						</td>
						<td style="color: red;">{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
							->format('M')}}</td>
							<td style="color: red;">{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
								->format('d, M Y')}}</td>
								@else
								<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
									->format('Y')}}</td>
									<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
										->format('M')}}</td>
										<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
											->format('d, M Y')}}</td>
											@endif


											<?php $id = Crypt::encryptString($value->id) ?>
											<?php  
											$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
											?>
											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('kode_upt') != $value->kode_upt)
											<td style="color: red;">{{$upts}}</td>
											@else
											<td>{{$upts}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('jenis_kegiatan') != $value->jenis_kegiatan)
											<td style="color: red;">{{\App\Model\Refrension\Kegiatan::where('id',\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('jenis_kegiatan'))->value('kegiatan')}}</td>
											@else
											<td>{{\App\Model\Refrension\Kegiatan::where('id',\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('jenis_kegiatan'))->value('kegiatan')}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('tempat') != $value->tempat)
											<td style="color: red;">{{$value->tempat}}</td>
											@else
											<td>{{$value->tempat}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('tema') != $value->tema)
											<td style="color: red;">{{$value->tema}}</td>
											@else
											<td>{{$value->tema}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('jumlah_peserta') != $value->jumlah_peserta)
											<td style="color: red;">{{$value->jumlah_peserta}}</td>
											@else
											<td>{{$value->jumlah_peserta}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('target_peserta') != $value->target_peserta)
											<td style="color: red;">{{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('target_peserta')}}</td>
											@else
											<td>{{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('target_peserta')}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('narasumber') != $value->narasumber)
											<td style="color: red;">{{$value->narasumber}}</td>
											@else
											<td>{{$value->narasumber}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('anggaran') != $value->anggaran)
											<td style="color: red;">Rp. {{number_format($value->anggaran, 2)}}</td>
											@else
											<td>Rp. {{number_format($value->anggaran, 2)}}</td>
											@endif

											@if(\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('keterangan') != $value->keterangan)
											<td style="color: red;">{{$value->keterangan}}</td>
											@else
											<td>{{$value->keterangan}}</td>
											@endif
											
											<td>
												@if($value->lampiran != null)
												<a href="{{route('lampiran-monevsosialisasi',$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>
												@endif
											</td>
											<td>
												<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"
												data-jenis-kegiatan="{{\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan')}}"
												data-tanggal-pelaksanaan="{{$value->tanggal_pelaksanaan}}"
												data-tema="{{$value->tema}}"
												data-tempat="{{$value->tempat}}"
												data-kategori-peserta="{{$value->target_peserta}}"
												data-jumlah-peserta="{{$value->jumlah_peserta}}"
												data-anggaran="{{$value->anggaran}}"
												data-lampiran="{{$value->lampiran}}"
												data-narasumber="{{$value->narasumber}}"
												data-keterangan="{{$value->keterangan}}"
												data-status="{{$value->status}}"
												><i class="fa fa-file"></i></a>

												<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm"					
												data-id="{{$id}}"
												data-id-rensos="{{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('id',$value->id_rensos)->value('tema')}}"
												data-tanggal-pelaksanaan="{{$value->tanggal_pelaksanaan}}"
												data-tema="{{$value->tema}}"
												data-tempat="{{$value->tempat}}"
												data-kategori-peserta="{{$value->target_peserta}}"
												data-jumlah-peserta="{{$value->jumlah_peserta}}"
												data-anggaran="{{$value->anggaran}}"
												data-lampiran="{{$value->lampiran}}"
												data-narasumber="{{$value->narasumber}}"
												data-keterangan="{{$value->keterangan}}"
												data-status="{{$value->status}}"
												><i class="fa fa-edit"></i></a>

												<a  class="btn btn-danger btn-sm" 
												id="getDelete" 
												data-toggle="modal" 
												data-target="#deleteModal" 
												data-id="{{$id}}"><i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
									@endif
									@endforeach
								</tbody>
							</table>
