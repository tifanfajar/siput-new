<div class="modal fade" id="detailModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL MONEV SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" id="did">
					<div class="col-md-6 form-group">
						<label for="tanggal_pelaksanaan">Tanggal Pelaksanaan *</label>
						<input type="text" class="form-control" id="dtanggal_pelaksanaan" name="tanggal_pelaksanaan"  disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="tema">Tema *</label>
						<input type="text" class="form-control" id="dtema" name="tema"  disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="tempat">Tempat *</label>
						<input type="text" class="form-control" id="dtempat" name="tempat"  disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="jumlah_peserta">Jumlah Peserta *</label>
						<input type="text" class="form-control" id="djumlah_peserta" name="jumlah_peserta"  disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="anggaran">Anggaran *</label>
						<input type="text" class="form-control" id="danggaran" name="anggaran"  disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="text" class="form-control" id="dlampiran" name="lampiran" disabled>
					</div>
					<div class="col-md-12  form-group">
						<label for="narasumber">Narasumber *</label>
						<input type="text" class="form-control" id="dnarasumber" name="narasumber"  disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="dketerangan" name="keterangan" rows="3" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>