@extends('dev.core.using')
@section('content')

@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.add')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.detail')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.edit')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.helpers.filter')
@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>

<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.chart')
	@else
	@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.chart-no-admin')
	@endif
</div>

<div class="row mt-5" id="parent_table">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = DB::table('modules')->where('menu_path',$url)->value('kdModule');
		$getCreate = DB::table('role_acl')
		->where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
		@include('dev.helpers.button.btnFilterData')
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;RENCANA PELAKSANAAN SOSIALISASI DAN BIMTEK PERIZINAN SFR & SOR</h3>
					</div>
				</div>
			</div>
			<div class="">
				<!-- Projects table -->
				<div class="">
					@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.table-rencana')
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-12 mb-5 mb-xl-0">
		<br>
		<br>
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;EVALUASI CAPAIAN PELAKSANAAN SOSIALISASI DAN BIMTEK PERIZINAN SFR DAN SOR</h3>
					</div>
				</div>
			</div>
			<div class="">
				<!-- Projects table -->
				<div class="">
					@include('templates.teknologi-bimbingan-sosialisasi.monev-sosialisasi.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.teknologi-bimbingan-sosialisasi.monev-sosialisasi.js')

@endsection