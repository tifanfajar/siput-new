<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Rencana Sosialisasi</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
				<thead class="thead-light" style="font-size: 10pt;">
					<tr>
						<th scope="col">TANGGAL DIBUAT</th>
						<th scope="col">TAHUN</th>
						<th scope="col">BULAN</th>
						<th scope="col">TANGGAL PELAKSANAAN</th>
						<th scope="col">NAMA UPT</th>
						<th scope="col">JENIS KEGIATAN</th>
						<th scope="col">TEMPAT</th>
						<th scope="col">TEMA</th>
						<th scope="col">JUMLAH PESERTA</th>
						<th scope="col">KATEGORI PESERTA</th>
						<th scope="col">NARASUMBER</th>
						<th scope="col">ANGGARAN</th>
						<th scope="col">KETERANGAN</th>
						<th scope="col">LAMPIRAN</th>
					</tr>
				</thead>
				<tbody style="font-size: 8pt;">
				@foreach($rensos as $value)
				@if($value->status == 1)
				<tr>
					<td>{{\Carbon\Carbon::parse($value->created_at)
					->format('d, M Y')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
						->format('Y')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
						->format('M')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
						->format('d, M Y')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
					<td>{{$upts}}</td>
					<td>{{\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan')}}</td>
					<td>{{$value->tempat}}</td>
					<td>{{$value->tema}}</td>
					<td>{{$value->jumlah_peserta}}</td>
					<td>{{$value->target_peserta}}</td>
					<td>{{$value->narasumber}}</td>
					<td>{{$value->anggaran}}</td>
					<td>{{$value->keterangan}}</td>
					<td>{{$value->lampiran}}</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>

			<hr>
		</div>
	</center>
</body>
</html>