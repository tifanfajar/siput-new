@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#dnamaupt').val(nama_upt);

			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			var idjenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan-id');
			$('#djeniskegiatan').val(jenis_kegiatan);			

			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#dtempat').val(tempat);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#dtema').val(tema);

			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#djumlahpeserta').val(jumlah_peserta);

			var lampiranSebelumnya=$(e.relatedTarget).attr('data-lampiran-sebelumnya');
			$('#dlampiranSebelumnya').val(lampiranSebelumnya);

			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#dkategoripeserta').val(kategori_peserta);

			var tanggalPelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#dtanggalPelaksanaan').val(tanggalPelaksanaan);

			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#dnarasumber').val(narasumber);

			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#danggaran').val(anggaran);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#dactive').val('Open');
			}else{
				$('#dactive').val('Close');
			}
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailApprovalModal').on('show.bs.modal', function (e) {
			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#danamaupt').val(nama_upt);

			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			var idjenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan-id');
			$('#dajeniskegiatan').val(jenis_kegiatan);			

			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#datempat').val(tempat);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#datema').val(tema);

			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#dajumlahpeserta').val(jumlah_peserta);

			var lampiranSebelumnya=$(e.relatedTarget).attr('data-lampiran-sebelumnya');
			$('#dalampiranSebelumnya').val(lampiranSebelumnya);

			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#dakategoripeserta').val(kategori_peserta);

			var tanggalPelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#datanggalPelaksanaan').val(tanggalPelaksanaan);

			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#danarasumber').val(narasumber);

			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#daanggaran').val(anggaran);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#daactive').val('Open');
			}else{
				$('#daactive').val('Close');
			}
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailRejectModal').on('show.bs.modal', function (e) {
			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#drnamaupt').val(nama_upt);

			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			var idjenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan-id');
			$('#drjeniskegiatan').val(jenis_kegiatan);			

			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#drtempat').val(tempat);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#drtema').val(tema);

			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#drjumlahpeserta').val(jumlah_peserta);

			var lampiranSebelumnya=$(e.relatedTarget).attr('data-lampiran-sebelumnya');
			$('#drlampiranSebelumnya').val(lampiranSebelumnya);

			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#drkategoripeserta').val(kategori_peserta);

			var tanggalPelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#drtanggalPelaksanaan').val(tanggalPelaksanaan);

			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#drnarasumber').val(narasumber);

			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#dranggaran').val(anggaran);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#dractive').val('Open');
			}else{
				$('#dractive').val('Close');
			}
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#drketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-upt');
			$('#eid').val(id);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#enamaupt').val(nama_upt);

			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			var idjenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan-id');
			$('#ejeniskegiatan').append('<option value="'+ idjenis_kegiatan +'" selected>'+ jenis_kegiatan +'</option>');

			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#etempat').val(tempat);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#etema').val(tema);

			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#ejumlahpeserta').val(jumlah_peserta);

			var lampiranSebelumnya=$(e.relatedTarget).attr('data-lampiran-sebelumnya');
			$('#elampiranSebelumnya').val(lampiranSebelumnya);

			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#ekategoripeserta').val(kategori_peserta);

			var tanggalPelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#etanggalPelaksanaan').val(tanggalPelaksanaan);

			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#enarasumber').val(narasumber);

			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#eanggaran').val(anggaran);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#eketerangan').val(keterangan);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 0){
				$('#eactive').append(
					'<option value="'+ active +'" selected> Close </option><option value="1" selected> Open </option>'
					);
			}else{
				$('#eactive').append(
					'<option value="'+ active +'" selected> Open </option><option value="0"> Close </option>'
					);
			}
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#editReject').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-upt');
			$('#erid').val(id);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#ernamaupt').val(nama_upt);

			var jenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan');
			var idjenis_kegiatan=$(e.relatedTarget).attr('data-jenis-kegiatan-id');
			$('#erjeniskegiatan').append('<option value="'+ idjenis_kegiatan +'" selected>'+ jenis_kegiatan +'</option>');

			var tempat=$(e.relatedTarget).attr('data-tempat');
			$('#ertempat').val(tempat);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#ertema').val(tema);

			var jumlah_peserta=$(e.relatedTarget).attr('data-jumlah-peserta');
			$('#erjumlahpeserta').val(jumlah_peserta);

			var lampiranSebelumnya=$(e.relatedTarget).attr('data-lampiran-sebelumnya');
			$('#erlampiranSebelumnya').val(lampiranSebelumnya);

			var kategori_peserta=$(e.relatedTarget).attr('data-kategori-peserta');
			$('#erkategoripeserta').val(kategori_peserta);

			var tanggalPelaksanaan=$(e.relatedTarget).attr('data-tanggal-pelaksanaan');
			$('#ertanggalPelaksanaan').val(tanggalPelaksanaan);

			var narasumber=$(e.relatedTarget).attr('data-narasumber');
			$('#ernarasumber').val(narasumber);

			var anggaran=$(e.relatedTarget).attr('data-anggaran');
			$('#eranggaran').val(anggaran);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erketerangan').val(keterangan);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 0){
				$('#eractive').append(
					'<option value="'+ active +'" selected> Close </option><option value="1" selected> Open </option>'
					);
			}else{
				$('#eractive').append(
					'<option value="'+ active +'" selected> Open </option><option value="0" selected> Close </option>'
					);
			}
		});
	})
</script>