@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('rensos-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('sosialisasi-bimtek/rencana-sosialisasi')}}">    
		@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')			
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="id_prov" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>
		@endif
		<label for="id_materi">Jenis Kegiatan *</label>
		<select class="form-control" name="jenis_kegiatan" style="margin-bottom: 20px; border: 2; border-radius: 6px;" required>
			<option selected disabled>Pilih salah satu</option>
			@foreach(\App\Model\Refrension\Kegiatan::all() as $value)
			<option value="{{$value->id}}">{{$value->kegiatan}}</option>
			@endforeach
		</select>
			
	<table class="table">

	  <thead>
	    <tr>
	      <th scope="col">Tempat *</th>
	      <th scope="col">Tanggal Pelaksanaan *</th>
	      <th scope="col">Tema *</th>
	      <th scope="col">Narasumber *</th>
	      <th scope="col">Kategori Peserta *</th>
	      <th scope="col">Jumlah Peserta *</th>
	      <th scope="col">Anggaran *</th>
	      <th scope="col">Keterangan *</th>
	      <th scope="col">Lampiran *</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $rensos => $rensoss)
	    <tr>
	      <td><input type="text" class="form-control" id="tempat" name="tempat[]" placeholder="Masukan Tempat" value="{{$rensoss[0]}}" data-validation="message" required></td>
	      <td><input type="date" class="form-control" id="tanggal_pelaksanaan" name="tanggal_pelaksanaan[]" value="{{$rensoss[1]}}" data-validation="date" required></td>
	      <td><input type="text" class="form-control" id="tema" name="tema[]" value="{{$rensoss[2]}}" placeholder="Masukan Tema" data-validation="message"  required></td>
	      <td><input type="text" class="form-control" id="narasumber" name="narasumber[]" value="{{$rensoss[3]}}" placeholder="Masukan Narasumber" data-validation="name" required></td>
	      <td><input type="text" class="form-control" id="target_peserta" name="target_peserta[]" value="{{$rensoss[4]}}" placeholder="Masukan Target Peserta" data-validation="number" data-validation="message" required></td>
	      <td><input type="text" class="form-control" id="jumlah_peserta" name="jumlah_peserta[]" value="{{$rensoss[5]}}" placeholder="Masukan Jumlah Peserta" data-validation="number" required></td>
	      <td><input type="text" class="form-control" id="anggaran" name="anggaran[]" value="{{$rensoss[6]}}" placeholder="Masukan Anggatan" data-validation="number" data-validation="message" required></td>
	      <td><input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$rensoss[7]}}" placeholder="Masukan Keterangan" data-validation="message" required></td>
	      <td><input type="file" id="lampiran" name="lampiran[]" required>
	      </td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
	});
</script>

@endsection

