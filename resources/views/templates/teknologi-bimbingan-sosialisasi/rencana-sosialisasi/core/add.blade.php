<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="modal fade" id="addRencanaSosialisasi" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH RENCANA SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('rensos-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-12 form-group">
						<label for="kegiatan">Jenis Kegiatan </label><label style="color: red;">*</label>
						<select name="jenis_kegiatan" class="form-control select2" required>
							<option selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\Refrension\Kegiatan::all() as $value)
							<option value="{{$value->id}}">{{$value->kegiatan}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal_pelaksanaan">Tanggal Pelaksanaan </label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="tanggal_pelaksanaan" name="tanggal_pelaksanaan" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="tema">Tema </label><label style="color: red;">*</label>						
						<input type="text" class="form-control" id="tema" name="tema" placeholder="Masukan Tema" required>
					</div>
					<div class="col-md-6  form-group" >
						<label for="tempat">Tempat </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Masukan Tempat" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="target_peserta">Kategori Peserta </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="target_peserta" name="target_peserta" placeholder="Masukan Kategori" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="jumlah_peserta">Jumlah Peserta </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="Masukan Jumlah" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="anggaran">Anggaran </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="anggaran" name="anggaran" placeholder="Masukan Anggaran" required>
					</div>
					<div class="col-md-12  form-group">
						<label for="lampiran">Lampiran </label><label style="color: red;">*</label>
						<input type="file" class="form-control" id="lampiran" name="lampiran">
					</div>
					<div class="col-md-12  form-group">
						<label for="narasumber">Narasumber </label><label style="color: red;">*</label>						<input type="text" class="form-control" id="narasumber" name="narasumber" placeholder="Masukan Narasumber" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="keterangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<select class="form-control" id="upt_provinsi" name="upt_provinsi">
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<select name="id_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

