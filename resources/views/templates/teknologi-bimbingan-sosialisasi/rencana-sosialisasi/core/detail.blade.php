<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL RENCANA SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="enamaupt">Nama UPT</label>
						<input type="text" class="form-control" id="dnamaupt" name="namaupt" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="ejenis_kegiatan">Jenis Kegiatan</label>
						<input type="text" class="form-control" id="djeniskegiatan" name="tempat" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="etempat">Tempat</label>
						<input type="text" class="form-control" id="dtempat" name="tempat" placeholder="Masukan Tempat" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="ertanggalPelaksanaan">Tanggal Pelaksanaan</label>
						<input type="date" class="form-control" id="dtanggalPelaksanaan" name="tanggal_pelaksanaan" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="etema">Tema</label>
						<input type="text" class="form-control" id="dtema" name="tema" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="ejumlah_peserta">Jumlah Peserta</label>
						<input type="text" class="form-control" id="djumlahpeserta" name="jumlah_peserta" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="ekategori_peserta">Kategori Peserta</label>
						<input type="text" class="form-control" id="dkategoripeserta" name="target_peserta" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label>Lampiran</label>
						<input type="text" class="form-control"  disabled="" id="dlampiranSebelumnya" name="lampiran_sebelumnya" placeholder="Kosong" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="enarasumber">Narasumer</label>
						<input type="text" class="form-control" id="dnarasumber" name="narasumber" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="eanggaran">Anggaran</label>
						<input type="number" class="form-control" id="danggaran" name="anggaran" placeholder="Admin" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="dketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>