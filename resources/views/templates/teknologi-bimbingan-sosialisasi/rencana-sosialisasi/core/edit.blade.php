<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH RENCANA SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('rensos-update')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="enamaupt">Nama UPT *</label>
						<input type="text" class="form-control" id="enamaupt" name="namaupt" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="ejenis_kegiatan">Jenis Kegiatan *</label>
						<select class="form-control" id="ejeniskegiatan" name="jenis_kegiatan" required>
							@foreach(\App\Model\Refrension\Kegiatan::all() as $value)
							<option value="{{$value->id}}">{{$value->kegiatan}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6  form-group">
						<label for="etempat">Tempat *</label>
						<input type="text" class="form-control" id="etempat" name="tempat" placeholder="Masukan Tempat">
					</div>
					<div class="col-md-6  form-group">
						<label for="etanggalPelaksanaan">Tanggal Pelaksanaan *</label>
						<input type="date" class="form-control" id="etanggalPelaksanaan" name="tanggal_pelaksanaan" placeholder="Masukan Tempat">
					</div>
					<div class="col-md-12  form-group">
						<label for="etema">Tema *</label>
						<input type="text" class="form-control" id="etema" name="tema" placeholder="Admin">
					</div>
					<div class="col-md-6  form-group">
						<label for="ejumlah_peserta">Jumlah Peserta *</label>
						<input type="text" class="form-control" id="ejumlahpeserta" name="jumlah_peserta" placeholder="Admin">
					</div>
					<div class="col-md-6  form-group">
						<label for="ekategori_peserta">Kategori Peserta *</label>
						<input type="text" class="form-control" id="ekategoripeserta" name="target_peserta" placeholder="Admin">
					</div>
					<div class="col-md-12 form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="file" class="form-control"  name="lampiran" placeholder="Admin">
					</div>
					<div class="col-md-6 form-group">
						<label>Lampiran sebelumnya</label>
						<input type="text" class="form-control"  disabled="" id="elampiranSebelumnya" name="lampiran_sebelumnya" placeholder="Kosong">
					</div>
					<div class="col-md-6  form-group">
						<label for="enarasumber">Narasumer *</label>
						<input type="text" class="form-control" id="enarasumber" name="narasumber" placeholder="Admin">
					</div>
					<div class="col-md-12  form-group">
						<label for="eanggaran">Anggaran *</label>
						<input type="number" class="form-control" id="eanggaran" name="anggaran" placeholder="Admin">
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="eketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					<input type="hidden" class="form-control" id="eid" name="id" placeholder="Admin">
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
						@include('dev.helpers.button.btnSimpan')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>