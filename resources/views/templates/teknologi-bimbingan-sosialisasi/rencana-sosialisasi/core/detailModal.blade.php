<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL BAHAN SOSIALISASI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('sosbim-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="dnamaupt">Nama UPT *</label>
						<input type="text" class="form-control" id="dnamaupt" name="dnamaupt" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="djenis_kegiatan">Jenis Kegiatan *</label>
						<input type="text" class="form-control" id="djenis_kegiatan" name="djenis_kegiatan" placeholder="Masukan Judul" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="dtempat">Tempat *</label>
						<input type="text" class="form-control" id="dtempat" name="dtempat" placeholder="Masukan Tempat" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="dtema">Tema *</label>
						<input type="text" class="form-control" id="dtema" name="dtema" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="djumlah_peserta">Jumlah Peserta *</label>
						<input type="text" class="form-control" id="djumlah_peserta" name="djumlah_peserta" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="dkategori_peserta">Kategori Peserta *</label>
						<input type="text" class="form-control" id="dkategori_peserta" name="dkategori_peserta" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="dnarasumber">Narasumer *</label>
						<input type="text" class="form-control" id="dnarasumber" name="dnarasumber" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="danggaran">Anggaran *</label>
						<input type="number" class="form-control" id="danggaran" name="danggaran" placeholder="Admin" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="dketerangan" name="dketerangan" rows="3" placeholder="Masukan Keterangan" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>