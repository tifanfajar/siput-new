<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">TANGGAL DIBUAT</th>
			<th scope="col">TAHUN</th>
			<th scope="col">BULAN</th>
			<th scope="col">TANGGAL PELAKSANAAN</th>
			<th scope="col">NAMA UPT</th>
			<th scope="col">JENIS KEGIATAN</th>
			<th scope="col">TEMPAT</th>
			<th scope="col">TEMA</th>
			<th scope="col">JUMLAH PESERTA</th>
			<th scope="col">KATEGORI PESERTA</th>
			<th scope="col">NARASUMBER</th>
			<th scope="col">ANGGARAN</th>
			<th scope="col">KETERANGAN</th>
			<th scope="col">LAMPIRAN</th>
			<th scope="col">TINDAKAN</th>
		</tr>
	</thead>
	<tbody>
		@foreach($rensos as $value)
		@if($value->status == 1)
		<tr>
			<td>{{\Carbon\Carbon::parse($value->created_at)
				->format('d, M Y')}}</td>
				<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
					->format('Y')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
						->format('M')}}</td>
						<td>{{\Carbon\Carbon::parse($value->tanggal_pelaksanaan)
							->format('d, M Y')}}</td>
							<?php $id = Crypt::encryptString($value->id) ?>
							<?php  
							$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
							?>
							<td>{{$upts}}</td>
							<td>{{!empty($value->jenis_kegiatan)?\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan'):'-'}}</td>
							<td>{{$value->tempat}}</td>
							<td>{{$value->tema}}</td>
							<td>{{$value->jumlah_peserta}}</td>
							<td>{{$value->target_peserta}}</td>
							<td>{{$value->narasumber}}</td>
							<td>{{$value->anggaran}}</td>
							<td>{{$value->keterangan}}</td>
							<td>
								@if($value->lampiran != null)
								<a href="{{route('rensos-lampiran',$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>
								@endif
							</td>
							<td>
								<!-- <a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"><i class="fa fa-file"></i></a> -->
								<a href="#" data-toggle="modal" 
								data-target="#detailModal"
								data-id-upt="{{$id}}"
								data-nama-upt="{{$upts}}"
								data-jenis-kegiatan="{{!empty($value->jenis_kegiatan)?\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan'):'-'}}"
								data-jenis-kegiatan-id="{{$value->jenis_kegiatan}}"
								data-tempat="{{$value->tempat}}"
								data-tema="{{$value->tema}}"
								data-jumlah-peserta="{{$value->jumlah_peserta}}"
								data-lampiran-sebelumnya="{{$value->lampiran}}"
								data-kategori-peserta="{{$value->target_peserta}}"
								data-narasumber="{{$value->narasumber}}"
								data-anggaran="{{$value->anggaran}}"
								data-keterangan="{{$value->keterangan}}"
								data-tanggal-pelaksanaan="{{$value->tanggal_pelaksanaan}}" 
								data-active = "{{$value->active}}"
								class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>

								<a href="#" data-toggle="modal" 
								data-target="#editModal"
								data-id-upt="{{$id}}"
								data-nama-upt="{{$upts}}"
								data-jenis-kegiatan="{{!empty($value->jenis_kegiatan)?\App\Model\Refrension\Kegiatan::where('id',$value->jenis_kegiatan)->value('kegiatan'):'-'}}"
								data-jenis-kegiatan-id="{{$value->jenis_kegiatan}}"
								data-tempat="{{$value->tempat}}"
								data-tema="{{$value->tema}}"
								data-jumlah-peserta="{{$value->jumlah_peserta}}"
								data-lampiran-sebelumnya="{{$value->lampiran}}"
								data-kategori-peserta="{{$value->target_peserta}}"
								data-narasumber="{{$value->narasumber}}"
								data-anggaran="{{$value->anggaran}}"
								data-keterangan="{{$value->keterangan}}"
								data-tanggal-pelaksanaan="{{$value->tanggal_pelaksanaan}}" 
								data-active = "{{$value->active}}"
								class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>

								<a  class="btn btn-danger btn-sm" 
								id="getDelete" 
								data-toggle="modal" 
								data-target="#deleteModal" 
								data-id="{{$id}}"><i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
