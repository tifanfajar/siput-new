<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.ST.core.detail')
@include('templates.SPP.ST.core.edit')
@include('templates.SPP.ST.import.import')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.SPP.ST.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
</style>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{$getUptName}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@include('templates.helpers.SPP.ST.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.SPP.ST.chart')
	@else
	@include('templates.helpers.SPP.ST.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col-md-12 mb-4 mb-xl-0" style="margin-top: 10px;">
		<div class="">
			@include('templates.SPP.ST.core.table')
		</div>
	</div>

	
</div>
@include('templates.helpers.SPP.ST.js')
@endsection