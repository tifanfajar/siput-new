@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
  	<form action="{{route('st-import-post')}}" method="POST" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('spp/st')}}">
	@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
		<label for="provinsi">Provinsi</label>
		<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
			<option value="" selected disabled>Provinsi</option>
			@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
			<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
			@endforeach
		</select>
		
		<label for="city">UPT</label>
		<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control" required>
			<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
		</select>			
	@endif
	<span class="text-danger my-3">*Data yang diinput harus sesuai dengan data yang ada di sims,dan format harap disesuaikan dengan sampel</span>
	<table class="table table-responsive">

	  <thead>
	    <tr>
	        <th rowspan="2" scope="col">No.Tagihan</th>
	        <th rowspan="2" scope="col">No.Klien</th>
	        <th rowspan="2" scope="col">Nama Klien</th>
	        <th rowspan="2" scope="col">BHP (Rp)</th>
	        <th rowspan="2" scope="col">Tanggal ( BI CREATE DATE )</th>
	        <th rowspan="2" scope="col">Status Pembayaran</th>
	        <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
	    </tr>
	    <tr>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Jenis ST</td>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $rt => $rts)
		<?php 
          $tanggal_jatuh_tempo = date("Y-m-d", strtotime($rts[7] . "- 1days"));
          $jenisST = \App\Model\SPP\StatusTagihan::where('no_spp', $rts[0])->value('bi_type');
      	?>
	    <tr>
	      <td>
	      	{{$rts[0]}}
	      </td>
	      <td>
	      	{{$rts[1]}}
	      </td>
	      <td>
	      	{{$rts[2]}}
	      </td>
	      <td>
	      	Rp. {{number_format($rts[3], 2)}}
	      </td>
	      <td>
			{{\Carbon\Carbon::parse($rts[4])->format('d, M Y')}}
	      </td>
	      <td>
	      	{{$rts[5]}}
	      </td>
	      <td>
      		<select class="form-control metode" style="border: 2; border-radius: 6px;" id="upayametode" name="upayametode[]" required>
				<option value="" selected>Pilih Data</option>
      			@if(!$rts[8])
					@foreach(\App\Model\Refrension\Metode::all() as $kabupaten)
					<option value="{{$kabupaten->id}}">{{$kabupaten->metode}}</option>
					@endforeach
				@else
					@foreach(\App\Model\Refrension\Metode::all() as $metode)
						<option value="{{$metode->id}}" {{ $metode->metode == strtoupper($rts[8]) ? 'selected' : " "}}>{{$metode->metode}}</option>
					@endforeach
				@endif
			</select>
	      </td>
	      <?php
	      	if ($rts[9] != '-') {
		      	if(is_int($rts)){
			      	$format_tanggal_upaya = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rts[9]));
			      	$tanggal_upaya = $format_tanggal_upaya->format('Y-m-d');
		      	}else{
			      	$tanggal_upaya = \Carbon\Carbon::parse($rts[9])->format('Y-m-d');
		      	}
		      }else{
		      	$tanggal_upaya = 0;
		      }
	      ?>
	      <td>
	      	<input type="date" class="form-control" id="tanggal_upaya" name="tanggal_upaya[]" value="{{$tanggal_upaya}}" placeholder="Format (yyyy-mm-dd)" required>
	      </td>
	      <td>
	      	<input type="file" name="bukti_dukung[]">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$rts[10]}}" placeholder="Masukan Keterangan">
	      </td>
	      <td>
	          @if($jenisST == 1)<a class="btn btn-success btn-sm text-white">ST 1</a> 
	          @elseif($jenisST == 9)<a class="btn btn-success btn-sm text-white">ST 1 Paid</a> 
	          @elseif($jenisST == 2)<a class="btn btn-info btn-sm text-white">ST 2</a> 
	          @elseif($jenisST == 10) <a class="btn btn-info btn-sm text-white">ST 2 Paid</a> 
	          @elseif($jenisST == 3)<a class="btn btn-warning btn-sm text-white">ST 3</a> 
	          @elseif($jenisST == 11)<a class="btn btn-warning btn-sm text-white">ST 3 Paid</a> 
	          @elseif($jenisST == 16)<a class="btn btn-danger btn-sm text-white">STT</a> 
	          @elseif($jenisST == 49)<a class="btn btn-danger btn-sm text-white">STT Paid</a> 
	          @endif
	      </td>
	    </tr>
	    <input type="hidden" name="kode_upt[]" value="{{Auth::user()->upt}}">
		<input type="hidden" name="id_prov[]" value="{{Auth::user()->province_code}}">
	  	<input type="hidden" name="no_spp[]" value="{{$rts[0]}}">
		<input type="hidden" name="no_klien[]" value="{{$rts[1]}}">
		<input type="hidden" name="nama_klien[]" value="{{$rts[2]}}">
		<input type="hidden" name="bhp[]" value="{{$rts[3]}}">
		<input type="hidden" name="bi_begin[]" value="{{$rts[4]}}">
		<input type="hidden" name="status_pembayaran[]" value="{{$rts[5]}}">
		<input type="hidden" name="tanggal_jatuh_tempo[]" value="{{$tanggal_jatuh_tempo}}">
		<input type="hidden" name="status[]" value="0">
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')

@endsection
