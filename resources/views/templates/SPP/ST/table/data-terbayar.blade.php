<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPrint" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">DATA @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif TERBAYAR (H-60 s.d. H-30)</h3>
        </div>
      </div>
    </div>

    <div class="">
      <div class="">
        <table id="server-side" class="table table-dark table-striped align-items-center">
          <thead class="thead-light">
            <tr>
              <th rowspan="2" scope="col">No.Tagihan</th>
              <th rowspan="2" scope="col">No. Klien</th>
              <th rowspan="2" scope="col">Nama Klien</th>
              <th rowspan="2" scope="col">Jenis ST</th>
              <th rowspan="2" scope="col">UPT</th>
              <th rowspan="2" scope="col">BHP (Rp)</th>
              <th rowspan="2" scope="col">Tanggal ( BI CREATE DATE )</th>
              <th rowspan="2" scope="col">Status Pembayaran</th>
              <th rowspan="2" scope="col">Tgl Jatuh Tempo</th>
              <th rowspan="2" scope="col">Tanggal Bayar</th>
              <th rowspan="2" scope="col">Service</th>
            </tr>
            <tr>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Ya / Belum / Terbayarkan</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
