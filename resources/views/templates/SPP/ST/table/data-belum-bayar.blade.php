<br>
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPrint" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="not paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="not paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="not paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
  @if($freezeTime >= \App\Model\SPP\FreezeDate::where('id',1)->value('date_start') && $freezeTime <= \App\Model\SPP\FreezeDate::where('id',1)->value('date_end') && $freezeMonth == $month)
  @else
  @if($getStatus == 'operator')
  <a class="btn btn-sm btn-success" style="color: white;" id="getImport" data-toggle="modal" data-target="#importModal" ><i class="ni ni-cloud-upload-96"></i>&nbsp;IMPORT</a>
  @endif
  @endif
</div>
<form action="{{route('spp-st-postORupdate')}}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="col-xl-12 mt-5">
    <div class="card shadow">
      <div class="card-header border-0" style="background-color: #5f5f5f;">
        <div class="row align-items-center">
          <div class="col">
            <h3 class="mb-0" style="color: #fff;"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;DATA @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif BELUM TERBAYAR (H-30 s.d. H)</h3>
          </div>
        </div>
      </div>
      <div class="table-responsive">
        <!-- asdas -->
        <div class="">
          <table id="example" class="table table-dark table-striped align-items-center table-flush data-table" >
            <thead class="thead-light">
              <tr>
                <th rowspan="2" scope="col">No.Tagihan</th>
                <th rowspan="2" scope="col">No.Klien</th>
                <th rowspan="2" scope="col">Nama Klien</th>
                <th rowspan="2" scope="col">Jenis ST</th>
                <th rowspan="2" scope="col">UPT</th>
                <th rowspan="2" scope="col">BHP (Rp)</th>
                <th rowspan="2" scope="col">Tanggal ( BI CREATE DATE )</th>
                <th rowspan="2" scope="col">Status Pembayaran</th>
                <th rowspan="2" scope="col">Tanggal Jatuh Tempo</th>
                <th rowspan="2" scope="col">Tanggal Upload</th>  
                <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
                <th rowspan="2" scope="col">Tindakan</th>
              </tr>
              <tr>
                <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Ya / Belum</td>
                <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
                <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
                <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
                <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
              </tr>
            </thead>
            <tbody>
              @foreach($rts_nopaid as $rt => $value)
              <tr>
                <th scope="row">{{$value->no_spp}}</th>
                <td>{{$value->no_klien}}</td>
                <td>{{$value->nama_perusahaan}}</td>
                <td>@if($value->bi_type == 1)<a class="btn btn-success btn-sm text-white">ST 1</a> @elseif($value->bi_type == 9)<a class="btn btn-success btn-sm text-white">ST 1 Paid</a> @elseif($value->bi_type == 2)<a class="btn btn-info btn-sm text-white">ST 2</a> @elseif($value->bi_type == 10) <a class="btn btn-info btn-sm text-white">ST 2 Paid</a> @elseif($value->bi_type == 3)<a class="btn btn-warning btn-sm text-white">ST 3</a> @elseif($value->bi_type == 11)<a class="btn btn-warning btn-sm text-white">ST 3 Paid</a> @elseif($value->bi_type == 16)<a class="btn btn-danger btn-sm text-white">STT</a> @elseif($value->bi_type == 49)<a class="btn btn-danger btn-sm text-white">STT Paid</a> @endif</td>
                <td>{{$value->upt}}</td>
                <td>Rp. {{number_format($value->tagihan, 2)}}</td>
                <td>{{\Carbon\Carbon::parse($value->bi_create_date)
                 ->format('d, M Y')}}</td>
                 <td><a class="btn btn-danger btn-sm text-white">Belum Bayar</a></td>
                 <td>{{\Carbon\Carbon::parse($value->bi_pay_until)
                   ->format('d, M Y')}}</td>
                   <td>
                    @if(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->value('updated_at'))
                    {{\Carbon\Carbon::parse(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->value('updated_at'))
                    ->format('d, M Y')}}
                    @else
                    -
                    @endif
                  </td>
                  <td>
                    @if(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->where('active',1)->first()) Sudah Disetujui @else Belum Disetujui @endif</td>

                    @if($getStatus == 'operator')
                    @if(!$value->bukti_dukung)
                    <input type="hidden" name="no_spp[]" value="{{$value->no_spp}}">
                    @endif
                    @if($freezeTime >= \App\Model\SPP\FreezeDate::where('id',1)->value('date_start') && $freezeTime <= \App\Model\SPP\FreezeDate::where('id',1)->value('date_end') && $freezeMonth == $month)
                    <td>
                      @if($value->upaya_metode)
                      <select name="upayametode[]" required disabled>
                        <option selected value="{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('id')}}">{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('metode')}}</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @else
                      <select name="upayametode[]"  required disabled>
                        <option value="" selected>Pilih salah satu</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @endif
                    </td>
                    <td>
                      @if($value->tanggal_upaya)
                      <input type="date" name="tanggal_upaya[]" value="{{$value->tanggal_upaya}}" required disabled>
                      @else 
                      <input type="date" name="tanggal_upaya[]" required disabled>
                      @endif
                    </td>
                    <td>
                      @if($value->bukti_dukung)
                      <a href="{{route('bukti-dukung-spp-st',$value->bukti_dukung)}}" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>
                      @else
                      <input type="file" name="bukti_dukung[]" required disabled>
                      @endif
                    </td>
                    <td>
                      @if($value->keterangan)
                      <input type="text" name="keterangan[]" value="{{$value->keterangan}}"  disabled>
                      @else
                      <input type="text" name="keterangan[]" required disabled>
                      @endif
                    </td>
                    <td>
                      @if(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->first())
                      <a href="" class="btn btn-success btn-sm"><i class="fa fa-file"></i></a>
                      @else
                      <label>-</label>
                      @endif
                    </td>
                    @else
                    <td>
                      @if($value->upaya_metode)
                      <select disabled>
                        <option selected value="{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('id')}}">{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('metode')}}</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @else
                      <select name="upayametode[]" required>
                        <option value="" selected>Pilih salah satu</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @endif
                    </td>
                    <td>
                      @if($value->tanggal_upaya)
                      <input type="date" id="input_tanggal_upaya_bb{{$rt+1}}" class="input_tanggal_upaya_bb" value="{{$value->tanggal_upaya}}" required disabled>
                      @else 
                      <input type="date" name="tanggal_upaya[]" id="input_tanggal_upaya_bb{{$rt+1}}" class="input_tanggal_upaya_bb">
                      @endif
                    </td>
                    <td>
                      @if($value->bukti_dukung)
                      <a href="{{route('bukti-dukung-spp-st',$value->bukti_dukung)}}" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>
                      @else
                      <input type="file" name="bukti_dukung[]" required>
                      @endif
                    </td>
                    <td>
                      @if($value->keterangan)
                      <input type="text" value="{{$value->keterangan}}"  required disabled>
                      @else
                      <input type="text" name="keterangan[]" required>
                      @endif
                    </td>
                    <td>
                      <?php $metode = $value->upaya_metode; ?>
                      <?php $id = Crypt::encryptString(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->value('id'))?>
                      @if($value->upaya_metode)
                      <a style="color: white;" href="#" data-target="#editModal" data-toggle="modal" class="btn btn-primary btn-sm"
                      data-id="{{$id}}"
                      data-no-tagihan="{{$value->no_spp}}"
                      data-no-client="{{$value->no_klien}}"
                      data-nama-client="{{$value->nama_perusahaan}}"
                      data-bi-begin="{{\Carbon\Carbon::parse($value->bi_begin)->format('d, M Y')}}"
                      data-bhp="{{$value->tagihan}}"
                      data-status-pembayaran="{{$value->status}}"
                      data-tanggal-jatuh-tempo="{{$value->bi_pay_until}}"
                      data-upaya-metode="{{\App\Model\Refrension\Metode::where('id', $metode)->value('metode')}}"
                      data-upaya-metode-id="{{$value->upaya_metode}}"
                      data-tanggal-upaya="{{$value->tanggal_upaya}}"
                      data-bukti-dukung="{{$value->bukti_dukung}}"
                      data-keterangan="{{$value->keterangan}}"
                      ><i class="fa fa-check-circle"></i>&nbsp;Ubah</a>

                      <a style="color: white;" href="#" data-target="#detailModal" data-toggle="modal" class="btn btn-success btn-sm"
                      data-id="{{$id}}"
                      data-no-tagihan="{{$value->no_spp}}"
                      data-no-client="{{$value->no_klien}}"
                      data-nama-client="{{$value->nama_perusahaan}}"
                      data-bi-begin="{{\Carbon\Carbon::parse($value->bi_begin)->format('d, M Y')}}"
                      data-bhp="{{$value->tagihan}}"
                      data-status-pembayaran="{{$value->status}}"
                      data-tanggal-jatuh-tempo="{{$value->bi_pay_until}}"
                      data-upaya-metode="{{\App\Model\Refrension\Metode::where('id', $metode)->value('metode')}}"
                      data-upaya-metode-id="{{$value->upaya_metode}}"
                      data-tanggal-upaya="{{$value->tanggal_upaya}}"
                      data-bukti-dukung="{{$value->bukti_dukung}}"
                      data-keterangan="{{$value->keterangan}}"
                      ><i class="fa fa-file"></i></a>

                      <!-- <a href="" class="btn btn-success btn-sm"><i class="fa fa-file"></i></a> -->
                      @else
                      <label>-</label>
                      @endif
                    </td>
                    @endif
                    @endif

                    @if($getStatus != 'operator')
                    <td>
                      @if($value->upaya_metode)
                      <select name="upayametode[]" required disabled>
                        <option selected value="{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('id')}}">{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('metode')}}</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @else
                      <select name="upayametode[]"  required disabled>
                        <option selected>Pilih salah satu</option>
                        @foreach(\App\Model\Refrension\Metode::all() as $metode)
                        <option value="{{$metode->id}}">{{$metode->metode}}</option>
                        @endforeach
                      </select>
                      @endif
                    </td>
                    <td>
                      @if($value->tanggal_upaya)
                      <input type="date" name="tanggal_upaya[]" value="{{$value->tanggal_upaya}}" required disabled>
                      @else 
                      <input type="date" name="tanggal_upaya[]" required disabled>
                      @endif
                    </td>
                    <td>
                      @if($value->bukti_dukung)
                      <a href="{{route('bukti-dukung-spp-st',$value->bukti_dukung)}}" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>
                      @else
                      <a href="#" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>
                      @endif
                    </td>
                    <td>
                      @if($value->keterangan)
                      <input type="text" name="keterangan[]" value="{{$value->keterangan}}"  disabled>
                      @else
                      <input type="text" name="keterangan[]" required disabled>
                      @endif
                    </td>
                    <td>
                      <?php $metode = $value->upaya_metode; ?>
                      <?php $id = Crypt::encryptString(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->value('id')) ?>
                      @if(\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->first())
                      <a style="color: white;" href="#" data-target="#detailModalAdmin" data-toggle="modal" class="btn btn-success btn-sm"
                      data-id="{{$id}}"
                      data-no-tagihan="{{$value->no_spp}}"
                      data-no-client="{{$value->no_klien}}"
                      data-nama-client="{{$value->nama_perusahaan}}"
                      data-bi-begin="{{\Carbon\Carbon::parse($value->bi_begin)->format('d, M Y')}}"
                      data-bhp="{{$value->tagihan}}"
                      data-status-pembayaran="{{$value->status}}"
                      data-tanggal-jatuh-tempo="{{$value->bi_pay_until}}"
                      data-upaya-metode="{{\App\Model\Refrension\Metode::where('id', $metode)->value('metode')}}"
                      data-upaya-metode-id="{{$value->upaya_metode}}"
                      data-tanggal-upaya="{{$value->tanggal_upaya}}"
                      data-bukti-dukung="{{$value->bukti_dukung}}"
                      data-keterangan="{{$value->keterangan}}"
                      ><i class="fa fa-file"></i></a>
                      @else
                      <label>-</label>
                      @endif
                    </td>
                    @endif
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>

        </div>

      </div>
      @if($getStatus == 'operator')
      <div class="form-group mt-5">
        <label for="exampleFormControlTextarea1" id="exampleFormControlLabel1">Catatan Petugas Pelaporan (Apabila ada) :</label>
        <textarea class="form-control" name="ket_operator" id="exampleFormControlTextarea1" rows="3"></textarea>
        <br>
        <button type="submit" id="btn-confirm-bb" class="btn btn-success float-right">Kirim ke atasan sebagai persetujuan</button>
      </div>
      @endif
    </form>
    <script>
      if($('.input_tanggal_upaya_bb').val() != '' || $('.input_tanggal_upaya_bb').val() != null){
        $('#btn-confirm-bb').hide();
        $('#exampleFormControlTextarea1').hide();
        $('#exampleFormControlLabel1').hide();
      }
      $( ".input_tanggal_upaya_bb" ).change(function() {
        if($(this).val() == '' || $(this).val() == null){
          $('#btn-confirm-bb').hide();
          $('#exampleFormControlTextarea1').hide();
          $('#exampleFormControlLabel1').hide();
        }else{
          $('#btn-confirm-bb').show();
          $('#exampleFormControlTextarea1').show();
          $('#exampleFormControlLabel1').show();
        }
      });
    </script>