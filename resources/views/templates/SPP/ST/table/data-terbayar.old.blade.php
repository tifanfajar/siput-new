<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPrint" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">DATA @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif TERBAYAR (H-60 s.d. H-30)</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <div class="widest">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
          <thead class="thead-light">
            <tr>
              <th rowspan="2" scope="col">No.Tagihan</th>
              <th rowspan="2" scope="col">No. Klien</th>
              <th rowspan="2" scope="col">Nama Klien</th>
              <th rowspan="2" scope="col">Jenis ST</th>
              <th rowspan="2" scope="col">UPT</th>
              <th rowspan="2" scope="col">BHP (Rp)</th>
              <th rowspan="2" scope="col">Tanggal ( BI CREATE DATE )</th>
              <th rowspan="2" scope="col">Status Pembayaran</th>
              <th rowspan="2" scope="col">Tgl Jatuh Tempo</th>
              <th rowspan="2" scope="col">Tanggal Bayar</th>
              <th rowspan="2" scope="col">Service</th>
            </tr>
            <tr>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Ya / Belum / Terbayarkan</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
            </tr>
          </thead>
          <tbody>
            @foreach($rts_paid as $rt => $value)
            <tr>
              <th scope="row">{{$value->no_spp}}</th>
              <td>{{$value->no_klien}}</td>
              <td>{{$value->nama_perusahaan}}</td>
              <td>
                @if($value->bi_type == 1)<a class="btn btn-success btn-sm text-white">ST 1</a> @elseif($value->bi_type == 9)<a class="btn btn-success btn-sm text-white">ST 1 Paid</a> @elseif($value->bi_type == 2)<a class="btn btn-info btn-sm text-white">ST 2</a> @elseif($value->bi_type == 10) <a class="btn btn-info btn-sm text-white">ST 2 Paid</a> @elseif($value->bi_type == 3)<a class="btn btn-warning btn-sm text-white">ST 3</a> @elseif($value->bi_type == 11)<a class="btn btn-warning btn-sm text-white">ST 3 Paid</a> @elseif($value->bi_type == 16)<a class="btn btn-danger btn-sm text-white">STT</a> @elseif($value->bi_type == 49)<a class="btn btn-danger btn-sm text-white">STT Paid</a> @endif
              </td>
              <td>{{$value->upt}}</td>
              <td>Rp. {{number_format($value->tagihan, 2)}}</td>
              <td>{{\Carbon\Carbon::parse($value->bi_create_date)
               ->format('d, M Y')}}</td>
               <td><center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center></td>
               <td>{{\Carbon\Carbon::parse($value->bi_pay_until)
                 ->format('d, M Y')}}</td>
                 <td>{{\Carbon\Carbon::parse($value->bi_money_received)
                   ->format('d, M Y')}}</td>
                   <td>{{$value->service_name}}</td>
                   <td>@if($value->active == 1) Sudah Disetujui @elseif($value->active == null) Terbayarkan @else Belum disetujui @endif</td>
                   <td>{{\App\Model\Refrension\Metode::where('id',\App\Model\SPP\StatusTagihan::where('no_spp',$value->no_spp)->value('upaya_metode'))->value('metode')}}</td>
                   <td>{{$value->tanggal_upaya}}</td>
                   <td>
                    @if($value->bukti_dukung)
                    <a href="{{url('spp/st/lampiran'.$value->bukti_dukung)}}" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>
                    @else
                    -
                    @endif
                  </td>
                  <td>{{$value->keterangan}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
