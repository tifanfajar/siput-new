@include('templates.SPP.ST.export.preview_search')
@include('templates.SPP.ST.export.export_search')
@include('templates.SPP.ST.export.print_search')
@include('templates.SPP.ST.dev.data')
<style>
  #example_filter {width: 990px !important;}
  .dataTables_length{width: 560px !important;}
</style>

@if($getStatus == 'administrator')
<?php 
$listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
?>
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="not_paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">REKAP PENGIRIMAN LAPORAN @if($jenis_st == 1) ST 1 @elseif($jenis_st == 9) ST 1 Paid @elseif($jenis_st == 2) ST 2 @elseif($jenis_st == 10) ST 2 Paid @elseif($jenis_st == 3) ST 3 @elseif($jenis_st == 11) ST 3 Paid @elseif($jenis_st == 16) STT @elseif($jenis_st == 49) STT Paid @endif</h3>
        </div>
      </div>
    </div>
    
    <div class="table-responsive">
      <div class="">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
              <th scope="col">{{$value->nama}}</th>
              @endforeach
            </tr>
          </thead>
          <tbody>
            @foreach($listUpt as $key => $value)
            <?php $uptName = $value->office_name ?>
            <tr>
              <td scope="row">
                {{$value->office_name}}
              </td>
              @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
              <?php
              if($value->id_bulan == 1){
                $fixMonth = 12;
                $fixYear = $year - 1;
              }else{
                $fixMonth = $value->id_bulan - 1;
                $fixYear = $year;
              }
              $checkingPengiriman = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$uptName)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
              ?>
              <td>
                @if($checkingPengiriman != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endif
<br>
@if($getStatus == 'administrator')
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="not_paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #ffff;">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG @if($jenis_st == 1) ST 1 @elseif($jenis_st == 9) ST 1 Paid @elseif($jenis_st == 2) ST 2 @elseif($jenis_st == 10) ST 2 Paid @elseif($jenis_st == 3) ST 3 @elseif($jenis_st == 11) ST 3 Paid @elseif($jenis_st == 16) STT @elseif($jenis_st == 49) STT Paid @endif
          </h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="1"></th>
            <th class="tg-0lax" colspan="12" style="background-color: #E9E214; color: #fff; text-align: center;">% ST TERBAYAR SEBELUM TNGGAL JATUH TEMPO</th>
          </tr>
          <tr>
            <td class="tg-0lax">UPT</td>
            @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
            <td class="tg-0lax">{{$value->nama}}</td>
            @endforeach
          </tr>
        </thead>
        @foreach($listUpt as $key => $value)
        <?php $uptName = $value->office_name ?>
        <tr>
          <td class="tg-0lax">{{$value->office_name}}</td>
          @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
          <?php
          if($value->id_bulan == 1){
            $fixMonth = 12;
            $fixYear = $year - 1;
          }else{
            $fixMonth = $value->id_bulan - 1;
            $fixYear = $year;
          }
          $paid_rts = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st_paid)->where('upt',$uptName)->count();
          ?>
          <td>{{$paid_rts}}</td>
          @endforeach
        </tr>
        @endforeach
      </table>
    </div>

  </div>
  @endif