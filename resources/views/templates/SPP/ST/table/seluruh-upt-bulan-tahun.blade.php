@include('templates.SPP.ST.export.preview_search')
@include('templates.SPP.ST.export.export_search')
@include('templates.SPP.ST.export.print_search')
@include('templates.SPP.ST.dev.data')
 <style>
  #example_filter {width: 1870px !important;}
  .dataTables_length{width: 560px !important;}
  .table-responsive #example_wrapper{width: 1900px;}
</style>

@if($getStatus == 'administrator')
<?php 
$listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
if($month == 1){
  $fixMonth = 12;
  $fixYear = $year - 1;
} else{
  $fixMonth = $month - 1;
  $fixYear = $year;
}
?>
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">REKAP PENGIRIMAN LAPORAN</h3>
        </div>
      </div>
    </div>
    
    <div class="table-responsive">
      <div class="">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              <th scope="col">Pengiriman Laporan</th>
            </tr>
          </thead>
          <tbody>
            @foreach($listUpt as $key => $value)
            <tr>
              <td scope="row">
                {{$value->office_name}}
              </td>
              <?php
              $checkingPengiriman = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
              ?>
              <td>
                @if($checkingPengiriman != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endif
<br>
@if($getStatus == 'administrator')
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="not_paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #ffff;">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="3"></th>
            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">RT BELUM TERBAYAR (H-30 s.d H)</th>
            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">RT TIDAK TERBAYAR (MENJADI REMINDER)</th>
          </tr>
          <tr>
            <td class="tg-0lax">UPT</td>
            <td class="tg-0lax">TOTAL RT TERBIT</td>
            <td class="tg-0lax">RT TERBAYAR</td>
            <td class="tg-0lax">% RT TERBAYAR</td>
            <td class="tg-0lax">JUMLAH RT YANG AKAN DI TL</td>
            <td class="tg-0lax">SUDAH DI-TL UPT</td>
            <td class="tg-0lax">BELUM DI-TL UPT</td>
            <td class="tg-0lax">% RT YANG DI TL</td>
            <td class="tg-0lax">Tanggal Upload</td>
          </tr>
        </thead>
        @foreach($listUpt as $key => $value)
        <tr>
          <td class="tg-0lax">{{$value->office_name}}</td>
          <?php
          $paid_rts = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$value->office_name)->count();
          $nopaid_rts = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$value->office_name)->count();
          $total_rts = $paid_rts + $nopaid_rts;
          if ($paid_rts == 0) {
            $rt_terbayar = 0;
          }else{
            $rt_terbayar = $paid_rts / $total_rts * 100;
          }
          $jumlah_rt_tl = $total_rts - $paid_rts;
          $sudah_tl = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('active',1)->count();
          $belum_tl = $total_rts - $sudah_tl;
          $tanggal_upload = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->select('updated_at')->distinct()->value('updated_at');
          $jumlah_rt = \App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$jenis_st)->where('upt',$value->office_name)->count();
          if ($sudah_tl == 0) {
              $rt_yang_tl = 0;
            }else{  
              $rt_yang_tl = $sudah_tl / $nopaid_rts * 100;
            }
          ?>
          <td>{{$total_rts}}</td>
          <td>{{$paid_rts}}</td>
          <td>{{$rt_terbayar}}</td>
          <td>{{$jumlah_rt_tl}}</td>
          <td>{{$sudah_tl}}</td>
          <td>{{$belum_tl}}</td>
          <td>{{number_format($rt_yang_tl, 2)}}</td>
          <td>
            @if($tanggal_upload)
            {{\Carbon\Carbon::parse($tanggal_upload)
             ->format('d, M Y')}}
             @else
             Belum Mengirim
             @endif
           </td>
         </tr>
         @endforeach
       </table>
     </div>

   </div>
   @endif