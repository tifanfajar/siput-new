@if($getStatus == 'operator')
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detail SPP RT</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('spp-rt-orupdate')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="did">
					<div class="col-md-6 form-group">
						<label for="company">NO TAGIHAN</label>
						<input type="number" id="dno_tagihan" class="form-control" name="no_tagihan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">NO CLIENT</label>
						<input type="number" id="dno_client" class="form-control" name="no_klien" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahun_pelimpahan">NAMA CLIENT</label>
						<input type="text" id="dnama_client" type="year" class="form-control" name="nama_klient" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahapan_pengurusan">BHP (RP)</label>
						<input type="number" class="form-control" id="dbhp" value="RP. " name="bhp" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">TANGGAL (BI BEGIN)</label>
						<input type="text" class="form-control" id="dbi_begin" name="bi_begin" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">STATUS PEMBAYARAN</label>
						<input type="text" class="form-control" id="dstatus_pembayaran" name="status_pembayaran" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal">TANGGAL JATUH TEMPO</label>
						<input type="date" class="form-control" id="dtanggal_jatuh_tempo" name="tanggal_jatuh_tempo" disabled>
					</div>
					<div class="col-md-12 form-group" style="text-align: center;">
						<label for="psbdt">TL UPT </label>
					</div>
					<div class="col-md-6 form-group">
						<label for="sisa_piutang">UPAYA/METODE </label>
						<select class="form-control" name="upaya_metode" id="dupayametode">
							@foreach(\App\Model\Refrension\Metode::all() as $value)
							<option value="{{$value->id}}">{{$value->metode}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">UPAYA / METODE </label>
						<input type="text" class="form-control" id="dupayametode" rows="3" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">TANGGAL UPAYA </label>
						<input type="date" class="form-control" id="dtanggal_upaya" name="tanggal_upaya" rows="3" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">BUKTI DUKUNG SEBELUMNYA </label>
						<input type="text" class="form-control" id="dbukti_dukung" rows="3" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">KETERANGAN </label>
						<textarea type="text" class="form-control" id="dketerangan" name="keterangan" rows="3" disabled></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endif