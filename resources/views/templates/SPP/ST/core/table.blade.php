<!-- Boxed Table -->
<style>
  .cs .select2{width: 150px; margin-top:5px;}
</style>
@include('dev.helpers.jquery')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="card shadow" style="background-color: #f3f3f3;" >
  <div class="card-header" style="padding: 30px 4px;">
    <div class="col-md-12">
      @if($getStatus == 'administrator')
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA ST (BERDASARKAN LAPORAN BULAN)</h4>
      @else
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA ST (BERDASARKAN LAPORAN BULAN)</h4>
      @endif
      <!-- Query Tahunan -->
      <form name="search_form" method="GET" action="{{url('spp/st/search_query')}}">
        <div class="dropdown" style="margin-left: 5px;padding-top: 5px; float: right;">
          <button class="btn btn-primary text-white" type="submit"><i class="fa fa-search"></i></button>
        </div>
        <div class="dropdown" style="margin-left: 5px;padding-top: 5px; float: right; width: 120px;">
          <input type="number" name="year" class="form-control" placeholder="Tahun" style="height: 34px;">
        </div>
        <div class="dropdown" style="margin-left: 5px;padding-top: 5px; float: right;">
          <select class="form-control select2" name="month" id="select_month">
            <option selected disabled>Pilih Bulan</option>
            @foreach(\App\Model\Date\ListMonth::all() as $value)
            <option value="{{$value->id_bulan}}">{{$value->nama}}</option>
            @endforeach
          </select>
        </div>
        <div class="dropdown" style="margin-left: 5px;padding-top: 5px; float: right;">
          <select class="form-control select2" name="jenis_st" id="jenis_st">
            <option selected disabled>Jenis ST</option>
            <option value="1">ST - 1</option>
            <option value="2">ST - 2</option>
            <option value="3">ST - 3</option>
            <option value="16">STT</option>
          </select>
        </div>
        @if($getStatus == 'administrator')
        <?php
        $upt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get(); 
        ?>
        <div class="dropdown cs" style="float: right; float: right;">
          <select class="form-control select2" name="upt">
            <option selected disabled>Pilih UPT</option>
            <option value="SELURUH UPT">SELURUH UPT</option>
            @foreach($upt as $value)
            <option value="{{$value->office_name}}">{{$value->office_name}}</option>
            @endforeach
          </select>
        </div>
        @else
        <input type="hidden" name="upt" value="{{$getUptName}}">
        @endif
      </form>
    </div>
  </div>
</div>
<br>

@if($getStatus == 'kepala-upt')
@if($checkStatusApproved != null)
<br>
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card text-white bg-danger" style="border:1px;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <marquee>
            <h4 class="mb-0" style="color: white;">LAPORAN PENCEGAHAN PIUTANG BULAN {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}} BELUM DIOTORISASI !!!</h4>
          </marquee>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endif

@include('templates.SPP.ST.result.query-rincian-bulanan')
@if($getStatus == 'kepala-upt')
@if($freezeTime >= \App\Model\SPP\FreezeDate::where('id',1)->value('date_start') && $freezeTime <= \App\Model\SPP\FreezeDate::where('id',1)->value('date_end') && $freezeMonth == $month)
@else
<form method="POST" action="{{route('spp-st-approved')}}" enctype="multipart/form-data">
  @csrf
  @include('templates.SPP.ST.table.approved-table')
  @if($getStatus == 'kepala-upt')
  <div class="form-group mt-5">
    <label for="exampleFormControlTextarea1">Catatan KASI (Apabila ada) :</label>
    <textarea class="form-control" name="catatankasi"></textarea>
  </div>
  <div class="col text-right" style="padding-top: 10px;">
    <button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
    &nbsp;
    <button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
  </div>
</form>
@endif
@endif
@endif
@include('templates.SPP.ST.table.data-terbayar')
@include('templates.SPP.ST.table.data-belum-bayar')