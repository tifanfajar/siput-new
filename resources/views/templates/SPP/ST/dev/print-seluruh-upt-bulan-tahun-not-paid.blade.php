<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<?php
	$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); 
	?>
	@if($getStatus == 'administrator')
	<?php 
	$listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
	if($month == 1){
	  $fixMonth = 12;
	  $fixYear = $year - 1;
	} else{
	  $fixMonth = $month - 1;
	  $fixYear = $year;
	}
	?>
	@endif
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Data Status Tagihan {{date('d F Y')}}</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
		        <thead class="thead-light">
		          <tr>
		            <th class="tg-0pky" colspan="3"></th>
		            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">ST BELUM TERBAYAR (H-30 s.d H)</th>
		            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">ST TIDAK TERBAYAR (MENJADI REMINDER)</th>
		          </tr>
		          <tr>
		            <td class="tg-0lax">UPT</td>
		            <td class="tg-0lax">TOTAL ST TERBIT</td>
		            <td class="tg-0lax">ST TERBAYAR</td>
		            <td class="tg-0lax">SUDAH DI-TL UPT</td>
		            <td class="tg-0lax">%</td>
		            <td class="tg-0lax">BELUM DI-TL UPT</td>
		            <td class="tg-0lax">%</td>
		            <td class="tg-0lax">JUMLAH ST</td>
		            <td class="tg-0lax">%</td>
		            <td class="tg-0lax">Tanggal Upload</td>
		          </tr>
		        </thead>
		        <tbody>
		        @foreach($listUpt as $key => $value)
		        <tr>
		          <td class="tg-0lax">{{$value->office_name}}</td>
		          <?php
		          $paid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
		          $nopaid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
		          $total_rts = $paid_rts + $nopaid_rts;
		          $sudah_tl = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
		          $belum_tl = $total_rts - $sudah_tl;
		          $tanggal_upload = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->select('updated_at')->distinct()->value('updated_at');
		          $jumlah_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('upt',$value->office_name)->count();
		          ?>
		          <td>{{$total_rts}}</td>
		          <td>{{$paid_rts}}</td>
		          <td>{{$sudah_tl}}</td>
		          <td>0 %</td>
		          <td>{{$belum_tl}}</td>
		          <td>0 %</td>
		          <td>{{$jumlah_rt}}</td>
		          <td>0 %</td>
		          <td>
		            @if($tanggal_upload)
		            {{\Carbon\Carbon::parse($tanggal_upload)
		             ->format('d, M Y')}}
		             @else
		             Belum Mengirim
		             @endif
		           </td>
		         </tr>
		         @endforeach
		         </tbody>
		       </table>
			<hr>
		</div>
	</center>
</body>
</html>