<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Data Status Tagihan {{date('d F Y')}}</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">No.Tagihan</th>
						<th scope="col">No. Klien</th>
						<th scope="col">Nama Klien</th>
						<th scope="col">Jenis ST</th>
						<th scope="col">BHP (Rp)</th>
						<th scope="col">Tanggal ( BI CREATE DATE )</th>
						<th scope="col">Status Pembayaran</th>
						<th scope="col">Tgl Jatuh Tempo</th>
						<th scope="col">Tanggal Bayar</th>
						<th scope="col1">Tanggal Upload</th>
						<th scope="col">Service</th>
  					</tr>
				</thead>
				<tbody>
					@foreach($rt as $key => $value)
					<tr>
						<td scope="col">{{$key+1}}</td>
						<th scope="row">{{$value->no_spp}}</th>
						<td>{{$value->no_klien}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>
							@if($value->bi_type == 1)<a class="btn btn-success btn-sm text-white">ST 1</a> @elseif($value->bi_type == 9)<a class="btn btn-success btn-sm text-white">ST 1 Paid</a> @elseif($value->bi_type == 2)<a class="btn btn-info btn-sm text-white">ST 2</a> @elseif($value->bi_type == 10) <a class="btn btn-info btn-sm text-white">ST 2 Paid</a> @elseif($value->bi_type == 3)<a class="btn btn-warning btn-sm text-white">ST 3</a> @elseif($value->bi_type == 11)<a class="btn btn-warning btn-sm text-white">ST 3 Paid</a> @elseif($value->bi_type == 16)<a class="btn btn-danger btn-sm text-white">STT</a> @elseif($value->bi_type == 49)<a class="btn btn-danger btn-sm text-white">STT Paid</a> @endif
						</td>
						<td>Rp. {{number_format($value->tagihan, 2)}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_create_date)
						->format('d, M Y')}}</td>
						<td><center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center></td>
						<td>{{\Carbon\Carbon::parse($value->bi_end)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_money_received)
						->format('d, M Y')}}</td>
						<td>19/06/2019</td>
						<td>{{$value->service_name}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>