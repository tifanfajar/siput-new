<!DOCTYPE html>
<?php 
  $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
  if($getStatus == 'administrator'){
      $listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
  }else{
      $listUpt = \App\Model\Setting\UPT::where('office_id', Auth::user()->upt)->select('office_id','office_name','province_name')->distinct()->get();
  }
?>
<html>
<head>
  <style>
    .table-bordered {
        border-collapse: collapse;
        width: 100%;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid black;
        padding: 8px;
    }
    .thead-light{
      background-color: #E9ECEF;
    }
    .table-bordered th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
    }
  </style>
</head>
<body>
  <center>
    <div>
      <hr>
      <h2 class="font-weight-bolder">REKAP PENGIRIMAN LAPORAN @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif</h2>
      <hr>
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              <th scope="col">Jan</th>
              <th scope="col">Feb</th>
              <th scope="col">Mar</th>

              <th scope="col">Apr</th>
              <th scope="col">Mei</th>
              <th scope="col">Jun</th>
              <th scope="col">Jul</th>

              <th scope="col">Aug</th>
              <th scope="col">Sep</th>
              <th scope="col">Okt</th>
              <th scope="col">Nov</th>
              <th scope="col">Des</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">
                {{$getUptName}}
              </th>
              <?php
              $fixYear = $year - 1; 
              $januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $april = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $september = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $november = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              $desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
              ?>
              <td>
                @if($januari != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($februari != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($maret != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($april != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($mei != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($juni != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($juli != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($agustus != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($september != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($oktober != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($november != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
              <td>
                @if($desember != 0)
                <a class="btn btn-success btn-sm text-white">YA</a>
                @else
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @endif
              </td>
            </tr>
          </tbody>
        </table>
      <hr>
    </div>
  </center>
</body>
</html>