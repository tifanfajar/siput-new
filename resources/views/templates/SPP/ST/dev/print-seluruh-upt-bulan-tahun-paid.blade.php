<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<?php
	$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); 
	?>
	@if($getStatus == 'administrator')
	<?php 
	$listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
	if($month == 1){
	  $fixMonth = 12;
	  $fixYear = $year - 1;
	} else{
	  $fixMonth = $month - 1;
	  $fixYear = $year;
	}
	?>
	@endif
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Data Status Tagihan {{date('d F Y')}}</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
	          <thead class="thead-light">
	            <tr>
	              <th scope="col">UPT</th>
	              <th scope="col">Pengiriman Laporan</th>
	            </tr>
	          </thead>
	          <tbody>
	            @foreach($listUpt as $key => $value)
	            <tr>
	              <td scope="row">
	                {{$value->office_name}}
	              </td>
	              <?php
	              $checkingPengiriman = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
	              ?>
	              <td>
	                @if($checkingPengiriman != 0)
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @else
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @endif
	              </td>
	            </tr>
	            @endforeach
	          </tbody>
	        </table>
			<hr>
		</div>
	</center>
</body>
</html>