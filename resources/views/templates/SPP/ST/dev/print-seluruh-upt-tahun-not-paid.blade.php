<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<?php 
		$listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
	?>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Data Status Tagihan {{date('d F Y')}}</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
	          <thead class="thead-light">
	            <tr>
	              <th scope="col">UPT</th>
	              @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
	              <th scope="col">{{$value->nama}}</th>
	              @endforeach
	            </tr>
	          </thead>
	          <tbody>
	            @foreach($listUpt as $key => $value)
	            <?php $uptName = $value->office_name ?>
	            <tr>
	              <td scope="row">
	                {{$value->office_name}}
	              </td>
	              @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
		              <?php
		              if($value->id_bulan == 1){
		                $fixMonth = 12;
		                $fixYear = $year - 1;
		              }else{
		                $fixMonth = $value->id_bulan - 1;
		                $fixYear = $year;
		              }
		              $checkingPengiriman = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$fixMonth)->whereYear('bi_create_date',$fixYear)->where('upt',$uptName)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->where('active',1)->count();
	              ?>
	              <td>
	                @if($checkingPengiriman != 0)
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @else
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @endif
	              </td>
	              @endforeach
	            </tr>
	            @endforeach
	          </tbody>
	        </table>
			<hr>
		</div>
	</center>
</body>
</html>