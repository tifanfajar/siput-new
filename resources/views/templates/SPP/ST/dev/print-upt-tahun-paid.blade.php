<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<?php 
	$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); 
	$upt = \App\Model\Setting\UPT::select('office_name','office_id')->distinct()->get();
	?>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">DATA @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif REKAP PENGIRIMAN LAPORAN</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
	          <thead class="thead-light">
	            <tr>
	              <th scope="col">UPT</th>
	              <th scope="col">Jan</th>
	              <th scope="col">Feb</th>
	              <th scope="col">Mar</th>

	              <th scope="col">Apr</th>
	              <th scope="col">Mei</th>
	              <th scope="col">Jun</th>
	              <th scope="col">Jul</th>

	              <th scope="col">Aug</th>
	              <th scope="col">Sep</th>
	              <th scope="col">Okt</th>
	              <th scope="col">Nov</th>
	              <th scope="col">Des</th>
	            </tr>
	          </thead>
	          <tbody>
	            @foreach($upt as $key => $value)
	            <tr>
	              <th scope="row">
	                {{$value->office_name}}
	              </th>
	              <?php
	              $fixYear = $year - 1; 
	              $januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',12)->whereYear('bi_create_date',$fixYear)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',1)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',2)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $april = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',3)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',4)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',5)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',6)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',7)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $september = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',8)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',9)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $november = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',10)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              $desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',11)->whereYear('bi_create_date',$year)->where('upt', $value->office_name)->where('status_izin','Perpanjangan')->where('bi_type',$bi_type)->count();
	              ?>
	              <td>
	                @if($januari == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($februari == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($maret == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($april == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($mei == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($juni == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($juli == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($agustus == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($september == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($oktober == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($november == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	              <td>
	                @if($desember == 0)
	                <a class="btn btn-danger btn-sm text-white">BELUM</a>
	                @else
	                <a class="btn btn-success btn-sm text-white">YA</a>
	                @endif
	              </td>
	            </tr>
	            @endforeach
	          </tbody>
	        </table>
			<hr>
		</div>
	</center>
</body>
</html>