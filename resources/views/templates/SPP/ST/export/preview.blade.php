<!-- Modal -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">CETAK DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{route('st-preview')}}" method="POST" enctype="multipart/enctype">
					<input type="hidden" name="status_pembayaran" id="prstatus-pembayaran">
					<input type="hidden" name="jenis_st" id="prjenis-st">
					<input type="hidden" name="bi_type" id="prbi_type">
					@csrf
					<center>
						<br>
						<button type="submit" class="btn btn-success" target="_blank"><i class="ni ni-single-copy-04"></i>&nbsp;&nbsp;Preview AS PDF</button>
						@include('dev.helpers.button.btnBatal')
					</center>
				</center>
			</form>
		</div>
	</div>
</div>
</div>