<!-- Modal -->
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DOWNLOAD AS XLS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{route('st-export')}}" method="POST" enctype="multipart/enctype">
					<input type="hidden" name="status_pembayaran" id="status-pembayaran">
					<input type="hidden" name="bi_type" id="bi_type">
					<center>
					<br>
					@csrf
					<button type="submit" class="btn btn-success"><i class="ni ni-single-copy-04"></i>&nbsp;&nbsp;DOWNLOAD AS XLSX</button>
					@include('dev.helpers.button.btnBatal')
					</center>
				</center>
			</form>
		</div>
	</div>
</div>
</div>