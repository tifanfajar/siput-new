 @if($getStatus == 'administrator')
 <div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #ffff;">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="3"></th>
            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">RT BELUM TERBAYAR (H-30 s.d H)</th>
            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">RT TIDAK TERBAYAR (MENJADI REMINDER)</th>
          </tr>
          <tr>
            <td class="tg-0lax">BULAN</td>
            <td class="tg-0lax">TOTAL RT TERBIT</td>
            <td class="tg-0lax">RT TERBAYAR</td>
            <td class="tg-0lax">SUDAH DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">BELUM DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">JUMLAH RT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">Tanggal Upload</td>
          </tr>
        </thead>
        <tr>
          <td class="tg-0lax">JAN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">FEB</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">MAR</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">APR</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">MEI</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JUN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JUL</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">AUG</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">SEP</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">OKT</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">NOV</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JAN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
      </table>
    </div>

  </div>

  <div class="col text-right" style="margin-top: 20px;">
    <div class="" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-sm btn-primary">Print</button>
      <button type="button" class="btn btn-sm btn-primary">Download PDF</button>
      <button type="button" class="btn btn-sm btn-primary">Download XLS</button>
    </div>
  </div>

</div>  
@endif