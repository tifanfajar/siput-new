<style>
  .select2-selection .select2-selection--single{height: 32px !important;}
</style>

<!-- Boxed Table -->
@include('dev.helpers.jquery')
@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<br>
<br>
<div style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <center>
            <h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$getUptName}}</h4>
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
@include('templates.helpers.SPP.RT.maps')
<div class="container" style="margin-top: 40px;">
  <br>
  @if($getStatus == 'administrator')
  @include('templates.helpers.SPP.RT.chart')
  @else
  @include('templates.helpers.SPP.RT.chart-no-admin')
  @endif
</div>
<br>
<br>
<div class="card shadow" style="background-color: #f3f3f3;" >
  <div class="card-header">
    <div class="col-md-12">
      @if($getStatus == 'administrator')
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA LAPORAN (BERDASARKAN LAPORAN)</h4>
      @else
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA LAPORAN (BERDASARKAN LAPORAN)</h4>
      @endif
      <!-- Query Tahunan -->
      <form name="search_form" method="GET" action="{{url('spp/rt/search_query')}}">
        <div class="dropdown" style="margin-left: 10px;padding-top: 5px; float: right;">
          <button class="btn btn-primary text-white" type="submit"><i class="fa fa-search"></i></button>
        </div>
        <div class="dropdown" style="margin-left: 10px;padding-top: 5px; float: right; width: 130px;">
          <input type="number" name="year" class="form-control" style="height: calc(1.80rem + 2px);">
        </div>
        <div class="dropdown" style="margin-left: 30px;padding-top: 5px; float: right;">
          <select class="form-control select2" name="month">
            <option selected disabled>Pilih Bulan</option>
            @foreach(\App\Model\Date\ListMonth::all() as $value)
            <option value="{{$value->id_bulan}}">{{$value->nama}}</option>
            @endforeach
          </select>
        </div>
        @if($getStatus == 'administrator')
        <?php
        $upt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get(); 
        ?>
        <div class="dropdown" style="float: right; float: right; width: 180px; margin-top: 3px;">
          <select class="form-control select2" name="upt">
            <option selected disabled>Pilih UPT</option>
            <option value="SELURUH UPT">SELURUH UPT</option>
            @foreach($upt as $value)
            <option value="{{$value->office_name}}">{{$value->office_name}}</option>
            @endforeach
          </select>
        </div>
        @else
        <input type="hidden" name="upt" value="{{$getUptName}}">
        @endif
      </form>
    </div>
  </div>
</div>
<br>

@include('templates.SPP.RT.table.upt-tahun')
@include('templates.helpers.SPP.RT.js')
@endsection

