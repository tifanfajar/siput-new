<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.RT.core.detail')
@include('templates.SPP.RT.core.edit')
@include('templates.SPP.RT.import.import')
@include('templates.helpers.delete')
@include('templates.SPP.RT.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@include('templates.helpers.SPP.RT.maps')
<br>
<br>
<div class="row">
	@foreach($getUPT as $upt)
	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">{{$upt->office_name}}</h5>
						<span class="h2 font-weight-bold mb-0"></span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-building"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-success mr-2"><i class="ni ni-curved-next"></i>&nbsp;&nbsp;Lihat Data ?</span>
					<span class="text-nowrap"><a href="{{route('spp-rt-search-multiple',$upt->office_id)}}">Klik Disini</a></span>
				</p>
			</div>
		</div>
	</div>
	@endforeach
</div>

@endsection