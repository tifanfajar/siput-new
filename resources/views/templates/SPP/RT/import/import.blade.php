<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">IMPORT DATA UPT</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body">
				<form action="{{route('rt-import-preview')}}" method="POST" enctype="multipart/form-data">
					@csrf
					<center>
						<input type="file" name="Import" class="fileinsert" style="margin-top: 10px; border: 2; border-radius: 6px;background-color: #324b8f; color:white;" ><br><br>

						<a href="{{route('rt-sampel')}}" class="btn btn-warning btn-sm">DATA IMPORT</a>
						<button type="submit" class="btn btn-primary btn-sm">SIMPAN</button>
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">TUTUP</button>
					</center>
				</form>
			</div>
		</div>
	</div>
</div>