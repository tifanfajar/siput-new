@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
  	<form action="{{route('rt-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{route('rt-import-post')}}">
	@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
		<label for="provinsi">Provinsi</label>
		<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
			<option selected disabled>Provinsi</option>
			@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
			<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
			@endforeach
		</select>
		
		<label for="city">UPT</label>
		<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
			<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
		</select>			
	@endif
		<label for="ket_operator">Keterangan Operator</label>
		<textarea class="form-control" name="ket_operator"></textarea>

		<span class="text-danger my-2">* Data yang diinput harus sesuai dengan data yang ada di sims,dan format harap disesuaikan dengan sampel</span>
	<table class="table table-responsive">

	  <thead>
	    <tr>
	        <th rowspan="2" scope="col">No.Tagihan</th>
	        <th rowspan="2" scope="col">No.Klien</th>
	        <th rowspan="2" scope="col">Nama Klien</th>
	        <th rowspan="2" scope="col">BHP (Rp)</th>
	        <th rowspan="2" scope="col">Tanggal ( BI BEGIN )</th>
	        <th rowspan="2" scope="col">Status Pembayaran</th>
	        <th rowspan="2" scope="col">Tanggal Jatuh Tempo</th>
	        <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
	    </tr>
	    <tr>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
	        <td class="text-center" scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tanggal Upaya <br/><span class="text-danger">( yyyy-mm-dd )</span></td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $rt => $rts)
		<?php 
          // $tanggal_jatuh_tempo = date("d, M Y", strtotime($rts[8] . "- 1days"));
          $tanggal_jatuh_tempo = date("d, M Y", strtotime($rts[7]));
          // $tglUpload = \Carbon\Carbon::parse($rts[7])->format('Y-m-d');
          if ($rts[7] != '-') {
	      	if(is_int($rts[7])){
		      	$format_tglUpload = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rts[7]));
		      	$tglUpload = $format_tglUpload->format('d, M Y');
	      	}else{
		      	$tglUpload = \Carbon\Carbon::parse($rts[7])->format('d,M Y');
	      	}
	      }else{
	      	$tglUpload = date("Y-m-d");
	      }
      	?>
	    <tr>
	      <td>
	      	{{$rts[0]}}
	      </td>
	      <td>
	      	{{$rts[1]}}
	      </td>
	      <td>
	      	{{$rts[2]}}
	      </td>
	      <td>
	      	Rp. {{number_format($rts[3], 2)}}
	      </td>
	      <td>
			{{\Carbon\Carbon::parse($rts[4])->format('d, M Y')}}
	      </td>
	      <td>
	      	{{$rts[5]}}
	      </td>
	      <td>
	      	{{$tanggal_jatuh_tempo}}
	      </td>
	      <td>
      		<select class="form-control metode" style="border: 2; border-radius: 6px;" id="upayametode[]" name="upayametode[]" required>
				<option selected value="">Pilih Data</option>
				@foreach(\App\Model\Refrension\Metode::all() as $metode)
					<option value="{{$metode->id}}" {{ $metode->metode == ucfirst($rts[8]) ? 'selected' : " "}}>{{$metode->metode}}</option>
				@endforeach
			</select>
	      </td>
	      <?php
		      if ($rts[9] != '-') {
		      	if(is_int($rts[9])){
			      	$format_tanggal_upaya = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rts[9]));
			      	$tanggal_upaya = $format_tanggal_upaya->format('Y-m-d');
		      	}else{
			      	$tanggal_upaya = \Carbon\Carbon::parse($rts[9])->format('Y-m-d');
		      	}
		      }else{
		      	$tanggal_upaya = date("Y-m-d");
		      }
	      	
	      ?>
	      <td>
	      	<input type="date" class="form-control" id="tanggal_upaya" name="tanggal_upaya[]" placeholder="Format (yyyy-mm-dd)" data-validation="date" value="{{$tanggal_upaya}}" required>
	      </td>
	      <td>
	      	<input type="file" name="bukti_dukung[]" required>
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$rts[10]}}" placeholder="Masukan Keterangan">
	      </td>
	    </tr>
	  	<input type="hidden" name="no_spp[]" value="{{$rts[0]}}">
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('templates.SPP.RT.import.validation')
@endsection