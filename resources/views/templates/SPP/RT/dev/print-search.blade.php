<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Data Rincian Tagihan {{date('d F Y')}} {{$getUptName}}</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Provinsi</th>
						<th scope="col">Nama UPT</th>
						<th scope="col">No.Tagihan</th>
						<th scope="col">No. Klien</th>
						<th scope="col">Nama Klien</th>
						<th scope="col">BHP (Rp)</th>
						<th scope="col">Tanggal ( BI BEGIN )</th>
						<th scope="col">Status Pembayaran</th>
						<th scope="col">Tgl Jatuh Tempo</th>
						<th scope="col">Tanggal Bayar</th>
						<th scope="col">Tanggal Upload</th>
						<th scope="col">Service</th>
						<th scope="col">Upaya / Methode</th>
						<th scope="col">Tanggal Upaya</th>
						<th scope="col">Keterangan</th>
  					</tr>
				</thead>
				<tbody>
					@foreach($rt as $key => $value)
					<?php 
			          $tanggal_jatuh_tempo = date("d, M Y", strtotime($value->bi_pay_until . "- 1days"));
			          if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
							$provinsi = $value->province;
							$nama_upt = $value->upt;				
						}else{
							$id_prov = Auth::user()->province_code;
							$kode_upt = Auth::user()->upt;

							$provinsi = \App\Model\Region\Provinsi::where('id', $id_prov)->value('nama');
							$nama_upt = $upts =  \App\Model\Setting\UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
						}
			      	?>
					<tr>
						<td scope="col">{{$key+1}}</td>
						<th scope="row">{{$provinsi}}</th>
						<th scope="row">{{$nama_upt}}</th>
						<th scope="row">{{$value->no_spp}}</th>
						<td>{{$value->no_klien}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>Rp. {{number_format($value->tagihan, 2)}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_begin)
						->format('d, M Y')}}</td>
						<td>
							@if($value->status == 'paid')
							<center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center>
							@else
							<center><a class="btn btn-danger btn-sm text-white">Belum Bayar</a></center>
							@endif
						</td>
						<td>{{$tanggal_jatuh_tempo}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_money_received)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->created_at)
						->format('d, M Y')}}</td>
						<td>{{$value->service_name}}</td>
						<td>{{\App\Model\Refrension\Metode::where('id', $value->upaya_metode)->value('metode')}}</td>
						@if($value->tanggal_upaya == '')
						<td></td>
						@else
						<td>{{\Carbon\Carbon::parse($value->tanggal_upaya)
						->format('d, M Y')}}</td>
						@endif
						<td>{{$value->keterangan}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>