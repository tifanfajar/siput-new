<!DOCTYPE html>
<?php 
  $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); 
  $listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
?>
<html>
<head>
  <style>
    .table-bordered {
        border-collapse: collapse;
        width: 100%;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid black;
        padding: 8px;
    }
    .thead-light{
      background-color: #E9ECEF;
    }
    .table-bordered th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
    }
  </style>
</head>
<body>
  <center>
    <div>
      <hr>
      <h2 class="font-weight-bolder">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG</h2>
      <hr>
      <table id="example" class="table table-bordered align-items-center">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="1"></th>
            <th class="tg-0lax" colspan="12" style="background-color: #E9E214; color: #fff; text-align: center;">% RT TERBAYAR SEBELUM TNGGAL JATUH TEMPO</th>
          </tr>
          <tr>
            <td class="tg-0lax">UPT</td>
            @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
            <td class="tg-0lax">{{$value->nama}}</td>
            @endforeach
          </tr>
        </thead>
        @foreach($listUpt as $key => $value)
        <?php $uptName = $value->office_name ?>
        <tr>
          <td class="tg-0lax">{{$value->office_name}}</td>
          @foreach(\App\Model\Date\ListMonth::all() as $key => $value)
          <?php
          if($value->id_bulan == 1){
            $fixMonth = 12;
            $fixYear = $year - 1;
          }else{
            $fixMonth = $value->id_bulan - 1;
            $fixYear = $year;
          }
          $paid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$uptName)->count();
          ?>
          <td>{{$paid_rts}}</td>
          @endforeach
        </tr>
        @endforeach
      </table>
      <hr>
    </div>
  </center>
</body>
</html>