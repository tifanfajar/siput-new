<!DOCTYPE html>
<?php 
  $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
  if($getStatus == 'administrator'){
      $listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
  }else{
      $listUpt = \App\Model\Setting\UPT::where('office_id', Auth::user()->upt)->select('office_id','office_name','province_name')->distinct()->get();
  }
  if($month == 1){
    $fixMonth = 12;
    $fixYear = $year - 1;
  } else{
    $fixMonth = $month - 1;
    $fixYear = $year;
  }
?>
<html>
<head>
  <style>
    .table-bordered {
        border-collapse: collapse;
        width: 100%;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid black;
        padding: 8px;
    }
    .thead-light{
      background-color: #E9ECEF;
    }
    .table-bordered th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
    }
  </style>
</head>
<body>
  <center>
    <div>
      <hr>
      <h2 class="font-weight-bolder">REKAP PENGIRIMAN LAPORAN</h2>
      <hr>
      <table id="example" class="table table-bordered align-items-center">
          <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="3"></th>
            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">RT BELUM TERBAYAR (H-30 s.d H)</th>
            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">RT TIDAK TERBAYAR (MENJADI REMINDER)</th>
          </tr>
          <tr>
            <td class="tg-0lax">UPT</td>
            <td class="tg-0lax">TOTAL RT TERBIT</td>
            <td class="tg-0lax">RT TERBAYAR</td>
            <td class="tg-0lax">% RT TERBAYAR</td>
            <td class="tg-0lax">JUMLAH RT YANG AKAN DI TL</td>
            <td class="tg-0lax">SUDAH DI-TL UPT</td>
            <td class="tg-0lax">BELUM DI-TL UPT</td>
            <td class="tg-0lax">% RT YANG DI TL</td>
            <td class="tg-0lax">Tanggal Upload</td>
          </tr>
        </thead>
        @foreach($listUpt as $key => $value)
        <tr>
          <td class="tg-0lax">{{$value->office_name}}</td>
          <?php
          $paid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
            $nopaid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
            $total_rts = $paid_rts + $nopaid_rts;
            if ($paid_rts == 0) {
              $rt_terbayar = 0;
            }else{
              $rt_terbayar = $paid_rts / $total_rts * 100;
            }
            $sudah_tl = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status','not paid')->where('active',1)->count();
            $belum_tl = $nopaid_rts - $sudah_tl;
            if ($sudah_tl == 0) {
              $rt_yang_tl = 0;
            }else{  
              $rt_yang_tl = $sudah_tl / $nopaid_rts * 100;
            }
            $tanggal_upload = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct()->value('updated_at');
            $jumlah_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
          ?>
          <td>{{$total_rts}}</td>
          <td>{{$paid_rts}}</td>
          <td>{{number_format($rt_terbayar, 2)}} %</td>
          <td>{{$nopaid_rts}}</td>
          <td>{{$sudah_tl}}</td>
          <td>{{$belum_tl}}</td>
          <td>{{number_format($rt_yang_tl, 2)}} &</td>
          <td>
            @if($tanggal_upload)
            {{\Carbon\Carbon::parse($tanggal_upload)
             ->format('d, M Y')}}
             @else
             Belum Mengirim
             @endif
           </td>
         </tr>
         @endforeach
        </table>
      <hr>
    </div>
  </center>
</body>
</html>