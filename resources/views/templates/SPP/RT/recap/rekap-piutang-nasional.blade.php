 @if($getStatus == 'administrator')
 <!-- Boxed Table -->
 <div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="mb-0">LAPORAN AKSI PENCEGAHAN PIUTANG NASIONAL BULAN ... TAHUN ...</h4>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-12 mb-5 mb-xl-0">
  <div class="card shadow">
    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">REKAP PENGIRIMAN LAPORAN</h3>
        </div>
      </div>
    </div>
    <div class="table-responsive" style="overflow: hidden;">
      <!-- Projects table -->
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table dataTable no-footer">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">UPT</th>
            <th scope="col">Pengiriman Laporan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">01</th>
            <td scope="row">Balmon Kelas II Aceh</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">02</th>
            <td scope="row">Balmon Kelas I Medan</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">03</th>
            <td scope="row">Balmon Kelas I Pekanbaru</td>
            <td scope="row">Tidak</td>
          </tr>
          <tr>
            <th scope="row">04</th>
            <td scope="row">Balmon Kelas I Pontianak</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">05</th>
            <td scope="row">Balmon Kelas I Yogyakarta</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">06</th>
            <td scope="row">Balmon Kelas I Bandung</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">07</th>
            <td scope="row">Balmon Kelas I Bali</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">08</th>
            <td scope="row">Balmon Kelas I Papua</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">09</th>
            <td scope="row">Balmon Kelas I Batam</td>
            <td scope="row">Ya</td>
          </tr>
          <tr>
            <th scope="row">10</th>
            <td scope="row">Balmon Kelas I Batam</td>
            <td scope="row">Ya</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endif