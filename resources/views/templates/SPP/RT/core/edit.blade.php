@if($getStatus == 'operator')
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH SPP RT</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('spp-rt-orupdate')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-6 form-group">
						<label for="company">NO TAGIHAN</label>
						<input type="number" id="eno_tagihan" class="form-control" name="no_tagihan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">NO CLIENT</label>
						<input type="number" id="eno_client" class="form-control" name="no_klien" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahun_pelimpahan">NAMA CLIENT</label>
						<input type="text" id="enama_client" type="year" class="form-control" name="nama_klient" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahapan_pengurusan">BHP (RP)</label>
						<input type="number" class="form-control" id="ebhp" value="RP. " name="bhp" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">TANGGAL (BI BEGIN)</label>
						<input type="text" class="form-control" id="ebi_begin" name="bi_begin" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">STATUS PEMBAYARAN</label>
						<input type="text" class="form-control" id="estatus_pembayaran" name="status_pembayaran" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal">TANGGAL JATUH TEMPO</label>
						<input type="date" class="form-control" id="etanggal_jatuh_tempo" name="tanggal_jatuh_tempo" disabled>
					</div>
					<div class="col-md-12 form-group" style="text-align: center;">
						<label for="psbdt">TL UPT </label>
					</div>
					<div class="col-md-6 form-group">
						<label for="sisa_piutang">UPAYA/METODE </label>
						<select class="form-control" name="upaya_metode" id="eupayametode">
							@foreach(\App\Model\Refrension\Metode::all() as $value)
							<option value="{{$value->id}}">{{$value->metode}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">TANGGAL UPAYA </label>
						<input type="date" class="form-control" id="etanggal_upaya" name="tanggal_upaya" rows="3" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">BUKTI DUKUNG </label>
						<input type="file" class="form-control" name="bukti_dukung"  rows="3">
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">BUKTI DUKUNG SEBELUMNYA </label>
						<input type="text" class="form-control" id="ebukti_dukung" rows="3" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">KETERANGAN </label>
						<textarea type="text" class="form-control" id="eketerangan" name="keterangan" rows="3" required></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endif