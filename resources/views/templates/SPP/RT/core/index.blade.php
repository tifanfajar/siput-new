<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.RT.core.detail')
@include('templates.SPP.RT.core.detailModalAdmin')
@include('templates.SPP.RT.core.edit')
@include('templates.SPP.RT.import.import')
@include('templates.helpers.delete')
@include('templates.SPP.RT.export.preview')
@include('templates.SPP.RT.export.export')
@include('templates.SPP.RT.export.print')
@include('templates.SPP.RT.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
	div.dataTables_wrapper div.dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 160px;
		margin-left: -100px;
		margin-top: -26px;
		text-align: center;
		color: #fff;
		padding: 1em 0;
		background-color: #17457beb;
		/* border: 1px solid #000; */
		border-radius: 59px;
	}
</style>
@if($getStatus != 'administrator')
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$getUptName}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@if($statusReminder != $checkStatusReminder)
<div class="col-xl-12" style="margin-bottom: 20px;">
	<div class="card text-white bg-danger" style="border:1px;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<marquee>
						<h4 class="mb-0" style="color: white;"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;PERHATIAN !!! ANDA BELUM MENGIRIMKAN LAPORAN AKSI PENGURANGAN PIUTANG {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}}  ( APLIKASI AKAN DI TUTUP PADA TANGGAL  {{\App\Model\SPP\FreezeDate::where('id',1)->value('date_start')}} - {{\App\Model\SPP\FreezeDate::where('id',1)->value('date_end')}} {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} {{$year}} )</h4>
					</marquee>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endif
@include('templates.helpers.SPP.RT.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.SPP.RT.chart')
	@else
	@include('templates.helpers.SPP.RT.chart-no-admin')
	@endif
</div>

<div class="row mt-5" id="parent_table">
	<div class="col-md-12 mb-4 mb-xl-0" style="margin-top: 10px;">
		<div class="" >
			@include('templates.SPP.RT.core.table')
		</div>
	</div>

	
</div>
@include('templates.helpers.SPP.RT.js')
@endsection