<!-- Boxed Table -->
@include('dev.helpers.jquery')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="card shadow" style="background-color: #f3f3f3;" >
  <div class="card-header">
    <div class="col-md-12">
      @if($getStatus == 'administrator')
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA LAPORAN (BERDASARKAN LAPORAN)</h4>
      @else
      <h4 class="mb-0 title-query" style="float: left; padding-top: 10px; margin-right: 10px;"><i class="fa fa-database"></i>&nbsp;&nbsp;QUERY DATA RINCIAN TAGIHAN (BERDASARKAN LAPORAN)</h4>
      @endif
      <!-- Query Tahunan -->
      <form name="search_form" method="GET" action="{{url('spp/rt/search_query')}}">
        <div class="dropdown" style="margin-left: 30px;padding-top: 5px; float: right;">
          <button class="btn btn-primary text-white" type="submit"><i class="fa fa-search"></i></button>
        </div>
        <div class="dropdown" style="margin-left: 10px;padding-top: 5px; float: right; width: 120px;">
          <input type="number" name="year" placeholder="Tahun" class="form-control" style="height: 34px; border-radius: 3px;">
        </div>
        <div class="dropdown" style="margin-left: 5px;padding-top: 5px; float: right;">
          <select class="form-control select2" name="month" id="select_month">
            <option selected disabled>Pilih Bulan</option>
            @foreach(\App\Model\Date\ListMonth::all() as $value)
            <option value="{{$value->id_bulan}}">{{$value->nama}}</option>
            @endforeach
          </select>
        </div>
        @if($getStatus == 'administrator')
        <?php
        $upt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get(); 
        ?>
        <div class="dropdown" style="float: right; float: right; width: 193px; margin-top: 5px; z-index: 1;">
          <select class="form-control select2" name="upt">
            <option selected disabled>Pilih UPT</option>
            <option value="SELURUH UPT">SELURUH UPT</option>
            @foreach($upt as $value)
            <option value="{{$value->office_name}}">{{$value->office_name}}</option>
            @endforeach
          </select>
        </div>
        @else
        <input type="hidden" name="upt" value="{{$getUptName}}">
        @endif
      </form>
    </div>
  </div>
</div>
<br>

@if($getStatus == 'kepala-upt')
@if($checkStatusApproved)
<br>
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card text-white bg-danger" style="border:1px;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <marquee>
            <h4 class="mb-0" style="color: white;"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;LAPORAN PENCEGAHAN PIUTANG BULAN {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}} BELUM DIOTORISASI !!!</h4>
          </marquee>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endif

@if($getStatus == 'kepala-upt')
<form method="POST" action="{{route('spp-rt-approved')}}" enctype="multipart/form-data">
  @csrf
  @include('templates.SPP.RT.table.approved-table')
  @if($getStatus == 'kepala-upt')
  @if($freezeTime >= \App\Model\SPP\FreezeDate::where('id',1)->value('date_start') && $freezeTime <= \App\Model\SPP\FreezeDate::where('id',1)->value('date_end') && $freezeMonth == $month)
  @else
  <div class="form-group mt-5">
    <label for="exampleFormControlTextarea1">Catatan (Apabila ada) :</label>
    <textarea class="form-control" name="catatankasi"></textarea>
  </div>
  <div class="col text-right" style="padding-top: 10px;">
    <button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
    &nbsp;
    <button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
  </div>
  @endif
  @endif
</form>
@endif
<br>
@include('templates.SPP.RT.result.query-rincian')
@include('templates.SPP.RT.table.data-terbayar')
@include('templates.SPP.RT.table.data-belum-bayar')
