
@extends('dev.core.using')
@section('content')

<div class="container">
	<br>
	<br>
	<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-key"></i>&nbsp;&nbsp;PENGATURAN FREEZE DATE</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	<br>
	<form action="{{route('freezeDate-update')}}" class="row" method="POST" enctype="multipart/form-data">
	@csrf
	<div class="col-md-6 form-group">
	<label for="date_start">Tanggal Mulai</label><label style="color: red;">*</label>
	<input type="number" class="form-control" id="date_start" name="date_start" placeholder="Masukan Tanggal Mulai" value="{{$freezeDate->date_start}}" required>
	</div>	
	<div class="col-md-6 form-group">
	<label for="date_end">Tanggal Selesai</label><label style="color: red;">*</label>
	<input type="number" class="form-control" id="date_end" name="date_end" placeholder="Masukan Tanggal Selesai" value="{{$freezeDate->date_end}}" required>
	</div>			
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button type="submit" class="btn btn-primary" onClick="confirm('Sudah yakin dengan tanggal yang baru?')"><i class="fa fa-save"></i>&nbsp;&nbsp;Update Data</button>
	<a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</a>
	</form>
</div>

@endsection