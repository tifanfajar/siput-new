@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#no_client').on("select2:select", function(e) {
			$.ajax({
				url: "{{ url('pelayanan/penanganan-piutang/getPerusahaan/') }}/" + $(this).val(),
				method: 'GET',
				success: function(data) {
					if(data != null || data != 'null' || data != ''){
						console.log(data.id_prov);
						$.confirm({
							title: 'Perusahaan ini sudah dibuat !',
							content: 'Apakah anda mau melanjutkan?',
							buttons: {
								iya: {
									text: 'Ya',
									btnClass: 'btn-blue',
									keys: ['enter'],
									action: function(){
										$.alert('Anda telah Melanjutkan');
										$('#aid').val(data.id);

										$('#form-penanganan-piutang-add').attr('action', "{{route('penanganan-piutang-update-unencrypt')}}");
										if($.trim(data.no_client)){
											$('.no_client').attr("readonly", "readonly");
										}


										$('#anilai_penyerahan').val(data.nilai_penyerahan);
										if($.trim(data.nilai_penyerahan)){
											$('#anilai_penyerahan').attr("readonly", "readonly");
										}

										$('#atema').val(data.tema);
										if($.trim(data.tema)){
											$('#atema').attr("readonly", "readonly");
										}

										$('#atahun_pelimpahan').val(data.tahun_pelimpahan);
										if($.trim(data.tahun_pelimpahan)){
											$('#atahun_pelimpahan').attr("readonly", "readonly");
										}

										$('#atahapan_pengurusan').val(data.tahapan_pengurusan);
										if($.trim(data.tahapan_pengurusan)){
											$('#atahapan_pengurusan').attr("readonly", "readonly");
										}

										$('#alunas').val(data.lunas);
										if($.trim(data.lunas)){
											$('#alunas').attr("readonly", "readonly");
										}

										$('#aangsuran').val(data.angsuran);
										if($.trim(data.angsuran)){
											$('#aangsuran').attr("readonly", "readonly");
										}

										$('#atanggal').val(data.tanggal);
										if($.trim(data.tanggal)){
											$('#atanggal').attr("readonly", "readonly");
										}

										$('#apsbdt').val(data.psbdt);
										if($.trim(data.psbdt)){
											$('#apsbdt').attr("readonly", "readonly");
										}

										$('#apembatalan').val(data.pembatalan);
										if($.trim(data.pembatalan)){
											$('#apembatalan').attr("readonly", "readonly");
										}

										$('#asisa_piutang').val(data.sisa_piutang);
										if($.trim(data.sisa_piutang)){
											$('#asisa_piutang').attr("readonly", "readonly");
										}

										$('#atanggal_psbdt').val(data.tanggal_psbdt);
										if($.trim(data.tanggal_psbdt)){
											$('#atanggal_psbdt').attr("readonly", "readonly");
										}

										$('#atanggal_pembatalan').val(data.tanggal_pembatalan);
										if($.trim(data.tanggal_pembatalan)){
											$('#atanggal_pembatalan').attr("readonly", "readonly");
										}


										$('#aketerangan').val(data.keterangan);
										if($.trim(data.keterangan)){
											$('#aketerangan').attr("readonly", "readonly");
										}

										$('.aid_prov').val(data.id_prov);
										if($.trim(data.id_prov)){
											$('.aid_prov').attr("readonly", "readonly");
										}

										if($.trim(data.nama_kpknl)){
											$('.anama_kpknl').attr("readonly", "readonly");
										}

										$.ajax({
											url: "{{ url('pelayanan/penanganan-piutang/provinsi/kabupaten/') }}/" + data.nama_kpknl,
											method: 'GET',
											success: function(data) {
												$('.anama_kpknl').html(data.html);
											}
										});

										$.ajax({
											url: "{{ url('pelayanan/penanganan-piutang/getUpt/') }}/" + data.kode_upt,
											method: 'GET',
											success: function(data) {
												$('.aid_upt').html(data.html);
											}
										});
										if($.trim(data.kode_upt)){
											$('.aid_upt').attr("readonly", "readonly");
										}
									}
								},
								tidak: {
									text: 'Tidak',
									btnClass: 'btn-blue',
									keys: ['n'],
									action: function(){
										$('#form-penanganan-piutang-add').attr('action', "{{route('penanganan-piutang-save')}}");
										$.post( "{{ url('pelayanan/penanganan-piutang/softDelete') }}", { id: data.no_client}).done(function( data ) {
											$.alert('Anda Tidak melanjutkan');
										});
									}
								},
								batal: {
									text: 'Batal',
									btnClass: 'btn-blue',
									keys: ['esc'],
									action: function(){
										$('#no_client').val('');
									}
								}
							}
						});
}
}
});
});
});
</script>

<script type="text/javascript">
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#nama_kpknl').html(data.html);
			}
		});
	});
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailApproval').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#dano_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#danilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#datema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#datahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#datahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#dalunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#daangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#datanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#dapsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#dapembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#dasisa_piutang').val(sisa_piutang);

			var tanggal_psbdt=$(e.relatedTarget).attr('data-tanggal-psbdt');
			$('#datanggal_psbdt').val(tanggal_psbdt);

			var tanggal_pembatalan=$(e.relatedTarget).attr('data-tanggal-pembatalan');
			$('#datanggal_pembatalan').val(tanggal_pembatalan);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#daid_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#danama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#daid_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailReject').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#drno_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#drnilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#drtema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#drtahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#drtahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#drlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#drangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#drtanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#drpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#drpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#drsisa_piutang').val(sisa_piutang);

			var tanggal_psbdt=$(e.relatedTarget).attr('data-tanggal-psbdt');
			$('#drtanggal_psbdt').val(tanggal_psbdt);

			var tanggal_pembatalan=$(e.relatedTarget).attr('data-tanggal-pembatalan');
			$('#drtanggal_pembatalan').val(tanggal_pembatalan);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#drketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#drid_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#drnama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#drid_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#dno_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#dnilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#dtema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#dtahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#dtahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#dlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#dangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#dtanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#dpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#dpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#dsisa_piutang').val(sisa_piutang);

			var tanggal_psbdt=$(e.relatedTarget).attr('data-tanggal-psbdt');
			$('#dtanggal_psbdt').val(tanggal_psbdt);

			var tanggal_pembatalan=$(e.relatedTarget).attr('data-tanggal-pembatalan');
			$('#dtanggal_pembatalan').val(tanggal_pembatalan);


			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#did_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#dnama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#did_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			var idperusahaan=$(e.relatedTarget).attr('data-perusahaan-id');
			var noperusahaan=$(e.relatedTarget).attr('data-perusahaan-no');
			$('#eno_client').append('<option value="'+ idperusahaan +'" selected>'+ perusahaan +'&nbsp;('+ noperusahaan +')</option>');

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#editnilai_penyerahan').val(nilai_penyerahan);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#etahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#edittahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#editlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#editangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#edittanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#editpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#editpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#editsisa_piutang').val(sisa_piutang);

			var tanggal_psbdt=$(e.relatedTarget).attr('data-tanggal-psbdt');
			$('#etanggal_psbdt').val(tanggal_psbdt);

			var tanggal_pembatalan=$(e.relatedTarget).attr('data-tanggal-pembatalan');
			$('#etanggal_pembatalan').val(tanggal_pembatalan);


			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#editketerangan').val(keterangan);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#editid_upt').val(nama_upt);

			var nama_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#editid_prov').val(nama_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#editnama_kpknl').val(nama_kpknl);

			var id=$(e.relatedTarget).attr('data-id');
			$('#editid').val(id);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editReject').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#rejectid').val(id);

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			var idperusahaan=$(e.relatedTarget).attr('data-perusahaan-id');
			var noperusahaan=$(e.relatedTarget).attr('data-perusahaan-no');
			$('#erno_client').append('<option value="'+ idperusahaan +'" selected>'+ perusahaan +'&nbsp;('+ noperusahaan +')</option>');

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#erditnilai_penyerahan').val(nilai_penyerahan);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#ertahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#erdittahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#erditlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#erditangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#erdittanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#erditpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#erditpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#erditsisa_piutang').val(sisa_piutang);

			var tanggal_psbdt=$(e.relatedTarget).attr('data-tanggal-psbdt');
			$('#ertanggal_psbdt').val(tanggal_psbdt);

			var tanggal_pembatalan=$(e.relatedTarget).attr('data-tanggal-pembatalan');
			$('#ertanggal_pembatalan').val(tanggal_pembatalan);


			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erditketerangan').val(keterangan);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#erditid_upt').val(nama_upt);

			var nama_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#erditid_prov').val(nama_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#erditnama_kpknl').val(nama_kpknl);

		});
	})
</script>