<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Penanganan Piutang</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
				<thead class="thead-light" style="font-size: 4pt;">
					<tr>
						<th scope="col">NO.</th>
						<th scope="col">TANGGAL DIBUAT</th>
						<th scope="col">TANGGAL UPDATE</th>
						<th scope="col">PROVINSI</th>
						<th scope="col">UPT</th>
						<th scope="col">NO.KLIEN</th>
						<th scope="col">WAJIB BAYAR</th>
						<th scope="col">NILAI PENYERAHAN</th>
						<th scope="col">TAHUN PELIMPAHAN</th>
						<th scope="col">NAMA KPKNL</th>
						<th scope="col">TAHAPAN PENGURUSAN</th>
						<th scope="col">LUNAS</th>
						<th scope="col">ANGSURAN</th>
						<th scope="col">TANGGAL</th>
						<th scope="col">PSBDT</th>
						<th scope="col">Tanggal PSBDT</th>
						<th scope="col">PEMBATALAN</th>
						<th scope="col">Tanggal PEMBATALAN</th>
						<th scope="col">SISA UTANG</th>
						<th scope="col">KET</th>
					</tr>
				</thead>
				<tbody style="font-size: 4pt;">
					@foreach($petang as $key => $value)
					@if($value->status == 1)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value->created_at}}</td>
						<td>{{$value->updated_at}}</td>
						<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						<td>{{$upts}}</td>
						<td>{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('company_id')}}</td>
						<td>{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('name')}}</td>
						<td>Rp. {{number_format($value->nilai_penyerahan, 2)}}</td>
						<td>{{$value->tahun_pelimpahan}}</td>
						<td>{{\App\Model\KPNKL::where('id', $value->nama_kpknl)->value('kpknl_name')}}</td>
						<td>{{$value->tahapan_pengurusan}}</td>
						<td>Rp. {{number_format($value->lunas, 2)}}</td>
						<td>Rp. {{number_format($value->angsuran, 2)}}</td>
						<td>{{$value->tanggal}}</td>
						<td>Rp. {{number_format($value->psbdt, 2)}}</td>
						<td>{{$value->tanggal_psbdt}}</td>
						<td>Rp. {{number_format($value->pembatalan, 2)}}</td>
						<td>{{$value->tanggal_pembatalan}}</td>
						<td>Rp. {{number_format($value->sisa_piutang, 2)}}</td>
						<td>{{$value->keterangan}}</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>