<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-save')}}" id="form-penanganan-piutang-add" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="aid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan </label><label style="color: red;">*</label>
						<select name="no_client" id="no_client" class="form-control no_client select2" data-placeholder="Pilih Salah Satu" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@if($getStatus == 'administrator')
							@foreach(\App\Model\Setting\Perusahaan::all() as $value)
							<option value="{{$value->id}}">{{$value->name}}&nbsp;( NO.KLIEN : {{$value->company_id}} )</option>
							@endforeach
							@else
							@foreach(\App\Model\Setting\Perusahaan::where('id_prov',Auth::user()->province_code)->get() as $value)
							<option value="{{$value->id}}">{{$value->name}}&nbsp;( NO.KLIEN : {{$value->company_id}} )</option>
							@endforeach
							@endif
						</select>
						@if($getStatus !='kepala-upt')
						<a href="{{route('pengaturan/perusahaan')}}">➤ Daftar Perusahaan disini</a>
						@endif
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan </label><label style="color: red;">*</label>
						<input type="number" class="form-control" name="nilai_penyerahan" id="anilai_penyerahan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan </label><label style="color: red;">*</label>
						<input type="number" class="form-control" name="tahun_pelimpahan" id="atahun_pelimpahan" required minlength="4">
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan </label><label style="color: red;">*</label>
						<select class="form-control aid_prov select2" id="tahapan_pengurusan" name="tahapan_pengurusan" data-placeholder="Pilih Salah Satu" required>
							<option selected disabled value="">Pilih salah satu</option>
							<option value="BELUM SP3N">BELUM SP3N</option>
							<option value="SP3N">SP3N</option>
							<option value="Panggilan Awal">Panggilan Awal</option>
							<option value="Panggilan Akhir">Panggilan Akhir</option>
							<option value="PJPN / BA Kesanggupan Bayar">PJPN / BA Kesanggupan Bayar</option>
							<option value="Surat Paksa">Surat Paksa</option>
							<option value="BA Surat Paksa">BA Surat Paksa</option>
							<option value="Lunas (SPPNL)">Lunas (SPPNL)</option>
							<option value="PSBDT">PSBDT</option>
							<option value="Pengembalian">Pengembalian</option>
							<option value="Penolakan">Penolakan</option>
							<option value="Pembatalan">Pembatalan</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="number" class="form-control" name="lunas" id="alunas">
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="number" class="form-control" name="angsuran" id="aangsuran">
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal Bayar</label>
						<input type="date" class="form-control" name="tanggal" id="atanggal">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="number" class="form-control" name="psbdt" id="apsbdt">
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="number" class="form-control" name="pembatalan" id="apembatalan">
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="number" class="form-control" name="sisa_piutang" id="asisa_piutang">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">Tanggal PSBDT </label>
						<input type="date" class="form-control" name="tanggal_psbdt" id="atanggal_psbdt">
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Tanggal Pembatalan </label>
						<input type="date" class="form-control" name="tanggal_pembatalan" id="atanggal_pembatalan">
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" name="keterangan" id="aketerangan" rows="3"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label><label style="color: red;">*</label>
						<select class="form-control aid_prov" data-placeholder="Khusus Kepala UPT & Operator" id="id_prov" name="id_prov" required>
							<option selected disabled value="">Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label><label style="color: red;">*</label>
						<select name="id_upt" id="id_upt" class="form-control aid_upt" required>
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator' )
					<div class="col-md-12 form-group">
						<label for="city">Nama KPKNL</label><label style="color: red;">*</label>
						<select name="nama_kpknl" class="form-control select2" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\KPNKL::all() as $key)
							<option value="{{$key->id}}">{{$key->kpknl_name}}</option>
							@endforeach
						</select>
					</div>
					@else
					<div class="col-md-12 form-group">
						<label for="city">Kabupaten/Kota </label><label style="color: red;">*</label>
						<select name="nama_kpknl" id="nama_kpknl" class="form-control select2" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\KPNKL::all() as $key)
							<option value="{{$key->id}}">{{$key->kpknl_name}}</option>
							@endforeach
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>