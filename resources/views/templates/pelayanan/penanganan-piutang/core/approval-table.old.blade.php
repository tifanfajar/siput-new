<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			@if($getStatus == 'kepala-upt')
			<th><input type="checkbox" id="selectAll" /></th>
			@endif
			<th scope="col">TANGGAL DIBUAT</th>
			<th scope="col">PROVINSI</th>
			<th scope="col">UPT</th>
			<th scope="col">NO.KLIEN</th>
			<th scope="col">WAJIB BAYAR</th>
			<th scope="col">NILAI PENYERAHAN</th>
			<th scope="col">TAHUN PELIMPAHAN</th>
			<th scope="col">NAMA KPKNL</th>
			<th scope="col">TAHAPAN PENGURUSAN</th>
			<th scope="col">LUNAS</th>
			<th scope="col">ANGSURAN</th>
			<th scope="col">TANGGAL</th>
			<th scope="col">PSBDT</th>
			<th scope="col">Tanggal PSBDT</th>
			<th scope="col">PEMBATALAN</th>
			<th scope="col">Tanggal PEMBATALAN</th>
			<th scope="col">SISA UTANG</th>
			<th scope="col">KET</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($petang as $value)
		@if($value->status == 0)
		<tr>
			@if($getStatus == 'kepala-upt')
			<td><input type="checkbox" name="id[]" value="{{ $value->id }}"></td>
			@endif
			<td>{{$value->created_at}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{$upts}}</td>
			<td>{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('company_id')}}</td>
			<td>{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('name')}}</td>
			<td>Rp. {{number_format($value->nilai_penyerahan, 2)}}</td>
			<td>{{$value->tahun_pelimpahan}}</td>
			<td>{{\App\Model\KPNKL::where('id',$value->nama_kpknl)->value('kpknl_name')}}</td>
			<td>{{$value->tahapan_pengurusan}}</td>
			<td>Rp. {{number_format($value->lunas, 2)}}</td>
			<td>Rp. {{number_format($value->angsuran, 2)}}</td>
			<td>{{$value->tanggal}}</td>
			<td>Rp. {{number_format($value->psbdt, 2)}}</td>
			<td>{{$value->tanggal_psbdt}}</td>
			<td>Rp. {{number_format($value->pembatalan, 2)}}</td>
			<td>{{$value->tanggal_pembatalan}}</td>
			<td>Rp. {{number_format($value->sisa_piutang, 2)}}</td>
			<td>{{$value->keterangan}}</td>
			<td>
				<a href="#" data-toggle="modal" data-target="#detailApproval" class="btn btn-success btn-sm"
				data-perusahaan="{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('name')}}"
				data-nilai-penyerahan="{{$value->nilai_penyerahan}}"
				data-tahun-pelimpahan="{{$value->tahun_pelimpahan}}"
				data-tahapan-pengurusan="{{$value->tahapan_pengurusan}}"
				data-lunas="{{$value->lunas}}"
				data-angsuran="{{$value->angsuran}}"
				data-tanggal="{{$value->tanggal}}"
				data-psbdt="{{$value->psbdt}}"
				data-pembatalan="{{$value->pembatalan}}"
				data-sisa-piutang="{{$value->sisa_piutang}}"

				data-tanggal-psbdt="{{$value->tanggal_psbdt}}"
				data-tanggal-pembatalan="{{$value->tanggal_pembatalan}}"
				
				data-keterangan="{{$value->keterangan}}"
				data-id-prov="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-nama-kpknl="{{\App\Model\KPNKL::where('id',$value->nama_kpknl)->value('kpknl_name')}}"
				data-id-upt="{{$upts}}"
				><i class="fa fa-file"></i></a>
			</td>
		</tr>
		@endif
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
	$('#selectAll').click(function(e){
		var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
	});
</script>