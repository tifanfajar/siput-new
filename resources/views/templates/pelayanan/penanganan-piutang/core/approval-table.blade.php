<table id="server-side-0" class="table table-striped table-dark align-items-center no-footer">
	<thead class="thead-light">
		<tr>
			@if($getStatus == 'kepala-upt')
			<th><input type="checkbox" id="selectAll" /></th>
			@endif
			<th scope="col">TANGGAL DIBUAT</th>
			<th scope="col">PROVINSI</th>
			<th scope="col">UPT</th>
			<th scope="col">NO.KLIEN</th>
			<th scope="col">WAJIB BAYAR</th>
			<th scope="col">NILAI PENYERAHAN</th>
			<th scope="col">TAHUN PELIMPAHAN</th>
			<th scope="col">NAMA KPKNL</th>
			<th scope="col">TAHAPAN PENGURUSAN</th>
			<th scope="col">LUNAS</th>
			<th scope="col">ANGSURAN</th>
			<th scope="col">TANGGAL</th>
			<th scope="col">PSBDT</th>
			<th scope="col">Tanggal PSBDT</th>
			<th scope="col">PEMBATALAN</th>
			<th scope="col">Tanggal PEMBATALAN</th>
			<th scope="col">SISA UTANG</th>
			<th scope="col">KET</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<script type="text/javascript">
	$('#selectAll').click(function(e){
		var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
		// $('#server-side-0').DataTable().draw(false);
    	event.stopPropagation();
	});
</script>