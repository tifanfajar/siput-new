<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan</label>
						<input type="text" class="form-control" id="dno_client" name="no_client" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan</label>
						<input type="number" class="form-control" id="dnilai_penyerahan" name="nilai_penyerahan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan</label>
						<input id="dtahun_pelimpahan" type="year" class="form-control" name="tahun_pelimpahan" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan</label>
						<input type="text" class="form-control" id="dtahapan_pengurusan" name="tahapan_pengurusan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="text" class="form-control" id="dlunas" name="lunas" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="number" class="form-control" id="dangsuran" name="angsuran" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal Bayar</label>
						<input type="date" class="form-control" id="dtanggal" name="tanggal" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="text" class="form-control" id="dpsbdt" name="psbdt" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="text" class="form-control" id="dpembatalan" name="pembatalan" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="text" class="form-control" id="dsisa_piutang" name="sisa_piutang" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">Tanggal PSBDT </label>
						<input type="date" class="form-control" name="tanggal_psbdt" id="dtanggal_psbdt" required disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Tanggal Pembatalan </label>
						<input type="date" class="form-control" name="tanggal_pembatalan" id="dtanggal_pembatalan" required disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" id="dketerangan" name="keterangan" rows="3" disabled></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" class="form-control" id="did_prov" name="id_prov" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT*</label>
						<input type="text" class="form-control" id="did_upt" name="id_upt" disabled>
					</div>
					@endif
					<div class="col-md-12 form-group">
						<label for="city">Nama KPKNL</label>
						<input type="text" class="form-control" id="dnama_kpknl" name="nama_kpknl" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>