@extends('dev.core.using')
@section('content')

@include('templates.pelayanan.penanganan-piutang.core.add')
@include('templates.pelayanan.penanganan-piutang.core.detail')
@include('templates.pelayanan.penanganan-piutang.core.edit')
@include('templates.pelayanan.penanganan-piutang.helpers.delete')
@include('templates.pelayanan.penanganan-piutang.helpers.print-search')
@include('templates.pelayanan.penanganan-piutang.helpers.unduh-search')
@include('templates.pelayanan.penanganan-piutang.rejected.detail')
@include('templates.pelayanan.penanganan-piutang.rejected.edit')
@include('templates.pelayanan.penanganan-piutang.approval.detail')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.helpers.filter')
@include('templates.pelayanan.penanganan-piutang.dev.data')


<style>
	#example_filter{position: absolute; right: 20px;}
	.chart{height: unset !important;}
	#example_wrapper{
		width: 2859px!important
	}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
	div.dataTables_wrapper div.dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 160px;
		margin-left: -100px;
		margin-top: -26px;
		text-align: center;
		color: #fff;
		padding: 1em 0;
		background-color: #17457beb;
		/* border: 1px solid #000; */
		border-radius: 59px;
	}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.pelayanan.penanganan-piutang.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.pelayanan.penanganan-piutang.chart')
	@else
	@include('templates.helpers.pelayanan.penanganan-piutang.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
		@include('dev.helpers.button.btnFilterData')
	</div>
	<div class="col-xl-12 mb-5 mb-xl-0">
		<form method="POST" action="{{route('penanganan-piutang-approved')}}" enctype="multipart/form-data" onsubmit="if($('.box-check:checkbox:checked').length <= 0) { alert('Silahkan memilih data terlebih dahulu'); return false;}">
			@csrf
			<div class="card shadow" style="border:1px solid #dedede;">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;MENUNGGU PERSETUJUAN</h3>
						</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.pelayanan.penanganan-piutang.core.approval-table')
				</div>
			</div>
		</div>
		@if($getStatus == 'kepala-upt')
		<div class="col text-right" style="padding-top: 10px;">
			<button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
			&nbsp;
			<button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
		</div>
		@endif
	</form>
</div>

@if($getStatus == 'operator')
<div class="col-xl-12 mb-5 mb-xl-0">
	<br>
	<br>
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header border-0" style="background-color: #5f5f5f;">
			<div class="row align-items-center">
				<div class="col">
					<h3 class="mb-0" style="color: #fff;"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;BERKAS DITOLAK</h3>
				</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.pelayanan.penanganan-piutang.core.reject-table')
				</div>
			</div>
		</div>
	</div>
@endif

	<div class="col-xl-12 mb-5 mb-xl-0">
		<br>
		<br>
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;PENANGANAN PIUTANG</h3>
					</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.pelayanan.penanganan-piutang.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.pelayanan.penanganan-piutang.js')

@endsection