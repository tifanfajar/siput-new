<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">EDIT PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="editid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan *</label>
						<select name="no_client" id="eno_client" class="form-control" required>
							@foreach(\App\Model\Setting\Perusahaan::all() as $value)
							<option value="{{$value->id}}">{{$value->name}}&nbsp;({{$value->company_id}})</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan *</label>
						<input type="number" id="editnilai_penyerahan" class="form-control" name="nilai_penyerahan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan *</label>
						<input id="etahun_pelimpahan" type="year" class="form-control" name="tahun_pelimpahan" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan *</label>
						<input type="text" class="form-control" id="edittahapan_pengurusan" name="tahapan_pengurusan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="text" class="form-control" id="editlunas" name="lunas">
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="number" class="form-control" id="editangsuran" name="angsuran">
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal Bayar</label>
						<input type="date" class="form-control" id="edittanggal" name="tanggal">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="number" class="form-control" id="editpsbdt" name="psbdt">
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="text" class="form-control" id="editpembatalan" name="pembatalan">
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="number" class="form-control" id="editsisa_piutang" name="sisa_piutang">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">Tanggal PSBDT </label>
						<input type="date" class="form-control" name="tanggal_psbdt" id="etanggal_psbdt">
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Tanggal Pembatalan </label>
						<input type="date" class="form-control" name="tanggal_pembatalan" id="etanggal_pembatalan">
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" id="editketerangan" name="keterangan" rows="3"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi *</label>
						<input type="text" class="form-control" id="editid_prov" name="id_prov" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT*</label>
						<input type="text" class="form-control" id="editid_upt" name="id_upt" disabled>
					</div>
					@endif
					<div class="col-md-12 form-group">
						<label for="city">Nama KPKNL</label>
						<input type="text" class="form-control" id="editnama_kpknl" name="nama_kpknl" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>