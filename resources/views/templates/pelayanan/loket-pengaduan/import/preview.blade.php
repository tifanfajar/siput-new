@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
		<form class="row" action="{{route('loket-pengaduan-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="route" id="route" value="{{url('pelayanan/loket-pengaduan')}}">    
			@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>			
			@endif
		<!-- <label for="city">Nama Perusahaan</label>
		<select name="nama_perusahaan" id="nama_perusahaan" class="form-control" style="margin-bottom: 20px">
			<option value="" selected disabled>Pilih Perusahaan</option>			
			@foreach(\App\Model\Setting\Perusahaan::all() as $data)
			<option value="{{$data->id}}">{{$data->name}}</option>
			@endforeach			
		</select>
		<label for="tujuan">Tujuan</label>
		<select name="tujuan" id="tujuan" class="form-control" style="margin-bottom: 20px">
			<option value="" selected disabled>Pilih Tujuan</option>			
			@foreach(\App\Model\Refrension\Tujuan::all() as $value)
			<option value="{{ $value->id }}">
				{{ $value->tujuan }}
			</option>
			@endforeach		
		</select> -->

		<div class="table-responsive">
			<table class="table" style="width: 1660px;">

				<thead>
					<tr>
						<th scope="col">Tanggal</th>
						<th scope="col">Nama</th>
						<th scope="col">Jabatan</th>
						<th scope="col">No.Telp</th>
						<th scope="col">No.HP</th>
						<th scope="col">Email</th>
						<th scope="col">Keperluan</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Lampiran</th>
						<th scope="col">Status</th>
						<th scope="col">Tujuan</th>
						<th scope="col">Perusahaan</th>
					</tr>
				</thead>
				<tbody>
					@foreach($result as $loket => $lokets)
					<tr>
						<td>
							<input type="date" class="form-control" id="tanggal_pelayanan" name="tanggal_pelayanan[]" value="{{$lokets[0]}}" placeholder="Masukan Tanggal Pelayanan" data-validation="date">
						</td>
						<td>
							<input type="text" class="form-control" id="nama_pengunjung" name="nama_pengunjung[]" value="{{$lokets[1]}}" placeholder="Masukan Nama Pengunjung" data-validation="message">
						</td>
						<td>
							<input type="text" class="form-control" id="jabatan_pengunjung" name="jabatan_pengunjung[]" value="{{$lokets[2]}}" placeholder="Masukan Jabatan Pengunjung" data-validation="message">
						</td>
						<td>
							<input type="text" class="form-control" id="no_telp" name="no_telp[]" value="{{$lokets[3]}}" placeholder="Masukan No. Telepon" data-validation="number">
						</td>
						<td>
							<input type="text" class="form-control" id="no_hp" name="no_hp[]" value="{{$lokets[4]}}" placeholder="Masukan No. HP" data-validation="number">
						</td>
						<td>
							<input type="email" class="form-control" id="email" name="email[]" value="{{$lokets[5]}}" placeholder="Masukan Email" data-validation="email">
						</td>
						<td>
							<input type="text" class="form-control" id="keperluan" name="keperluan[]" value="{{$lokets[6]}}" placeholder="Masukan Keperluan" data-validation="message">
						</td>
						<td>
							<input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$lokets[7]}}" placeholder="Masukan Keterangan" data-validation="message">
						</td>
						<td>
							<input type="file" class="form-control" id="lampiran" name="lampiran[]" placeholder="Masukan File">
						</td>
						<td style="width: 120px;">
							<select name="active[]" id="active" class="form-control _select_pengaduan_" style="{{ $lokets[8] != 'Pengaduan' ? 'display: none;' : null }}margin-bottom: 20px">
								<option value="0" selected>Pilih Status</option>			
								<option value="1" {{ ucfirst($lokets[10]) == 'Open' ? 'selected' : ''}}>Open</option>
								<option value="0" {{ ucfirst($lokets[10]) == 'Close' ? 'selected' : ''}}>Close</option>
								<option value="3" {{ ucfirst($lokets[10]) == 'Re-Open' ? 'selected' : ''}}>Re-Open</option>
							</select>
						</td>
						<td>
							<!-- <input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$lokets[8]}}" placeholder="Masukan Keterangan" data-validation="message"> -->
							<select name="tujuan[]" id="tujuan" class="form-control tujuan" style="margin-bottom: 20px" required>
								<option value="">Pilih Tujuan</option>			
								@foreach(\App\Model\Refrension\Tujuan::all() as $value)
								<option value="{{$value->id}}" {{ $value->tujuan == $lokets[8] ? 'selected' : ''}}>
									{{ $value->tujuan }}
								</option>
								@endforeach		
							</select>
						</td>
						<td style="width: 170px;">
							<input type="text" class="form-control" id="keterangan" name="nama_perusahaan[]" value="{{$lokets[9]}}" placeholder="Masukan Keterangan" data-validation="message">
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
		<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
	</form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
	});
	$('.tujuan').change(function(event) {
		var select_pengaduan = $(this).closest('tr').find('._select_pengaduan_');
		var tujuan = $(this);
		if (tujuan.val() == '1') {
			select_pengaduan.show();
		}
		else {
			select_pengaduan.hide();	
		}
		console.log(tujuan.val());
	});
</script>

@endsection

