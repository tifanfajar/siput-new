<style>
	.form-check {
	    position: relative;
	    display: block;
	    padding-left: 1.25rem;
	    float: left !important;
	    width: 100%;
	    margin-bottom: 7px !important;
	}
	.form-check-label {
	    margin-bottom: 0;
	    float: left;
	}
	.calBox{
		border-radius: 5px;
		border:1px solid #dedede;
	}
</style>
<div class="modal fade" id="downloadIndexSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="searchIndexPrint">DOWNLOAD AS XLS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden; padding: 10px;">
				<center>
				<form action="{{route('loket-pengaduan-export-search',$changeUPT)}}" method="GET" enctype="multipart/enctype">
					@csrf
					<center>
						<center>
							<div class="row">
								<div class="col-md-6 form-group">
									<label for="date">Dari</label>
									<input type="date" class="form-control" id="fromDownload" name="dari">
								</div>
								<div class="col-md-6 form-group">
									<label for="date">Sampai</label>
									<input type="date" class="form-control" id="toDownload" name="sampai">
								</div>
							</div>
						</center>
						<br>
					<center>
						@include('dev.helpers.button.btnExport')
						@include('dev.helpers.button.btnBatal')
					</center>
				</form>
			</center>
			</div>
		</div>
	</div>
</div>
@include('dev.helpers.jquery')

<script type="text/javascript">
	$("#fromDownload")
	.on('input', function() {
		var activeFee = (this.value === '0' || this.value === '') ? false : true;
		$('#toDownload').prop('required', activeFee);
	})
	.trigger('input');
</script>

<script type="text/javascript">
	$("#toDownload")
	.on('input', function() {
		var activeFee = (this.value === '0' || this.value === '') ? false : true;
		$('#fromDownload').prop('required', activeFee);
	})
	.trigger('input');
</script>
<div class="modal fade" id="downloadIndexSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/excel.png" alt="" style="width: 60px; height: 50px; padding-right: 10px; ">
				<h5 class="modal-title" id="downloadIndexSearch" style="margin-top: 15px;">Unduh Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>			
			<div class="modal-body">
				<center>
				<a class="btn btn-success" style="color: white;" href="{{route('loket-pengaduan-export-search',$changeUPT)}}"><i class="ni ni-cloud-download-95"></i>&nbsp;Unduh</a>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</button>
				</center>
		</div>
	</div>
</div>
</div>