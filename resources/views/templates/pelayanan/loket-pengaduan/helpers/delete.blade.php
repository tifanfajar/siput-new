<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">HAPUS DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{route('loket-pengaduan-delete')}}" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="dmid">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>