<style>
	#example_wrapper{width: 2749px !important;}
</style>
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<!-- @if($getStatus == 'kepala-upt')
			<th><input type="checkbox" id="selectAll" /></th>
			@endif -->
			<th scope="col">Tahun</th>
			<th scope="col">Bulan</th>
			<th scope="col">Tanggal</th>
			<th scope="col">Nama UPT</th>
			<th scope="col">Nama</th>
			<th scope="col">Jabatan</th>
			<th scope="col">Nama Perusahaan</th>
			<th scope="col">No.Telp</th>
			<th scope="col">No.HP</th>
			<th scope="col">Email</th>
			<th scope="col">Keperluan</th>
			<th scope="col">Keterangan</th>
			<th scope="col">Tujuan</th>
			<th scope="col">Lampiran</th>
			<th scope="col">Status</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($loketpengaduan as $value)
		@if($value->status == 1)
		<tr>
			<!-- @if($getStatus == 'kepala-upt')
			<td><input type="checkbox" name="id[]" value="{{ $value->id }}"></td>
			@endif -->
			<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
				->format('Y')}}</td>
				<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
					->format('M')}}</td>
					<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
						->format('d, M Y')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<td>{{\App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name')}}</td>
						<td>{{$value->nama_pengunjung}}</td>
						<td>{{$value->jabatan_pengunjung}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>{{$value->no_telp}}</td>
						<td>{{$value->no_hp}}</td>
						<td>{{$value->email}}</td>
						<td>{{$value->keperluan}}</td>
						<td>{{$value->keterangan}}</td>
						<td>{{\App\Model\Refrension\Tujuan::where('id',$value->tujuan)->value('tujuan')}}</td>
						<td>@if($value->lampiran != null)<a href="{{route('loket-pengaduan-lampiran',$value->lampiran)}}" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>@endif</td>
						@if($value->tujuan == 1)
						<td>
							@if($value->active == 1) OPEN @endif 
							@if($value->active == 0) CLOSE @endif
							@if($value->active == 3) RE-OPEN @endif
						</td>
						@else
						<td>
							-
						</td>
						@endif
						<td>
							<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"
							data-perusahaan = "{{$value->nama_perusahaan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-upt = "{{\App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name')}}"
							data-tujuan = "{{\App\Model\Refrension\Tujuan::where('id',$value->tujuan)->value('tujuan')}}"
							data-provinsi = "{{\App\Model\Region\Provinsi::where('id_row',$value->id_prov)->value('nama')}}"
							data-keperluan = "{{$value->keperluan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-tanggal-pelayanan = "{{$value->tanggal_pelayanan}}"
							data-nama-pengunjung = "{{$value->nama_pengunjung}}"
							data-jabatan-pengunjung = "{{$value->jabatan_pengunjung}}"
							data-email = "{{$value->email}}"
							data-no-hp = "{{$value->no_hp}}"
							data-no-telp = "{{$value->no_telp}}"
							data-active = "{{$value->active}}"
							data-keterangan = "{{$value->keterangan}}"
							data-created-by ="{{Auth::user()->where('id', $value->created_by)->value('name')}}"
							><i class="fa fa-file"></i></a>

							<a href="#" data-toggle="modal" data-target="#editModal" 
							data-id="{{$id}}" id="getEdit"
							data-perusahaan = "{{$value->nama_perusahaan}}"
							data-id-upt = "{{$value->kode_upt}}"
							data-upt = "{{\App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name')}}"
							data-tujuan-id = "{{$value->tujuan}}"
							data-tujuan = "{{\App\Model\Refrension\Tujuan::where('id',$value->tujuan)->value('tujuan')}}"
							data-id-provinsi = "{{$value->id_prov}}"
							data-provinsi = "{{\App\Model\Region\Provinsi::where('id_row',$value->id_prov)->value('nama')}}"
							data-keperluan = "{{$value->keperluan}}"
							data-lampiran = "{{$value->lampiran}}"
							data-tanggal-pelayanan = "{{$value->tanggal_pelayanan}}"
							data-nama-pengunjung = "{{$value->nama_pengunjung}}"
							data-jabatan-pengunjung = "{{$value->jabatan_pengunjung}}"
							data-email = "{{$value->email}}"
							data-no-hp = "{{$value->no_hp}}"
							data-no-telp = "{{$value->no_telp}}"
							data-active = "{{$value->active}}"
							data-keterangan = "{{$value->keterangan}}"
							class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>

							<a  class="btn btn-danger btn-sm" 
							id="getDelete" 
							data-toggle="modal" 
							data-target="#deleteModal" 
							data-id="{{$id}}"><i class="fa fa-trash"></i>
						</a>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
<!-- <script type="text/javascript">
	$('#selectAll').click(function(e){
		var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
	});
</script> -->