<div class="modal fade" id="detailModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL LOKET PELAYANAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="tujuan">Perusahaan</label><label style="color: red;">*</label>
						<input type="text" name="nama_perusahaan" id="dnama_perusahaan" class="form-control" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tujuan">Tujuan</label><label style="color: red;">*</label>
						<input type="text" name="tujuan" id="dtujuan" class="form-control" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="keperluan">Keperluan</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="dkeperluan" name="keperluan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal_pelayanan">Tanggal Pelayanan</label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="dtanggal_pelayanan" name="tanggal_pelayanan" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="dlampiran" name="lampiran" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="nama_pengunjung">Nama Pengunjung</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="dnama_pengunjung" name="nama_pengunjung" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="jabatan_pengunjung">Jabatan Pengunjung</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="djabatan_pengunjung" name="jabatan_pengunjung" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="email">Email </label><label style="color: red;">*</label>
						<input type="email" class="form-control" id="demail" name="email" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="no_hp">No. HP </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="dno_hp" name="no_hp" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="no_telp">No. Telp </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="dno_telp" name="no_telp" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label><label style="color: red;">*</label>
						<textarea class="form-control" id="dketerangan" name="keterangan" rows="3" disabled></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label><label style="color: red;">*</label>
						<input type="text" name="prov" id="dprovinsi" class="form-control" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label><label style="color: red;">*</label>
						<input type="text" name="kode_upt" id="dupt" class="form-control" disabled>
					</div>
					@endif
					<div class="col-md-12 form-group">
						<label for="active">Status </label><label style="color: red;">*</label>
						<input type="text" name="active" id="dactive" class="form-control" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="created_by">Dibuat Oleh </label>
						<input type="text" name="created_by" id="dcreated_by" class="form-control" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>