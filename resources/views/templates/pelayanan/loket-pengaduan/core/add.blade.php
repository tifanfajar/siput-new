<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="modal fade" id="addModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH LOKET PENGADUAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('loket-pengaduan-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukan Nama Perusahaan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tujuan">Tujuan</label><label style="color: red;">*</label>
						<select name="tujuan" id="tujuan" class="form-control select2">
							<option value="" selected disabled>Pilih Tujuan</option>
							@foreach(\App\Model\Refrension\Tujuan::all() as $value)
							<option value="{{ $value->id }}">
								{{ $value->tujuan }}
							</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="keperluan">Keperluan</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="ekeperluan" name="keperluan" placeholder="Masukan Keperluan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal_pelayanan">Tanggal Pelayanan</label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="tanggal_pelayanan" name="tanggal_pelayanan" placeholder="Masukan Tanggal" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran</label>
						<input type="file" class="form-control" name="lampiran">
					</div>
					<div class="col-md-12 form-group">
						<label for="nama_pengunjung">Nama Pengunjung</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="nama_pengunjung" name="nama_pengunjung" placeholder="Masukan Nama" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="jabatan_pengunjung">Jabatan Pengunjung</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="jabatan_pengunjung" name="jabatan_pengunjung" placeholder="Masukan Jabatan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="email">Email </label><label style="color: red;">*</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Masukan Email" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="no_hp">No. HP </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="no_hp" name="no_hp" placeholder="Masukan No. HP" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="no_telp">No. Telp </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="no_telp" name="no_telp" placeholder="Masukan No. Telepon" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan</label><label style="color: red;">*</label>
						<textarea class="form-control" id="eketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label><label style="color: red;">*</label>
						<select class="form-control" id="id_prov" name="id_prov" required>
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label><label style="color: red;">*</label>
						<select name="kode_upt" id="kode_upt" class="form-control" >
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="col-md-12 form-group">
						<label for="active">Status ( Isi Bila Tujuan Pengaduan )</label>
						<select class="form-control select2" id="active" name="active" required>
							<option selected disabled>PILIH SALAH SATU</option>
							<option value="0">CLOSE</option>
							<option value="1">OPEN</option>
							<option value="3">RE-OPEN</option>
						</select>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>