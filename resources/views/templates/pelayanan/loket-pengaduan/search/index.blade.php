<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.pelayanan.loket-pengaduan.core.add')
@include('templates.pelayanan.loket-pengaduan.core.detail')
@include('templates.pelayanan.loket-pengaduan.core.edit')
@include('templates.pelayanan.loket-pengaduan.helpers.delete')
@include('templates.pelayanan.loket-pengaduan.helpers.print-search')
@include('templates.pelayanan.loket-pengaduan.helpers.unduh-search')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.pelayanan.loket-pengaduan.dev.data')


<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.pelayanan.loket-pengaduan.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@include('templates.helpers.pelayanan.loket-pengaduan.chart-search')
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		$getRead = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('read_acl');
		$getRead = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('read_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@if($getRead == $getKdModule)
		<a class="btn btn-sm btn-info" style="color: white;" data-toggle="modal" data-target="#searchIndexPrint"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
		<a class="btn btn-sm btn-danger" style="color: white;" data-toggle="modal" data-target="#downloadIndexSearch"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
		@endif
		<a href="{{route('pelayanan/loket-pengaduan')}}" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i>&nbsp;SHOW ALL</a>
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;LOKET PENGADUAN</h3>
					</div>
				</div>
			</div>
			<div class="">
				<!-- Projects table -->
				<div class="">
					@include('templates.pelayanan.loket-pengaduan.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.pelayanan.loket-pengaduan.js-search')

@endsection