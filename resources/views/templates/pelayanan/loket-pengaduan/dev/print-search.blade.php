<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Loket Pengaduan</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center">
				<thead class="thead-light">
					<tr>
						<th scope="col">Tahun</th>
						<th scope="col">Bulan</th>
						<th scope="col">Tanggal</th>
						<th scope="col">Nama UPT</th>
						<th scope="col">Nama</th>
						<th scope="col">Jabatan</th>
						<th scope="col">Nama Perusahaan</th>
						<th scope="col">No.Telp</th>
						<th scope="col">No.HP</th>
						<th scope="col">Email</th>
						<th scope="col">Keperluan</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Tujuan</th>
						<th scope="col">Lampiran</th>
						<th scope="col">Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($loketpengaduan as $value)
					<tr>
						<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
							->format('Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
							->format('M')}}</td>
						<td>{{\Carbon\Carbon::parse($value->tanggal_pelayanan)
								->format('d, M Y')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<td>{{\App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name')}}</td>
						<td>{{$value->nama_pengunjung}}</td>
						<td>{{$value->jabatan_pengunjung}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>{{$value->no_telp}}</td>
						<td>{{$value->no_hp}}</td>
						<td>{{$value->email}}</td>
						<td>{{$value->keperluan}}</td>
						<td>{{$value->keterangan}}</td>
						<td>{{\App\Model\Refrension\Tujuan::where('id',$value->tujuan)->value('tujuan')}}</td>
						<td>{{$value->lampiran}}</td>
						@if($value->active == 1)
						<td>Open</td>
						@elseif($value->active == 3)
						<td>Re-Open</td>
						@else
						<td>Close</td>
						@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			<hr>
		</div>
	</center>
</body>
</html>