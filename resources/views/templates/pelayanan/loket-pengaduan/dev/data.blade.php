@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kode_upt').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();	
		$('#detailApproval').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#danama_perusahaan').val(perusahaan);

			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			$('#datujuan').val(tujuan);

			var keperluan=$(e.relatedTarget).attr('data-keperluan');
			$('#dakeperluan').val(keperluan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dalampiran').val(lampiran);

			var tanggal_pelayanan=$(e.relatedTarget).attr('data-tanggal-pelayanan');
			$('#datanggal_pelayanan').val(tanggal_pelayanan);

			var nama_pengunjung=$(e.relatedTarget).attr('data-nama-pengunjung');
			$('#danama_pengunjung').val(nama_pengunjung);

			var jabatan_pengunjung=$(e.relatedTarget).attr('data-jabatan-pengunjung');
			$('#dajabatan_pengunjung').val(jabatan_pengunjung);

			var email=$(e.relatedTarget).attr('data-email');
			$('#daemail').val(email);

			var no_hp=$(e.relatedTarget).attr('data-no-hp');
			$('#dano_hp').val(no_hp);

			var no_telp=$(e.relatedTarget).attr('data-no-telp');
			$('#dano_telp').val(no_telp);

			var prov=$(e.relatedTarget).attr('data-provinsi');
			$('#daprovinsi').val(prov);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#daupt').val(upt);

			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dacreated_by').val(created_by);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#daactive').val('Open');
			}
			else if(active == 3){
				$('#daactive').val('Re-Open');
			}
			else{
				$('#daactive').val('Close');
			}
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();	
		$('#detailModal').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#dnama_perusahaan').val(perusahaan);

			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			$('#dtujuan').val(tujuan);

			var keperluan=$(e.relatedTarget).attr('data-keperluan');
			$('#dkeperluan').val(keperluan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dlampiran').val(lampiran);

			var tanggal_pelayanan=$(e.relatedTarget).attr('data-tanggal-pelayanan');
			$('#dtanggal_pelayanan').val(tanggal_pelayanan);

			var nama_pengunjung=$(e.relatedTarget).attr('data-nama-pengunjung');
			$('#dnama_pengunjung').val(nama_pengunjung);

			var jabatan_pengunjung=$(e.relatedTarget).attr('data-jabatan-pengunjung');
			$('#djabatan_pengunjung').val(jabatan_pengunjung);

			var email=$(e.relatedTarget).attr('data-email');
			$('#demail').val(email);

			var no_hp=$(e.relatedTarget).attr('data-no-hp');
			$('#dno_hp').val(no_hp);

			var no_telp=$(e.relatedTarget).attr('data-no-telp');
			$('#dno_telp').val(no_telp);

			var prov=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(prov);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#dupt').val(upt);

			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#dcreated_by').val(created_by);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#dactive').val('Open');
			}
			else if(active == 3){
				$('#dactive').val('Re-Open');
			}
			else{
				$('#dactive').val('Close');
			}
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();	
		$('#detailReject').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#drnama_perusahaan').val(perusahaan);

			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			$('#drtujuan').val(tujuan);

			var keperluan=$(e.relatedTarget).attr('data-keperluan');
			$('#drkeperluan').val(keperluan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#drlampiran').val(lampiran);

			var tanggal_pelayanan=$(e.relatedTarget).attr('data-tanggal-pelayanan');
			$('#drtanggal_pelayanan').val(tanggal_pelayanan);

			var nama_pengunjung=$(e.relatedTarget).attr('data-nama-pengunjung');
			$('#drnama_pengunjung').val(nama_pengunjung);

			var jabatan_pengunjung=$(e.relatedTarget).attr('data-jabatan-pengunjung');
			$('#drjabatan_pengunjung').val(jabatan_pengunjung);

			var email=$(e.relatedTarget).attr('data-email');
			$('#dremail').val(email);

			var no_hp=$(e.relatedTarget).attr('data-no-hp');
			$('#drno_hp').val(no_hp);

			var no_telp=$(e.relatedTarget).attr('data-no-telp');
			$('#drno_telp').val(no_telp);

			var prov=$(e.relatedTarget).attr('data-provinsi');
			$('#drprovinsi').val(prov);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#drupt').val(upt);

			var created_by=$(e.relatedTarget).attr('data-created-by');
			$('#drcreated_by').val(created_by);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#dractive').val('Open');
			}
			else if(active == 3){
				$('#dractive').val('Re-Open');
			}
			else{
				$('#dractive').val('Close');
			}
			
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#drketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();	
		$('#editModal').on('show.bs.modal', function (e) {	
			var id=$(e.relatedTarget).attr('data-id');
			$('#editid').val(id);

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#enama_perusahaan').val(perusahaan);

			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			var idtujuan=$(e.relatedTarget).attr('data-tujuan-id');
			$('#etujuan').append('<option value="'+ idtujuan +'" selected>'+ tujuan +'(Sebelumnya)</option>');

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#editketerangan').val(keterangan);

			var keperluan=$(e.relatedTarget).attr('data-keperluan');			
			$('#editkeperluan').val(keperluan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#editlampiran').val(lampiran);

			var tanggal_pelayanan=$(e.relatedTarget).attr('data-tanggal-pelayanan');
			$('#etanggal_pelayanan').val(tanggal_pelayanan);

			var nama_pengunjung=$(e.relatedTarget).attr('data-nama-pengunjung');
			$('#enama_pengunjung').val(nama_pengunjung);

			var jabatan_pengunjung=$(e.relatedTarget).attr('data-jabatan-pengunjung');
			$('#ejabatan_pengunjung').val(jabatan_pengunjung);

			var email=$(e.relatedTarget).attr('data-email');
			$('#eemail').val(email);

			var no_hp=$(e.relatedTarget).attr('data-no-hp');
			$('#eno_hp').val(no_hp);

			var no_telp=$(e.relatedTarget).attr('data-no-telp');
			$('#eno_telp').val(no_telp);

			var prov=$(e.relatedTarget).attr('data-provinsi');
			$('#eprovinsi').val(prov);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#eupt').val(upt);

			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#eactive').append('<option value="'+ active +'" selected> Open (Sebelumnya) </option>');
			}
			else if(active == 3){
				$('#eactive').append('<option value="'+ active +'" selected> Re-Open (Sebelumnya) </option>');
			}
			else{
				$('#eactive').append('<option value="'+ active +'" selected> Close (Sebelumnya) </option>');;
			}	
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();	
		$('#editReject').on('show.bs.modal', function (e) {	
			var id=$(e.relatedTarget).attr('data-id');
			$('#erid').val(id);

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			var idperusahaan=$(e.relatedTarget).attr('data-perusahaan-id');
			$('#erperusahaan').append('<option value="'+ idperusahaan +'" selected>'+ perusahaan +'(Sebelumnya)</option>');

			var tujuan=$(e.relatedTarget).attr('data-tujuan');
			var idtujuan=$(e.relatedTarget).attr('data-tujuan-id');
			$('#ertujuan').append('<option value="'+ idtujuan +'" selected>'+ tujuan +'(Sebelumnya)</option>');

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erketerangan').val(keterangan);

			var keperluan=$(e.relatedTarget).attr('data-keperluan');			
			$('#erkeperluan').val(keperluan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#erlampiran').val(lampiran);

			var tanggal_pelayanan=$(e.relatedTarget).attr('data-tanggal-pelayanan');
			$('#ertanggal_pelayanan').val(tanggal_pelayanan);

			var nama_pengunjung=$(e.relatedTarget).attr('data-nama-pengunjung');
			$('#ernama_pengunjung').val(nama_pengunjung);

			var jabatan_pengunjung=$(e.relatedTarget).attr('data-jabatan-pengunjung');
			$('#erjabatan_pengunjung').val(jabatan_pengunjung);

			var email=$(e.relatedTarget).attr('data-email');
			$('#eremail').val(email);

			var no_hp=$(e.relatedTarget).attr('data-no-hp');
			$('#erno_hp').val(no_hp);

			var no_telp=$(e.relatedTarget).attr('data-no-telp');
			$('#erno_telp').val(no_telp);

			var prov=$(e.relatedTarget).attr('data-provinsi');
			$('#erprovinsi').val(prov);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#erupt').val(upt);

			var active=$(e.relatedTarget).attr('data-active');
			var active=$(e.relatedTarget).attr('data-active');
			if(active == 1){
				$('#eractive').append('<option value="'+ active +'" selected> Open (Sebelumnya) </option>');
			}
			else if(active == 3){
				$('#eractive').append('<option value="'+ active +'" selected> Re-Open (Sebelumnya) </option>');
			}
			else{
				$('#eractive').append('<option value="'+ active +'" selected> Close (Sebelumnya) </option>');;
			}	
		});
	})
</script>