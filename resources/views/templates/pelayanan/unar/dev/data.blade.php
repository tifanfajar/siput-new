@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<!-- <script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>
 -->
<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<!-- EDIT -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);

			var prov=$(e.relatedTarget).attr('data-prov');
			$('#eprovinsi').val(prov);

			var nama_upt=$(e.relatedTarget).attr('data-upt');
			$('#eupt').val(nama_upt);

			var id_unar=$(e.relatedTarget).attr('data-id-unar');
			$('#eid_unar').val(id_unar);

			var tipe=$(e.relatedTarget).attr('data-tipe');
			if(tipe == 1){
				$('#etipe').append('<option value="'+ tipe +'" selected> REGULER (Sebelumnya) </option>');
			}
			else if(tipe == 0){
				$('#etipe').append('<option value="'+ tipe +'" selected> NON-REGULER (Sebelumnya) </option>');
			}
			else{
				$('#etipe').val(null);
			}	

			var tanggal_ujian=$(e.relatedTarget).attr('data-tanggal-ujian');
			$('#etanggal_ujian').val(tanggal_ujian);

			var lokasi=$(e.relatedTarget).attr('data-lokasi');
			$('#elokasi').val(lokasi);

			var jumlah_siaga=$(e.relatedTarget).attr('data-jumlah-siaga');
			$('#ejumlah_siaga').val(jumlah_siaga);

			var jumlah_penggalang=$(e.relatedTarget).attr('data-jumlah-penggalang');
			$('#ejumlah_penggalang').val(jumlah_penggalang);

			var jumlah_penegak=$(e.relatedTarget).attr('data-jumlah-penegak');
			$('#ejumlah_penegak').val(jumlah_penegak);

			var lulus_siaga=$(e.relatedTarget).attr('data-lulus-siaga');
			$('#elulus_siaga').val(lulus_siaga);

			var lulus_penggalang=$(e.relatedTarget).attr('data-lulus-penggalang');
			$('#elulus_penggalang').val(lulus_penggalang);

			var lulus_penegak=$(e.relatedTarget).attr('data-lulus-penegak');
			$('#elulus_penegak').val(lulus_penegak);

			var tdk_lulus_siaga=$(e.relatedTarget).attr('data-tidak-lulus-siaga');
			$('#etdk_lulus_siaga').val(tdk_lulus_siaga);

			var tdk_lulus_penggalang=$(e.relatedTarget).attr('data-tidak-lulus-penggalang');
			$('#etdk_lulus_penggalang').val(tdk_lulus_penggalang);

			var tdk_lulus_penegak=$(e.relatedTarget).attr('data-tidak-lulus-penegak');
			$('#etdk_lulus_penegak').val(tdk_lulus_penegak);
			
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#elampiran').val(lampiran);
			
			var full_cat=$(e.relatedTarget).attr('data-full-cat');
			if(full_cat == 1){
				$('#efull_cat').append('<option value="'+ full_cat +'" selected> IYA (Sebelumnya) </option>');
			}
			else if(full_cat == 0){
				$('#efull_cat').append('<option value="'+ full_cat +'" selected> TIDAK (Sebelumnya) </option>');
			}
			else{
				$('#efull_cat').val(null);
			}	
		});
	})
</script>

<!-- DETAIL -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#dupt').val(upt);			

			var id_unar=$(e.relatedTarget).attr('data-id-unar');
			$('#did_unar').val(id_unar);

			var tipe=$(e.relatedTarget).attr('data-tipe');
			if (tipe == 1) {
				$('#dtipe').val('REGULER');
			}else if (tipe == 0){
				$('#dtipe').val('NON-REGULER');
			}else{
				$('#dtipe').val(null);
			}

			var tanggal_ujian=$(e.relatedTarget).attr('data-tanggal-ujian');
			$('#dtanggal_ujian').val(tanggal_ujian);

			var lokasi=$(e.relatedTarget).attr('data-lokasi');
			$('#dlokasi').val(lokasi);

			var jumlah_siaga=$(e.relatedTarget).attr('data-jumlah-siaga');
			$('#djumlah_siaga').val(jumlah_siaga);

			var jumlah_penggalang=$(e.relatedTarget).attr('data-jumlah-penggalang');
			$('#djumlah_penggalang').val(jumlah_penggalang);

			var jumlah_penegak=$(e.relatedTarget).attr('data-jumlah-penegak');
			$('#djumlah_penegak').val(jumlah_penegak);

			var lulus_siaga=$(e.relatedTarget).attr('data-lulus-siaga');
			$('#dlulus_siaga').val(lulus_siaga);

			var lulus_penggalang=$(e.relatedTarget).attr('data-lulus-penggalang');
			$('#dlulus_penggalang').val(lulus_penggalang);

			var lulus_penegak=$(e.relatedTarget).attr('data-lulus-penegak');
			$('#dlulus_penegak').val(lulus_penegak);

			var tdk_lulus_siaga=$(e.relatedTarget).attr('data-tidak-lulus-siaga');
			$('#dtdk_lulus_siaga').val(tdk_lulus_siaga);

			var tdk_lulus_penggalang=$(e.relatedTarget).attr('data-tidak-lulus-penggalang');
			$('#dtdk_lulus_penggalang').val(tdk_lulus_penggalang);

			var tdk_lulus_penegak=$(e.relatedTarget).attr('data-tidak-lulus-penegak');
			$('#dtdk_lulus_penegak').val(tdk_lulus_penegak);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dlampiran').val(lampiran);

			var full_cat=$(e.relatedTarget).attr('data-full-cat');
			if(full_cat == 1){
				$('#dfull_cat').val('IYA');	
			}else if(full_cat == 0){
				$('#dfull_cat').val('TIDAK');
			}else{
				$('#dfull_cat').val(null);
			}

			var created_by=$(e.relatedTarget).attr('data-pembuat');
			$('#dcreated_by').val(created_by);

			var created_at=$(e.relatedTarget).attr('data-dibuat');
			$('#dcreated_at').val(created_at);


		});
	})
</script>
<!-- END DETAIL -->
