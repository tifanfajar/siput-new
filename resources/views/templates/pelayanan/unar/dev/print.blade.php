<!DOCTYPE html>
<html>
<head>
	<style>
		.table-bordered {
	  		border-collapse: collapse;
	  		width: 100%;
		}

		.table-bordered td, .table-bordered th {
	  		border: 1px solid black;
	  		padding: 8px;
		}
		.thead-light{
			background-color: #E9ECEF;
		}
		.table-bordered th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
		}
	</style>
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Unar</h2>
			<hr>
			<table id="example" class="" style=" width: 100%; font-size: 12px;">
				<thead style="background-color: #333; color: #fff">
					<tr>
						<th rowspan="2" scope="col">NO</th>
						<th rowspan="2" scope="col">ID UNAR</th>
						<th rowspan="2" scope="col">TANGGAL DIBUAT</th>
						<th rowspan="2" scope="col">PROVINSI</th>
						<th rowspan="2" scope="col">UPT</th>
						<th rowspan="2" scope="col">TANGGAL PELAKSANAAN UJIAN</th>
						<th rowspan="2" scope="col">LOKASI UJIAN</th>
						<th rowspan="2" scope="col">TIPE UNAR</th>
						<th style="text-align: center;" colspan="3" scope="col">JUMLAH PESERTA</th>
						<th style="text-align: center;" colspan="3" scope="col">PESERTA LULUS</th>
						<th style="text-align: center;" colspan="3" scope="col">PESERTA TIDAK LULUS</th>
						<th rowspan="2" scope="col">FULL CAT</th>
					</tr>
					<tr>
						<th scope="col">SIAGA</th>
						<th scope="col">PENGGALANG</th>
						<th scope="col">PENEGAK</th>
						<th scope="col">SIAGA</th>
						<th scope="col">PENGGALANG</th>
						<th scope="col">PENEGAK</th>
						<th scope="col">SIAGA</th>
						<th scope="col">PENGGALANG</th>
						<th scope="col">PENEGAK</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($unar as $key => $value)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value->id_unar}}</td>
						<td>{{\Carbon\Carbon::parse($value->created_at)->format('d,M Y')}}</td>
						<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						<td>{{$upts}}</td>
						<td>{{$value->tanggal_ujian}}</td>
						<td>{{$value->lokasi_ujian}}</td>
						<td>
							@if($value->tipe == 1)
							REGULER
							@elseif($value->tipe == 0)
							NON-REGULER
							@else
							-
							@endif
						</td>
						<td>{{$value->jumlah_siaga}}</td>
						<td>{{$value->jumlah_penggalang}}</td>
						<td>{{$value->jumlah_penegak}}</td>
						<td>{{$value->lulus_siaga}}</td>
						<td>{{$value->lulus_penggalang}}</td>
						<td>{{$value->lulus_penegak}}</td>
						<td>{{$value->tdk_lulus_siaga}}</i></td>
						<td>{{$value->tdk_lulus_penggalang}}</td>
						<td>{{$value->tdk_lulus_penegak}}</td>
						<td style="text-align: center;">
							@if($value->full_cat == 1)
							&check;
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>