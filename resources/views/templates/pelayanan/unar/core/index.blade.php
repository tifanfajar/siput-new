@extends('dev.core.using')
@section('content')

@include('templates.pelayanan.unar.core.add')
@include('templates.pelayanan.unar.core.detail')
@include('templates.pelayanan.unar.core.edit')
@include('templates.pelayanan.unar.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.helpers.filter')
@include('templates.pelayanan.unar.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>

<style>
	#example_filter{position: absolute; right: 20px;}
	.dataTables_wrapper{overflow-y: hidden;}
	.dataTables_filter{position: absolute; right: 13px;}
	.dataTables_length{position: sticky; left: 10px;}
	.previous {color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.previous:hover{color: #fff !important; border:1px solid #fff;}
	.next{color: #87a5d8 !important; border:1px solid #87a5d8; border-radius: 20px; padding:3px;}
	.next:hover{color: #fff !important; border:1px solid #fff;}
	.processing{color: red !important;}
	div.dataTables_wrapper div.dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 160px;
		margin-left: -100px;
		margin-top: -26px;
		text-align: center;
		color: #fff;
		padding: 1em 0;
		background-color: #17457beb;
		/* border: 1px solid #000; */
		border-radius: 59px;
	}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$upts}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('templates.helpers.pelayanan.unar.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.pelayanan.unar.chart')
	@else
	@include('templates.helpers.pelayanan.unar.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
		@include('dev.helpers.button.btnFilterData')
		<a href="{{route('pelayanan/unar')}}" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i>&nbsp;SHOW ALL</a>
		<form method="POST" action="{{route('update-rencana-unar')}}" enctype="multipart/form-data" style="display: inline;">
			@csrf
			<input type="number" name="jumlah_rencana_unar" style="width: 100px!important;" {{ $getStatus == 'administrator' ? null : 'disabled' }} value="{{$unar_data->jumlah}}">
			<input type="hidden" name="id_rencana_unar" value="{{$unar_data->id}}">
			@if($getStatus == 'administrator')
			<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-upload"></i>&nbsp;UPDATE JUMLAH RENCANA</button>
			@endif
		</form>
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;UNAR</h3>
					</div>
				</div>
			</div>
			<div class="">
				<div class="">
					@include('templates.pelayanan.unar.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.pelayanan.unar.js')

@endsection