<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('unar-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi </label><label style="color: red;">*</label>
						<select class="form-control" name="id_prov" id="upt_provinsi" required>
							<option selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\Region\Provinsi::all() as $provinsis)
							<option value="{{ $provinsis->id_row }}">
								{{ $provinsis->nama }}
							</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<select name="id_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="col-md-6 form-group">
						<label for="id_unar">ID UNAR </label><label style="color: red;">*</label>
						<input type="number" class="form-control" id="id_unar" name="id_unar" placeholder="Masukan ID" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tipe">Tipe UNAR </label><label style="color: red;">*</label>
						<select class="form-control" name="tipe" id="tipe" required>
							<option selected disabled>Pilih Salah Satu</option>
							<option value="1">REGULER</option>
							<option value="0">NON-REGULER</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="tanggal_ujian">Tanggal Pelaksanaan </label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="tanggal_ujian" name="tanggal_ujian" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="lokasi_ujian">Lokasi Ujian </label><label style="color: red;">*</label>
						<input class="form-control" id="lokasi_ujian" name="lokasi_ujian" rows="3" placeholder="Masukan Lokasi" required>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Peserta Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Peserta Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_siaga" name="jumlah_siaga" rows="3" placeholder="Siaga">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_siaga" name="lulus_siaga" rows="3" placeholder="Siaga">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_siaga" name="tdk_lulus_siaga" rows="3" placeholder="Siaga">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_penggalang" name="jumlah_penggalang" rows="3" placeholder="Penggalang">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_penggalang" name="lulus_penggalang" rows="3" placeholder="Penggalang">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_penggalang" name="tdk_lulus_penggalang" rows="3" placeholder="Penggalang">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_penegak" name="jumlah_penegak" rows="3" placeholder="Penegak">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_penegak" name="lulus_penegak" rows="3" placeholder="Penegak">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_penegak" name="tdk_lulus_penegak" rows="3" placeholder="Penegak">
					</div>
					<div class="col-md-6 form-group">
						<label for="lampiran">Lampiran </label><label style="color: red;">*</label>
						<input type="file" class="form-control" id="lampiran" name="lampiran">
					</div>
					<div class="col-md-6 form-group">
						<label for="tipe">Full Cat </label><label style="color: red;">*</label>
						<select class="form-control" name="full_cat" id="full_cat" required>
							<option selected disabled>Pilih Salah Satu</option>
							<option value="1">Iya</option>
							<option value="0">Tidak</option>
						</select>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>