<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" id="dprovinsi" class="form-control" name="provinsi" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="upt">UPT</label>
						<input type="text" id="dupt" class="form-control" name="upt" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="id_unar">ID UNAR</label>
						<input type="text" id="did_unar" class="form-control" name="id_unar" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tipe">Tipe UNAR</label>
						<input type="text" id="dtipe" class="form-control" name="tipe" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="lampiran">Tanggal Pelaksanaan Ujian</label>
						<input type="date" class="form-control" id="dtanggal_ujian" name="tanggal_ujian" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="lokasi">Lokasi Ujian</label>
						<textarea class="form-control" id="dlokasi" name="deskripsi" rows="3" disabled></textarea>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Peserta Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Peserta Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_siaga" name="jumlah_siaga" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_siaga" name="lulus_siaga" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_siaga" name="tdk_lulus_siaga" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_penggalang" name="jumlah_penggalang" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_penggalang" name="lulus_penggalang" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_penggalang" name="tdk_lulus_penggalang" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_penegak" name="jumlah_penegak" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_penegak" name="lulus_penegak" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_penegak" name="tdk_lulus_penegak" rows="3" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="lampiran">Lampiran</label>
						<input type="text" id="dlampiran" class="form-control" name="lampiran" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="full_cek">Full Cat</label>
						<input type="text" id="dfull_cat" class="form-control" name="full_cat" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label>Dibuat Oleh</label>
						<input type="text" class="form-control" id="dcreated_by" name="created_by" rows="3" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label>Dibuat Pada Tanggal</label>
						<input type="text" class="form-control" id="dcreated_at" name="created_at" rows="3" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>