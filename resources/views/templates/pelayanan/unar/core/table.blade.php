<style>
	#example_wrapper{width: 2634px !important;}
</style>
<table id="server-side" class="table table-striped table-dark align-items-center no-footer">
	<thead class="thead-light">
		<tr>
			<th rowspan="2" scope="col">ID UNAR</th>
			<th rowspan="2" scope="col">TAHUN</th>
			<th rowspan="2" scope="col">BULAN</th>
			<th rowspan="2" scope="col">TANGGAL DIBUAT</th>
			<th rowspan="2" scope="col">PROVINSI</th>
			<th rowspan="2" scope="col">UPT</th>
			<th rowspan="2" scope="col">TANGGAL PELAKSANAAN UJIAN</th>
			<th rowspan="2" scope="col">LOKASI UJIAN</th>
			<th rowspan="2" scope="col">TIPE UNAR</th>
			<th style="text-align: center;" colspan="4" scope="col">JUMLAH PESERTA</th>
			<th style="text-align: center;" colspan="4" scope="col">PESERTA LULUS</th>
			<th style="text-align: center;" colspan="4" scope="col">PESERTA TIDAK LULUS</th>
			<th rowspan="2" scope="col">LAMPIRAN</th>
			<th rowspan="2" scope="col">FULL CAT</th>
			<th rowspan="2" scope="col">TINDAKAN</th>
		</tr>
		<tr>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL JUMLAH PESERTA</th>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL PESERTA LULUS</th>
			<th scope="col">SIAGA</th>
			<th scope="col">PENGGALANG</th>
			<th scope="col">PENEGAK</th>
			<th scope="col">TOTAL PESERTA TIDAK LULUS</th>
			
		</tr>
	</thead>
	<tbody></tbody>
</table>
