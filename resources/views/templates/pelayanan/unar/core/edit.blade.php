<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('unar-update')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="getStatus" value="1">
					<div class="col-md-6 form-group">
						<label for="id_prov">Provinsi</label>
						<input type="text" class="form-control" name="id_prov" id="eprovinsi" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="kode_upt">UPT</label>
						<input type="text" class="form-control" id="eupt" name="kode_upt" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="id_unar">ID UNAR</label>
						<input type="number" class="form-control" id="eid_unar" name="id_unar">
					</div>
					<div class="col-md-6 form-group">
						<label for="tipe">Tipe UNAR </label><label style="color: red;">*</label>
						<select class="form-control" id="etipe" name="tipe" required>
							<option value="1">REGULER</option>
							<option value="0">NON-REGULER</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal_ujian">Tanggal Pelaksanaan *</label>
						<input type="date" class="form-control" id="etanggal_ujian" name="tanggal_ujian" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="lokasi">Lokasi Ujian</label>
						<textarea class="form-control" id="elokasi" name="lokasi_ujian" rows="3"></textarea>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_siaga" name="jumlah_siaga" rows="3" placeholder="Siaga" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_siaga" name="lulus_siaga" rows="3" placeholder="Siaga" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_siaga" name="tdk_lulus_siaga" rows="3" placeholder="Siaga" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_penggalang" name="jumlah_penggalang" rows="3" placeholder="Penggalang" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_penggalang" name="lulus_penggalang" rows="3" placeholder="Penggalang" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_penggalang" name="tdk_lulus_penggalang" rows="3" placeholder="Penggalang" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_penegak" name="jumlah_penegak" rows="3" placeholder="Penegak" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_penegak" name="lulus_penegak" rows="3" placeholder="Penegak" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_penegak" name="tdk_lulus_penegak" rows="3" placeholder="Penegak" >
					</div>
					<div class="col-md-6 form-group">
						<label for="lampiran">Lampiran</label>
						<input type="file" class="form-control" name="lampiran" rows="3">
					</div>
					<div class="col-md-6 form-group">
						<label for="lampiran">Lampiran Sebelumnya</label>
						<input type="text" class="form-control" id="elampiran" rows="3" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="full_cat">Full Cat</label>
						<select class="form-control" id="efull_cat" name="full_cat" required>
							<option value="1">IYA</option>
							<option value="0">TIDAK</option>
						</select>
					</div>
					<input type="hidden" class="form-control" id="eid" name="id">
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
						@include('dev.helpers.button.btnSimpan')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>