<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('unar-search', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$lulus_siaga = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_siaga');
		$lulus_penggalang = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_penggalang');
		$lulus_ddpenegak = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_penegak');
		$lulus = $lulus_ddpenegak + $lulus_penggalang + $lulus_siaga;
		$tdk_lulus_siaga = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_siaga');
		$tdk_lulus_penggalang = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_penggalang');
		$tdk_lulus_ddpenegak = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_penegak');
		$tdk_lulus = $tdk_lulus_ddpenegak + $tdk_lulus_penggalang + $tdk_lulus_siaga;
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: 'Lulus',
				y: {{$lulus}}
			},
			{	
				name: 'Tidak Lulus',
				y: {{$tdk_lulus}}
			},
			]
		}]
	});
</script>
<script>
	Highcharts.chart('unar-persentase-pelaksanaan', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		
		$tahun_sekarang = date('Y');
		$teralisasi = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->where('full_cat',1)->count();
		$total_data = \App\RencanaUnar::where('tahun',$tahun_sekarang)->value('jumlah');
		if ($teralisasi == 0) {
			$teralisasi_fix = 0;
		}else {
			$teralisasi_fix = ( $teralisasi / $total_data ) * 100;
		}

		?>
		series: [{
			name: 'Persentase',
			colorByPoint: true,
			data: [
			{	
				name: 'Terealisasi',
				y: {{$teralisasi_fix}}
			},
			]
		}]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<?php
$end = explode('/', url()->current());
$end = end($end);
?>
<script type="text/javascript">
	var j = jQuery.noConflict();
	function trimNull(data)
	{
		if (data != null) {
			return data;
		}

		return '';
	}
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pelayanan/unar') }}{{ $end != 'unar' ? ($end == 'search' ? '/search' : ($end != 'search' ? '/searchMultiple/'.$end : null)) : null }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					<?php
					if (!empty($_GET['id_map'])) {
						?>
						"id_map": "{{ $_GET['id_map'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['dari'])) {
						?>
						"dari": "{{ $_GET['dari'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['sampai'])) {
						?>
						"sampai": "{{ $_GET['sampai'] }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [
		{
			"data": "id_unar",
		},
		{
			"data": "year",
		},
		{
			"data": "month",
		},
		{
			"data": "date",
		},
		{
			"data": "province",
		},
		{
			"data": "upt_name",
		},
		{
			"data": "exam_date",
		},
		{
			"data": "lokasi_ujian",
		},
		{
			"data": "unar_type",
		},
		{
			"data": "jumlah_siaga",
		},
		{
			"data": "jumlah_penggalang",
		},
		{
			"data": "jumlah_penegak",
		},
		{
			"data": "all_total",
		},
		{
			"data": "lulus_siaga",
		},
		{
			"data": "lulus_penggalang",
		},
		{
			"data": "lulus_penegak",
		},
		{
			"data": "pass_total",
		},
		{
			"data": "tdk_lulus_siaga",
		},
		{
			"data": "tdk_lulus_penggalang",
		},
		{
			"data": "tdk_lulus_penegak",
		},
		{
			"data": "not_pass_total",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if (data.lampiran != null) {
					return '<a href="{{ url("pelayanan/unar/lampiran") }}' + '/' + trimNull(data.lampiran) + '" class="btn btn-info btn-sm btn-lmprn"><i class="fa fa-download"></i></a>'
				}
				return null;
			}
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if (data.full_cat == 1) {
					return '<i class="fas fa-check"></i>'
				}
				return null;
			}
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
				'data-id="' + trimNull(data.id) + '"' +
				'data-id-unar="' + trimNull(data.id_unar) + '"' +
				'data-tipe="' + trimNull(data.tipe) + '"' +
				'data-provinsi="' + trimNull(data.province) + '"' +
				'data-upt="' + trimNull(data.upt_name) + '"' +
				'data-tanggal-ujian="' + trimNull(data.tanggal_ujian) + '"' +
				'data-lokasi="' + trimNull(data.lokasi_ujian) + '"' +
				'data-jumlah-siaga="' + trimNull(data.jumlah_siaga) + '"' +
				'data-jumlah-penggalang="' + trimNull(data.jumlah_penggalang) + '"' +
				'data-jumlah-penegak="' + trimNull(data.jumlah_penegak) + '"' +
				'data-lulus-siaga="' + trimNull(data.lulus_siaga) + '"' +
				'data-lulus-penggalang="' + trimNull(data.lulus_penggalang) + '"' +
				'data-lulus-penegak="' + trimNull(data.lulus_penegak) + '"' +
				'data-tidak-lulus-siaga="' + trimNull(data.tdk_lulus_siaga) + '"' +
				'data-tidak-lulus-penggalang="' + trimNull(data.tdk_lulus_penggalang) + '"' +
				'data-tidak-lulus-penegak="' + trimNull(data.tdk_lulus_penegak) + '"' +
				'data-lampiran="' + trimNull(data.lampiran) + '"' +
				'data-full-cat="' + trimNull(data.full_cat) + '"' +
				'data-pembuat="' + trimNull(data.created_by) + '"' +
				'data-dibuat="' + trimNull(data.created_at) + '"' +
				'><i class="fa fa-file"></i></a>' +

				'<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm"' +
				'data-id="' + trimNull(data.id) + '"' +
				'data-id-unar="' + trimNull(data.id_unar) + '"' +
				'data-tipe="' + trimNull(data.tipe) + '"' +
				'data-prov="' + trimNull(data.province) + '"' +
				'data-upt="' + trimNull(data.upt_name) + '"' +
				'data-tanggal-ujian="' + trimNull(data.tanggal_ujian) + '"' +
				'data-lokasi="' + trimNull(data.lokasi_ujian) + '"' +
				'data-jumlah-siaga="' + trimNull(data.jumlah_siaga) + '"' +
				'data-jumlah-penggalang="' + trimNull(data.jumlah_penggalang) + '"' +
				'data-jumlah-penegak="' + trimNull(data.jumlah_penegak) + '"' +
				'data-lulus-siaga="' + trimNull(data.lulus_siaga) + '"' +
				'data-lulus-penggalang="' + trimNull(data.lulus_penggalang) + '"' +
				'data-lulus-penegak="' + trimNull(data.lulus_penegak) + '"' +
				'data-tidak-lulus-siaga="' + trimNull(data.tdk_lulus_siaga) + '"' +
				'data-tidak-lulus-penggalang="' + trimNull(data.tdk_lulus_penggalang) + '"' +
				'data-tidak-lulus-penegak="' + trimNull(data.tdk_lulus_penegak) + '"' +
				'data-lampiran="' + trimNull(data.lampiran) + '"' +
				'data-full-cat="' + trimNull(data.full_cat) + '"' +
				'data-kota="' + trimNull(data.city) + '"' +
				'><i class="fa fa-edit"></i></a>' +
				'<a  class="btn btn-danger btn-sm"' +
				'id="getDelete"' +
				'data-toggle="modal"' +
				'data-target="#deleteModal"' +
				'data-id="' + trimNull(data.id) + '"' +
				'><i class="fa fa-trash"></i>' +
				'</a>';
			}
		},
		],
		"ordering": true
	});
<?php
if (!empty($_GET['dari']) || !empty($_GET['sampai'])) {
	?>
	j('html, body').animate({
		scrollTop: $("#parent_table").offset().top
	}, 2000);
	<?php
}
?>
</script>