<div class="row">
	<!-- Grafik -->
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Jumlah Per Tujuan</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">
					<div id="loket-pengaduan-tujuan" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
			<br>
			<?php 
			$count = \App\Model\Service\LoketPengaduan::where('tujuan',1)->count();
			$open = \App\Model\Service\LoketPengaduan::where('active',1)->where('tujuan',1)->count();
			$reopen = \App\Model\Service\LoketPengaduan::where('active',3)->where('tujuan',1)->count();
			$closed = \App\Model\Service\LoketPengaduan::where('active',0)->where('tujuan',1)->count();
			if ($count == 0) {
				$fixOpen = 0;
				$fixClosed = 0;
			}else{
				$fixOpen = ( $reopen + $open ) / $count * 100;
				$fixClosed = $closed / $count * 100;
			}
			?>
			<div class="card-footer" style="padding:0px;">
				<table style="width: 100%; text-align: center;">
					<br>
					<center>
					<label style="font-size: 20px; font-weight: bold;">PERSENTASE PENGADUAN</label>
					</center>
					<br>
					<tr>
						<td style="font-size: 20px; font-weight: bold; border-right: 1px solid #dedede; padding: 10px 0px;">
							<label>OPEN : </label> {{number_format($fixOpen, 2)}} %</td>
						<td style="font-size: 20px; font-weight: bold;"><label>CLOSED : </label> {{number_format($fixClosed, 2)}} %</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Jumlah Loket Pengaduan Per Provinsi</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">	
					<div id="loket-pengaduan" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
	</div>
</div>
