<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	<?php
	$tujuan = \App\Model\Refrension\Tujuan::where('id_prov',$provinceCode)->get();
	?>
	Highcharts.chart('loketpengaduan-search-multiple', {
		@foreach($getUPT as $upts)
		@foreach($tujuan as $key => $value)
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			<?php
			// dd(request()->dari);
			$dari = request()->dari;
			$sampai = request()->sampai;
			if ($dari && $sampai) {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->where('kode_upt',$changeUPT)->whereBetween('tanggal_pelayanan', [$dari, $sampai])->count();
			} else {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->where('kode_upt',$changeUPT)->count();
			}
			?>
			{	
				name: '{{$value->tujuan}}',
				y: {{$peng}}
			},
			]
		}]
		@endforeach
		@endforeach
	});
</script>