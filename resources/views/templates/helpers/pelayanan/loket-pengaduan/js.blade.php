<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('loket-pengaduan-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$tujuan = \App\Model\Refrension\Tujuan::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($tujuan as $key => $value)
			<?php
			// dd(request()->dari);
			$dari = request()->dari;
			$sampai = request()->sampai;
			if ($dari && $sampai) {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->where('kode_upt',Auth::user()->upt)->whereBetween('tanggal_pelayanan', [$dari, $sampai])->count();
			} else {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->where('kode_upt',Auth::user()->upt)->count();
			}
			?>
			{	
				name: '{{$value->tujuan}}',
				y: {{$peng}}
			},
			@endforeach
			]
		}]
	});
</script>
<script>
	Highcharts.chart('loket-pengaduan-tujuan', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$tujuan = \App\Model\Refrension\Tujuan::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($tujuan as $key => $value)
			<?php
			// dd(request()->dari);
			$dari = request()->dari;
			$sampai = request()->sampai;
			if ($dari && $sampai) {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->whereBetween('tanggal_pelayanan', [$dari, $sampai])->count();
			} else {
				$peng = \App\Model\Service\LoketPengaduan::where('tujuan',$value->id)->count();
			}
			?>
			{	
				name: '{{$value->tujuan}}',
				y: {{$peng}}
			},
			@endforeach
			]
		}]
	});
</script>

<script>
	Highcharts.chart('loket-pengaduan', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$provinsi = \App\Model\Region\Provinsi::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($provinsi as $key => $value)
			<?php
			// dd(request()->dari);
			$dari = request()->dari;
			$sampai = request()->sampai;
			if ($dari && $sampai) {
				$peng = \App\Model\Service\LoketPengaduan::where('id_prov',$value->id_row)->whereBetween('tanggal_pelayanan', [$dari, $sampai])->count();
			} else {
				$peng = \App\Model\Service\LoketPengaduan::where('id_prov',$value->id_row)->count();
			}
			?>
			{	
				name: '{{$value->nama}}',
				y: {{$peng}}
			},
			@endforeach
			]
		}]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var j = jQuery.noConflict();
	function trimNull(data)
	{
		if (data != null) {
			return data;
		}

		return '';
	}
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pelayanan/loket-pengaduan') }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					<?php
					if (!empty($_GET['dari'])) {
						?>
						"dari": "{{ $_GET['dari'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['sampai'])) {
						?>
						"sampai": "{{ $_GET['sampai'] }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [{
			"data": "year",
		},
		{
			"data": "month",
		},
		{
			"data": "date",
		},
		{
			"data": "upt_name",
		},
		{
			"data": "nama_pengunjung",
		},
		{
			"data": "jabatan_pengunjung",
		},
		{
			"data": "nama_perusahaan",
		},
		{
			"data": "no_telp",
		},
		{
			"data": "no_hp",
		},
		{
			"data": "email",
		},
		{
			"data": "keperluan",
		},
		{
			"data": "keterangan",
		},
		{
			"data": "purpose",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if (data.lampiran != null) {
					return '<a href="{{ url("pelayanan/loket-pengaduan/lampiran") }}' + '/' + trimNull(data.lampiran) + '" class="btn btn-info btn-sm btn-lmprn"><i class="fa fa-download"></i></a>'
				}
				return null;
			}
		},
		{
			"data": 'status',
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
				'data-perusahaan = "' + trimNull(data.nama_perusahaan) + '"' +
				'data-lampiran = "' + trimNull(data.lampiran) + '"' +
				'data-upt = "' + trimNull(data.upt_name) + '"' +
				'data-tujuan = "' + trimNull(data.purpose) + '"' +
				'data-provinsi = "' + trimNull(data.province) + '"' +
				'data-keperluan = "' + trimNull(data.keperluan) + '"' +
				'data-lampiran = "' + trimNull(data.lampiran) + '"' +
				'data-tanggal-pelayanan = "' + trimNull(data.tanggal_pelayanan) + '"' +
				'data-nama-pengunjung = "' + trimNull(data.nama_pengunjung) + '"' +
				'data-jabatan-pengunjung = "' + trimNull(data.jabatan_pengunjung) + '"' +
				'data-email = "' + trimNull(data.email) + '"' +
				'data-no-hp = "' + trimNull(data.no_hp) + '"' +
				'data-no-telp = "' + trimNull(data.no_telp) + '"' +
				'data-active = "' + trimNull(data.active) + '"' +
				'data-keterangan = "' + trimNull(data.keterangan) + '"' +
				'data-created-by ="' + trimNull(data.created_by) + '"' +
				'><i class="fa fa-file"></i></a>' +
				'<a href="#" data-toggle="modal" data-target="#editModal"' +
				'data-id = "' + trimNull(data.id) + '" id="getEdit"' +
				'data-perusahaan = "' + trimNull(data.nama_perusahaan) + '"' +
				'data-id-upt = "' + trimNull(data.kode_upt) + '"' +
				'data-upt = "' + trimNull(data.upt_name) + '"' +
				'data-tujuan-id = "' + trimNull(data.tujuan) + '"' +
				'data-tujuan = "' + trimNull(data.purpose) + '"' +
				'data-id-provinsi = "' + trimNull(data.id_prov) + '"' +
				'data-provinsi = "' + trimNull(data.province) + '"' +
				'data-keperluan = "' + trimNull(data.keperluan) + '"' +
				'data-lampiran = "' + trimNull(data.lampiran) + '"' +
				'data-tanggal-pelayanan = "' + trimNull(data.tanggal_pelayanan) + '"' +
				'data-nama-pengunjung = "' + trimNull(data.nama_pengunjung) + '"' +
				'data-jabatan-pengunjung = "' + trimNull(data.jabatan_pengunjung) + '"' +
				'data-email = "' + trimNull(data.email) + '"' +
				'data-no-hp = "' + trimNull(data.no_hp) + '"' +
				'data-no-telp = "' + trimNull(data.no_telp) + '"' +
				'data-active = "' + trimNull(data.active) + '"' +
				'data-keterangan = "' + trimNull(data.keterangan) + '"' +
				'class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>' +
				'<a class="btn btn-danger btn-sm" id="getDelete" data-toggle="modal" data-target="#deleteModal"' +
				'data-id="' + trimNull(data.id) + '"><i class="fa fa-trash"></i>';
			}
		},
		],
		"ordering": true
	});
	<?php
	if (!empty($_GET['dari']) || !empty($_GET['sampai'])) {
		?>
		j('html, body').animate({
			scrollTop: $("#parent_table").offset().top
		}, 2000);
		<?php
	}
	?>
</script>