<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	<?php
	$kabupaten = \App\Model\Region\KabupatenKota::where('id_prov',$provinceCode)->get();
	?>
	Highcharts.chart('penangananpiutang-search-multiple', {
		@foreach($getUPT as $upts)
		@foreach($kabupaten as $key => $value)
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: '{{$value->nama}}',
				y: {{\App\Model\Service\PenangananPiutang::where('nama_kpknl',$value->id)->where('kode_upt',$changeUPT)->count()}}
			},
			]
		}]
		@endforeach
		@endforeach
	});
</script>