<div class="row">
	<!-- Grafik -->
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Jumlah Per KPKNL</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">
					<div id="penanganan-piutang-tujuan" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Jumlah Penanganan Piutang Per Provinsi</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">	
					<div id="penanganan-piutang" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>