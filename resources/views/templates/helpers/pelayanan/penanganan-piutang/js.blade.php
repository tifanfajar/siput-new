<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('penanganan-piutang-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kabupatenoperator = \App\Model\KPNKL::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($kabupatenoperator as $key => $value)
			{	
				name: '{{$value->kpknl_name}}',
				y: {{\App\Model\Service\PenangananPiutang::where('nama_kpknl',$value->id)->where('kode_upt',Auth::user()->upt)->where('id_prov',Auth::user()->province_code)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>
<script>
	Highcharts.chart('penanganan-piutang-tujuan', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kabupatentujuan = \App\Model\KPNKL::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($kabupatentujuan as $key => $value)
			{	
				name: '{{$value->kpknl_name}}',
				y: {{\App\Model\Service\PenangananPiutang::where('nama_kpknl',$value->id)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script>
	Highcharts.chart('penanganan-piutang', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$provinsi = \App\Model\Region\Provinsi::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($provinsi as $key => $value)
			{	
				name: '{{$value->nama}}',
				y: {{\App\Model\Service\PenangananPiutang::where('id_prov',$value->id_row)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var j = jQuery.noConflict();
	function trimNull(data)
	{
		if (data != null) {
			return data;
		}

		return '';
	}
	var table = j('#server-side-0').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pelayanan/penanganan-piutang') }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					"status": "0",
					<?php
					if (!empty($_GET['dari'])) {
						?>
						"dari": "{{ $_GET['dari'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['sampai'])) {
						?>
						"sampai": "{{ $_GET['sampai'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['id_map'])) {
						?>
						"id_map": "{{ $_GET['id_map'] }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [
		<?php if ($getStatus == 'kepala-upt') { ?>
			{
				"data": 'id',
				"render": function (data, type, row, meta) {
					return '<td><input type="checkbox" class="box-check" name="id[]" value="' + data + '"></td>';
				}
			},
			<?php } ?>
		{
			"data": "created_at",
		},
		{
			"data": "province",
		},
		{
			"data": "upt_name",
		},
		{
			"data": "company_id",
		},
		{
			"data": "company_name",
		},
		{
			"data": "submission_value",
		},
		{
			"data": "tahun_pelimpahan",
		},
		{
			"data": "kpknl_name",
		},
		{
			"data": "tahapan_pengurusan",
		},
		{
			"data": "paid_off",
		},
		{
			"data": "installments",
		},
		{
			"data": "tanggal",
		},
		{
			"data": "psbdt",
		},
		{
			"data": "tanggal_psbdt",
		},
		{
			"data": "cancellation",
		},
		{
			"data": "tanggal_pembatalan",
		},
		{
			"data": "remaining_receivable",
		},
		{
			"data": "keterangan",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
				'data-perusahaan="' + trimNull(data.company_name) + '"' +
				'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
				'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
				'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
				'data-lunas="' + trimNull(data.lunas) + '"' +
				'data-angsuran="' + trimNull(data.angsuran) + '"' +
				'data-tanggal="' + trimNull(data.tanggal) + '"' +
				'data-psbdt="' + trimNull(data.psbdt) + '"' +
				'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
				'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

				'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
				'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +
				'data-keterangan="' + trimNull(data.keterangan) + '"' +
				'data-id-prov="' + trimNull(data.province) + '"' +
				'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
				'data-id-upt="' + trimNull(data.upt_name) + '"' +
				'><i class="fa fa-file"></i></a>' +
				'<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm"' +
				'data-perusahaan="' + trimNull(data.company_name) + '"' +
				'data-perusahaan-id="' + trimNull(data.no_client) + '"' +
				'data-perusahaan-no="' + trimNull(data.company_id) + '"' +
				'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
				'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
				'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
				'data-lunas="' + trimNull(data.lunas) + '"' +
				'data-angsuran="' + trimNull(data.angsuran) + '"' +
				'data-tanggal="' + trimNull(data.tanggal) + '"' +
				'data-psbdt="' + trimNull(data.psbdt) + '"' +
				'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
				'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

				'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
				'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +

				'data-keterangan="' + trimNull(data.keterangan) + '"' +
				'data-id-prov="' + trimNull(data.province) + '"' +
				'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
				'data-nama-upt="' + trimNull(data.upt_name) + '"' +
				'data-id="' + trimNull(data.id) + '"' +
				'><i class="fa fa-edit"></i></a>' +
				'<a  class="btn btn-danger btn-sm"' + 
				'id="getDelete"' + 
				'data-toggle="modal" ' +
				'data-target="#deleteModal" ' +
				'data-id="' + trimNull(data.id) + '"><i class="fa fa-trash"></i>' +
				'</a>';
			}
		},
		],
		"ordering": true
	});
	var table = j('#server-side-1').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ route('pelayanan/penanganan-piutang') }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					"status": "1",
					<?php
					if (!empty($_GET['dari'])) {
						?>
						"dari": "{{ $_GET['dari'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['sampai'])) {
						?>
						"sampai": "{{ $_GET['sampai'] }}",
						<?php
					}
					?>
					<?php
					if (!empty($_GET['id_map'])) {
						?>
						"id_map": "{{ $_GET['id_map'] }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [
		{
			"data": "created_at",
		},
		{
			"data": "updated_at",
		},
		{
			"data": "province",
		},
		{
			"data": "upt_name",
		},
		{
			"data": "company_id",
		},
		{
			"data": "company_name",
		},
		{
			"data": "submission_value",
		},
		{
			"data": "tahun_pelimpahan",
		},
		{
			"data": "kpknl_name",
		},
		{
			"data": "tahapan_pengurusan",
		},
		{
			"data": "paid_off",
		},
		{
			"data": "installments",
		},
		{
			"data": "tanggal",
		},
		{
			"data": "psbdt",
		},
		{
			"data": "tanggal_psbdt",
		},
		{
			"data": "cancellation",
		},
		{
			"data": "tanggal_pembatalan",
		},
		{
			"data": "remaining_receivable",
		},
		{
			"data": "keterangan",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
				'data-perusahaan="' + trimNull(data.company_name) + '"' +
				'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
				'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
				'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
				'data-lunas="' + trimNull(data.lunas) + '"' +
				'data-angsuran="' + trimNull(data.angsuran) + '"' +
				'data-tanggal="' + trimNull(data.tanggal) + '"' +
				'data-psbdt="' + trimNull(data.psbdt) + '"' +
				'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
				'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

				'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
				'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +
				'data-keterangan="' + trimNull(data.keterangan) + '"' +
				'data-id-prov="' + trimNull(data.province) + '"' +
				'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
				'data-id-upt="' + trimNull(data.upt_name) + '"' +
				'><i class="fa fa-file"></i></a>' +
				'<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm"' +
				'data-perusahaan="' + trimNull(data.company_name) + '"' +
				'data-perusahaan-id="' + trimNull(data.no_client) + '"' +
				'data-perusahaan-no="' + trimNull(data.company_id) + '"' +
				'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
				'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
				'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
				'data-lunas="' + trimNull(data.lunas) + '"' +
				'data-angsuran="' + trimNull(data.angsuran) + '"' +
				'data-tanggal="' + trimNull(data.tanggal) + '"' +
				'data-psbdt="' + trimNull(data.psbdt) + '"' +
				'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
				'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

				'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
				'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +

				'data-keterangan="' + trimNull(data.keterangan) + '"' +
				'data-id-prov="' + trimNull(data.province) + '"' +
				'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
				'data-nama-upt="' + trimNull(data.upt_name) + '"' +
				'data-id="' + trimNull(data.id) + '"' +
				'><i class="fa fa-edit"></i></a>' +
				'<a  class="btn btn-danger btn-sm"' + 
				'id="getDelete"' + 
				'data-toggle="modal" ' +
				'data-target="#deleteModal" ' +
				'data-id="' + trimNull(data.id) + '"><i class="fa fa-trash"></i>' +
				'</a>';
			}
		},
		],
		"ordering": true
	});

	<?php
	if ($getStatus == 'operator') {
		?>
		var table = j('#server-side-3').DataTable({
			"lengthChange": true,
			orderCellsTop: true,
			fixedHeader: true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"type": "GET",
				"url": "{{ route('pelayanan/penanganan-piutang') }}",
				"data": function (d) {
					console.log(d);
					return j.extend({}, d, {
						"type": "WEB",
						"status": "3",
						<?php
						if (!empty($_GET['dari'])) {
							?>
							"dari": "{{ $_GET['dari'] }}",
							<?php
						}
						?>
						<?php
						if (!empty($_GET['sampai'])) {
							?>
							"sampai": "{{ $_GET['sampai'] }}",
							<?php
						}
						?>
						<?php
						if (!empty($_GET['id_map'])) {
							?>
							"id_map": "{{ $_GET['id_map'] }}",
							<?php
						}
						?>
					});
				},
			},
			"columns": [
			{
				"data": "created_at",
			},
			{
				"data": "province",
			},
			{
				"data": "upt_name",
			},
			{
				"data": "company_id",
			},
			{
				"data": "company_name",
			},
			{
				"data": "submission_value",
			},
			{
				"data": "tahun_pelimpahan",
			},
			{
				"data": "kpknl_name",
			},
			{
				"data": "tahapan_pengurusan",
			},
			{
				"data": "paid_off",
			},
			{
				"data": "installments",
			},
			{
				"data": "tanggal",
			},
			{
				"data": "psbdt",
			},
			{
				"data": "tanggal_psbdt",
			},
			{
				"data": "cancellation",
			},
			{
				"data": "tanggal_pembatalan",
			},
			{
				"data": "remaining_receivable",
			},
			{
				"data": "keterangan",
			},
			{
				"data": null,
				"render": function (data, type, row, meta) {
					var url = '{{ route("penanganan-piutang-reupload", ":id" ) }}';
					url = url.replace(':id', trimNull(data.id));
					return '<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"' +
					'data-perusahaan="' + trimNull(data.company_name) + '"' +
					'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
					'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
					'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
					'data-lunas="' + trimNull(data.lunas) + '"' +
					'data-angsuran="' + trimNull(data.angsuran) + '"' +
					'data-tanggal="' + trimNull(data.tanggal) + '"' +
					'data-psbdt="' + trimNull(data.psbdt) + '"' +
					'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
					'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

					'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
					'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +
					'data-keterangan="' + trimNull(data.keterangan) + '"' +
					'data-id-prov="' + trimNull(data.province) + '"' +
					'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
					'data-id-upt="' + trimNull(data.upt_name) + '"' +
					'><i class="fa fa-file"></i></a>' +
					'<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm"' +
					'data-perusahaan="' + trimNull(data.company_name) + '"' +
					'data-perusahaan-id="' + trimNull(data.no_client) + '"' +
					'data-perusahaan-no="' + trimNull(data.company_id) + '"' +
					'data-tahun-pelimpahan="' + trimNull(data.tahun_pelimpahan) + '"' +
					'data-nilai-penyerahan="' + trimNull(data.nilai_penyerahan) + '"' +
					'data-tahapan-pengurusan="' + trimNull(data.tahapan_pengurusan) + '"' +
					'data-lunas="' + trimNull(data.lunas) + '"' +
					'data-angsuran="' + trimNull(data.angsuran) + '"' +
					'data-tanggal="' + trimNull(data.tanggal) + '"' +
					'data-psbdt="' + trimNull(data.psbdt) + '"' +
					'data-pembatalan="' + trimNull(data.pembatalan) + '"' +
					'data-sisa-piutang="' + trimNull(data.sisa_piutang) + '"' +

					'data-tanggal-psbdt="' + trimNull(data.tanggal_psbdt) + '"' +
					'data-tanggal-pembatalan="' + trimNull(data.tanggal_pembatalan) + '"' +

					'data-keterangan="' + trimNull(data.keterangan) + '"' +
					'data-id-prov="' + trimNull(data.province) + '"' +
					'data-nama-kpknl="' + trimNull(data.kpknl_name) + '"' +
					'data-nama-upt="' + trimNull(data.upt_name) + '"' +
					'data-id="' + trimNull(data.id) + '"' +
					'><i class="fa fa-edit"></i></a>' +
					'<a  class="btn btn-danger btn-sm"' + 
					'id="getDelete"' + 
					'data-toggle="modal" ' +
					'data-target="#deleteModal" ' +
					'data-id="' + trimNull(data.id) + '"><i class="fa fa-trash"></i>' +
					'</a>' +

					'<a href="' + url + '"' +
					'class="btn btn-primary btn-sm text-white">' +
					'<i class="fa fa-check-circle"></i>&nbsp;Ajukan Ulang</a>';
				}
			},
			],
			"ordering": true
		});
		<?php
	}
	?>

	<?php
	if (!empty($_GET['dari']) || !empty($_GET['sampai'])) {
		?>
		j('html, body').animate({
			scrollTop: $("#parent_table").offset().top
		}, 2000);
		<?php
	}
	?>
</script>