<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<?php 
$getUptName = \App\Model\Setting\UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
?>
<script>
	Highcharts.chart('inspeksi-kegiatan-search', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			'{{$getUptName}}',
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Population (millions)',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' millions'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total Kegiatan Inspeksi',
			data: [
			{{\App\Model\Inspeksi\Inspeksi::where('kode_upt',$changeUPT)->count()}},
			]
		}, 
		]
	});
</script>
<script>
	Highcharts.chart('inspeksi-hasil-search', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			'{{$getUptName}}',
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Population (millions)',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' millions'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Berdasarkan Capaian Hasil',
			data: [
			{{number_format($capaian, 2)}},
			]
		}, 
		]
	});
</script>