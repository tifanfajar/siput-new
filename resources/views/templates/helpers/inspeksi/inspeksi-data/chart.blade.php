<div class="row">
	<!-- Grafik -->
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Capaian Kegiatan Inspeksi</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">
					<div id="inspeksi-kegiatan" style="min-width: 310px; height: 2000px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Capaian Hasil Inspeksi</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="inspeksi-hasil" style="min-width: 310px; height: 2000px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<br>
<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Capaian Total</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">	
					<div id="inspeksi-capaian-total" style="min-width: 310px; height: 350px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>