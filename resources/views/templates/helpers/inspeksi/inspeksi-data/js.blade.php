<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>


<script>
	Highcharts.chart('inspeksi-kegiatan', {
		chart: {
			type: 'bar'
		},
		title:false,
		<?php 
		$upt = \App\Model\Setting\UPT::select('office_name','office_id')->distinct()->get();
		?>
		xAxis: {
			categories: [
			@foreach($upt as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Capaian Kegiatan Inspeksi',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: 'Kegiatan'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total Kegiatan Inspeksi',
			data: [
			@foreach($upt as $key => $value)
			{{\App\Model\Inspeksi\Inspeksi::where('kode_upt',$value->office_id)->count()}},
			@endforeach
			]
		}, 
		]
	});
</script>
<script>
	Highcharts.chart('inspeksi-capaian-total', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			'Jumlah Kegiatan',
			'Capaian Hasil Inspeksi',
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Capaian Total',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: '%'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Berdasarkan Capaian Hasil',
			data: [
			<?php 
			$jumlahdata = \App\Model\Inspeksi\Inspeksi::count();
			?>	
			{{$jumlahdata}},
			{{number_format($capaian, 2)}},
			]
		}, 
		]
	});
</script>
<script>
	Highcharts.chart('inspeksi-hasil', {
		chart: {
			type: 'bar'
		},
		title:false,
		<?php 
		$upt = \App\Model\Setting\UPT::select('office_name','office_id')->distinct()->get();
		?>
		xAxis: {
			categories: [
			@foreach($upt as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Capaian Hasil Inspeksi',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: '%'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Berdasarkan Capaian Hasil',
			data: [
			@foreach($upt as $key => $value)
			<?php 
			$date = \App\Model\Inspeksi\Inspeksi::select('tanggal_lapor')->where('kode_upt',$value->office_id)->orderBy('tanggal_lapor','desc')->first();
        	$time=strtotime($date->tanggal_lapor);
	        $month=date("m",$time);
	        $year=date("Y",$time);
			$hi_sesuai_isr = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$value->office_id)->sum('hi_sesuai_isr');
			$tl_sesuai_isr = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$value->office_id)->sum('tl_sesuai_isr');
			$data_sampling = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',$value->office_id)->sum('data_sampling');
			if($data_sampling == 0){
                    $capaian = 0;
                }else{
                    $capaian = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
                }
                if ($capaian >= 100) {
                    $capaian = 100;
                }
			?>	
			{{number_format($capaian, 2)}},
			@endforeach
			]
		}, 
		]
	});
</script>
<?php 
$getUptName = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
?>
@if($getStatus != 'administator')
<script>
	Highcharts.chart('inspeksi-kegiatan-operator', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			'{{$getUptName}}',
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Capaian Kegiatan Inspeksi',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: 'Kegiatan'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total Kegiatan Inspeksi',
			data: [
			{{\App\Model\Inspeksi\Inspeksi::where('kode_upt',Auth::user()->upt)->count()}},
			]
		}, 
		]
	});
</script>
<script>
	Highcharts.chart('inspeksi-hasil-operator', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			'{{$getUptName}}',
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Capaian Hasil Inspeksi',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: '%'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Berdasarkan Capaian Hasil',
			data: [
			<?php 
			$date = \App\Model\Inspeksi\Inspeksi::select('tanggal_lapor')->where('kode_upt',Auth::user()->upt)->orderBy('tanggal_lapor','desc')->first();
			if($date != null && $date != ""){
        	$time=strtotime($date->tanggal_lapor);
	        $month=date("m",$time);
	        $year=date("Y",$time);
			$hi_sesuai_isr = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('hi_sesuai_isr');
			$tl_sesuai_isr = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('tl_sesuai_isr');
			$data_sampling = \App\Model\Inspeksi\Inspeksi::where('status',1)->whereMonth('tanggal_lapor',$month)->whereYear('tanggal_lapor',$year)->where('kode_upt',Auth::user()->upt)->sum('data_sampling');
			if($data_sampling == 0){
                    $calculationData = 0;
                }else{
                    $calculationData = ( $hi_sesuai_isr + $tl_sesuai_isr ) / ($data_sampling) * 100;
                }
                if ($calculationData >= 100) {
                    $calculationData = 100;
                }
            }else{

                   $calculationData = 0;
            }
			?>	
			{{number_format($calculationData, 2)}},
			]
		}, 
		]
	});
</script>
@endif

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>

<script type="text/javascript">
	$('form#inspeksi-save').submit(function(event) {
		var data_sampling = parseInt($('#data_sampling').val());		
		var hi_sesuai_isr = parseInt($('#hi_sesuai_isr').val());		
		var hi_proses_isr = parseInt($('#hi_proses_isr').val());		
		var hi_tidak_aktif = parseInt($('#hi_tidak_aktif').val());		
		var hi_tidak_sesuai_isr = parseInt($('#hi_tidak_sesuai_isr').val());		
		var jumlah_hi = parseInt(hi_sesuai_isr + hi_proses_isr + hi_tidak_aktif + hi_tidak_sesuai_isr);		
		
		if (jumlah_hi > data_sampling) {
			$("#data_sampling").addClass("is-invalid");
			$("#data_sampling").focus();
			return false;
		}
		else {
			$("#data_sampling").removeClass("is-invalid");
			return true;	
		}
	});

	$('#data_sampling').keydown(function(event) {
		$("#data_sampling").removeClass("is-invalid");
	});


</script>