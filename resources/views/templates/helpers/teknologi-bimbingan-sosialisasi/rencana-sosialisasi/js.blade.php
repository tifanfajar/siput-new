@include('dev.helpers.jquery')
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('rencanasosialisasi-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kegiatan = \App\Model\Refrension\Kegiatan::all();
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			@foreach($kegiatan as $key => $value)
			{	
				name: '{{$value->kegiatan}}',
				y: {{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('jenis_kegiatan',$value->id)->where('kode_upt',Auth::user()->upt)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>
<script>
	Highcharts.chart('rencanasosialisasi-admin', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kegiatan = \App\Model\Refrension\Kegiatan::all();
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			@foreach($kegiatan as $key => $value)
			{	
				name: '{{$value->kegiatan}}',
				y: {{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('jenis_kegiatan',$value->id)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script>
	Highcharts.chart('rencanasosialisasi-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		<?php 
		$upt = \App\Model\Setting\UPT::select('office_name','office_id')->distinct()->get();
		?>
		xAxis: {
			categories: [
			@foreach($upt as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Teraslisasi',
			data: [
			@foreach($upt as $key => $value)
			{{\App\Model\SosialisasiBimtek\MonevSosialisasi::whereNotNull('id_rensos')->where('kode_upt',$value->office_id)->count()}},
			@endforeach
			]
		}, {
			name: 'Belum Teraslisasi',
			data: [
			@foreach($upt as $key => $value)
			<?php 
			$terealisasi = \App\Model\SosialisasiBimtek\MonevSosialisasi::whereNotNull('id_rensos')->where('kode_upt',$value->office_id)->count();
			$blm = \App\Model\SosialisasiBimtek\RencanaSosialisasi::where('kode_upt',$value->office_id)->count();
			$total = $blm - $terealisasi;
			?>
			{{$total}},
			@endforeach
			]
		}, 
		]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>