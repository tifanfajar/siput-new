<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('rencanasosialisasi-search', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kegiatan = \App\Model\Refrension\Kegiatan::all();
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			@foreach($kegiatan as $key => $value)
			{	
				name: '{{$value->kegiatan}}',
				y: {{\App\Model\SosialisasiBimtek\RencanaSosialisasi::where('jenis_kegiatan',$value->id)->where('kode_upt',$changeUPT)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>