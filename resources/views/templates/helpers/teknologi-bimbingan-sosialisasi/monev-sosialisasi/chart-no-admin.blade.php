<div class="row justify-content-center">
	<!-- Grafik -->
	<div class="col">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Jumlah Monev Sosialisasi Per Kegiatan {{$upts}}</h6>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="chart">
					<div id="monevsosialisasi-operator" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>