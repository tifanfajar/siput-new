<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	<?php
	$materi = \App\Model\Materi::all();
	?>
	Highcharts.chart('bahansosialisasi-search-multiple', {
		@foreach($getUPT as $upts)
		@foreach($materi as $key => $value)
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: '{{$value->jenis_materi}}',
				y: {{\App\Model\SosialisasiBimtek\BahanSosialisasi::where('id_materi',$value->id)->where('kode_upt',$upts->office_id)->count()}}
			},
			]
		}]
		@endforeach
		@endforeach
	});
</script>