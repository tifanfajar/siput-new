<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>

<script>
	Highcharts.chart('chart1', {
		chart: {
			type: 'bar'
		},
		xAxis: {
			categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Population (millions)',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' millions'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#000',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [{
			name: 'Year 1800',
			data: [107, 31, 635, 203, 2]
		}]
	});
</script>

<script>
	Highcharts.chart('chart2', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [{
				name: 'Chrome',
				y: 61.41,
				sliced: true,
				selected: true
			}, {
				name: 'Internet Explorer',
				y: 11.84
			}, {
				name: 'Firefox',
				y: 10.85
			}, {
				name: 'Edge',
				y: 4.67
			}, {
				name: 'Safari',
				y: 4.18
			}, {
				name: 'Sogou Explorer',
				y: 1.64
			}, {
				name: 'Opera',
				y: 1.6
			}, {
				name: 'QQ',
				y: 1.2
			}, {
				name: 'Other',
				y: 2.61
			}]
		}]
	});
</script>
<script>
	Highcharts.chart('chart3', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: ['Aceh', 'Lampung', 'Jakarta', 'Bandung', 'Bali'],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [{
			name: 'Tahun 2017',
			data: [107, 31, 635, 203, 2]
		}, {
			name: 'Tahun 2018',
			data: [133, 156, 947, 408, 6]
		}, {
			name: 'Tahun 2019',
			data: [814, 841, 3714, 727, 31]
		}, {
			name: 'Tahun 2020',
			data: [1216, 1001, 4436, 738, 40]
		}]
	});
</script>

<script>
	Highcharts.chart('bahansosialisasi-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$materi = \App\Model\Refrension\Materi::all();
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			@foreach($materi as $key => $value)
			{	
				name: '{{$value->jenis_materi}}',
				y: {{\App\Model\SosialisasiBimtek\BahanSosialisasi::where('id_materi',$value->id)->where('kode_upt',Auth::user()->id_upt)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>
<script>
	Highcharts.chart('bahansosialisasi-admin', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$materi = \App\Model\Refrension\Materi::all();
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			@foreach($materi as $key => $value)
			{	
				name: '{{$value->materi}}',
				y: {{\App\Model\SosialisasiBimtek\BahanSosialisasi::where('id_materi',$value->id)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script>
	Highcharts.chart('bahansosialisasi-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		<?php 		
		$upt = \App\Model\Setting\UPT::distinct('office_id')->get();
		?>
		xAxis: {
			categories: [
			@foreach($upt as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Teraslisasi',
			data: [
			@foreach($upt as $key => $value)
			{{\App\Model\SosialisasiBimtek\BahanSosialisasi::where('status',0)->where('kode_upt',$value->office_id)->count()}},
			@endforeach
			]
		}, {
			name: 'Belum Teraslisasi',
			data: [
			@foreach($upt as $key => $value)
			{{\App\Model\SosialisasiBimtek\BahanSosialisasi::where('status',1)->where('kode_upt',$value->office_id)->count()}},
			@endforeach
			]
		}, 
		]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>