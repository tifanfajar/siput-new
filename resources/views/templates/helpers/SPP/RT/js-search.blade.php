<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>


<script>	
	Highcharts.chart('sppRT-search', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total RT Terbit Diterbitkan',
			data: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			0,
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-60 s.d H-30 )',
			data: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 11;
				$minYears = $year - 1;
			}
			elseif ($value->id_bulan == 2) {
				$minMonth = 12;
				$minYears = $year - 1;
			} 
			else{
				$minMonth = $value->id_bulan - 2;
				$minYears = $year;
			}
			?>
			0,
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-30 s.d H ) Hasil TL UPT',
			data: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 11;
				$minYears = $year - 1;
			}
			elseif ($value->id_bulan == 2) {
				$minMonth = 12;
				$minYears = $year - 1;
			} 
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $year;
			}
			?>
			@if(\App\Model\SPP\RT::where('status_pembayaran','paid')->get() == null)
			0,
			@else
			0,
			@endif
			@endforeach
			]
		},
		{
			name: 'RT Belum Terbayar ( Belum Jatuh Tempo )',
			data: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 11;
				$minYears = $year - 1;
			}
			elseif ($value->id_bulan == 2) {
				$minMonth = 12;
				$minYears = $year - 1;
			} 
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $year;
			}
			?>
			0,
			@endforeach
			]
		},
		{
			name: 'RT Tidak Membayar ( Menjadi Reminder )',
			data: [
			@foreach(\App\Model\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 11;
				$minYears = $year - 1;
			}
			elseif ($value->id_bulan == 2) {
				$minMonth = 12;
				$minYears = $year - 1;
			} 
			else{
				$minMonth = $value->id_bulan;
				$minYears = $year;
			}
			?>
			0,
			@endforeach
			]
		},
		]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>