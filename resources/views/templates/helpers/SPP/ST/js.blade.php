<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>

<?php 
$myTime = Carbon\Carbon::now();
$fixYears = '20'.$myTime->format('y');
$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
?>

@if($getStatus == 'administrator')
<script>	
	Highcharts.chart('sppRT-admin', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-1 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',1)->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-1 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',1)->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-admin1', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-2 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',2)->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-2 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',2)->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>


<script>	
	Highcharts.chart('sppRT-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-3 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',3)->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-3 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',3)->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-admin3', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'STT Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp',16)->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'STT Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp',16)->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
@endif

<script>	
	Highcharts.chart('sppRT-operator', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-1 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',1)->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-1 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',1)->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-operator1', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-2 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',2)->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-2 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',2)->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>


<script>	
	Highcharts.chart('sppRT-operator2', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-3 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',3)->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-3 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',3)->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-operator3', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'STT Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',16)->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'STT Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\StatusTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('bi_type',16)->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var j = jQuery.noConflict();
	var table = j('#server-side').DataTable({
		"lengthChange": true,
		orderCellsTop: true,
		fixedHeader: true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"type": "GET",
			"url": "{{ url()->current() }}",
			"data": function (d) {
				console.log(d);
				return j.extend({}, d, {
					"type": "WEB",
					<?php
					foreach ($_GET as $key => $value) {
						?>
						"{{ $key }}": "{{ $value }}",
						<?php
					}
					?>
				});
			},
		},
		"columns": [
		{
			"data": "no_spp",
		},
		{
			"data": "no_klien",
		},
		{
			"data": "nama_perusahaan",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if (data.bi_type == 1) {
					return '<a class="btn btn-success btn-sm text-white">ST 1</a>';
				}
				else if (data.bi_type == 9) {
					return '<a class="btn btn-success btn-sm text-white">ST 1 Paid</a>';
				}
				else if (data.bi_type == 2) {
					return '<a class="btn btn-info btn-sm text-white">ST 2</a>';
				}
				else if (data.bi_type == 10) {
					return '<a class="btn btn-info btn-sm text-white">ST 2 Paid</a>';
				}
				else if (data.bi_type == 3) {
					return '<a class="btn btn-warning btn-sm text-white">ST 3</a>';
				}
				else if (data.bi_type == 11) {
					return '<a class="btn btn-warning btn-sm text-white">ST 3 Paid</a>';
				}
				else if (data.bi_type == 16) {
					return '<a class="btn btn-danger btn-sm text-white">STT</a>';
				}
				else if (data.bi_type == 49) {
					return '<a class="btn btn-danger btn-sm text-white">STT Paid</a>';
				}
				else {
					return '-';
				}
			}
		},
		{
			"data": "upt",
		},
		{
			"data": "bhp",
		},
		{
			"data": "bi_created_at",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				return '<center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center>';
			}
		},
		{
			"data": "due_date",
		},
		{
			"data": "pay_date",
		},
		{
			"data": "service_name",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if (data.active == 1) {
					return 'Sudah Disetujui';
				}
				else if(data.active == null) {
					return 'Terbayarkan';
				}
				else {
					return 'Belum disetujui';
				}
			}
		},
		{
			"data": "method",
		},
		{
			"data": "tanggal_upaya",
		},
		{
			"data": null,
			"render": function (data, type, row, meta) {
				if(data.bukti_dukung != null) {
					return '<a href="{{url("spp/st/lampiran")}}' + '/' + data.bukti_dukung + '" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a>';
				}
				else {
					return '-';
				}
			}
		},
		{
			"data": "keterangan",
		}
		],
		"ordering": true
	});
</script>