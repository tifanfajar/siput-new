<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
 .ic {
   width: 1.5rem;
   height: 1.3rem;
   border-radius: .5rem;
   text-align: center;
   cursor:pointer;
   /* background: #cdcdcd; */
   /* padding-top: .3rem; */
   font-size: 1em;
   list-style: none;
   color: #000;
 }
 .ic:hover {
  background: #eee;
  color: #000;
 }
 .dataTables_wrapper, .dataTables_info, .dataTables_length {
   background: #f8f9fe !important;
   color: #000 !important;
 }
 
</style>
<br/>
<br/>
<div class="col">

 <div class="input-group mb-3">
   <div class="input-group-prepend" style="height: 2.63em">
     <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
   </div>
   <input type="text" class="form-control"  style="height: 3em" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1">
 </div>
 <div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-1">

    <a type="button" href="{{route('tahun-program.add')}}" class="btn btn-primary" style="background: #193865;color: #fff">Add New</a>
    </div>
  </div>
 <table class="table table-striped" id="tbl">
     <thead >
       <tr class="table-dark">
         <th scope="col">Tahun</th>
         <th scope="col">Keterangan</th>
         <th scope="col" class="stts">Status</th>
         <th scope="col" class="dgcActions"><i class="fab fa-whmcs" style="font-size: 2em;text-align: center"></i></th>
       </tr>
     </thead>
     <tbody>
       
       
     </tbody>
   </table>
   <div id="demo"></div>
</div>
<script src="{{ asset('js/pagination-min.js')}}" type="text/javascript"></script>
<script>
 let table =''
async function deleteItem(e) {
  console.log(e.getAttribute('data-id'))
  const url = `{{url('api/tahun-program/')}}`

  const res = await fetch(url+`/${e.getAttribute('data-id')}`, {
     method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
     mode: 'cors', // no-cors, *cors, same-origin
     cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
     credentials: 'same-origin', // include, *same-origin, omit
     headers: {
       'Content-Type': 'application/json'
       // 'Content-Type': 'application/x-www-form-urlencoded',
     },
     redirect: 'follow', // manual, *follow, error
     referrerPolicy: 'no-referrer'
  })
  table.ajax.reload();
}
$(document).ready( async function () {
    let totalPage
   table = await $('#tbl').DataTable({
      "serverSide": true,
      "processing": true,
      "paging": true,
      "ordering": false,
      "searching": false,
      iTotalDisplayRecords: 15,
      "lengthMenu": [ [10, 25, 50, 100], [10, 25, 50, 100] ],
      "pageLength": 10,
      ajax: {
       url: '{{url("/api/tahun-program")}}' + '?' + $.param({ paramInQuery: 1 }),
       
      },
      columns: [
       { data: 'value' },
       { data: 'label_tahun_program' },
       // { data: 'status' },
       { data: null, searchable: false, orderable: false },
       { data: null, searchable: false, orderable: false }
      ],
      columnDefs: [
           {
               targets: "dgcActions",
               //data: null,
               render: function (data, type, row) {
                   var html = `
                     <a href="{{url('tahun-program/edit')}}/${data.id}" class="ic"><i class="fas fa-edit" style="color: #3C8F61"></i></a>
                    <a href="{{url('tahun-program/edit')}}/${data.id}" class="ic"><i class="fas fa-eye" style="color: #0093DD"></i></a>
                    <a class="ic" data-id="${data.id}" onclick="deleteItem(this)"><i class="fas fa-trash" style="color: #E12D2D" ></i></a>
                   `;
 
                   return html;
               }
           },
           {
               targets: "stts",
               //data: null,
               render: function (data, type, row) {
                   var html = `
                    aktif
                   `;
 
                   return html;
               }
           }
       ]
    });
    // $('#tbl').DataTable().ajax.reload();
} );
// $('#demo').pagination({
//     dataSource: [1, 2, 3, 4, 5, 6, 7, 195],
//     callback: function(data, pagination) {
//         // template method of yourself
//         var html = template(data);
//         dataContainer.html(html);
//     }
// })


</script>
@endsection