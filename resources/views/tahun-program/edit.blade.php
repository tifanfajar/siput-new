<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
 input, select {
     border: 0 !important;
     background: transparent !important;
     border-bottom: 1px solid #cdcdcd !important;
 }
 .tb-container{
   background: #fff;
   margin-top: 1em;
 }
 tbody > tr {
    line-height: 45px !important;
    min-height: 45px !important;
    height: 45px !important;
 }
 .ic {
   width: 1.5rem;
   height: 1.3rem;
   border-radius: .5rem;
   text-align: center;
   cursor:pointer;
   /* background: #cdcdcd; */
   /* padding-top: .3rem; */
   font-size: 1em;
   list-style: none;
   color: #000;
 }
</style>
<br/>
<br/>
<div class="col">
  <div class="col">
  <div class="alert alert-success" id="success" role="alert">
    Berhasil merubah tahun program
  </div>
  <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Tahun Program *</label>
         <input type="" class="form-control" id="tahunProgram" placeholder="No Data Available">
         <small id="tahunProgramHelp" class="form-text text-muted" style="color:red !important"></small>
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Keterangan *</label>
         <input type="" class="form-control" id="keterangan" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Status *</label>
         <input type="" class="form-control" id="status" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
     <div class="row">
       <div class="col-md-10"></div>
       <div class="btn btn-primary" style="background: #B4CD93" id="add"><i class="fas fa-plus-circle"></i> Tambah</div>
     </div>
    </div> 
    <div class="btn btn-primary btn-lg btn-block" style="background: #193865;margin-top: 1em">Program</div>
  </div>
  <div class="tb-container col">
   <table class="table table-bordered">
     <thead >
       <tr class="table-dark">
         <th scope="col">Nama Program</th>
         <th scope="col">Periode</th>
         <th scope="col">Unit</th>
         <th scope="col"></th>
       </tr>
     </thead>
     <tbody id="tbd">
      
     </tbody>
   </table>
  </div>
  <div class="row">
    <div class="col-md-10"></div>
    <div class="row" style="gap: 1em">
    <button type="button" class="btn btn-primary" style="background: #193865" id="save">Simpan</button>
    <a href="{{route('tahun-program')}}" class="btn btn-primary" id="cancel-btn" style="background: #fff;color: #193865">Batal</a>
    </div>
  </div>
</div>
<script>
let id=[]
function deleteList (e) {
 console.log(e.parentNode.parentNode.parentNode.getAttribute('item-id'))
 e.parentNode.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode.parentNode)
}
function deleteListAda (e) {
 // console.log(e.parentNode.parentNode.getAttribute('item-id'))
 id.push(e.parentNode.parentNode.getAttribute('item-id'))
 e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode)
}
$(document).ready(function () {
   $('#success').hide()
   $('#err-al').hide()
   init()
   async function init() {
    await getListProgram()
    getDetail()

   }
   let select = []
   async function getListProgram () {
     const url = "{{url('api/template-program')}}"
     const res = await fetch(url,{
         method: 'GET', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         }
       })
     const resp = await res.json()
     // console.log(resp.data)
     if (resp.status === 200) {
       select = resp.data
     }
   }
   async function getDetail () {
      const url = `{{url('api/tahun-program/')}}/{{$id}}`
      const res = await fetch(url, {
         method: 'GET', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         },
         redirect: 'follow', // manual, *follow, error
         referrerPolicy: 'no-referrer'
      })
      const resp = await res.json()
      var html = ''
      if (resp.status === 200) {
        $('#tahunProgram').val(resp.data.label_tahun_program)
        $('#keterangan').val(resp.data.value)
        if (resp.data.tahun_program.length > 0) {
         console.log(select)
           for (let dd in resp.data.tahun_program) {
              html += `
                <tr class="item-program" item-id="${resp.data.tahun_program[dd].id}">
                  <td scope="row">
              `
              html += `<select class="form-control target">`
              // html += `<option value="">Pilih program</option>`
              for (let dc in select) {
                 if (resp.data.tahun_program[dd].id_template_program_detail !== select[dc].id) {
                  html += `<option value="${select[dc].id}">${select[dc].nama}</option>` 
                 } else {
                  html += `<option value="${select[dc].id}" selected>${select[dc].nama}</option>` 
                 }
              } 
              html += `</select>`
              html += `
              
                  </td>
                  <td>
                   <input type="text" class="form-control satuan" value="${resp.data.tahun_program[dd].satuan}">
                  </td>
                  <td>
                   <input type="text" class="form-control unit"  value="${resp.data.tahun_program[dd].unit}">
                  </td>
                  <td>
                  <div class="ic" id-target=${resp.data.tahun_program[dd].id} onclick="deleteListAda(this)"><i class="fas fa-trash" style="color: #E12D2D" ></i></div>
                  </td>
                </tr>
              `
           }
           $('#tbd').html(html)
        }
      }
   }

   $('#add').click(function() {
      var html2 = $('#tbd').html()

      html2 += `
         <tr class="item-program" item-id="0">
           <td scope="row">
       `
       html2 += `<select class="form-control target">`
       // html2 += `<option value="">Pilih program</option>`
       for (let dc in select) {
        
           html2 += `<option value="${select[dc].id}">${select[dc].nama}</option>` 
       } 
       html2 += `</select>`
       html2 += `
       
           </td>
           <td>
            <input type="text" class="form-control satuan" >
           </td>
           <td>
            <input type="text" class="form-control unit" >
           </td>
           <td>
           <div class="ic" id-target=${0}><i class="fas fa-trash" style="color: #E12D2D" onclick="deleteList(this)
           "></i></div>
           </td>
         </tr>
       `
       $('#tbd').html(html2)
   })

     // $(".ic").on('click', function(event){
     //     console.log('tes')
     //     // event.stopPropagation();
     //     // event.stopImmediatePropagation();
     //     console.log(event)
     //     //(... rest of your JS code)
     // });
   async function deleteById () {
    const url = `{{url('api/tahun-program-detail/')}}`
     for (let dd in id) {
      const res = await fetch(url+`/${id[dd]}`, {
         method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         },
         redirect: 'follow', // manual, *follow, error
         referrerPolicy: 'no-referrer'
      })
     }
   }
   $('#save').click(async (e) => {
      console.log(id)
      deleteById()
      let res = 'tes'
      $('#save').html(`
      <div class="spinner-border"></div>
      `)
      $('#cancel-btn').attr("href", '#')
      
      const items = $('.item-program').each(function () {
        return $(this)
      })
      
      const satuans = $('.satuan').each(function () {
        return $(this)
      })

      const units = $('.unit').each(function () {
        return $(this)
      })

      const targets = $('.target').each(function () {
        return $(this)
      })

      await items.map(async (val, ind) => {
        console.log(targets[val].value)
        if (items[val].getAttribute('item-id') !== '0') {
        } else {
         var url = "{{url('api/tahun-program-detail/create')}}"
         body = {
             "id_tahun_program": @php echo $id; @endphp,
             "id_template_program_detail": targets[val].value,
             "unit": units[val].value,
             "satuan": satuans[val].value,
             "status": 1
         }
         res = await fetch(url,
          {
           method: 'POST', // *GET, POST, PUT, DELETE, etc.
           mode: 'cors', // no-cors, *cors, same-origin
           cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
           credentials: 'same-origin', // include, *same-origin, omit
           headers: {
             'Content-Type': 'application/json'
             // 'Content-Type': 'application/x-www-form-urlencoded',
           },
           redirect: 'follow', // manual, *follow, error
           referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
           body: JSON.stringify(body) // body data type must match "Content-Type" header
         });

        }
      })
      $('#success').show()
      $('#save').html(`
        Simpan
      `)
      $('#success').show()
      $('#cancel-btn').attr("href", "{{route('template-program')}}")

   })

})

</script>
@endsection