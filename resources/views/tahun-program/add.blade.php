<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
 input {
     border: 0 !important;
     background: transparent !important;
     border-bottom: 1px solid #cdcdcd !important;
 }
 .tb-container{
   background: #fff;
   margin-top: 1em;
 }
 tbody > tr {
    line-height: 45px !important;
    min-height: 45px !important;
    height: 45px !important;
 }
</style>
<br/>
<br/>
<div class="col">
<div class="alert alert-success" id="success" role="alert">
  Berhasil menambah tahun program
</div>
<div class="alert alert-danger" id="err-al" role="alert">
  Gagal menambah tahun program
</div>
  <div class="col">
    <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Tahun Program *</label>
         <input type="" class="form-control" id="tahunProgram" placeholder="No Data Available">
         <small id="tahunProgramHelp" class="form-text text-muted" style="color:red !important"></small>
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Keterangan *</label>
         <input type="" class="form-control" id="keterangan" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="exampleInputEmail1">Status *</label>
         <input type="" class="form-control" id="status" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-10"></div>
      <div class="row" style="gap: 1em">
      <button type="button" class="btn btn-primary" id="save" style="background: #193865">Simpan</button>
      <a type="button" class="btn btn-primary" id="cancel-btn" href="{{route('tahun-program')}}" style="background: #fff;color: #193865">Batal</a>
      </div>
    </div>
</div>
<script>
$(document).ready(function () {
  $('#success').hide()
  $('#err-al').hide()
 // $('#cancel-btn').href('#')
 // $('#cancel-btn').attr("href", '#')
 var errNama = true
 function validateName () {
   var value = $('#tahunProgram').val()
   if (value.length < 1) {
     $('#tahunProgramHelp').show()
     $('#tahunProgramHelp').html('Nama program tidak boleh kosong')
     errNama = true
   } else {
    errNama = false
    $('#tahunProgramHelp').hide()
   }
 }
 $('#save').click(async function (){
    
    validateName()
    if (errNama) { 
      return false
    } else {
      $('#save').html(`
      <div class="spinner-border"></div>
      `)
      $('#cancel-btn').attr("href", '#')
      const url = "{{url('api/tahun-program/create')}}"
      const body = {
        value: $('#tahunProgram').val(),
        label: $('#keterangan').val(),
        status: 1,
        created_by: "{{$nama}}"
      }
     const res = await fetch(url, {
         method: 'POST', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         },
         redirect: 'follow', // manual, *follow, error
         referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
         body: JSON.stringify(body) // body data type must match "Content-Type" header
       });
       const resp = await res.json()
       if (resp.status === 200) {
        $('#save').html(`
          Simpan
        `)
        $('#success').show()
        $('#cancel-btn').attr("href", "{{route('tahun-program')}}")
        window.location = `{{route('tahun-program')}}`
        @php {{session()->flash('status', 'Berhasil Menambah Task!');}} @endphp
       } else {
         $('#save').html(`
           Simpan
         `)
         $('#err-al').show()
         $('#cancel-btn').attr("href", "{{route('tahun-program')}}")
       }
       // console.log(resp)
      // alert("{{$nama}}")
    }
 })
})
</script>
@endsection