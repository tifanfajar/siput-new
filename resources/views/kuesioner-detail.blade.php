@extends('dev.core.using')
@section('content')
@include('templates.pelayanan.unar.core.add')
@include('templates.pelayanan.unar.core.detail')
@include('templates.pelayanan.unar.core.edit')
@include('templates.pelayanan.unar.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.helpers.filter')
@include('templates.pelayanan.unar.dev.data')

<div class="container-fluid" style="margin-top: 40px;">
	<div class="row justify-content-center">
		<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
		@if($getStatus != 'administator')
		<div class="col-sm-12">
			<div class="pt-4" style="margin-bottom: 20px;">
				<div class="card shadow" style="border:1px solid #dedede;">
					<div class="card-header bg-transparent">
						<div class="row align-items-center">
							<div class="col">
								<center>
									<h4 class="mb-0">{{ $kuesioner->title }}</h4>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		<div class="col-xl-12">
			<div class="card shadow form-group">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;">PERTANYAAN</h3>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
						<thead class="thead-light">
							<tr>
								<th scope="col">NO</th>
								<th scope="col">Pertanyaan</th>
								<th scope="col">Jawaban Terbanyak</th>
								<th scope="col">Presentase</th>
							</tr>
						</thead>
						<tbody>
							@foreach($questions as $key => $question)
							<tr>
								<th>{{ ++$key }}</th>
								<td>{{ $question['pertanyaan'] }}</td>
								<td>{{ $question['answer'] }}</td>
								<td>{{ $question['percentage'] }}%</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="card shadow form-group">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;">ESSAY</h3>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
						<thead class="thead-light">
							<tr>
								<th scope="col">NO</th>
								<th scope="col">Pertanyaan</th>
								<th scope="col">UPT</th>
								<th scope="col">Jawaban Essay</th>
							</tr>
						</thead>
						<tbody>
							@foreach($essais as $key => $essay)
							@foreach($essay_answer[$essay->id] as $key2 => $answer)
							<tr>
								<th>{{ ++$key }}</th>
								<td>{{ $essay->pertanyaan }}</td>
								<td>{{ $key2 }}</td>
								<td>{{ $answer }}</td>
							</tr>
							@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="card shadow form-group">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;">LIST OPTION</h3>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
						<thead class="thead-light">
							<tr>
								<th scope="col">NO</th>
								<th scope="col">Pertanyaan</th>
								<th scope="col">Option</th>
								<th scope="col">Total Jawaban</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							@foreach($options['questions'] as $key => $question)
							@foreach($options['option_detail'][$question] as $key2 => $answer)
							<tr>
								<th>{{ $i++ }}</th>
								<td>{{ $key }}</td>
								<td>{{ $key2 }}</td>
								<td>{{ $answer }}</td>
							</tr>
							@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection