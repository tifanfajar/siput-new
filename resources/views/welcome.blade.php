<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
	.footer {padding-bottom: 0px !important;}
</style>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0 text-uppercase"><i class="fas fa-calendar-alt"></i>&nbsp;&nbsp;{{$id_bulan}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Penyelesaian Pengaduan Perizinan</h5>
						<span class="h2 font-weight-bold mb-0">{!!  substr(strip_tags($loketpengaduanpercent), 0, 5) !!} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-support-16"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Penyelesaian Pengaduan Perizinan</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Sosialisasi dan Bimtek</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($total_sosialisasi, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-send"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Sosialisasi dan Bimtek</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Penanganan Tagihan dan Piutang BHP Frekuensi Radio Rincian Tagihan</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($rt_presentation, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-money-coins"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap" style="display: block; width: 300px; white-space: normal !important;">Rincian Tagihan</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Penanganan Tagihan dan Piutang BHP Frekuensi Radio Status Tagihan</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($st_presentation, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-money-coins"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap" style="display: block; width: 300px; white-space: normal !important;">Status Tagihan</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Piutang Telah Selesai di KPKNL</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($petang, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-money-coins"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="">Piutang Telah Selesai di KPKNL</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Pelaksanaan UNAR</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($unarpercent, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="fas fa-percent"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Pelaksanaan UNAR</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Validasi dan Inspeksi</h5>
						<span class="h2 font-weight-bold mb-0">{{number_format($total_inspeksi, 2)}} %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-app"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Validasi dan Inspeksi</span>
				</p>
			</div>
		</div>
	</div>


</div>

<hr>

<div class="btn-group" role="group" aria-label="Basic example">
	<button type="button" class="btn btn-sm btn-info">Lihat Rincian Per UPT</button>
	<button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modalBulans">Lihat Capaian Per Bulan</button>
	<a href="http://surveysdppi.pw/login" type="button"  class="btn btn-sm btn-info">SURVEY LINK</a>
</div>

<!-- Modal -->
<div class="modal fade" id="modalBulans" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">Lihat Capaian Per Bulan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<form method="GET" action="{{route('dashboard/month')}}" enctype="multipart/form-data">
						<div class="row">
							<select class="form-control" name="id_bulan">
								<option selected>Pilih Salah Satu</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
							<br>
							<br>
							<button class="btn btn-primary btn-block" type="submit">Submit</button>
						</div>
					</form>
				</center>
			</div>
		</div>
	</div>
</div>
@endsection
