<!DOCTYPE html>
<html lang="pt-BR">
<link rel="icon" href="{{asset('public/assets/images/icon.png')}}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>LOGIN - APLIKASI PELAPORAN PELAYANAN</title>
    @include('auth.asset.css')
   <link rel="stylesheet" href="{{asset('templates/assets/css/jquery-confirm.css') }}">
   <link href="{{asset('templates/assets/css/argon-dashboard.css?v=1.1.0')}}" rel="stylesheet" />
   <link href="{{asset('templates/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" />
</head>
<style>
 input::placeholder {
  color: grey;
  font-size: 0.9em;
}
input:hover {
  color: grey;
  font-size: 0.9em;
}
input {
  color: grey;
  font-size: 0.9em;
}
</style>
<div class="custom-login-wrapper">
    <div class="custom-login-form">
        <div class="row">
            <div class="col-md-6">
              <div class="row" style="margin-top:-150px;margin-left:20px">
                  <div class="col-md-4">
                      <img src="{{asset('templates/assets/img/logo-kominfo.png')}}" alt="..." width="275px">
                  </div>
              </div>
              <h1 style="color: white;margin-top:90px">APLIKASI PELAPORAN PELAYANAN<br>
                SPEKTRUM FREKUENSI RADIO DAN
                SERTIFIKASI OPERATOR RADIO DI UPT <br>
                MONITOR SPEKTRUM FREKUENSI RADIO</h1>
              <div class="row" style="margin-left:-600px;margin-top:275px">
                <div class="col-md-12">
                <span class="copyright" style="color: white;float:left">&copy 2022 Kementerian Komunikasi dan Informatika Republik Indonesia</span>
                </div>
              </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                  <div class="card shadow" style="border:1px solid #dedede;margin-top:-50px;height:350px;opacity:0.6;border-radius:0px">
                    <div class="card-header bg-transparent">
                          
                    </div>
                  </div>
                  <div class="card shadow" style="border:1px solid #dedede;margin-left:26px;margin-top:-375px;height:400px;width:92%;opacity:0.8;border-radius:0px">
                    <div class="card-header bg-transparent">
                          <h1 style="margin-top:20px">LOGIN</h1>
                          <small>Silakan masukan informasi nama pengguna dan kata sandi untuk login</small>
                          <form class="form" method="POST" action="{{ route('postLogin') }}">
                              @csrf
                              <span class="info" style="color: white;">Silahkan masukan informasi nama pengguna dan kata sandi anda untuk login</span>
                              <input type="text" placeholder="Nama Pengguna" name="email" required style="border: none;border-bottom: 1px solid grey;">
                              @if ($errors->has('username') || $errors->has('email'))
                              <span class="invalid-feedback">
                                <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                            </span>
                            @endif
                            <input type="password" placeholder="Kata Sandi" name="password" required style="border: none;border-bottom: 1px solid grey;">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                            <button type="submit" class="btn btn-info" style="border-radius:40px">LOGIN</button>
                        </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
  @include('auth.asset.js')
</body>
</html>
