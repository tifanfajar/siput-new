@extends('layouts.core.app')
@section('title')  
Registrasi Stasiun Bumi
@endsection
@section('pageIcon') fa fa-globe-asia @endsection
@section('pageTitle') Registrasi Stasiun Bumi @endsection
@section('subPageTitle') Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code. @endsection

@section('content')

<div id='beranda' style='width: 100%; height: 100%;'></div>
<br><br>


@endsection

@section('script')
@include('layouts.dev.regSTbumi.script')
@endsection
