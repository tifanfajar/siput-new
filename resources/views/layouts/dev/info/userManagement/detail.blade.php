@extends('helpers.modals.detailModal')
@section('detailModalForm')
<div class="modal-body">
    <div class="row">
        <div class="position-relative form-group col-md-6">
            <label for="detailName" class="importantField">Name</label>
            <input id="detailName" placeholder="Enter a name" type="text" class="form-control" readonly>
        </div>
        <div class="position-relative form-group col-md-6">
            <label for="detailEmail" class="importantField">Email</label>
            <input id="detailEmail" placeholder="Enter a email" type="email" class="form-control" readonly>
        </div>
        <div class="position-relative form-group col-md-6">
            <label for="detailRoleId" class="importantField">Role</label>
            <input id="detailRoleId" placeholder="Enter a role" type="text" class="form-control" readonly>
        </div>
        <div class="position-relative form-group col-md-6">
            <label for="detailUserType" class="importantField">User Type</label>
            <input id="detailUserType" placeholder="Enter a user type" type="text" class="form-control" readonly>
        </div>
        <div class="position-relative form-group col-md-6">
            <label for="detailTypeCode" class="importantField">Type Code</label>
            <input id="detailTypeCode" placeholder="There is no type code" type="text" class="form-control" readonly>
        </div>
        <div class="position-relative form-group col-md-6">
            <label for="detailPhotoProfile" class="optionalField">Profile Photo</label>
            <br>
            <div id="detailPhotoProfile"></div>
        </div>
    </div>
</div>
<div class="modal-footer">
    @include('helpers.button.buttonClose')
</div>
@endsection 