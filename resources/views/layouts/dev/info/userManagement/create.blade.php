@extends('helpers.modals.createModal')
@section('createModalForm')
<form method="POST" action="{{ route('createUser') }}" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="position-relative form-group col-md-6">
                <label for="name" class="importantField">Name</label>
                <input name="name" id="create_name" placeholder="Enter a name" type="text" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="email" class="importantField">Email</label>
                <input name="email" id="create_email" placeholder="Enter a email" type="email" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="password" class="importantField">Password</label>
                <input name="password" id="create_password" placeholder="Enter a password" type="password" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="role_id" class="importantField">Role</label>
                <select name="role_id" id="create_role_id" class="form-control" required>
                    <option value="" selected disabled>Enter a Role</option>
                    @foreach(\App\Model\Privillage\Role::all() as $key => $value)
                    <option value="{{$value->id}}">{{$value->role_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="userType" class="importantField">User Type</label>
                <select id="userType" name="userType" class="form-control" required>
                    <option value="" selected disabled>Enter a type</option>
                    <option value="user">User</option>
                    <option value="upt">UPT</option>
                    <option value="waba">Waba</option>
                </select>
            </div>
            <div class="position-relative form-group col-md-6 div-type-code">
                <label for="typeCode" id="labeltypeCode" class="importantField">Select User Type First</label>
                <select name="typeCode" id="typeCode" class="form-control selectData">
                    <option value="" selected disabled>Enter a sub type</option>
                </select>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="profile_photo" class="optionalField">Profile Photo</label>
                <input name="profile_photo" id="create_profile_photo" type="file" class="form-control-file" accept="image/*" onchange="document.getElementById('PhotoProfilePreview').src = window.URL.createObjectURL(this.files[0])">
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="profile_photo" class="optionalField">Preview Photo</label>
                <br>
                <img style="border-radius: 50%;" id="PhotoProfilePreview" src="{{ route ('userLoginImages','default.jpg')}}" width="150" height="150">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('helpers.button.buttonClose')
        @include('helpers.button.buttonSave')
    </div>
</form>
@endsection