@extends('helpers.modals.createModal')
@section('createModalForm')
<form method="POST" action="{{ route('createPrivillage') }}" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="position-relative form-group col-md-6">
                <label for="roleName" class="importantField">Role Name</label>
                <input name="role_name" id="createRoleName" placeholder="Enter a role name" type="text" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-6">
                <label for="descriptionRole" class="importantField">Description Role</label>
                <input name="description" id="descriptionRole" placeholder="Enter a description role" type="text" class="form-control" required>
            </div>
            <div class="position-relative form-group col-md-12">
                <table class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 350px;">Beranda - Module</th>
                            <th scope="col">Create</th>
                            <th scope="col">Read</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Check All</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="minimal-checkbox-100019" id='minimal-checkbox-100019'>
                            <td>Beranda - Dasboard</td>
                            <td><input name="100019_create" class="minimal-checkbox-100019" type="checkbox" id="create-minimal-checkbox-100019" value="100019"></td>
                            <td><input name="100019_read" class="minimal-checkbox-100019" type="checkbox" id="read-minimal-checkbox-100019" value="100019"></td>
                            <td><input name="100019_update" class="minimal-checkbox-100019" type="checkbox" id="update-minimal-checkbox-100019" value="100019"></td>
                            <td><input name="100019_delete" class="minimal-checkbox-100019" type="checkbox" id="delete-checkbox-100019" value="100019"></td>
                            <td><input type="checkbox" id="all-100019" value="all" name="all" onChange="check(100019)" /></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 350px;">ISR QR Code - Module</th>
                            <th scope="col">Create</th>
                            <th scope="col">Read</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Check All</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='minimal-checkbox-10001'>
                            <td>ISR QR Code - Dashboard</td>
                            <td><input name="10001_create" type="checkbox" id="create-minimal-checkbox-10001" value="10001"></td>
                            <td><input name="10001_read" type="checkbox" id="read-minimal-checkbox-10001" value="10001"></td>
                            <td><input name="10001_update" type="checkbox" id="update-minimal-checkbox-10001" value="10001"></td>
                            <td><input name="10001_delete" type="checkbox" id="delete-minimal-checkbox-10001" value="10001"></td>
                            <td><input type="checkbox" id="all-10001" value="all" name="all" onChange="check(10001)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10002'>
                            <td>ISR QR Code - QR Code Map</td>
                            <td><input name="10002_create" type="checkbox" id="create-minimal-checkbox-10002" value="10002"></td>
                            <td><input name="10002_read" type="checkbox" id="read-minimal-checkbox-10002" value="10002"></td>
                            <td><input name="10002_update" type="checkbox" id="update-minimal-checkbox-10002" value="10002"></td>
                            <td><input name="10002_delete" type="checkbox" id="delete-minimal-checkbox-10002" value="10002"></td>
                            <td><input type="checkbox" id="all-10002" value="all" name="all" onChange="check(10002)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10003'>
                            <td>ISR QR Code - QR Code Location</td>
                            <td><input name="10003_create" type="checkbox" id="create-minimal-checkbox-10003" value="10003"></td>
                            <td><input name="10003_read" type="checkbox" id="read-minimal-checkbox-10003" value="10003"></td>
                            <td><input name="10003_update" type="checkbox" id="update-minimal-checkbox-10003" value="10003"></td>
                            <td><input name="10003_delete" type="checkbox" id="delete-minimal-checkbox-10003" value="10003"></td>
                            <td><input type="checkbox" id="all-10003" value="all" name="all" onChange="check(10003)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10004'>
                            <td>ISR QR Code - Print QR Code</td>
                            <td><input name="10004_create" type="checkbox" id="create-minimal-checkbox-10004" value="10004"></td>
                            <td><input name="10004_read" type="checkbox" id="read-minimal-checkbox-10004" value="10004"></td>
                            <td><input name="10004_update" type="checkbox" id="update-minimal-checkbox-10004" value="10004"></td>
                            <td><input name="10004_delete" type="checkbox" id="delete-minimal-checkbox-10004" value="10004"></td>
                            <td><input type="checkbox" id="all-10004" value="all" name="all" onChange="check(10004)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10005'>
                            <td>ISR QR Code - Form Inspeksi</td>
                            <td><input name="10005_create" type="checkbox" id="create-minimal-checkbox-10005" value="10005"></td>
                            <td><input name="10005_read" type="checkbox" id="read-minimal-checkbox-10005" value="10005"></td>
                            <td><input name="10005_update" type="checkbox" id="update-minimal-checkbox-10005" value="10005"></td>
                            <td><input name="10005_delete" type="checkbox" id="delete-minimal-checkbox-10005" value="10005"></td>
                            <td><input type="checkbox" id="all-10005" value="all" name="all" onChange="check(10005)" /></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 350px;">Registrasi Stasiun Bumi - Module</th>
                            <th scope="col">Create</th>
                            <th scope="col">Read</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Check All</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='minimal-checkbox-10006'>
                            <td>Registrasi Stasiun Bumi - Dashboard</td>
                            <td><input name="10006_create" type="checkbox" id="create-minimal-checkbox-10006" value="10006"></td>
                            <td><input name="10006_read" type="checkbox" id="read-minimal-checkbox-10006" value="10006"></td>
                            <td><input name="10006_update" type="checkbox" id="update-minimal-checkbox-10006" value="10006"></td>
                            <td><input name="10006_delete" type="checkbox" id="delete-minimal-checkbox-10006" value="10006"></td>
                            <td><input type="checkbox" id="all-10006" value="all" name="all" onChange="check(10006)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10007'>
                            <td>Registrasi Stasiun Bumi - QR Code Map</td>
                            <td><input name="10007_create" type="checkbox" id="create-minimal-checkbox-10007" value="10007"></td>
                            <td><input name="10007_read" type="checkbox" id="read-minimal-checkbox-10007" value="10007"></td>
                            <td><input name="10007_update" type="checkbox" id="update-minimal-checkbox-10007" value="10007"></td>
                            <td><input name="10007_delete" type="checkbox" id="delete-minimal-checkbox-10007" value="10007"></td>
                            <td><input type="checkbox" id="all-10007" value="all" name="all" onChange="check(10007)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10008'>
                            <td>Registrasi Stasiun Bumi - QR Code Location</td>
                            <td><input name="10008_create" type="checkbox" id="create-minimal-checkbox-10008" value="10008"></td>
                            <td><input name="10008_read" type="checkbox" id="read-minimal-checkbox-10008" value="10008"></td>
                            <td><input name="10008_update" type="checkbox" id="update-minimal-checkbox-10008" value="10008"></td>
                            <td><input name="10008_delete" type="checkbox" id="delete-minimal-checkbox-10008" value="10008"></td>
                            <td><input type="checkbox" id="all-10008" value="all" name="all" onChange="check(10008)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-10009'>
                            <td>Registrasi Stasiun Bumi - Print QR Code</td>
                            <td><input name="10009_create" type="checkbox" id="create-minimal-checkbox-10009" value="10009"></td>
                            <td><input name="10009_read" type="checkbox" id="read-minimal-checkbox-10009" value="10009"></td>
                            <td><input name="10009_update" type="checkbox" id="update-minimal-checkbox-10009" value="10009"></td>
                            <td><input name="10009_delete" type="checkbox" id="delete-minimal-checkbox-10009" value="10009"></td>
                            <td><input type="checkbox" id="all-10009" value="all" name="all" onChange="check(10009)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100010'>
                            <td>Registrasi Stasiun Bumi - Form Inspeksi</td>
                            <td><input name="100010_create" type="checkbox" id="create-minimal-checkbox-100010" value="100010"></td>
                            <td><input name="100010_read" type="checkbox" id="read-minimal-checkbox-100010" value="100010"></td>
                            <td><input name="100010_update" type="checkbox" id="update-minimal-checkbox-100010" value="100010"></td>
                            <td><input name="100010_delete" type="checkbox" id="delete-minimal-checkbox-100010" value="100010"></td>
                            <td><input type="checkbox" id="all-100010" value="all" name="all" onChange="check(100010)" /></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 350px;">Registrasi BTS IPFR - Module</th>
                            <th scope="col">Create</th>
                            <th scope="col">Read</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Check All</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='minimal-checkbox-100011'>
                            <td>Registrasi BTS IPFR - Dashboard</td>
                            <td><input name="100011_create" type="checkbox" id="create-minimal-checkbox-100011" value="100011"></td>
                            <td><input name="100011_read" type="checkbox" id="read-minimal-checkbox-100011" value="100011"></td>
                            <td><input name="100011_update" type="checkbox" id="update-minimal-checkbox-100011" value="100011"></td>
                            <td><input name="100011_delete" type="checkbox" id="delete-minimal-checkbox-100011" value="100011"></td>
                            <td><input type="checkbox" id="all-100011" value="all" name="all" onChange="check(100011)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100012'>
                            <td>Registrasi BTS IPFR - QR Code Map</td>
                            <td><input name="100012_create" type="checkbox" id="create-minimal-checkbox-100012" value="100012"></td>
                            <td><input name="100012_read" type="checkbox" id="read-minimal-checkbox-100012" value="100012"></td>
                            <td><input name="100012_update" type="checkbox" id="update-minimal-checkbox-100012" value="100012"></td>
                            <td><input name="100012_delete" type="checkbox" id="delete-minimal-checkbox-100012" value="100012"></td>
                            <td><input type="checkbox" id="all-100012" value="all" name="all" onChange="check(100012)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100013'>
                            <td>Registrasi BTS IPFR - QR Code Location</td>
                            <td><input name="100013_create" type="checkbox" id="create-minimal-checkbox-100013" value="100013"></td>
                            <td><input name="100013_read" type="checkbox" id="read-minimal-checkbox-100013" value="100013"></td>
                            <td><input name="100013_update" type="checkbox" id="update-minimal-checkbox-100013" value="100013"></td>
                            <td><input name="100013_delete" type="checkbox" id="delete-minimal-checkbox-100013" value="100013"></td>
                            <td><input type="checkbox" id="all-100013" value="all" name="all" onChange="check(100013)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100014'>
                            <td>Registrasi BTS IPFR - Print QR Code</td>
                            <td><input name="100014_create" type="checkbox" id="create-minimal-checkbox-100014" value="100014"></td>
                            <td><input name="100014_read" type="checkbox" id="read-minimal-checkbox-100014" value="100014"></td>
                            <td><input name="100014_update" type="checkbox" id="update-minimal-checkbox-100014" value="100014"></td>
                            <td><input name="100014_delete" type="checkbox" id="delete-minimal-checkbox-100014" value="100014"></td>
                            <td><input type="checkbox" id="all-100014" value="all" name="all" onChange="check(100014)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100015'>
                            <td>Registrasi BTS IPFR - Form Inspeksi</td>
                            <td><input name="100015_create" type="checkbox" id="create-minimal-checkbox-100015" value="100015"></td>
                            <td><input name="100015_read" type="checkbox" id="read-minimal-checkbox-100015" value="100015"></td>
                            <td><input name="100015_update" type="checkbox" id="update-minimal-checkbox-100015" value="100015"></td>
                            <td><input name="100015_delete" type="checkbox" id="delete-minimal-checkbox-100015" value="100015"></td>
                            <td><input type="checkbox" id="all-100015" value="all" name="all" onChange="check(100015)" /></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Info - Module</th>
                            <th scope="col">Create</th>
                            <th scope="col">Read</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                            <th scope="col">Check All</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='minimal-checkbox-100016'>
                            <td>Info - User Management</td>
                            <td><input name="100016_create" type="checkbox" id="create-minimal-checkbox-100016" value="100016"></td>
                            <td><input name="100016_read" type="checkbox" id="read-minimal-checkbox-100016" value="100016"></td>
                            <td><input name="100016_update" type="checkbox" id="update-minimal-checkbox-100016" value="100016"></td>
                            <td><input name="100016_delete" type="checkbox" id="delete-minimal-checkbox-100016" value="100016"></td>
                            <td><input type="checkbox" id="all-100016" value="all" name="all" onChange="check(100016)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100017'>
                            <td>Info - Privillage</td>
                            <td><input name="100017_create" type="checkbox" id="create-minimal-checkbox-100017" value="100017"></td>
                            <td><input name="100017_read" type="checkbox" id="read-minimal-checkbox-100017" value="100017"></td>
                            <td><input name="100017_update" type="checkbox" id="update-minimal-checkbox-100017" value="100017"></td>
                            <td><input name="100017_delete" type="checkbox" id="delete-minimal-checkbox-100017" value="100017"></td>
                            <td><input type="checkbox" id="all-100017" value="all" name="all" onChange="check(100017)" /></td>
                        </tr>
                        <tr id='minimal-checkbox-100018'>
                            <td>Info - Backup Database</td>
                            <td><input name="100018_create" type="checkbox" id="create-minimal-checkbox-100018" value="100018"></td>
                            <td><input name="100018_read" type="checkbox" id="read-minimal-checkbox-100018" value="100018"></td>
                            <td><input name="100018_update" type="checkbox" id="update-minimal-checkbox-100018" value="100018"></td>
                            <td><input name="100018_delete" type="checkbox" id="delete-minimal-checkbox-100018" value="100018"></td>
                            <td><input type="checkbox" id="all-100018" value="all" name="all" onChange="check(100018)" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('helpers.button.buttonClose')
        @include('helpers.button.buttonSave')
    </div>
</form>
@endsection