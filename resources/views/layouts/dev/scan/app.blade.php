@extends('layouts.core.app')

@section('title', 'Scan Result')
@section('pageIcon', 'fa fa-qrcode')
@section('pageTitle', 'Welcome, '.Auth::user()->name.'! ')
@section('subPageTitle', 'Data Service portal for radio station identification with QR Code.')

@section('content')
<div class="row">
	@foreach($result as $key => $value)
	<div class="col-md-3">
		<div class="mb-3 card card-body"><h5 class="card-title"><i class="fa fa-broadcast-tower"></i>&nbsp;&nbsp;{{$site_name}}</h5>
			<table class="table">
				<tbody>
					<tr>
						<td>CLIENT ID : {{$value->clnt_id}}</td>
					</tr>
					<tr>
						<td>{{$value->clnt_name}}</td>
					</tr>
					<tr>
						<td><b>{{\App\Model\ISR\Site::where('id',$value->site_id)->value('code')}}</b></td>
					</tr>
				</tbody>
			</table>
			<a class="btn btn-info btn-sm text-white buttonDetail" data-toggle="modal" data-target=".modalDetail" data-id="{{$value->id}}"><i class="fa fa-desktop"></i>&nbsp;&nbsp;DETAIL</a>
		</div>
	</div>
	@endforeach
</div>
@endsection
@section('modals')
@include('layouts.dev.scan.detail')
@endsection
@section('script')
@include('layouts.dev.scan.script')
@endsection
