@extends('layouts.core.app')
@section('title') 
ISR QR Code Location
@endsection
@section('pageIcon') fa fa-map-marker @endsection
@section('pageTitle') QR Code Location @endsection
@section('subPageTitle') Data Service portal for radio station identification with QR Code. @endsection

@section('content')
<div class="row justify-content-end">
	<div class="position-relative form-group col-md-2" style="display: none;" id="addSiteBtn">
		<button type="submit" data-toggle="modal" data-target=".modalSiteCreate" class="btn btn-info btn-block btn-lg"><i class="fa fa-fw fa-check"></i> ADD SITE</button>
	</div>
	<div class="position-relative form-group col-md-2" style="display: none;" id="editSiteBtn">
		<button type="submit" data-toggle="modal" data-target=".modalSiteEdit" class="btn btn-warning btn-block btn-lg" style="color: white;"><i class="fa fa-fw fa-edit"></i> EDIT SITE</button>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" data-placeholder="SEMUA NO. APLIKASI">
			<option value="" selected disabled> - SEMUA NO. APLIKASI - </option>
			@foreach($client_ids as $key => $client_id)
			<option value="{{ $client_id }}">{{ $client_id }}</option>
			@endforeach
		</select>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" data-placeholder="SEMUA NO. SIMS">
			<option value="" selected disabled> - SEMUA NO. SIMS - </option>
			@foreach($client_names as $key => $client_name)
			<option value="{{ $client_name }}">{{ $client_name }}</option>
			@endforeach
		</select>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" data-placeholder="SEMUA QRCODE ID">
		</select>
	</div>
</div>
<div class="row justify-content-end">
	<div class="position-relative form-group col-md-2">
		<input type="number" class="form-control" id="longtitudeInput" name="" placeholder="LONGTITUDE">
	</div>
	<div class="position-relative form-group col-md-2">
		<input type="number" class="form-control" id="latitudeInput" name="" placeholder="LATITUDE">
	</div>
	<div class="position-relative form-group col-md-2">
		<button type="submit" class="btn btn-secondary btn-block btn-lg" onclick="getDataSite();"><i class="fa fa-search"></i>&nbsp;&nbsp;CHECK IN</button>
	</div>
</div>
<div id='qrCodeMap'></div>
<br><br>
@endsection

@section('modals')
@include('layouts.dev.isrQRcode.qrCodeLocation.createSite')
@include('layouts.dev.isrQRcode.qrCodeLocation.editSite')
@endsection

@section('script')
@include('layouts.dev.isrQRcode.qrCodeLocation.script')
@endsection
