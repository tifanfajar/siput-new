@extends('helpers.modals.editSiteModal')
@section('editSiteModalForm')
<form action="{{ route('ISRQrCodeSiteLocation') }}" method="POST">
	@csrf
	<div class="modal-body">
		<label class="label-control">Site Name</label>
		<input type="text" name="site" class="form-control" required>
		<textarea id="editPayload" name="payload" style="display: none;"></textarea>
	</div>
	<div class="modal-footer">
		@include('helpers.button.buttonClose')
		@include('helpers.button.buttonSave')
	</div>
</form>
@endsection