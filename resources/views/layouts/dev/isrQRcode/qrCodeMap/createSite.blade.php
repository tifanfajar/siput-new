@extends('helpers.modals.createSiteModal')
@section('createSiteModalForm')
<form action="{{ route('ISRQrCodeSiteMap') }}" method="POST">
	@csrf
	<div class="modal-body">
		<label class="label-control">Site Name</label>
		<input type="text" name="site" class="form-control" required>
		<textarea id="createLongLat" name="longlat" style="display: none;"></textarea>
		<textarea id="createPayload" name="payload" style="display: none;"></textarea>
	</div>
	<div class="modal-footer">
		@include('helpers.button.buttonClose')
		@include('helpers.button.buttonSave')
	</div>
</form>
@endsection