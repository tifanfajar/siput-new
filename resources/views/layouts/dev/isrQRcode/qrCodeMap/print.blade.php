@extends('layouts.core.app')

@section('title', 'ISR QR Code')
@section('pageIcon', 'fa fa-map-marked')
@section('pageTitle', 'QR Code Map')
@section('subPageTitle', 'Portal Layanan Data untuk indentifikasi statiun radio dengan QR Code.')

@section('content')
<form  action="">
	@csrf
	<div class="row justify-content-start">
		<div class="position-relative form-group col-md-2">
			<input name="url" id="url" placeholder="URL Link" type="url" class="form-control">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success btn-block form-control"><i class="fa fa-fw fa-qrcode"></i> Generate QR</button>
		</div>
	</div>
</form>
@endsection
