@extends('layouts.core.app')
@section('title') 
ISR QR Code Map
@endsection
@section('pageIcon') fa fa-map-marked @endsection
@section('pageTitle') QR Code Map @endsection
@section('subPageTitle') Data Service portal for radio station identification with QR Code. @endsection

@section('content')
<div class="row justify-content-end">
	<div class="position-relative form-group col-md-2" style="display: none;" id="addSiteBtn">
		<button type="submit" data-toggle="modal" data-target=".modalSiteCreate" class="btn btn-info btn-block btn-lg"><i class="fa fa-fw fa-check"></i> ADD SITE</button>
	</div>
	<div class="position-relative form-group col-md-2" style="display: none;" id="editSiteBtn">
		<button type="submit" data-toggle="modal" data-target=".modalSiteEdit" class="btn btn-warning btn-block btn-lg" style="color: white;"><i class="fa fa-fw fa-edit"></i> EDIT SITE</button>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" id="app_select" data-placeholder="SEMUA NO. APLIKASI">
		</select>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" id="sims_select" data-placeholder="SEMUA NO. SIMS">
		</select>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" id="qrcode_select" data-placeholder="SEMUA QRCODE ID">
		</select>
	</div>
</div>
<div id='qrCodeMap'></div>
<br><br>
@endsection

@section('modals')
@include('layouts.dev.isrQRcode.qrCodeMap.createSite')
@include('layouts.dev.isrQRcode.qrCodeMap.editSite')
@endsection

@section('script')
@include('layouts.dev.isrQRcode.qrCodeMap.script')
@endsection
