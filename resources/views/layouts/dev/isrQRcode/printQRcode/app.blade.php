@extends('layouts.core.app')

@section('title', 'ISR QR Code Print')
@section('pageIcon', 'fa fa-print')
@section('pageTitle', 'Print QR Code')
@section('subPageTitle', 'Data Service portal for radio station identification with QR Code.')

@section('content')
<div class="row justify-content-end">
	<div class="position-relative form-group col-md-2" style="display: none;" id="printSiteBtn">
		<form action="{{ url()->current().'/print' }}" method="POST">
			@csrf
			<input type="hidden" name="site_code" id="sitePayload">
			<button type="submit" class="btn btn-success btn-block btn-lg" style="color: white;"><i class="fa fa-fw fa-print"></i> PRINT SITE</button>
		</form>
	</div>
	<div class="position-relative form-group col-md-2">
		<select class="form-control" id="qrcode_select" data-placeholder="SEMUA QRCODE ID">
		</select>
	</div>
</div>
<div id='qrCodeMap'></div>
<br><br>
@endsection

@section('script')
@include('layouts.dev.isrQRcode.printQRcode.script')
@endsection
