<script type="text/javascript">
    var randomColorGenerator = function () { 
        return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    };

    var isr = document.getElementById('data-isr').getContext('2d');
    var data_isr = new Chart(isr, {

        type: 'pie',


        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: [
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator()
                ],
                data: [0, 10, 5, 2, 20, 30, 45]
            }]
        },


        options: {}
    });

    var inspeksi = document.getElementById('data-inspeksi').getContext('2d');
    var data_inspeksi = new Chart(inspeksi, {

        type: 'doughnut',


        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: [
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator(),
                randomColorGenerator()
                ],
                data: [0, 10, 5, 2, 20, 30, 45]
            }]
        },


        options: {}
    });
</script>