<?php
if (request()->segment(1) == null) {
  $segment = '/';
}else{
  $segment = request()->segment(1);
}
$module_childs = DB::table('role_acl')
->join('modules','role_acl.module_id','=','modules.kdModule')
->where('pathParent',$segment)
->where('role_id', Auth::user()->role_id)
->where(function ($query) {
  $query->where('create_acl','<>',0)
  ->orWhere('read_acl','<>',0)
  ->orWhere('update_acl','<>',0)
  ->orWhere('delete_acl','<>',0);
})
->orderBy('menu_order','asc')
->get();
?>
<ul class="vertical-nav-menu">
    <li class="app-sidebar__heading">Menu</li>
    @foreach($module_childs as $child)
    <li>
        <a href="{{url($child->menu_path)}}" class="{{ Request::is($child->menu_path) ? 'mm-active' : '' }}">
            <i class="metismenu-icon {{$child->menu_icon}}"></i>
            {{$child->module_name}}
        </a>
    </li>
    @endforeach
</ul>
