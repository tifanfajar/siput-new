<script>
    @if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
    $.confirm({
        title: '{{Session::get('info')}}',
        content: '{{Session::get('alert')}}',
        type: '{{Session::get('colors')}}',
        icon: '{{Session::get('icons')}}',
        typeAnimated: true,
        buttons: {
            close: function () {
            }
        }
    });
    @elseif(count($errors) == 0)
    @else
    $.confirm({
        title: '{{Session::has('info')}}',
        content: '{{Session::get('alert')}}',
        type: 'red',
        typeAnimated: true,
        icon: 'fas fa-exclamation-triangle',
        buttons: {
            close: function () {
            }
        }
    });
    @endif

    $('.deleteConfirmation').confirm({
        title: 'Delete',
        autoClose: 'no|8000',
        type: 'red',
        typeAnimated: true,
        icon: 'fas fa-trash',
        content: 'Are you sure to delete ?',
        buttons: {
            deleteUser: {
                text: 'Yes',
                btnClass: 'btn-danger',
                action: function () {
                    location.href = this.$target.attr('href');
                }
            },
            no: function () {

            }
        }
    });

    $('.logoutConfirmation').confirm({
        title: 'Logout',
        autoClose: 'no|8000',
        type: 'red',
        typeAnimated: true,
        icon: 'fas fa-sign-out-alt',
        content: 'Are you sure to logout ?',
        buttons: {
            logout: {
                text: 'Yes',
                btnClass: 'btn-danger',
                action: function () {
                    event.preventDefault();
                    document.getElementById('logout-form').submit();
                    location.href = this.$target.attr('href');
                }
            },
            no: function () {

            }
        }
    });

    $('#dataTable').dataTable()
    
</script>