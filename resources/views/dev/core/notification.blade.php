<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">PEMBERITAHUAN</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="text-white">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="height: 500px;overflow-y: auto">
				<table cellpadding="10px">
				  <thead>
				    <tr>
				      <th scope="col" width="20%" class="text-center">MENU</th>
				      <th scope="col" class="text-center">PESAN</th>
				      <th scope="col" class="text-center">TANGGAL</th>
				    </tr>
				  </thead>
				  <tbody id="ntbody">
				 
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</div>