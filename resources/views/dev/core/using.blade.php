<!DOCTYPE html>
<html lang="en">
<head>
  @include('dev.helpers.css')
  @include('dev.helpers.js')
  @include('dev.helpers.meta')
  @include('dev.helpers.title')
</head>

<body class="">
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main" style="overflow: hidden;">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="{{url('/')}}">
        <img src="{{asset('templates/assets/img/logo-kominfo.png')}}" class="navbar-brand-img" alt="...">
      </a>
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
      <!-- Collapse header -->
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{url('/')}}">
              <img src="{{asset('templates/assets/img/brand/blue.png')}}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navigation -->
      <ul class="navbar-nav" style="margin-top: 30px;">
       @include('dev.core.menu')
     </ul>
   </div>
 </div>
</nav>
<div class="main-content">
  <!-- Navbar -->
  <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main" style="border-bottom: 1px solid #dedede; background:transparent;">
    <div class="container-fluid">
      <!-- Brand -->
      <a class="h4 text-black text-uppercase d-none d-lg-inline-block" href="#"><i class="ni ni-tv-2 text-black"></i>&nbsp;&nbsp;@if(Request::segment(1) == null) Dashboard @else {{ Request::segment(1) }} @endif</a>
      @include('dev.core.navbar')
    </div>
</nav>
<!-- End Navbar -->
<!-- Header -->
<div class="header pb-6 pt-6">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<div class="container-fluid mt--7">
  @yield('content')
  
  @include('dev.core.footer')
</div>
</div>
@include('dev.notification.data') 
@include('dev.core.notification') 


</body>

</html>