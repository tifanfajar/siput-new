
@extends('dev.core.using')
@section('content')

<div class="container">
	<br>
	<br>
	<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-key"></i>&nbsp;&nbsp;UBAH KATA SANDI</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	<br>
	<form action="{{route('postPassword')}}" class="row" method="POST" enctype="multipart/form-data">
	@csrf
	<div class="col-md-4 form-group">
	<label for="password">Masukan Kata Sandi Baru </label><label style="color: red;">*</label>
	<input type="password" class="form-control mb-3" id="kata_sandi" name="kata_sandi" placeholder="Masukan Kata Sandi Baru" required>
	<button type="submit" class="btn btn-primary" onClick="confirm('Sudah yakin dengan kata sandi yang baru?')"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan Data</button>
	<a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</a>
	</div>				
	&nbsp;&nbsp;&nbsp;&nbsp;
	</form>
</div>

@endsection