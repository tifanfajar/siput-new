<ul class="breadcrumbs">
  <?php $segments = ''; ?>
  @foreach(Request::segments() as $segment)
  <?php $segments .= '/'.$segment; ?>
  <li style="text-transform: uppercase;">
    <a class="text-info" href="{{ url($segments) }}">{{ substr($segment,0, 30) }}</a>
  </li>
  @endforeach
</ul>
<?php
$module_parent = DB::table('role_acl')
->select('module_parent','module_name','pathParent','menu_icon')
->join('modules','role_acl.module_parent','=','modules.id')
->where('role_id', Auth::user()->role_id)
->where(function ($query){
  $query->where('create_acl','<>',0)
  ->orWhere('read_acl','<>',0)
  ->orWhere('update_acl','<>',0)
  ->orWhere('delete_acl','<>',0);
})
->groupBy('module_parent')
->groupBy('module_name')
->groupBy('pathParent')
->groupBy('menu_icon')
->orderBy('module_parent','asc')
->get();
?>
<!-- User -->
<ul class="navbar-nav align-items-center d-none d-md-flex user-inf">
<li class="nav-item dropdown" style="border-bottom: 0 none;">
   <a href="#" class="mailBx" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php 
    $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
      if($getStatus == 'administrator'){
          $total_notif = \App\Model\Notification\Notification::where('readableAdmin',0)->count();
      }else{
          $total_notif = \App\Model\Notification\Notification::where('readable',0)->where('id_upt', Auth::user()->upt)->count();
      }
    ?>
    <div class="notifed" id="notifiedAll">{{$total_notif}}</div>
    <i class="ni ni-bell-55 text-info"></i>
  </a>
  <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" style="top: 20px; height: 330px; overflow-y: scroll;">
    @foreach($module_parent as $parent)
        <?php
      $module_childs = DB::table('role_acl')
    ->join('modules','role_acl.module_id','=','modules.kdModule')
    ->where('module_parent',$parent->module_parent)
    ->where('role_id', Auth::user()->role_id)
    ->where(function ($query) {
      $query->where('create_acl','<>',0)
      ->orWhere('read_acl','<>',0)
      ->orWhere('update_acl','<>',0)
      ->orWhere('delete_acl','<>',0);
    })
    ->orderBy('menu_order','asc')
    ->get();
    ?>
  @foreach($module_childs as $child)
    <?php
    if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
      $readable = \App\Model\Notification\Notification::where('id_module', $child->id)->where('readableAdmin',0)->count();
    }else{
      $readable = \App\Model\Notification\Notification::where('id_module', $child->id)->where('id_upt',Auth::user()->upt)->where('readable',0)->count();
    }
    ?>
    <button class="dropdown-item btn btn-block btnNotification" id="btnNotification" data-toggle="modal" 
    data-target="#notificationModal"
    data-id-module="{{$child->id}}"
    style="position: relative;">{{$child->module_name}} <div class="notifed" style="right: 5px; top: 10px;">{{$readable}}</div></button>
    @endforeach
    @endforeach
  </div>
</li>
</li>

<li class="nav-item dropdown" style="border-bottom: 0 none;">
  <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <div class="media align-items-center">
      <span class="avatar avatar-sm rounded-circle">
        <img alt="Image placeholder" src="{{url('images/user/'.Auth::user()->profile_photo)}}">
      </span>
      <div class="media-body ml-2 d-none d-lg-block">
        <span class="mb-0 text-sm  font-weight-bold text-adm">{{Auth::user()->name}}</span>
      </div>
    </div>
  </a>
  <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
    <div class=" dropdown-header noti-title">
      <?php $getInformation = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->distinct()->value('office_name'); ?>
      <h6 class="text-overflow m-0">
       @if($getInformation)
       {{$getInformation}}
       @else
       Administrator
       @endif
      </h6>
    </div>
    <div class="dropdown-divider"></div>
     <a href="{{ route('settingPassword') }}" class="dropdown-item">
    <i class="fas fa-key"></i>
    <span>Ubah Kata Sandi</span>
   </a>
    <a href="{{ url('logout') }}"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();" class="dropdown-item">
    <i class="ni ni-button-power"></i>
    <span>Keluar</span>
  </a>
  <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
</div>
</li>

</ul>