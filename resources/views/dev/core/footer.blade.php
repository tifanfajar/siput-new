<!-- Footer -->
<footer class="footer">
  <div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
      <div class="copyright text-center text-xl-left text-muted">
        &copy; 2019 Kementerian Komunikasi dan Informatika Republik Indonesia
      </div>
      <div class="copyright text-center text-xl-left text-muted" style="font-size: 10px; color: #cdced0 !important;">
        Powered by INTIMULTISOLUSI
      </div>
    </div>

  </div>
</footer>