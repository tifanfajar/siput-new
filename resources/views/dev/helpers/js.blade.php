<!--   Core   -->
@include('dev.helpers.jquery')
<script src="{{asset('templates/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!--   Optional JS   -->
<script src="{{asset('templates/assets/js/plugins/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('templates/assets/js/plugins/chart.js/dist/Chart.extension.js')}}"></script>
<script src="{{asset('templates/assets/js/jquery-confirm.js') }}"></script>

<!--   Optional JS   -->
<script src="https://maps.googleapis.com/maps/api/js?key="></script>
<!--   Argon JS   -->
<script src="{{asset('templates/assets/js/argon-dashboard.min.js?v=1.1.0&callback=initMap')}}"></script>
<script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>

<!-- Map -->
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>

<!-- Datatables -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

@include('dev.helpers.maps')
@include('dev.helpers.maps-spp')
<script>	
	$(document).ready(function(){
		$.noConflict();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('table.data-table').DataTable({
			"pagingType": "numbers",
		});	

		$('table.data-table-inspeksi').DataTable({
			"pagingType": "numbers",
			"ordering": false
		});	

		$('#table-upt').DataTable({
            processing: true,
            serverSide: true,
            ajax:"{{ url('dash') }}" ,
            columns: [
                { data: 'id', name: 'ID'},
                { data: 'office_name', name: 'Nama UPT'},
                { data: 'province_name', name: 'Nama Provinsi'}
            ]
        })

		$('.select2').select2({
			theme: "bootstrap"
		});

		// ON CHANGE SELECT 2 IN HERE
		$('#id_rensos').on('select2:select', function (e) {
			$.ajax({
				url: "{{ url('sosialisasi-bimtek/monev-sosialisasi/get-rensos/') }}/" + $(this).val(),
				method: 'GET',
				success: function(data) {
					// console.log(data);
					$('#tanggal_pelaksanaan').val(data['tanggal_pelaksanaan'])
					$('#tema').val(data['tema'])
					$('#tempat').val(data['tempat'])
					$('#jumlah_peserta').val(data['jumlah_peserta'])
					$('#anggaran').val(data['anggaran'])
					$('#narasumber').val(data['narasumber'])
					$('#keterangan').val(data['keterangan'])
					$('#lampiran-sebelumnya').val(data['lampiran'])
				}
			});
		});
	});
</script>

<script>
	$('.drop-down-show-hide').hide();
	$('#dropDown').change(function () {
		$('.drop-down-show-hide').hide()
		$('#' + this.value).show();
	});
	$('#dropDown').change(function () {
		$('.slowws').hide()
	});
</script>

<script>
	$('.drop-down-show-hide').hide();
	$('#dropDown2').change(function () {
		$('.drop-down-show-hide').hide()
		$('#' + this.value).show();
	});
	$('#dropDown2').change(function () {
		$('.slowws').hide()
	});
</script>
<script>
	@if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
	$.confirm({
		title: '{{Session::get('info')}}',
		content: '{{Session::get('alert')}}',
		type: '{{Session::get('colors')}}',
		icon: '{{Session::get('icons')}}',
		typeAnimated: true,
		buttons: {
			close: function () {
			}
		}
	});
	@elseif(count($errors) == 0)
	@else
	$.confirm({
		title: '{{Session::has('info')}}',
		content: '{{Session::get('alert')}}',
		type: 'red',
		typeAnimated: true,
		icon: 'fas fa-exclamation-triangle',
		buttons: {
			close: function () {
			}
		}
	});
	@endif
</script>