<script>

    var mapChart;

    var data = [
    @foreach(\App\Model\Region\Provinsi::all() as $provinsi => $value)
    <?php 
    $myTime = Carbon\Carbon::now();
    $month = $myTime->format('m');
    $year = '20'.$myTime->format('y');
    if ($month == 1) {
        $dataTerbayar = 12;
        $dataBelumTerbayar = 12;
        $fixYear = $year - 1;
    }
    else{
        $dataTerbayar = $month - 1;
        $dataBelumTerbayar = $month - 1; 
        $fixYear = $year;
    }
    ?>
    ['{{$value->id_map}}',@if(\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('bi_type',0)->where('province',$value->nama)->where('active',1)->count() != 0) 1 @else 100 @endif],
    @endforeach
    ];

    Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

        proceed.apply(this, Array.prototype.slice.call(arguments, 1));

        var points = mapChart.getSelectedPoints();
        if (points.length) {
            if (points.length === 1) {
                $('#info-spp #flag-spp-rt').attr('class', 'flag ' + points[0].flag);
            // $('#info-spp-rt h2').html(points[0].name);
            $('#info-spp-rt #map-spp-rt').attr('value', points[0].options['hc-key']);
        } else {
            $('#info-spp-rt #flag-spp-rt').attr('class', 'flag');
            // $('#info-spp-rt h2').html('Comparing countries');
            $('#info-spp-rt #map-spp-rt').attr('value', '');

        }

    } else {
        $('#info-spp-rt #flag-spp-rt').attr('class', '');
    }
});

// Create the chart
mapChart = Highcharts.mapChart('maps-spp-rt', {
	chart: {
		map: 'countries/id/id-all'
	},

	title: {
		enabled: false,
		text: ''
	},

	mapNavigation: {
		enabled: true,
		buttonOptions: {
			verticalAlign: 'top'
		}
	},

	colorAxis: {
        min: 1,
        type: 'logarithmic',
        minColor: '#0000FF',
        maxColor: '#FF0000',
        stops: [
        [0, '#0d00f2'],
        [0.67, '#800080'],
        [1, '#FF0000']
        ]
    },

    series: [{
      data: data,
      name: 'UPT',
      allowPointSelect: true,
      cursor: 'pointer',
      states: {
         hover: {
            color: '#BADA55'
        }
    }
}]
});
</script>

<script>

    var mapChart;

    var data = [
    @foreach(\App\Model\Region\Provinsi::all() as $provinsi => $value)
    <?php 
    $myTime = Carbon\Carbon::now();
    $month = $myTime->format('m');
    $year = '20'.$myTime->format('y');
    if ($month == 1) {
        $dataTerbayar = 12;
        $dataBelumTerbayar = 12;
        $fixYear = $year - 1;
    }
    else{
        $dataTerbayar = $month - 1;
        $dataBelumTerbayar = $month - 1; 
        $fixYear = $year;
    }
    ?>
    ['{{$value->id_map}}',@if(\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('province',$value->nama)->where('active',1)->count() != 0) 1 @else 100 @endif],
    @endforeach
    ];

    Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

        proceed.apply(this, Array.prototype.slice.call(arguments, 1));

        var points = mapChart.getSelectedPoints();
        if (points.length) {
            if (points.length === 1) {
                $('#info-spp #flag-spp-st').attr('class', 'flag ' + points[0].flag);
            // $('#info-spp-st h2').html(points[0].name);
            $('#info-spp-st #map-spp-st').attr('value', points[0].options['hc-key']);
        } else {
            $('#info-spp-st #flag-spp-st').attr('class', 'flag');
            // $('#info-spp-st h2').html('Comparing countries');
            $('#info-spp-st #map-spp-st').attr('value', '');

        }

    } else {
        $('#info-spp-st #flag-spp-st').attr('class', '');
    }
});

// Create the chart
mapChart = Highcharts.mapChart('maps-spp-st', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        enabled: false,
        text: ''
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'top'
        }
    },

    colorAxis: {
        min: 1,
        type: 'logarithmic',
        minColor: '#0000FF',
        maxColor: '#FF0000',
        stops: [
        [0, '#0d00f2'],
        [0.67, '#800080'],
        [1, '#FF0000']
        ]
    },

    series: [{
      data: data,
      name: 'UPT',
      allowPointSelect: true,
      cursor: 'pointer',
      states: {
         hover: {
            color: '#BADA55'
        }
    }
}]
});
</script>