<script type="text/javascript">
$(document).ready(function() {
	/* contact form validation */
  var Validator = function(formObject) {
    this.form = $(formObject);
    var Elements = {
      name: {
        reg: /^[a-zA-Z ]{2,20}$/,
        require: true,
        error: "Nama Tidak Valid.",
      },

      companyName: {
        reg: /^(?=.{2,100}$).*$/,
        require: true,
        error: "Nama Perusahaan Max 50 karakter.",
      },

      email: {
        reg: /^[a-z-0-9_+.-]+\@([a-z0-9-]+\.)+[a-z0-9]{2,7}$/i,
        error: "E-mail tidak valid",
      },
      phone: {
        reg: /^\(?([0-9])\)?$/,
        error: "Nomor Telp harus diisi nomor.",
      },
      year: {
        reg: /^\(?([0-9]{1,4})\)?$/,
        error: "Tahun Terlalu lebih.",
      },

      number: {
        reg: /^[0-9]+(?!\s*$)*$/,
        error: "Harap Isi angka.",
      },

      date: {
        reg: /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/,
        error: "Harap Isi tanggal sesuai format yang sesuai pada browser anda.",
      },

      message: {
        reg: /^(?!\s*$).+/,
        error: "Data Tidak Boleh Kosong.",
      },

      decimal: {
        reg: /^\d*\.?\d*$/,
        error: "Harap Isi Angka.",
      },

      gender: {
        error: "Jenis Kelamin Tidak Valid / Kosong",
      },
      selectOption: {
        error: "Data Ini Kosong",
        required: true
      }
    };

    var handleError = function(element, message) {
      element.addClass('input-error');
      var $err_msg = element.parent('td');
      $err_msg.find('.error').remove();

      var error = $('<div class="error"></div>').text(message);
      error.appendTo($err_msg);

      element.on('keypress change', function() {
        $(error).fadeOut(1000, function() {          
          element.removeClass('input-error');
        });
      });

    };

    /* Select Option */

    this.validate = function() {
      var errorCount = 0;

      this.form.find("select").each(function(index, field) {
        var type = $(field).data("validation");
        var validation = Elements[type];
        if ($(field).val() == "") {
          errorCount++;
          handleError($(field), validation.error);
        }
      });

      this.form.find("input, textarea").each(function(index, field) {
        var type = $(field).data("validation");
        var validation = Elements[type];
        if (validation !== undefined) {
          var re = new RegExp(validation.reg);
          if (validation) {
            if (!re.test($(field).val())) {
              errorCount++;
              handleError($(field), validation.error);
            }
          }
        }
      })

      /* Radio button */

      var radioList = $('input:radio');
      var radioNameList = new Array();
      var radioUniqueNameList = new Array();
      var notCompleted = 0;
      for (var i = 0; i < radioList.length; i++) {
        radioNameList.push(radioList[i].name);
      }
      radioUniqueNameList = jQuery.unique(radioNameList);
      for (var i = 0; i < radioUniqueNameList.length; i++) {
        var field = $('#' + radioUniqueNameList[i]);
        var type = field.data("validation");
        var validation = Elements[type];
        if ($('input[name=' + type + ']:checked', '#test').val() == undefined) {
          errorCount++;
          handleError($(field), validation.error);
        }
      }

      return errorCount == 0;
    };
  };

  /* Submit form*/

  $(function() {
    $("form#form-validate").on('submit', function(e) {      
      var NoErrors = new Validator(this).validate();   
      if (NoErrors == true) { 
        // $.ajax({
        //   url: this.action,
        //   enctype: 'multipart/form-data',
        //   type: 'POST',
        //   data: $(this).serialize(),
        //   processData: false,
        //   success: function() {
        //     // AJAX request finished, handle the results and error msg            
        //     $('input[type!="submit"], textarea').removeClass('error');
        //     // console.log(data);
        //     window.location.href = $('#route').val();
        //   }
        // });

        $('#form-validate').ajaxForm();

      }      
      return false;
    });
    var NoErrors = new Validator(this).validate();
  });
});
</script>