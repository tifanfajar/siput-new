
@extends('dev.core.using')
@section('content')

<div class="container">
	<br>
	<br>
	<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-link"></i>&nbsp;&nbsp;SURVEY LINK</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	<br>
	<form action="{{route('postSurveyLink')}}" class="row" method="POST" enctype="multipart/form-data">
	@csrf
	<div class="col-md-12 form-group">
	<label for="link">Link Tersimpan Saat Ini </label><label style="color: red;">*</label>
	<input type="text" class="form-control" id="link" name="link" placeholder="Masukan Link Baru" value="{{\App\SurveyLink::where('id',1)->value('link')}}">
	</div>				
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button type="submit" class="btn btn-primary" onClick="confirm('Sudah yakin dengan link yang baru?')"><i class="fa fa-save"></i>&nbsp;&nbsp;Update Link</button>
	@if(\App\SurveyLink::where('id',1)->value('active') == 0)
	<a href="{{ route('activesurveyLink') }}" class="btn btn-warning"><i class="fa fa-eye"></i>&nbsp;&nbsp;Aktifkan</a>
	@else
	<a href="{{ route('nonactivesurveyLink') }}" class="btn btn-info"><i class="fa fa-eye"></i>&nbsp;&nbsp;Non-Aktifkan</a>
	@endif
	<a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</a>
	</form>
</div>

@endsection