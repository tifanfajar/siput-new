@include('dev.helpers.jquery')
<script type="text/javascript">
		$(".btnNotification").click(function(){
			var id_module=$(this).attr('data-id-module');
			var notification=$(this).children('div');
			var notificationAll = $('#notifiedAll').text();
			$.ajax({
				url: "{{ url('getNotification/') }}/" + id_module,
				method: 'GET',
				success: function(data) {
					$('#ntbody').html(data.html);
					$.ajax({
						url: "{{ url('updateToRead') }}/" + data.id,
						method: 'GET',
						success: function(data) {
							var textNotification = notification.text();
							var jmlNotification = textNotification - data;
							var total = notificationAll - data;

							if(isNaN(total) || total <= 0) {
								$('#notifiedAll').html(0);
								
							}else{
								$('#notifiedAll').html(total);
							}

							if(isNaN(jmlNotification) || jmlNotification <= 0) {
								notification.text(0);
								
							}else{
								notification.text(total);
							}

						}
					});
				}
			});



		});	
</script>