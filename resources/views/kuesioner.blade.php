<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<br>
<br>
<br>
<div class="card-shadow">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col">
				<center>
					<h3 class="mb-0 text-white"><i class="fas fa-question"></i>&nbsp;&nbsp;Kuesioner</h4>
					</center>
				</div>
			</div>
		</div>
		@foreach($kuesioner as $value)
		<?php $id = Crypt::encryptString($value->id) ?>
		<div class="card-body">
			<div class="card">
				<div class="card-body text-black">
					<h2 class="card-title"><i class="fas fa-question-circle"></i>&nbsp;{{$value->title}}</h2>
					<h4>Deskripsi : <br><p>{{$value->description}}</p></h4>
					<h3 class="text-center"><a href="{{route('kuesioner-form',$id)}}"><i class="fas fa-link"></i> Click Here !</a></h3>
				</div>	
			</div>
		</div>
		@endforeach
	</div>
	@endsection
