@extends('dev.core.using')
@section('content')
@include('templates.pelayanan.unar.core.add')
@include('templates.pelayanan.unar.core.detail')
@include('templates.pelayanan.unar.core.edit')
@include('templates.pelayanan.unar.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.helpers.filter')
@include('templates.pelayanan.unar.dev.data')

<div class="container-fluid" style="margin-top: 40px;">
	<div class="row justify-content-center">
		<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
		@if($getStatus != 'administator')
		<div class="col-sm-12">
			<div class="pt-4" style="margin-bottom: 20px;">
				<div class="card shadow" style="border:1px solid #dedede;">
					<div class="card-header bg-transparent">
						<div class="row align-items-center">
							<div class="col">
								<center>
									<h4 class="mb-0">{{ $kuesioner->title }}</h4>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		<div class="col-sm-10">
			<form method="GET" id="kuesioner-form">
				<select class="form-control select2" name="kuesioner">
					@foreach($questionnaires as $key => $questionnaire)
					<option value="{{ Crypt::encryptString($questionnaire->id) }}" {{ $questionnaire->id == $kuesioner->id ? 'selected' : null }}>{{ $questionnaire->title }}</option>
					@endforeach
				</select>
			</form>
		</div>
		<div class="col-sm-2"><button class="btn btn-primary btn-block" onclick="event.preventDefault(); document.getElementById('kuesioner-form').submit();">Cari</button></div>
		<div class="col-sm-6">
			<div class="card shadow">
				<div class="card-header bg-transparent">
					<div class="row align-items-center">
						<div class="col">
							<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Presentase Kuesioner Per UPT</h6>
						</div>
					</div>
				</div>
				<div class="card-body"  style="overflow-y: scroll;">
					<div class="chart">
						<div id="unar-admin2" style="min-width: 310px; height: 2000px; max-width: 600px; margin: 0 auto"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<div class="card shadow">
					<div class="card-header bg-transparent">
						<div class="row align-items-center">
							<div class="col">
								<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Presentase Keseluruhan</h6>
							</div>
						</div>
					</div>
					<div class="card-body" style="overflow: hidden;">
						<div class="chart">	
							<div id="unar-admin" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-sm-6">
						<div class="form-group">
							<a href="{{ url('kuesioner/detail/'.Crypt::encryptString($questionnaire->id)) }}"><button class="btn btn-block btn-lg btn-primary">Detail</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- foot -->
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	// pie
	Highcharts.chart('unar-admin', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}%</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}%'
				}
			}
		},
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			{	
				name: '5 Poin',
				y: {{ $all_percentage }}
			}
			]
		}]
	});

	// bar
	Highcharts.chart('unar-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach($upts as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Persen'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Presentase',
			data: [
			@foreach($upts as $key => $value)
			{{ $percentage[$value->office_id] }},
			@endforeach
			]
		}
		]
	});
</script>
<!-- endfoot -->

@endsection