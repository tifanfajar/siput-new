<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<form method="POST" action="{{route('kuesioner-form-post')}}" enctype="multipart/form-data">
@csrf
<br>
<br>
<br>
<div class="card-shadow">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col">
				<center>
					<h3 class="mb-0 text-white"><i class="fas fa-question"></i>&nbsp;&nbsp;{{$kuisioner}}</h4>
					</center>
				</div>
			</div>
		</div>
		<?php 
		$i = 1;
		?>
		@foreach($question as $key => $value)
		<input type="hidden" name="id_kuesioner[{{$value->id}}]" value="{{$value->id_kuisioner}}">
		<input type="hidden" name="id_pertanyaan[{{$value->id}}]" value="{{$value->id}}">
		<input type="hidden" name="jenis_pertanyaan[{{$value->id}}]" value="{{$value->jenis_pertanyaan}}">
		@if($value->jenis_pertanyaan == 1)
		<div class="card-body">
			<div class="card">
				<div class="card-body text-black">
					<h2 class="card-title">&nbsp;{{$i++}}.&nbsp;{{$value->pertanyaan}}</h2>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-sm-10">
								@foreach(\App\Model\Setting\Kuisioner\Option::where('id_pertanyaan',$value->id)->get() as $option)
								<div class="form-check">
									<input class="form-check-input" type="radio" name="option[{{$value->id}}]" id="{{$option->id}}" value="{{$option->id}}">
									<label class="form-check-label" for="{{$option->id}}">
										{{$option->option}}
									</label>
								</div>
								@endforeach
							</div>
						</div>
					</fieldset>
				</div>	
			</div>
		</div>
		@else
		<div class="card-body">
			<div class="card">
				<div class="card-body text-black">
					<h2 class="card-title">&nbsp;{{$i++}}.&nbsp;{{$value->pertanyaan}}</h2>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="option[{{$value->id}}]" placeholder="Silahkan Mengisi Disini" required></textarea>
								</div>
							</div>
						</div>
					</fieldset>
				</div>	
			</div>
		</div>
		@endif
		@endforeach
	</div>
	<br>
	<button class="btn btn-primary float-right">KIRIM</button>
	<br>
	@endsection
