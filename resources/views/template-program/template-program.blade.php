<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>

 .parent-list {
   list-style: none;
 }
 .arr-btn{
   font-size: 20px;
   margin-right: .5em;
   cursor: pointer;
   width: 1.3rem;
   height: 1.3rem;
   border-radius: .5rem;
   text-align: center;
 }
 .arr-btn:hover{
  background: #cdcdcd
 }
 .ic-container{
   /* margin-top: .3em; */
   gap:.5em;
 }
 .ic-container > .ic {
   width: 1.5rem;
   height: 1.3rem;
   border-radius: .5rem;
   text-align: center;
   cursor:pointer;
   background: #cdcdcd;
   /* padding-top: .3rem; */
   font-size: .8em;
   list-style: none;
   color: #000;
 }
 .ic-container > .ic:hover {
  background: #eee;
 }
</style>
<br/>
<br/>
<div class="card card-stats">
 <div class="card-body">
  <div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-1">

    <a type="button" href="{{route('template-program.add')}}" class="btn btn-primary" style="background: #193865;color: #fff">Add New</a>
    </div>
  </div>
				<div class="col">
     <ul class="parent-list" id="par-list">
      <li>
      </li>
       <!-- <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Provinsi</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Upt</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Id UNAR</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Tipe UNAR</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Tanggal Pelaksanaan</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Lokasi Ujian</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Jumlah Peserta</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Peserta Lulus</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Peserta Tidak Lulus</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">ID Unar</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
       <ol class="row">
        <div class="col-xs-4 col-sm-4 col-md-7">Full CAT</div>
        <div class="row ic-container">
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="{{route('template-program.edit', 1)}}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
       </ol>
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Piutang
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Menu Sosialisasi
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Menu Konsultasi dan Pengaduan
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>MOTS
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>ISR Maritim Nelayan Program MOTS
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>LKE ZI
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Pengembangan Zona Integritas
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Kinerja Unit Penyelenggara Pelayanan Publik
      </li>
      <li><i class="fas fa-angle-down arr-btn"></i>Penugasan Lain
      </li> -->
     </ul>
    </div>
</div>
</div>
<script>

 $(document).ready(function () {
  var base = "{{url('/')}}"
  $(document).ready(function () {
   getList()

  })
   async function getList () {
     const url = "{{url('api/template-program')}}"
     const res = await fetch(url,{
         method: 'GET', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         }
       })
     const resp = await res.json()
     console.log(resp.data)
     if (resp.status === 200) {
       var html = ""
       for (let dd in resp.data) {
         html += `<li class="row">
        <div class="col-xs-4 col-sm-4 col-md-7"><b>${resp.data[dd].nama}</b></div>
        <div class="row ic-container">
         <a href="${base}/template-program/edit/${resp.data[dd].id}" class="ic"><i class="fas fa-edit" ></i></a>
         <a href="${base}/template-program/edit/${resp.data[dd].id}" class="ic"><i class="fas fa-eye" ></i></a>
         <a href="${base}/template-program/edit/${resp.data[dd].id}" class="ic"><i class="fas fa-trash" ></i></a>
        </div>
         </li>`
       }
       $('#par-list').html(
         html
       )
     }
   }
 })
</script>
@endsection