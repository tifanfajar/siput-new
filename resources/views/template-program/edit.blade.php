<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
 input, select {
     border: 0 !important;
     background: transparent !important;
     border-bottom: 1px solid #cdcdcd !important;
 }
 .tb-container{
   background: #fff;
   margin-top: 1em;
 }
 tbody > tr {
    line-height: 45px !important;
    min-height: 45px !important;
    height: 45px !important;
 }
</style>
<br/>
<br/>
<div class="col">
  <div class="alert alert-success" id="success" role="alert">
    Berhasil menambah item program
  </div>
  <div class="col">
    <div>
     <div class="form-group">
         <label for="">Nama Program *</label>
         <input type="text" class="form-control" id="namaProgram" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="">Label *</label>
         <input type="text" class="form-control" id="label" placeholder="No Data Available">
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
    </div> 
    <div>
     <div class="form-group">
         <label for="">Wajib *</label>
         <select class="form-control" id="wajib">
           <option value="yes">Yes</option>
           <option value="no">No</option>
         </select>
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
     <div class="form-group">
         <label for="">Icon Program</label>
         <input name="file" class="form-control" type="file" id="file_input" multiple />
         <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
     </div>
     <div class="col-md-4" style="display:none" id="card_file_preview">
        <div class="card">
            <div class="card-body pt-2" style="max-height:185px">
            Preview:<br>
                <center>
                    <img src="" id="file_preview" style="width:130px">
                </center>
            </div>
        </div>
    </div>
     
    </div> 
    <div class="row">
      <div class="col-md-9"></div>
      <div class="btn btn-primary" style="background: #B4CD93" id="add-item"><i class="fas fa-plus-circle"></i> Tambah Item Program</div>
    </div>
    <button type="button" class="btn btn-primary btn-lg btn-block" style="background: #193865;margin-top: 1em">Item Program</button>
  </div>
  <div class="tb-container col">
   <table class="table table-bordered">
     <thead >
       <tr class="table-dark">
         <th scope="col">Order By</th>
         <th scope="col">Kolom</th>
         <th scope="col">Label</th>
         <th scope="col">Tipe Kolom</th>
         <th scope="col">Target</th>
       </tr>
     </thead>
     <tbody id="body">
     </tbody>
   </table>
  </div>
  <div class="row">
    <div class="col-md-10"></div>
    <div class="row" style="gap: 1em">
    <button type="button" class="btn btn-primary" id="save" style="background: #193865">Simpan</button>
    <button type="button" class="btn btn-primary" id="cancel-btn" style="background: #fff;color: #193865">Batal</button>
    </div>
  </div>
</div>
<script>
 $(document).ready(function() {
  $('#success').hide()
  $('#err-al').hide()
   $('#add-item').click(function() {
     var oldHtml = $('#body').html()
     oldHtml += `
      <tr class="item-program" item-id=0>
       <td scope="row">
        <input type="text" class="form-control orderBy" >
       </td>
       <td>
        <input type="text" class="form-control kolom" >
       </td>
       <td>
        <input type="text" class="form-control label" >
       </td>
       <td>
        <input type="text" class="form-control tipeKolom">
       </td>
       <td>
        <select class="target form-control" >
          <option value="tidak">Tidak</option>
          <option value="iya">iya</option>
        </select>
       </td>
     </tr>
     `
     $('#body').html(oldHtml)
   })
   const url = "{{url('api/template-program')}}"
   getDetail()
   async function getDetail () {
     const res = await fetch(url+'/{{$id}}', {
       method: 'GET', // *GET, POST, PUT, DELETE, etc.
       mode: 'cors', // no-cors, *cors, same-origin
       cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
       credentials: 'same-origin', // include, *same-origin, omit
       headers: {
         'Content-Type': 'application/json'
         // 'Content-Type': 'application/x-www-form-urlencoded',
       }
     })
     const resp = await res.json()
     const tbody = $('#body')
     var html = ""
     if (resp.status === 200) {
       $('#namaProgram').val(resp.data.nama)
       $('#label').val(resp.data.nama)
       if (resp.data.item_program.length > 0) {
        for (let dd in resp.data.item_program) {
          console.log(resp.data.item_program[dd].kolom)
          html += `
            <tr class="item-program" item-id="${resp.data.item_program[dd].id}">
              <td scope="row">
               <input type="text" class="form-control orderBy" value="${resp.data.item_program[dd].order_by}">
              </td>
              <td>
               <input type="text" class="form-control kolom" value="${resp.data.item_program[dd].kolom}">
              </td>
              <td>
               <input type="text" class="form-control label" value="${resp.data.item_program[dd].label}">
              </td>
              <td>
               <input type="text" class="form-control tipeKolom"  value="${resp.data.item_program[dd].tipe_kolom}">
              </td>
              <td>
               <select class="target form-control" >
                 <option value="tidak" ${(resp.data.item_program[dd].target === 'tidak') ? 'selected' : ''}>Tidak</option>
                 <option value="iya" ${(resp.data.item_program[dd].target === 'iya') ? 'selected' : ''}>iya</option>
               </select>
              </td>
            </tr>
          `
        }
        tbody.html(html)
       
       }
     } 
   }
   $('#save').click(async function() {
      let res = 'tes'
      $('#save').html(`
      <div class="spinner-border"></div>
      `)
      $('#cancel-btn').attr("href", '#')
      
      const items = $('.item-program').each(function () {
        return $(this)
      })
      const koloms = $('.kolom').each(function (){
        return $(this)
      })
      const orders = $('.orderBy').each(function (){
        return $(this)
      })
      const labels = $('.label').each(function (){
        return $(this)
      })
      const tipeKoloms = $('.tipeKolom').each(function (){
        return $(this)
      })
      const targets = $('.target').each(function (){
        return $(this)
      })
      // console.log(items[0].getAttribute('item-id'))
      var body = {}
      
      await items.map(async (val, ind) => {
        
        if (items[val].getAttribute('item-id') !== '0') {

          var url = "{{url('api/item-program/update')}}"
          body = {
           "id_template": @php echo $id; @endphp,
           "kolom": `${koloms[val].value}`,
           "label": `${labels[val].value}`,
           "target": `${targets[val].value}`,
           "created_by": "{{$nama}}",
           "tipe_kolom": `${tipeKoloms[val].value}`,
           "order_by": `${orders[val].value}`
          }
          res = await fetch(url+`/${items[val].getAttribute('item-id')}`,
           {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json'
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(body) // body data type must match "Content-Type" header
          });
         } else {
               var url = "{{url('api/item-program/create')}}"
               body = {
                "id_template": @php echo $id; @endphp,
                "kolom": `${koloms[val].value}`,
                "label": `${labels[val].value}`,
                "target": `${targets[val].value}`,
                "created_by": "{{$nama}}",
                "tipe_kolom": `${tipeKoloms[val].value}`,
                "order_by": `${orders[val].value}`
               }
               res = await fetch(url,
                {
                 method: 'POST', // *GET, POST, PUT, DELETE, etc.
                 mode: 'cors', // no-cors, *cors, same-origin
                 cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                 credentials: 'same-origin', // include, *same-origin, omit
                 headers: {
                   'Content-Type': 'application/json'
                   // 'Content-Type': 'application/x-www-form-urlencoded',
                 },
                 redirect: 'follow', // manual, *follow, error
                 referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                 body: JSON.stringify(body) // body data type must match "Content-Type" header
               });

        }
      })
      $('#success').show()
      $('#save').html(`
        Simpan
      `)
      $('#success').show()
      $('#cancel-btn').attr("href", "{{route('template-program')}}")
      // window.location = `{{route('template-program')}}`
      // console.log(res)
      // for (let dd in items) {
      //  // console.log(dd)
      //  // if (parseInt(dd)) {
      //   console.log(koloms[dd].value)
      //  // }

      // }
   })
 })
</script>

<script>
    $('#file_input').on('change',function(){
      var file = $(this).get(0).files[0];
      if(file){
          var reader = new FileReader();

          reader.onload = function(){
              $('#file_preview').attr('src',reader.result);
              $('#card_file_preview').show();
          }

          reader.readAsDataURL(file);
      }else{
        $('#card_file_preview').hide();
      }
      
    });
</script>
@endsection