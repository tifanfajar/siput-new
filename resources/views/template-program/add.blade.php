<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')
<style>
 input {
     border: 0 !important;
     background: transparent !important;
     border-bottom: 1px solid #cdcdcd !important;
 }
 .tb-container{
   background: #fff;
   margin-top: 1em;
 }
 tbody > tr {
    line-height: 45px !important;
    min-height: 45px !important;
    height: 45px !important;
 }

 @-webkit-keyframes spinner-border {
 	to {
 		-webkit-transform: rotate(360deg);
 		transform: rotate(360deg);
 	}
 }

 @keyframes spinner-border {
 	to {
 		-webkit-transform: rotate(360deg);
 		transform: rotate(360deg);
 	}
 }

 .spinner-border {
 	display: inline-block;
 	width: 2rem;
 	height: 2rem;
 	vertical-align: text-bottom;
 	border: 0.25em solid currentColor;
 	border-right-color: transparent;
 	border-radius: 50%;
 	-webkit-animation: spinner-border .75s linear infinite;
 	animation: spinner-border .75s linear infinite;
 }

 .spinner-border-sm {
 	width: 1rem;
 	height: 1rem;
 	border-width: 0.2em;
 }

 @-webkit-keyframes spinner-grow {
 	0% {
 		-webkit-transform: scale(0);
 		transform: scale(0);
 	}
 	50% {
 		opacity: 1;
 	}
 }

 @keyframes spinner-grow {
 	0% {
 		-webkit-transform: scale(0);
 		transform: scale(0);
 	}
 	50% {
 		opacity: 1;
 	}
 }

 .spinner-grow {
 	display: inline-block;
 	width: 2rem;
 	height: 2rem;
 	vertical-align: text-bottom;
 	background-color: currentColor;
 	border-radius: 50%;
 	opacity: 0;
 	-webkit-animation: spinner-grow .75s linear infinite;
 	animation: spinner-grow .75s linear infinite;
 }

 .spinner-grow-sm {
 	width: 1rem;
 	height: 1rem;
 }
</style>
<br/>
<br/>
<div class="col">
<div class="alert alert-success" id="success" role="alert">
  Berhasil menambah program
</div>
<div class="alert alert-danger" id="err-al" role="alert">
  Gagal menambah program
</div>
<div class="col">
    <div>
        <div class="form-group">
            <label for="">Nama Program *</label>
            <input type="text" class="form-control" id="namaProgram" placeholder="" required>
            <small id="namaProgramHelp" class="form-text text-muted" style="color:red !important"></small>
        </div>
        <div class="form-group">
            <label for="">Label *</label>
            <input type="text" class="form-control" id="label" placeholder="No Data Available">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label for="">Wajib *</label>
            <select class="form-control" id="wajib">
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label for="">Icon Program</label>
            <input name="file" class="form-control" type="file" id="file_input" multiple />
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="row">
          <div class="col-md-4" style="display:none" id="card_file_preview">
              <div class="card">
                  <div class="card-body pt-2" style="max-height:185px">
                  Preview:<br>
                      <center>
                          <img src="" id="file_preview" style="width:130px">
                      </center>
                  </div>
              </div>
          </div>
        <div>
      </div> 
  </div>
  <div class="row">
      <div class="col-md-9"></div>
      <div class="col-md-3" style="float:right">
        <button type="button" class="btn btn-primary" id="save" style="background: #193865">Simpan</button>
        <a type="button" class="btn btn-primary" id="cancel-btn" href="{{route('template-program')}}" style="background: #fff;color: #193865">Batal</a>
      </div>
  </div>
  
</div>
<script>
$(document).ready(function () {
  $('#success').hide()
  $('#err-al').hide()
 // $('#cancel-btn').href('#')
 // $('#cancel-btn').attr("href", '#')
 var errNama = true
 function validateName () {
   var value = $('#namaProgram').val()
   if (value.length < 1) {
     $('namaProgramHelp').show()
     $('#namaProgramHelp').html('Nama program tidak boleh kosong')
     errNama = true
   } else {
    errNama = false
    $('#namaProgramHelp').hide()
   }
 }
 $('#save').click(async function (){
    
    validateName()
    if (errNama) { 
      return false
    } else {
      $('#save').html(`
      <div class="spinner-border"></div>
      `)
      $('#cancel-btn').attr("href", '#')
      const url = "{{url('api/template-program/create')}}"
      const body = {
        nama: $('#namaProgram').val(),
        status: 2,
        created_by: "{{$nama}}"
      }
     const res = await fetch(url, {
         method: 'POST', // *GET, POST, PUT, DELETE, etc.
         mode: 'cors', // no-cors, *cors, same-origin
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         credentials: 'same-origin', // include, *same-origin, omit
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         },
         redirect: 'follow', // manual, *follow, error
         referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
         body: JSON.stringify(body) // body data type must match "Content-Type" header
       });
       const resp = await res.json()
       if (resp.status === 200) {
        $('#save').html(`
          Simpan
        `)
        $('#success').show()
        $('#cancel-btn').attr("href", "{{route('template-program')}}")
        window.location = `{{route('template-program')}}`
        @php {{session()->flash('status', 'Berhasil Menambah Task!');}} @endphp
       } else {
         $('#save').html(`
           Simpan
         `)
         $('#err-al').show()
         $('#cancel-btn').attr("href", "{{route('template-program')}}")
       }
       // console.log(resp)
      // alert("{{$nama}}")
    }
 })
})
</script>

<script>
    $('#file_input').on('change',function(){
      var file = $(this).get(0).files[0];
      if(file){
          var reader = new FileReader();

          reader.onload = function(){
              $('#file_preview').attr('src',reader.result);
              $('#card_file_preview').show();
          }

          reader.readAsDataURL(file);
      }else{
        $('#card_file_preview').hide();
      }
      
    });
</script>

@endsection